PATH 		:= /home/danieler/Utilities/pgi/linux86-64/2017/bin/:$(PATH)

CCINC		= -Iinclude -I$(BUILDDIR)
CC		= pgc++ -std=c++14

#if you are using a obsolete version of gcc add the flag -std=c++11
CPPOPTIMIZATION	= -O3
CFLAGS		= $(INCLUDE) -pedantic $(CPPOPTIMIZATION)
CMAINFLAGS	= $(CFLAGS)

#quando non mi serve piu` una build di debug aggiungere -O3 a CFLAGS e -s alle Mainflags e togliere -g

F90		= pgfortran
F90OPTIMIZZATIONS=
F90FLAGS	= -cpp $(F90OPTIMIZZATIONS)

FC		= pgfortran
FOPTIMIZZATIONS = -O3
FFLAGS		= -cpp $(FOPTIMIZZATIONS)

F90MOD_OUT	= -module $(BUILDDIR)
F90MOD_DIR	=  $(F90MOD_OUT)

LINKER		= $(CC)
LINKERFLAGS 	:= -s
LINKERLIBS 	:= -pgf90libs
