# BH++

## Description

This project has born as a rewriting of the old "Basin Hopping" program.

It has started as a simple basin hopping suite for metallic nanoclusters and nanoparticles, now it is a small suite with a basin hopping algorithm with internal minimizazion (the `bh++` program) and a basin hopping program (`mover`) that can be interfaced with external tools.

Moreover the suite hosts libraries aimed at the geometrical and chemical analisys of the clusters.

The main programs in the project are:

- `bh++` the main program that execute a basin hopping
- `mover` the basin hopping interface with an external suite (we made it work  with [Quantum Espresso](http://www.quantum-espresso.org/) and [CP2K](https://www.cp2k.org/))
- `signatureAnalyzer` an utility that performs the signature analysis on a list of xyz files
  
The detailed info for the inputs are written in the "Related Pages" tab if you are browsing the documentation by the html formato or in the doc folder.

## Instructions

The program has been compiled and tested with gcc 7.4

## Compilation

You can choose between two ways to compile the suite.
  
For a system-wide installation we recomend to use **cmake**: this method does require minimal tuning, and is the more "secure" (cmake should get the right settings for your computer):

- `mkdir builddir` : create a new directory, this is mandatory, cmake must run in a directory different from the source one.
- `cd builddir` : Here you have three possibilities:
  - `cmake path/to/BH++/source cmakeoptions`
    - `cmake-gui path/to/BH++/source cmakeoptions` will simplify the configuration process with a simple gui
    - `ccmake path/to/BH++/source cmakeoptions` will also simplify the configuration process by using a gui terminal
- for the `cmakeoptions` you could choose:
  - **CMAKE_BUILD_TYPE**: choose between: *None, Debug, Release, RelWithDebInfo, MinSizeRel*
  - **CMAKE_INSTALL_PREFIX**: the installation directory if you want to install libraries and programs
  - **OPT_SHAREDBUILD**: _boolean_, if true build the program with shared libraries
  - **OPT_DOCUMENTATION**: _boolean_, if you have dot and doxygen a taget called "documentation" is added to the make options, it will generate a html page as a guide to the library
  - **OPT_SHAREDBUILD**: _boolean_, if true build the program with shared libraries
  - **OPT_USE_Q4Q6**: _boolean_, adds the Q4 and Q6 order parameters, if it is possible to find boost in your system
- `make` to compile the library and the programs
- `make install` to install library and executables in the directory indicated by **CMAKE_INSTALL_PREFIX**
  - to uninstall all the files simply do `xargs rm < install_manifest.txt`

## Extra

`bh++` can be used with options (for example ``bh++ BH``)

- **FL** launches the F/L variant 
- **BH** launches a classic basing hopping minimization
- **memory** print on screen how much memory some of the BH classes use
- **settingsInput** print on screen the default input file
- **walkerInput** print on screen the default walker input file, accept the walker algorithm to print the default options for that algorithms
- **center** same as calling ``centerer``, with the same options
- **energy** same as calling ``energyCalculator``, with the same options
- **help** give a brief hint on these options

## Release Notes

### Version 0.21

- Now BH++ is under GPL v3.0
- Bugfixes (see changelog)
- Added the "Generic atom" to the `clusterCreator`

### Version 0.20

- Improvement on all the CMakeLists.txt:
  - Now minimum requirement is 3.16
  - Reworked the package distribuition feature and installation procedure
- Added unit tests for checking the components using Catch2v3
- The test folder in independent from the project, it is possible to use it to test the installation procedure
- Added Q4 and Q6 order parameters if CMake can find Boost
- BHClusterAnalyser is now clearer for developers, working on making it also threadsafe
- FIRE algorithm can be chosen by the user for minimization
- F/L/H is now a little more flexible and can be launched trough the standard bh++ executable, via options in the input file
  - the bhFLH_* executables can set up athe input file for the desired kind of minimization

### Version 0.19

- The new name for the Companion algorithm is FlyingLanding
- Improved the CMakeLists.txt
- Now you can add documentation with cmake (tested with cmake 3.16)
- Some code polishing

### Version 0.17.2

- Added pressure calculation to the class BHEnergyCalculator
- Added pressure calculation to the programm EnergyCalculator
- Added two new global algorithms: Spawner (WIP) and Companion
- Added a struct to simplify the passing of information to the walker algorithm
- Added a way to copy walker atom configuration in another walker
- BasinHoppingWithQE.sh now works properly
- Solved a bug in ExchangeInBulk and in ExchangeOnSurface: now the exchange will fail only if the move is not possible at all
- Solved a bug in MoveBall: the behavior of minimum displacement was inverted
- Solved a bug in IslandsCollapser : the atom is collapsed in the right position without changing its type.

### Version 0.17.1a (hotfix)

- Solved a bug in the move parsers: "accTemp" and "decriptor" didn't work

### Version 0.17.1

- Solved some bugs in the IO
- Solved some bugs in mover, due to the conversion to 0.17.0
  
### Version 0.17.0

- Now The walker creator acts as a factory
- Rewritten the concept of Walkers using strategy pattern
- Rewritten the concept of Moves using strategy pattern
- Now walkers "has" moves, and each walkers has only the needed moves, instead of knowing all the moves and then selecting what it will need
- These simplifications make sure that adding a new move or a new Walker algorithm is easier

### Version 0.16.1

- Solved bug in moveBonds

### Version 0.16.0

- Some general IO adjustments
- Corrected some bugs in some moves
- Some improvements in loading ClusterAtoms from file (now the xyz comment is stored)
- Added the possibility to use the SMATB+Gauss potential (only for AgPt alloys)
- Added 3 new support programs, installable by cmake:
  - `clusterCreator` that can create clusters from command line
    - The functions used by `clusterCreator` are included in the BHstructure library
  - `BHinputFileCreator` that can help in creating a default input file for the main program
  - `BHinputWalkerFileCreator` that can help in creating a default input file for walkers
