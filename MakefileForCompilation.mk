#PATH		:=

CCINC		= -iquoteinclude -iquote$(BUILDDIR)
CC		= g++

#if you are using a obsolete version of gcc add the flag -std=c++11
CPPOPTIMIZATION	= -O3 #-fno-inline
#-malign-double -mpreferred-stack-boundary=8 must know before using
CFLAGS		= $(INCLUDE) -Wall -Wextra $(CPPOPTIMIZATION) -pedantic -std=c++14 -Wno-unknown-pragmas -Werror -Wfatal-errors
CMAINFLAGS	= $(CFLAGS)

#quando non mi serve piu` una build di debug aggiungere -O3 a CFLAGS e -s alle Mainflags e togliere -g
F90		= gfortran
F90OPTIMIZZATIONS = -O3 #-Og
F90FLAGS	= -cpp $(F90OPTIMIZZATIONS) -Wall -Werror -Wfatal-errors

FC		= gfortran
FOPTIMIZZATIONS = -O3
FFLAGS		= -cpp $(FOPTIMIZZATIONS) -Wno-uninitialized #-fcheck=bounds

F90MOD_OUT	= -J$(BUILDDIR)
F90MOD_DIR	= -I$(BUILDDIR)

LINKER		= $(CC)
LINKERFLAGS 	:= #-s
LINKERLIBS 	:= -lgfortran
