
include MakefileForCompilation.mk
#include MakefileForpgi.mk

BH++_VERSION_MAJOR	=1
BH++_VERSION_MINOR	=0
LIB_VERSION_MAJOR	=0
LIB_VERSION_MINOR	=19
LIB_PATCH_LEVEL 	=0


OPTIONFLAGS	:= -DSETTINGSVERBOSE -DMETALPARVERBOSE -DWALKERSETTINGVERBOSE #-DUSESMATBGAUSS #-D__DOMINIMIZATION_WITH_FIRE
DEBUGFLAGS	:= 
#-DINTERFACEDB
#-DALLDEBUG
#-DMETALPARDEBUG
#-DWALKERSETTINGDEBUG
#-DWALKERDEBUG
#-DCUTOFFDEBUG
#-DBHPPDEFINITIONS is needed to ignore the cmakedefine commands in BHversion.hpp

LIBOUTDIR	:= lib

LINKERFLAGS	:= $(LINKERFLAGS) -L$(LIBOUTDIR)

BUILDDIR 	:= build

#searchdirectories:
CCSRC		:= src
F90SRC		:= src
FSRC		:= src

CCSRCEXT   	:= cpp
CCSOURCES  	:= $(wildcard $(CCSRC)/*.$(CCSRCEXT))
CCOBJS	   	:= $(patsubst $(CCSRC)/%,$(BUILDDIR)/%,$(CCSOURCES:.$(CCSRCEXT)=.o))

FSRCEXT   	:= f
FSOURCES  	:= $(wildcard $(FSRC)/*.$(FSRCEXT))
FOBJS	    	:= $(patsubst $(FSRC)/%,$(BUILDDIR)/%,$(FSOURCES:.$(FSRCEXT)=.o))


F90SRCEXT   	:= f90
F90SOURCES  	:= $(wildcard $(F90SRC)/*.$(F90SRCEXT)) 
F90OBJS	    	:= $(patsubst $(F90SRC)/%,$(BUILDDIR)/%,$(F90SOURCES:.$(F90SRCEXT)=.o))
F90OBJS	    	:= $(shell echo $(F90OBJS) | sed 's/module_//g')

EXESRCEXT   	:= cxx
EXESOURCES  	:= $(wildcard $(CCSRC)/*.$(EXESRCEXT))
EXEOBJS	    	:= $(addprefix $(BUILDDIR)/,$(notdir $(EXESOURCES:.$(EXESRCEXT)=.o)))

LBFGSOBJS	:= l-bfgs-b.o #linpack.o blas.o timer.o

#Including libraries with dependencies
BHLIBLIGHTOBJS	:= BHmyMath.o \
		  BHvector.o BHatom.o BHcluster.o BHclusterAtoms.o \
		  BHmetalParameters.o \
		  BHcouple.o BHclusterData.o \
		  BHparsers.o BHsettings.o BHhisto.o BHorderParameter.o \
		  BHchargedAtom.o  BHclusterAnalyser.o BHtiming.o BHanalysisSupport.o BHclusterUtilities.o \
		  BHclusterCreatorUtilities.o

BHMINIMIZATIONOBJS		:= \
		BHwalker.o \
		BHwalkerStandard.o BHwalkerObstacle.o BHwalkerParallelExcited.o BHwalkerRegionExcited.o \
		BHmover.o BHmoveTester.o \
		BHmove.o BHmoveShake.o BHmoveShell.o BHmoveBonds.o BHmoveBall.o \
		BHwalkerSelectiveParallelExcited.o \
		BHwalkerHistogram.o \
		BHwalkerSettingsLoader.o BHwalkerSettings.o \
		BHabstractBasinHopping.o BHbasinHopping.o\
		BHinterface.o BHminimization_rgl.o BHminimization_tol.o\
	  	BHworkWithFortran.o \
		BHmoveExchange.o \
		BHmoveExchangeAll.o  BHmoveExchangeOnSurface.o BHmoveExchangeInBulk.o BHmoveExchangeCoreShell.o\
		BHmoveExchangeInterface.o BHmoveExchangeSeparate.o BHmoveExchangeMix.o \
		BHmoveBrownian.o BHmoveBrownianSurface.o

#BHmoveHighEnergyAtoms.o


BHENERGY	:= BHenergyCalculator.o BHenergyCalculator_SMATB.o module_BHmodule.o  BHenergyCalculator_SMATB_GAUSS.o

ALLBHLIBS	:= $(LIBOUTDIR)/liblbfgsb.a $(LIBOUTDIR)/libBHenergy.a $(LIBOUTDIR)/libBHenergy_omp.a $(LIBOUTDIR)/libBHminimization.a $(LIBOUTDIR)/libBHstructure.a $(LIBOUTDIR)/libBHdemo.a


CC_DEPS		:= $(CCOBJS:.o=.d) $(EXEOBJ:.o=.d) 

F90_MOD_SOURCES := $(shell find $(F90SRC) -type f -name "module_*.$(F90SRCEXT)")

DOXYGEN		:= $(shell command -v doxygen 2> /dev/null)

ANALYSISEXE	:= signatureAnalyzer ppanalyser
EXE 		:= bh++ $(ANALYSISEXE) mover islemain BHcharges centerer speciesRandomizer energyCalculator
#these needs -lBHstructure
LIGHTEXE	:= signatureAnalyzer ppanalyser centerer latticeCreator BHcharges speciesRandomizer clusterCreator inputFileCreator MetalParametersTest
#these needs -lBH -lbfgs
COMPLETEEXE	:= bh++ mover energytest movetester islemain BHmemory minimizer inputWalkerFileCreator
OMPEXE		:= bh++_omp
ENERGYEXE	:= energyCalculator
COMPILEALLEXE	:= $(COMPLETEEXE) $(LIGHTEXE) plotter
ALLEXE		:= $(EXE) energytest plotter latticeCreator movetester mover #bh++.omp
DOCANALYSIS	:= $(ANALYSISEXE)
DOCALL		:= bh++ mover islemain BHcharges centerer analysis
DOCEVERITHING	:= all analysis plotter

#.SUFFIXES:
#.SUFFIXES: .o .f90 .F .f .cxx .cpp
default:
	@echo "\033[1m"
	@echo '************************************************************************'
	@echo 'It is preferible to create a new directory and use cmake for compilation'
	@echo '************************************************************************'
ifdef DOXYGEN
	@echo ""
	@echo "Command for creating the documentation:"
	@echo '"make document" will create or update the documentation of this suite'
endif
	@echo "\033[0m"
	@echo 'You can single install various programs from the suite'
	@echo ' '
	@echo 'The various pieces of the suite:'
	@echo '  bh++			the main program that execute a basin hopping'
	@echo '  energyCalculator       A routine that calculates the single point energy of a cluster'
	@echo '  mover			the interface with an external suite'
	@echo '  movetester		an utility to test the moves on a cluster'
	@echo '  energytest		A routines that confront the energy calculation in cpp and fortran'
	@echo '  islemain		a small utility to test the collapsing isle algorithm'
	@echo '  signatureAnalyser	performs the signature analysis on xyz files'
	@echo '  ppanalyser		creates a param_best.out from the signatureAnalyser output'
	@echo '  BHcharges		computes distance in function of charges from "xyz with charge" files'
	@echo '  centerer		centers the given cluster in the point given in the file center.in'
	@echo '  speciesRandomizer      take a cluster and randomize its composition on user choice'
	@echo '  analysis		same as "$(DOCANALYSIS)"'
	@echo '  all			same as "$(DOCALL)"'
	@echo '  everything		same as "$(DOCEVERITHING)"'
	@echo '  clean			delete the objects from the build directory and the executables'
	@echo '  veryclean		delete als the dependance info and the build directory'
	@echo ' '
	@echo 'Some debugging options:'
	@echo '  mainvariables		plot the variables relatives the mainfiles'
	@echo '  cvariables		plot the variables relatives to the C++ objects'
	@echo '  fvariables		plot the variables relatives to the fortran objects'
	@echo '  libvariables		plot the variables relatives to the libraries'
	@echo '  maindo		same as "make mainvariables", but also compiles them'
	@echo '  cdo			same as "make cvariables", but also compiles them'
	@echo '  fdo			same as "make fvariables", but also compiles them'
	@echo '  libdo			same as "make libvariables", but also compiles them'
	@echo '  variables		same as "make mainvariables fvariables cvariables libvariables"'



all: $(EXE)
everything:$(COMPILEALLEXE)
analysis: $(ANALYSISEXE)

# pull in dependency info for *existing* .o files
-include $(CC_DEPS)

fdo:fvariables $(F90OBJS) $(FOBJS)

cdo:cvariables $(CCOBJS) 

libdo: libvariables $(ALLBHLIBS)

maindo:mainvariables $(EXEOBJS)

variables:mainvariables fvariables cvariables libvariables
	@echo build directory:
	@echo $(BUILDDIR)

cvariables:
	@echo dependencies:
	@echo $(CC_DEPS)
	@echo CPP objects:
	@echo $(CCOBJS)

libvariables:
	@echo libraries:
	@echo $(sort $(ALLBHLIBS))

mainvariables:
	@echo mainfiles:
	@echo $(EXEOBJS)

fvariables:
	@echo F objects:
	@echo $(FOBJS)
	@echo F90 objects:
	@echo $(F90OBJS)
	@echo modules:
	@echo $(F90_MOD_SOURCES)

clean:
	@echo cleaning build
	@rm -f $(LIBOUTDIR)/*.a -v
	@rm -f $(BUILDDIR)/*.o $(BUILDDIR)/*.mod -v
	@rm -f $(ALLEXE) -v
	@rm -f include/BHversion.hpp -v

veryclean:
	@echo cleaning build and dependencies
	@rm -f $(LIBOUTDIR)/*.a -v
	@rm -rf $(BUILDDIR) $(LIBOUTDIR) -v
	@rm -f $(ALLEXE) -v
	@rm -f build/BHversion.hpp -v

document:
ifndef DOXYGEN
	$(error "doxygen is not available please install it")
else
	@doxygen Doxyfile
	@echo "\033[1mnow you can open the documentation from:"
	@echo "file://$(shell pwd)/html/index.html"
	@echo "you can simply ctrl+click the link above\033[0m"
endif

#this should be forced on each new version change
COMP_HEADER	=build/BHversion.hpp
build/BHversion.hpp: Makefile
	@mkdir -p build
	@echo Setting up $@
	@sed -e "s/@BH++_VERSION_MAJOR@/$(BH++_VERSION_MAJOR)/g" \
	        -e "s/@BH++_VERSION_MINOR@/$(BH++_VERSION_MINOR)/g" \
		-e "s/@LIB_VERSION_MAJOR@/$(LIB_VERSION_MAJOR)/g" \
	        -e "s/@LIB_VERSION_MINOR@/$(LIB_VERSION_MINOR)/g" \
	        -e "s/@LIB_PATCH_LEVEL@/$(LIB_PATCH_LEVEL)/g" \
		templates/BHversion.hpp.in > $@

.PHONY:all clean fdo cdo libdo maindo veryclean variables mainvariables fvariables cvariables libvariables

####libs
BHARCHIVES	:=$(LIBOUTDIR)/libBHminimization.a $(LIBOUTDIR)/libBHstructure.a $(LIBOUTDIR)/libBHenergy.a $(LIBOUTDIR)/libBHenergy_omp.a
#general rule:
$(LIBOUTDIR):
	@mkdir -p $(LIBOUTDIR)

$(BHARCHIVES):|$(LIBOUTDIR)
	@$(AR) rvs $@ $^

$(LIBOUTDIR)/libBHenergy.a: $(addprefix $(BUILDDIR)/,$(BHENERGY))
$(LIBOUTDIR)/libBHenergy_omp.a: $(addprefix $(BUILDDIR)/,$(BHENERGY:.o=_omp.o))
$(LIBOUTDIR)/libBHstructure.a: $(addprefix $(BUILDDIR)/,$(BHLIBLIGHTOBJS))
$(LIBOUTDIR)/libBHminimization.a: $(addprefix $(BUILDDIR)/,$(BHMINIMIZATIONOBJS))
#must be an external lib
$(LIBOUTDIR)/liblbfgsb.a: $(addprefix $(BUILDDIR)/,$(LBFGSOBJS))|$(LIBOUTDIR)
	@$(AR) rvs $@ $^

$(LIBOUTDIR)/lib%.a: $(BUILDDIR)/%.o
	@$(AR) rvs $@ $^


#####mains
#general rules:
$(ENERGYEXE)	: $(LIBOUTDIR)/libBHstructure.a $(LIBOUTDIR)/libBHenergy.a $(LIBOUTDIR)/liblbfgsb.a
	@echo building $@
	$(LINKER) $(LINKERFLAGS) -o $@ $(word 4, $^)  -lBHstructure -lBHenergy -llbfgsb $(LINKERLIBS)

$(LIGHTEXE)	: $(LIBOUTDIR)/libBHstructure.a
	@echo building $@
	$(LINKER) $(LINKERFLAGS) -o $@ $(word 2, $^) -lBHstructure $(LINKERLIBS)

$(OMPEXE)	: $(LIBOUTDIR)/libBHstructure.a $(LIBOUTDIR)/libBHminimization.a $(LIBOUTDIR)/libBHenergy_omp.a $(LIBOUTDIR)/liblbfgsb.a
	@echo building $@
	$(LINKER) $(LINKERFLAGS) -o $@ $(word 5, $^) -lBHminimization -lBHstructure -lBHenergy_omp -llbfgsb -fopenmp $(LINKERLIBS)

$(COMPLETEEXE)	: $(LIBOUTDIR)/libBHstructure.a $(LIBOUTDIR)/libBHminimization.a $(LIBOUTDIR)/libBHenergy.a $(LIBOUTDIR)/liblbfgsb.a
	@echo building $@
	$(LINKER) $(LINKERFLAGS) -o $@ $(word 5, $^) -lBHminimization -lBHstructure -lBHenergy -llbfgsb $(LINKERLIBS)

minimizer	: $(BUILDDIR)/minimizator.o
energyCalculator: $(BUILDDIR)/calculateEnergy.o
bh++_omp	: $(BUILDDIR)/main_omp.o
bh++		: $(BUILDDIR)/main.o
mover		: $(BUILDDIR)/mover.o
signatureAnalyzer:$(BUILDDIR)/signatureAnalyzer.o
islemain	: $(BUILDDIR)/islemain.o
ppanalyser	: $(BUILDDIR)/ppanalyser.o
latticeCreator	: $(BUILDDIR)/lattice.o
centerer	: $(BUILDDIR)/centerer.o
movetester	: $(BUILDDIR)/movetester.o
BHmemory	: $(BUILDDIR)/memory.o
BHcharges	: $(BUILDDIR)/chargeAnalisys.o
energytest	: $(BUILDDIR)/energytest.o
speciesRandomizer:$(BUILDDIR)/speciesRandomizer.o
clusterCreator  : $(BUILDDIR)/clusterCreator.o
inputFileCreator: $(BUILDDIR)/inputFileCreator.o
inputWalkerFileCreator: $(BUILDDIR)/inputWalkerFileCreator.o
MetalParametersTest:$(BUILDDIR)/MetalParametersTest.o

plotter		: $(BUILDDIR)/plotenergy.o $(LIBOUTDIR)/libBHstructure.a $(LIBOUTDIR)/liblbfgsb.a $(LIBOUTDIR)/libBHminimization.a $(LIBOUTDIR)/libBHenergy.a $(LIBOUTDIR)/libBHdemo.a
	echo building $@
	$(LINKER) $(LINKERFLAGS) -o $@ $< -lBHdemo -lBHenergy -lBHstructure -lBHminimization -llbfgsb $(LINKERLIBS)

#bh++.omp	: $(CCOBJS) $(F90OBJSOMP) $(BUILDDIR)/main_omp.o
#	@echo building $@
#	$(CC) $(CMAINFLAGS) -o $@ $^ $(LINKERFLAGS) -fopenmp

####objects
#main files
$(BUILDDIR)/%.o: $(CCSRC)/%.$(EXESRCEXT) $(COMP_HEADER)
	@echo building $@
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(CCINC) $< -c $(DEBUGFLAGS) $(OPTIONFLAGS) -o $@	
	@$(CC)  $(CCINC) -MM -MF $(BUILDDIR)/$*.d -MQ $@ $(CFLAGS) $< -c $(DEBUGFLAGS) $(OPTIONFLAGS)

$(BUILDDIR)/main_omp.o: $(CCSRC)/main.cxx $(COMP_HEADER)
	@echo building $@
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(CCINC) $< -fopenmp -c $(DEBUGFLAGS) $(OPTIONFLAGS) -o $@
	@$(CC)  $(CCINC) -MM -MF $(BUILDDIR)/main_omp.d -MQ $@ $(CFLAGS) $< -c $(DEBUGFLAGS) $(OPTIONFLAGS)

#object files
$(BUILDDIR)/%.o: $(CCSRC)/%.$(CCSRCEXT) $(COMP_HEADER)
	@echo building $@
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(CCINC) $< -c $(DEBUGFLAGS) $(OPTIONFLAGS) -o $@
	@$(CC)  $(CCINC) -MM -MF $(BUILDDIR)/$*.d -MQ $@ $(CFLAGS) $< -c $(DEBUGFLAGS) $(OPTIONFLAGS)

$(BUILDDIR)/%_omp.o: $(CCSRC)/%.$(CCSRCEXT) $(COMP_HEADER)
	@echo building $@
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(CCINC) -fopenmp $< -c $(DEBUGFLAGS) $(OPTIONFLAGS) -o $@
	@$(CC)  $(CCINC) -fopenmp -MM -MF $(BUILDDIR)/$*.d -MQ $@ $(CFLAGS) $< -c $(DEBUGFLAGS) $(OPTIONFLAGS)

#fortran menageries
#modules
#$(BUILDDIR)/parameters.o:$(BUILDDIR)/module_parameters.o
#	@mv $< $@

#$(BUILDDIR)/function_par.o:$(BUILDDIR)/module_function_par.o
#	@mv $< $@

#$(BUILDDIR)/bh_mod.o:$(BUILDDIR)/module_bh_mod.o
#	@mv $< $@


$(BUILDDIR)/BHmodule_omp.o:$(F90SRC)/module_BHmodule.f90
	@mkdir -p $(dir $@)
	@echo building $@
	$(F90) -fopenmp $(F90FLAGS) $(F90MOD_OUT) $(F90MOD_DIR) -c $< -o $@

$(BUILDDIR)/BHmodule.o:$(F90SRC)/module_BHmodule.f90
	@mkdir -p $(dir $@)
	@echo building $@
	$(F90) $(F90FLAGS) $(F90MOD_OUT) $(F90MOD_DIR) -c $< -o $@
#others

$(BUILDDIR)/%.o:$(FSRC)/%.f
	@mkdir -p $(dir $@)
	@echo building $@
	$(FC) $(FFLAGS) -c $< -o $@

#%.o:%.f
#	@echo building $@
#	@mkdir -p $(dir $@)
#	$(F90) $(F90FLAGS) -c $< -o $@

#l-bfgs should be compiled:
#gfortran -O2 -shared -Wl,-soname,liblbfgsb.so.0 -o liblbfgsb.so.0 -fPIC lbfgsb.f linpack.f timer.f -lblas

$(BUILDDIR)/%_omp.o:$(F90SRC)/%.f90 $(BUILDDIR)/BHmodule.o $(F90_MOD_SOURCES)
	@mkdir -p $(dir $@)
	@echo building $@
	$(F90) $(F90FLAGS) $(F90MOD_DIR) -c $< -o $@

$(BUILDDIR)/%.o:$(F90SRC)/%.f90 $(BUILDDIR)/BHmodule.o $(F90_MOD_SOURCES)
	@mkdir -p $(dir $@)
	@echo building $@
	$(F90) $(F90FLAGS) $(F90MOD_DIR) -c $< -o $@

$(BUILDDIR)/BHinterface_omp.o:$(F90SRC)/BHinterface.f90 $(BUILDDIR)/bh_module_omp.o $(F90_MOD_SOURCES)
	@mkdir -p $(dir $@)
	@echo building $@
	$(F90)  -fopenmp $(F90FLAGS) $(F90MOD_DIR) -c $< -o $@


