How to start bh++ {#bhppmanual}
=================
@brief This page will show how to set up the files for a run fo bh++

`bh++` does not accept 
	- input_bh++.in where the general parameters of the simulation are setted, with the algorithm for the walkers, see [this page](@ref parserBHSettings) for the instruction on how to write this file
	- MetalParameter.in where [metal parameters](@ref parserMetalParameters) are written
	- a various number of files with the [settings for the walkers](@ref parserBHWalkerSettings)

and one of:
  - noseed_rgl.in one needed if you want an unseeded run
  - seed*.in if you opt for a seeded run (they must be `.xyz` files) one for each walker, the * has to be repalced by a number in ascending order from 0, 1, 2, 3...


Files
--


example for noseed_rgl.in for a cluster of 27 Ag and 7 Cu
~~~
34 #nat3d
Ag Cu #metalname1 metalname2
27 7 #number for each metal
spherical #shape of the random distribution
4,5. #r_min,r_max (angstrom)
~~~

another example for noseed_rgl.in for a cluster of 155 Ag
~~~
155 #nat3d
Ag #metal name
155 #comments
cartesian #shape of the random distribution
-5 5 #x_min,x_max (angstrom)
-5 5 #y_min,y_max
-5 5 #z_min,z_max
~~~

Old files
--

This is a old version of input_bh++.in, the program is still compatible with them for legacy runs
~~~~~~~~~~~~~
******Where do we start from?
seeded			!unseedeed/seeded
no 			!refresh random number generator (si/no) (if no it start with numbers 1234 5678)
******Interaction potentials
yes			!cut-off (si/no)
MetalParameters.in	        !input file for potential parametrization
******Run settings
1000	        	!n_mc (numero di passi del Monte Carlo)
*******Global optimization algorithms
10.             	!weight (if .eq.0. the histogram is not used)
4 2			!order_parameter
			!1=legamimisti AgCu
			!2=legami tipo1 AgAg
			!3=legami tipo2 CuCu
			!4=signature555
			!5=signature422
			!6=frazione atomi in contatto col substrato
0.0			!start_histo(1) (ascissa di partenza dell'istogramma)
0.01            	!hist_step(1) (larghezza delle barre)
100			!nbars(1) (il numero delle barre) 
0.			!start_histo(2)
0.5			!hist_step(2)
0             		!nbars(2) 
1000001         	!renew_histo
2			!walkers_turnover (1 ordinal, 2 random, 3 change with freq:freq_walker_exchange)
5			!freq_walker_exchange
1               	!nwalkers
input_walker0.in	!filename for walker0
******Output instructions
no              	!would you like different output for different walkers? si or no
yes			!lista_minimi '.true.' or '.false.'
0. 1.			!param_min,param_max (intervallo di variabilita' del primo parametro d'ordine)
50			!parameter_partitioning (numero di suddivisioni dell'intervallo)
~~~~~~~~~~~~~

this is the old vesion of input_walker#.in:
~~~
******Settings file for a walker
no              	!temperatura_variabile ('si' or 'no') 
100.             	!hightemp (K) (ignorare)
0.			!delta_temperatura (semiampiezza sinusoide) (solo per temperatura variabile)
0.			!periodo_temperatura (periodo sinusoide) (solo per temperatura variabile)
******Moves
0. 1.5          	!rho_inf,rho_sup (angstrom, used by move_single, shake, highenergy)
3.              	!displamin (angstrom, used by move ball, shell, highenergy, bonds)
1 500               	!probmove1,temp_bonds (move_bonds)
2 100               	!probmove2,temp_ball (move_ball)
3 100			!probmove3,temp_shell (move_shell)
4. 1500   	 	!probmove4=probabilità della mossa,temp_shake=temperatura di accettazione (move_shake)  usa questo
5 100			!probmove5,temp_highener (move_highenergyatoms)
6. 100			!probmove6 (move exchange) usa questo
7. 200 2000 1500        !probmove7,npas_bro,temp_brow,temp_brow_accept (move brownian) usa questo
8. 50 100 200           !probmove8,npas_brosurf,temp_browsurf,temp_browsurf_accept (move brownian on the surface)
100			!temp_single (if the sum of the probs above is less than 1, then move single is used)
1 100			!prob_exch_all,temp_all usa questo (se metti gli scambi)
2 20			!prob_exch_sur,temp_sur
3 10 20 30		!prob_exch_csh,bignn_csh,smallnn_csh,temp_csh
4 30 40 400		!prob_exch_int,bignn_int,smallnn_int,temp_int
5. 50 500 T		!prob_exch_sep,exp_sep,temp_sep,small_inside
6. 60 600		!prob_exch_mix,temp_mix 
*******Global optimization algorithm
0.7             	!repwalkers
0.03 200.0      	!widthwalkers(1)
pew			!choose algorithm ( pew , mew1 , mew2 , histo , hexc )
no			!option restart
-99.82          	!ener_restart
~~~

