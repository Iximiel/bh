Settings file for BH::BHWalker {#parserBHWalkerSettings}
==============
@brief The documentation for the BH::BHWalkerSettings parser

The new versions can contain comments. You can start a comment line with `*#!`.
To tell the parser that you are giving a parsed input you need the first line containing the word `parsed`, lowercase.

The parser works with cards to divide the parameters in separated arguments. To enter in a specific card simply add a comment block with the name of the card inside in UPPERCASE letters.

The settings must be written in lowercase letters and followed by an equal sign (`=`) and then the data.
To separate the settings you can write them on a new line or separate them with a comma (`,`).

The cards are:

- **TVARIABLE** not yet implemented, can be omitted
	+ *Tvariable* **bool**, default `false`
	+ *highT* **double**
	+ *deltaT* **double**
	+ *periodT* **double**
- **MOVES** in this card you can select the moves that will evolveyour minimization, you don't need to specify how many moves you will use. All moves have two option for controlling the temperature and the probability of acceptance. Moreover each move has its own options. For specify a move you need write the name followed by the options, the options must be separated with commas `,`. The default options for each move are:
  + *shake*                prob = 1.0, accTemp = 300, Rmin = 0, Rmax = 1.4
  + *bonds*                prob = 1.0, accTemp = 300, Rmin = 0, Rmax = 1.4
  + *ball*                 prob = 1.0, accTemp = 300, minimumDisplacement =3.0
  + *shell*                prob = 1.0, accTemp = 300, minimumDisplacement =3.0, shellThickness = 1.5
  + *brownian*             prob = 1.0, accTemp = 1500, steps = 500, brownianTemperature = 2000
  + *brownianSurface*      prob = 1.0, accTemp = 1500, steps = 500, brownianTemperature = 2000, fewNeighbours = 10
  + *exchangeAll*          prob = 1.0, accTemp = 100
  + *exchangeInBulk*       prob = 1.0, accTemp = 100, fewNeighbours = 10
  + *exchangeMix*          prob = 1.0, accTemp = 100, fewNeighbours = 10
  + *exchangeOnSurface*    prob = 1.0, accTemp = 100, fewNeighbours = 10
  + *exchangeSeparate*     prob = 1.0, accTemp = 100, fewNeighbours = 10, weight = 2, smallInside = true
  + *exchangeCoreShell*    prob = 1.0, accTemp = 100, fewNeighbours = 10, NNforHeavy = 1, NNforLight = 1
  + *exchangeInterface*    prob = 1.0, accTemp = 100, fewNeighbours = 10, NNforHeavy = 1, NNforLight = 1
- **ALGORITHM**
  + *seed* **string** name of the file that contiains the seed for this walker, if not specified the walker will be randomly initialized using `noseed_rgl.in`
  + *algorithm* **string** name of the algorithm to use, accepts `stnd`, `histo`, `pew`, `rew`, `obst`, default `stnd`
  + *repwalkers* **double** repulsion energyfrom other walkers to use with `pew` and `rew` algorithms, default `0.7`
  + *widthwalker* dimension in the parameter space, to be used with `pew` algorithm
	+ <i>.param1</i> **double** dimension in the order parameter 1 space, default `0.03`
	+ <i>.p1</i> fast form for <i>.param1</i>
	+ <i>.param2</i> **double** dimension in the order parameter 2 space, default `1.0`
	+ <i>.p2</i> fast form for <i>.param1</i>
  + *lowRepulsion* lower limits of the repulsions zones in for the `rew` walker 
   	+ <i>.param1</i> **double** lower limit of the repulsion zone in the in the order parameter 1 space, default `0.0`
	+ <i>.param2</i> **double** lower limit of the repulsion zone in the in the order parameter 2 space, default `0.0`	
  + *highRepulsion* upper limits of the repulsions zones in for the `rew` walker 
  	+ <i>.param1</i> **double** upper limit of the repulsion zone in the order parameter 1 space, default `0.5`
	+ <i>.param2</i> **double** upper limit of the repulsion zone in the order parameter 2 space, default `1.0`
  + *restart* **bool** not implemented
  + *restartEnergy* **double** not implemented
  
###Some examples:###

A PEW (Parallel Excited Walker) increases the probability of accepting a move if its actual minimum is near (in the space of the order parameters) to an another walker.

Here an example of an PEW with only shake moves:
~~~
#parsed
#MOVES
shake prob = 1.0, accTemp = 1500
#ALGORITHM
seed              =
algorithm      	  =	pew
repwalkers        = 0.7
widthwalker.param1=	0.03
widthwalker.param2=	0.3
~~~

An HISTO (HISTOgram) walker increases the probability of accepting a move based on how much is populated the bin in the Order Parameter space in wich the actual minimum has fallen. The histogram algorithm is more slower because the OP analysis in calculated for each minima and not only for the accepted one.

Here an example of an HISTO walker with exhanges (using defaults settings for each move):
~~~
#parsed
#MOVES
exchangeAll          prob = 10.0, accTemp = 100
exchangeMix          prob = 1.0 , accTemp = 100
exchangeInterface    prob = 1.0 , accTemp = 100
#ALGORITHM
algorithm	   =	histo
~~~

An OBST (OBSTacle) walker has to be coupled with one or more PEW walkers. The aim of an obstacle is to create a configuration from wich the other PEW should stay far (NB: an OBST walker won't move, so you canno start a minimzation made only by obst walkers).

Here an example of an OBST walker:
~~~
#parsed
#an obstacle walker does not need any move declared 
#ALGORITHM
seed		=	seedFile.in
algorithm	=	obst
~~~
