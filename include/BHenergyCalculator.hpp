/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of class BHEnergyCalculator

   some variable are a double of the ones in BHMetalParameters, this class uses
   them in a way that wants to be efficient for calculation instead of
   classification
   @file BHenergyCalculator.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 05/06/2017
   @version 1.2.1

   Interacting neighbours moved here from child class

   @date 29/05/2017
   @version 1.2

   Adding FIRE algorithm

   @date 16/10/2017
   @version 0.1

   @date 17/10/2017
   @version 0.2

   energy defined

   @date 19/10/2017
   @version 0.4.1

   Gradient defined and working, divided base class from SMATB.

   And added minimization routines to the base class
*/

#ifndef BHENERGYCALCULATOR_H
#define BHENERGYCALCULATOR_H
#include <map>

#include "BHcluster.hpp"
#include "BHmetalParameters.hpp"
#include "BHmyMath.hpp"
#include "BHworkWithFortran.hpp"
namespace BH {
  /** @brief This class is thebase class for the energy calculators

      This is a pure virtual class, that contains a preassembled driver for the
     minimizatione engine l-bfgs-b.

      The Gradients and the Energy functions have to be implemented in a child
     class.

  */
  class BHEnergyCalculator {
  public:
    BHEnergyCalculator (const BHCluster &, const BHMetalParameters &);
    BHEnergyCalculator (const BHEnergyCalculator &);
    BHEnergyCalculator (BHEnergyCalculator &&) noexcept;
    BHEnergyCalculator &operator= (const BHEnergyCalculator &) = delete;
    BHEnergyCalculator &operator= (const BHEnergyCalculator &&) = delete;
    virtual ~BHEnergyCalculator ();
    BHdouble Energy (const BHClusterAtoms &);
    virtual BHdouble Energy (BHdouble *x) = 0;
    virtual BHdouble Energy_tol (BHdouble *x) = 0;
    virtual BHdouble Gradient (BHdouble *x, BHdouble *g) = 0;
    virtual BHdouble Pressure (BHdouble *x, BHdouble *p);
    virtual void Neighbours_for_tol (BHdouble *x);
    virtual BHdouble Gradient_tol (BHdouble *x, BHdouble *g) = 0;
    /// TriangularArrayIndex
    inline int TAI (const int i, const int j) { return i + (j * (j + 1)) / 2; }
    unsigned nAtoms () const;
    virtual BHEnergyCalculator *clone () = 0;

  protected:
    const unsigned nAtoms_;
    const unsigned n3Atoms_;
    const unsigned nTypes_;
    const unsigned nInteractions_;
    unsigned short *typeList_;
    unsigned short *intType_;
    // calculation aid:
    unsigned short *nNeigh_;
    // *intID_;
    /// the neighbours list per atom
    unsigned int **NeighList_; // usable by verlet list and others caclulations
    /// used for define interactin Neighbours distance in Neighbours_for_tol
    BHdouble *IntNeighDistTol2_;
    /// converts the atomname to his ID
    std::map<std::string, unsigned short> Name2ID;
    /// converts the interation name to its id
    std::map<std::string, unsigned short> intName2ID_;
    /// converts BHMetalParameters IDs to IDs used in this class
    std::map<unsigned int, int> INT_MP2ID;
  };
} // namespace BH
#endif // BHENERGYCALCULATOR_H
