/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of the interface BHWalker

   @file BHwalker.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 26/4/2018
   @version 0.5
   added isInForbiddenRegion for rew algorithm

   @date 6/3/2018
   @version 0.4.6

   removing islads collapser from the BHWalker class

   @date 5/12/2017
   @version 0.4.5

   changing the analysis to BHClusterAnalysis

   @date 16/11/2017
   @version 0.4.4
   Adding documentation to the code, changing some names for simplify reading
   the code

   @date 21/3/2017
   @version 0.4.1
*/
#ifndef BHOPERATIONSINOPSPACE_H
#define BHOPERATIONSINOPSPACE_H
#include <iosfwd> //foward declaration so that iostream is included only if needeed (for IO)

#include "BHwalkerSettings.hpp"

namespace BH {

  /// \brief contains the settings of BHWalkerSettings and apply them to OPs
  ///
  /// BHOPspaceOperations wraps the settings from BHWalkerSettings an adds
  /// two new functions (isNeighbour and is InForbiddenRegion).
  ///
  /// BHWalkerSettings are loaded by BHWalkerSettingsLoader and this service
  /// class is  created in BHWalker to interact with BHWalkerAlgorithms.
  class BHOperationsInOPSpace : public BHWalkerSettings {
  public:
    /// Constructor, the seed can be created by the user or loaded form a file
    BHOperationsInOPSpace (const BHWalkerSettings &walkersettings);
    BHOperationsInOPSpace ();
    BHOperationsInOPSpace (const BHOperationsInOPSpace &);
    BHOperationsInOPSpace (BHOperationsInOPSpace &&) noexcept;
    /// confront two collection of OPS anda tell if are neighbours
    bool isNeighbour (
      const std::vector<BHdouble> &OPs,
      const std::vector<BHdouble> &otherOPs) const;
    /// returns true if this walker is in the forbidden region of the OP space
    bool isInForbiddenRegion (const std::vector<BHdouble> &OPs) const;
  };
} // namespace BH
#endif // BHOPERATIONSINOPSPACE_H
