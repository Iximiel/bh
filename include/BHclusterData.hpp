/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of class BHClusterData, containers for values of energy
   and value of orders parameters
   @file BHclusterData.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @version 0.9.2
   @date 6/3/2018

   trying to add a  rule of 5 to this

   @version 0.9.1
   @date 11/12/2017
   small changes in the structure

   @date 21/3/2017
   @version 0.9
*/
#ifndef BHCLUSTERDATA_H
#define BHCLUSTERDATA_H
#include <iosfwd> //foward declaration so that iostream is included only if needed, needed for IO operators

#include "BHmyMath.hpp"
#include "BHorderParameter.hpp"
#include <vector>

namespace BH {

  /// \brief The BHClusterData class stores the energy and order parameter
  /// values
  class BHClusterData {
  public:
    //  static const NofOrderPar = 2;//maybe will be useful
    BHClusterData ();
    BHClusterData (const BHdouble Etot, const BHdouble OP0, const BHdouble OP1);
    BHClusterData (const BHdouble Etot, const std::vector<BHdouble> &OPs);
    BHClusterData (const BHdouble Etot, const std::vector<BHdouble> &&OPs);
    BHClusterData (const BHClusterData &other);
    BHClusterData (BHClusterData &&other) noexcept;
    virtual ~BHClusterData () = default;
    BHClusterData &operator= (const BHClusterData &);
    BHClusterData &operator= (BHClusterData &&) noexcept;

    bool differentFrom (
      const BHClusterData &other,
      const BHdouble energyDiff = MINIMUN_ENERGY_DIFFERENCE,
      const BHdouble OPDiff = MINIMUM_OP_DIFFERENCE);

    BHdouble Energy () const;
    BHdouble orderParameter (unsigned opID) const;
    std::vector<BHdouble> orderParameters () const;
    void setOrderParameters (std::vector<BHdouble> &&);
    void setOrderParameters (std::vector<BHdouble> &);
    size_t numberOfOP () const;

    void setEnergy (const BHdouble);
    void setOrderParameter (const unsigned opID, const BHdouble);
    void
    SetData (const BHdouble &Etot, const BHdouble &OP0, const BHdouble &OP1);

    std::string plotDataForxyz (const std::vector<BHOrderParameter> &OP) const;

    static std::string
    ClusterData_list (const std::vector<BHOrderParameter> &OP);
    // void SetData(BHdouble MOE, BHdouble MME, BHdouble OP1, BHdouble OP2);

    /*
      BHdouble M_O_Energy() const;
      BHdouble M_M_Energy() const;
      void M_O_Energy(BHdouble );
      void M_M_Energy(BHdouble );
    */

  protected: // I want to get them manipulated by Molecular Dinamics
    BHdouble myEtot{0.0};
    /*
        BHdouble myE_met_ox{0.0};
        BHdouble myE_met_me{0.0};
    */
    std::vector<BHdouble> myOrder_par{0.0, 0.0};
  };

  // IO
  // std::istream& operator>> (std::istream&, BH::BHClusterData&);
  std::ostream &operator<< (std::ostream &, const BH::BHClusterData &);
} // namespace BH
#endif // BHCLUSTER_H
