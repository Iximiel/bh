/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#ifndef BHENUMS_HPP
#define BHENUMS_HPP
#include <string>
namespace BH {
#define MINALGO_PARSER
#define MinAlgorithmParser(key, Class) key,

  enum class BHminimizationAlgorithmType {
#include "BHminimizationAlgorithms.hpp"
    NofKnownAlgoriythms
  };

#undef MinAlgorithmParser
#undef MINALGO_PARSER

  BHminimizationAlgorithmType minAlgorithmFromStr (std::string);
  std::string strFromMinimizationAlgorithm (const BHminimizationAlgorithmType);

#define WALKER_ALGORITHM_PARSER
#define WalkerAlgorithmParser(key, Class, standalone, skipSteps) key,

  enum class BHwalkerAlgorithmType {
#include "BHwalkerAlgorithms.hpp"
    NofKnownAlgoriythms
  };

#undef WalkerAlgorithmParser
#undef WALKER_ALGORITHM_PARSER
  BHwalkerAlgorithmType walkerAlgorithmFromStr (std::string);
  std::string strFromWalkerAlgorithm (const BH::BHwalkerAlgorithmType);
} // namespace BH
#endif // BHENUMS_HPP
