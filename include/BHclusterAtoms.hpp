/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Definition of class BHClusterAtoms that contains an array of BHAtom s
   and some infos
   @file BHclusterAtoms.hpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 13/9/2017
   @version 0.10
*/
#ifndef BHCLUSTERATOMS_H
#define BHCLUSTERATOMS_H
#include "BHatom.hpp"
#include <vector>
namespace BH {
  /// Stores a cluster of atoms, and some informations about composition.
  class BHClusterAtoms {
  public:
    /// Create an empty cluster
    BHClusterAtoms ();
    BHClusterAtoms (const unsigned int);
    /// Create a cluster from an array of BHVector
    BHClusterAtoms (const unsigned int, BHAtom *);
    BHClusterAtoms (std::vector<BHAtom>);
    BHClusterAtoms (const BHClusterAtoms &);

    BHClusterAtoms (BHClusterAtoms &&) noexcept;
    BHClusterAtoms (std::string);
    virtual ~BHClusterAtoms ();
    BHClusterAtoms &operator= (const BHClusterAtoms &);
    BHClusterAtoms &operator= (BHClusterAtoms &&) noexcept;
    /// Copy only the positions of the atoms
    BHClusterAtoms &operator<< (const BHClusterAtoms &);

    /// Return the i-th atom in the cluster
    BHAtom &operator() (const unsigned);
    /// Return the i-th atom in the cluster
    const BHAtom &operator() (const unsigned) const;
    /// Return the i-th atom in the cluster
    BHAtom &operator[] (const unsigned);
    /// Return the i-th atom in the cluster
    const BHAtom &operator[] (const unsigned) const;

    BHAtom *begin () const noexcept;
    BHAtom *end () const noexcept;
    const BHAtom *cbegin () const noexcept;
    const BHAtom *cend () const noexcept;

    BHVector getGeometricCenter () const;
    /// Returns the radius of the smallest sphere that wraps the center of the
    /// cluster atoms centered in the geometric center
    BHdouble getGeometricSphereRadius () const;
    void AssignFromVector (BHdouble *x);
    void AssignToVector (BHdouble *x) const;
    void setAdditionalInfos (const std::string &);
    std::string getAdditionalInfos () const;
    /// Copies the position from another cluster and ensures that composition is
    /// the same
    void take_positions (const BHClusterAtoms &);
    // getters
    virtual unsigned getNofAtoms () const;
    /// Set a new numbers of atoms
    /**When called store a new value for NofAtoms_ and then
     *reinitialize the atoms array.<br>
     * Needed to initialize the cluster.
     */
    virtual void setNofAtoms (unsigned);
    unsigned int NofSpecies () const;
    /// returns the complete compositions
    std::string Composition () const;
    /// gives the ith atom type in the composition
    std::string Composition (const unsigned) const;
    /// set the composition from the internal atoms
    void updateComposition ();
    /// return false if the configuration of the two clusters are not the same
    bool
    testSameConfiguration (const BHClusterAtoms &, bool verbose = false) const;
    static BHClusterAtoms getScaledCluster (BHClusterAtoms, const BHdouble &);
    static BHClusterAtoms getCenteredCluster (
      BHClusterAtoms cluster, BHVector center = {0.0, 0.0, 0.0});

  protected: // I want to get them manipulated by Molecular Dinamics
    unsigned int NofAtoms_{0};
    BHAtom *Atoms_{nullptr};
    std::string additionalInfos_{
      ""}; ///< string that stores the comment line if
           ///< this cluster is obtained by operator>>
    std::vector<std::string> Composition_{}; // names of type of atoms
  };
  /*
   * Ithought that these would be a good workaround, but i was wrong
  std::istream& operator>> (std::istream&, BHClusterAtoms&);
  std::ostream& operator<< (std::ostream&, const BHClusterAtoms&);
  */
} // namespace BH
  // IO
std::istream &operator>> (std::istream &, BH::BHClusterAtoms &);
std::ostream &operator<< (std::ostream &, const BH::BHClusterAtoms &);

#endif // BHCLUSTERATOMS_H
