/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Contains the typedef and some declaration for the time analisys
   @file BHtiming.hpp
   @author Daniele Rapetti (iximie@gmail.com)
   @date 21/12/2017
   @version 1.0
*/
#ifndef BHTIMING_H
#define BHTIMING_H
#include <chrono>
#include <iosfwd>

namespace BH {
  using ms = std::chrono::steady_clock::duration;
  using tp = std::chrono::steady_clock::time_point;
  void
  timing (std::ostream &stream, const std::chrono::steady_clock::duration &obj);
} // namespace BH
#endif // BHTIMING_H
