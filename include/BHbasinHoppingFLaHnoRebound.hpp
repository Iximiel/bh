/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#ifndef BHBASINHOPPINGFLYINGLANDINGALLWHIKINGWALKERnoREBOUND_HPP
#define BHBASINHOPPINGFLYINGLANDINGALLWHIKINGWALKERnoREBOUND_HPP
#include "BHabstractBasinHopping.hpp"
namespace BH {
  // shortcut for a bhFLH_4 minimization
  class BHBasinHoppingFLH4 : public BHAbstractBasinHopping {
  public:
    const char *logo () override;
    static const char *staticLogo ();
    BHBasinHoppingFLH4 (const BHSettings &settings);
    BHBasinHoppingFLH4 (BHBasinHoppingFLH4 &) = delete;
    BHBasinHoppingFLH4 (BHBasinHoppingFLH4 &&) = delete;
    BHBasinHoppingFLH4 &operator= (BHBasinHoppingFLH4 &) = delete;
    BHBasinHoppingFLH4 &operator= (BHBasinHoppingFLH4 &&) = delete;
    ~BHBasinHoppingFLH4 () override;
    const std::string BHStyleName () override;
  };
} // namespace BH
#endif // BHBASINHOPPINGFLYINGLANDINGALLWHIKINGWALKERnoREBOUND_HPP
