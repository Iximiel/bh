/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @file BHclusterUtilities.hpp
   @brief Definitions of the function in the namespace BHClusterUtilities

   @author Daniele Rapetti (iximiel@gmail.com)
   @date 6/3/2018
   @version 0.1.1

   Separating these function from BHCluster and adding some other utilities

   @date 6/3/2018
   @version 0.1

   Separating these function from BHClusterAnalyser

*/
#ifndef BHCLUSTERUTILITIES_H
#define BHCLUSTERUTILITIES_H
#include "BHcluster.hpp"
#include "BHclusterData.hpp"
#include "BHcouple.hpp"
#include "BHmetalParameters.hpp"
#include "BHrandom.hpp"
#include <unordered_map>
#include <vector>

namespace BH {
  /// Contains Function that can act on BHCluster or on BHClusterAtoms
  namespace BHClusterUtilities {
    struct BHClusterRandomizerData {
      BHClusterRandomizerData ();
      BHClusterRandomizerData (std::string);
      enum class typeOfRandom { sphere, cartesian };
      struct Limits {
        BHdouble min;
        BHdouble max;
      };
      struct TypeAndNumber {
        std::string Type;
        unsigned NofAtoms;
      };
      unsigned N{0};
      typeOfRandom randomClusterShape{typeOfRandom::cartesian};
      Limits XLimits{0.0, 1.0};
      Limits YLimits{0.0, 1.0};
      Limits ZLimits{0.0, 1.0};
      std::vector<TypeAndNumber> nAtomByTypeAndN{0};
    };
    std::istream &operator>> (std::istream &, BHClusterRandomizerData &);
    void plotRandomClusterData (const BHClusterRandomizerData hintsForRnd);

    BHClusterAtoms randomCluster (
      const BHClusterRandomizerData hintsForRnd, rndEngine &my_rnd_engine);
    struct IslandInfo {
      /// The index of one of the atoms of the island
      unsigned oneAtom;
      /// The number of atoms in the island
      unsigned nOfAtoms;
    };
    struct IslandInfoSurf {
      /// The index of one of the atoms of the island
      unsigned oneAtom;
      /// The number of atoms in the island
      unsigned nOfAtoms;
      /// The number of atoms on the surface of the island
      unsigned nOfAtomsOnSurf;
    };
    /// Returns a vector with the IslandInfos
    std::vector<IslandInfo>
    IslandsAnalysis (BHCluster &cluster, const BHMetalParameters &bhmp);
    /// returns the number of islands in the cluster
    unsigned CountIslands (BHCluster &cluster, const BHMetalParameters &bhmp);
    /// Returns a vector with the dimensions of the isolated islands in the
    /// cluster
    std::vector<IslandInfoSurf> IslandsAnalysisWithSurface (
      BHCluster &cluster, const BHMetalParameters &bhmp);

    /// Worker function, users should call the non "eat" versions
    namespace BHIslandWorkers {
      /// returns the dimension of an "island"
      /**and "eats" (by flipping to false) the elements of the unvisited
       * vector*/
      unsigned eatIsland (
        unsigned j, const BHCluster &cluster, std::vector<bool> &unvisited);
      /// returns the dimension of an "island" of the same kind of atoms
      /**and "eats" (by flipping to false) the elements of the unvisited
       * vector*/
      unsigned eatIsland_sameKind (
        unsigned i, const BHCluster &cluster, std::vector<bool> &unvisited);

      /// returns the dimension of an "island" and the number og atoms on its
      /// surface
      /**and "eats" (by flipping to false) the elements of the unvisited
       * vector*/
      std::pair<unsigned, unsigned> eatIslandAndSurface (
        unsigned i, const BHCluster &cluster, std::vector<bool> &unvisited);

      /// Pushes back recursively the elements of an island in the elements
      /// vector
      /**and flips to false the elements of the unvisited vector.

         The dimension of the island is stored as the size of the elements
         vector.
      */
      vectorAtomID getElementsOfIsland (
        unsigned j, const BHCluster &cluster, std::vector<bool> &unvisited);
    } // namespace BHIslandWorkers
    /// reorder the indexes so that atom of same species are organized in blocks
    void MassesReorder (BHClusterAtoms &, const BHMetalParameters &bhmp);
    /// reorder the indexes so that atom of same species are organized in blocks
    void AlphabeticalReorder (BHClusterAtoms &);

    void IslandsCollapser (
      BHCluster &cluster, const BHMetalParameters &bhmp, rndEngine &RNG);

    enum splitAndJuxtaposePlane { xy, xz, yz };
    /// Splits two clusters along an axis and juxtaposes the two halves
    BHClusterAtoms splitAndJuxtapose (
      const BHClusterAtoms &firstCluster,
      const BHClusterAtoms &secondCluster,
      splitAndJuxtaposePlane plane = yz);

    std::string plotCenteredClusterWithData (
      const BHClusterAtoms &cluster,
      const BHClusterData &clusterData,
      const std::vector<BHOrderParameter> &OPnames);
    std::string plotClusterWithData (
      const BHClusterAtoms &cluster,
      const BHClusterData &clusterData,
      const std::vector<BHOrderParameter> &OPnames);
    std::string
    plotClusterWithComment (const BHClusterAtoms &cluster, std::string comment);
    /// Calculates the CNA signatures of the cluster
    ///
    /// @returns a vector of couples with the calculated CNA signature assigned
    std::vector<BHCouple> calculateCNAsignatures (const BHCluster &cluster);
    void printCNASignatures (const std::vector<BHCouple> &couples);
    std::unordered_map<short, int>
    extractCNASignatures (const std::vector<BHCouple> &couples);
    void monoatomicTrajectory (
      std::string atomName,
      std::string fname,
      unsigned nat,
      BHdouble *x,
      std::string comment = "",
      bool append = false,
      BHdouble *v = nullptr);
  } // namespace BHClusterUtilities
} // namespace BH

#endif // BHCLUSTERUTILITIES_H
