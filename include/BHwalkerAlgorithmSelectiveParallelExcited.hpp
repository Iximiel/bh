/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of the cfunction BHWalkerSelectiveParallelExcited

@file BHwalkerSelectiveParallelExcited.hpp
  @author Daniele Rapetti (iximiel@gmail.com)

@date 21/11/2019
  @version 0.1

  creation
  @date 25/11/2019
  @version 1.0

  was a class, now is a function

    */

#ifdef WALKER_ALGORITHM_PARSER
WalkerAlgorithmParser (
  spew, BHWalkerAlgorithmSelectiveParallelExcited, false, false)
#else
#ifndef BHWALKERSELECTIVEPARALLELEXCITED_H
#define BHWALKERSELECTIVEPARALLELEXCITED_H
#include "BHwalkerAlgorithm.hpp"
namespace BH {
  namespace BHWalkerAlgorithms {
    /// \brief The BHWalkerSelectiveParallelExcited acts as a
    /// BHWalkerParallelExcited, but only on a selection of other walkers
    BHdouble BHWalkerAlgorithmSelectiveParallelExcited (
      BHWalker &workon,
      std::vector<BHWalker> &walkers,
      const BHMetalParameters &,
      BHWalkerSupport::BHWalkerAlgorithmSupport &parameters,
      rndEngine &rng);
  } // namespace BHWalkerAlgorithms
} // namespace BH
#endif // BHWALKERSELECTIVEPARALLELEXCITED_H
#endif // WALKER_ALGORITHM_PARSER
