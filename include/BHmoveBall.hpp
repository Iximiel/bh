/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
  @brief Declaration of move Ball
   @file BHmoveBall.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 10/4/2019
   @version 0.1
   creating the move
  */
#ifdef MOVE_PARSER
MoveParser (ball, BHMoveBall)
#else
#ifndef BHMOVEBALL_H
#define BHMOVEBALL_H
#include "BHmove.hpp"
namespace BH {
  /**
   * @brief The move Ball class displace a single atom randomly within the
   *cluster
   *
   *
   * This move consists in:
   * -# randomly choosing an atom (iatom) of the cluster
   * -# locating the maximum distance between atoms and cdm (maxdist)
   * -# moving iatom in a randomly chosen position within a sphere centered in
   *cdm and of radius=maxdist
   * -# if iatom has thus been displaced of a quantity bigger then displamin,
   *the program returns. If the displacement is smaller, a move is attempted to
   *another point within the sphere
   */
  class BHMoveBall : public BHMove {
  public:
    BHMoveBall ();
    // BHMoveBall (const BHMoveBall &);
    ~BHMoveBall () override;
    void Specialize (const BHCluster &in, const BHMetalParameters &) override;
    static std::string DefaultString ();
    std::unique_ptr<BHMove> clone () override;

  protected:
    bool doMoveAlgorithm (
      BHCluster &in,
      BHCluster &out,
      BHClusterAnalyser &,
      const BHMetalParameters &,
      rndEngine &) override;
    std::string printSettingsSpecialized () const override;
    std::string printSettingsSpecializedForInput () const override;
    bool parseSpecialized (const std::string &) override;
    bool parseAfter () override;
    BHdouble MinimumMoveDisplacement_{3.0};
    BHdouble MinimumMoveDisplacementSQ_;
    BHUIntRND rndSelector_{0, 0};
  };
  namespace BHParsers {
    namespace BHMV {
      const std::array<std::string, 1> BALL = {
        "minimumdisplacement" // double //0
      };
    }
  } // namespace BHParsers
} // namespace BH
#endif // BHMOVEBALL_H
#endif
