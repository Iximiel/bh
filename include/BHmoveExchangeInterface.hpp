/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
  @brief Declaration of move Exchange Interface
   @file BHmoveExchangeInterface.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 12/4/2019
   @version 0.1
   creating the move
  */
#ifdef MOVE_PARSER
MoveParser (exchangeInterface, BHMoveExchangeInterface)
#else
#ifndef BHMOVEEXCHANGEINTERFACE_H
#define BHMOVEEXCHANGEINTERFACE_H
#include "BHmoveExchange.hpp"

namespace BH {

  /// \brief The move Exchange Interface swap two atoms that have a neigbours of
  /// different kind
  /**
     This functions makes two lists of atoms:

     - one for the heavy atoms with more than BHExchangeOpt#smallNN atoms of
     different kind.
     - the chosenAtom2_ for light atoms with more than BHExchangeOpt#smallNN
     atoms of different kind.

     Then it updates #chosenAtom1_ and #chosenAtom2_ with random atoms each
     selected from the two lists.
  */
  class BHMoveExchangeInterface : public BHMoveExchange {
  public:
    BHMoveExchangeInterface ();
    ~BHMoveExchangeInterface () override;
    static std::string DefaultString ();
    std::unique_ptr<BHMove> clone () override;

  protected:
    bool selectAtoms (
      BHCluster &out,
      BHClusterAnalyser &,
      const BHMetalParameters &,
      rndEngine &) override;
    std::string printSettingsSpecialized () const override;
    std::string printSettingsSpecializedForInput () const override;
    bool parseSpecialized (const std::string &) override;
    unsigned int fewNeighbours_{11}, NNforHeavy_{1}, NNforLight_{1};
  };
  namespace BHParsers {
    namespace BHMV {
      enum class EXCHANGEINTERFACEvariable {
        fewNeighbours_,
        NNforHeavy_,
        NNforLight_
      };

      const std::array<std::string, 3> EXCHANGEINTERFACE = {
        "fewneighbours", // int
        "nnforheavy",    // int
        "nnforlight"     // int
      };
    } // namespace BHMV
  }   // namespace BHParsers
} // namespace BH
#endif // BHMOVEEXCHANGEINTERFACE_H
#endif
