/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of BHWalkerSettings
   @file BHwalkerSettings.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 26/4/2018
   @version 0.7
   added RepulsionZone for rew algorithm

   @date 12/1/2018
   @version 0.6.1

   Added GENERAL_MOVES card to the parser

   @date 21/11/2017
   @version 0.6

   Implementation of the parser
   @date 21/11/2017
   @version 0.5

   Starting to add a parser

   @date 21/3/2017
   @version 0.4

   @date 24/04/2019
   @version 0.6

   Removing old settings classes
*/
#ifndef BHWALKERSETTINGS_H
#define BHWALKERSETTINGS_H
#include <string>

#include "BHclusterAtoms.hpp"
#include "BHmove.hpp"
#include "BHrandom.hpp"
#include "BHsettings.hpp"
//#include "BHenums.hpp"

namespace BH {
  /// The dimension of a walker in the Order Parameters space
  struct BHwalkerRepulsionZone {
    BHdouble low{0.0}, high{1.0};
  };
  /// A container that stores the options of a single walker
  struct BHWalkerSettings {
    struct MoveNameAndString {
      std::string name, settingsString;
    };
    struct interactionInOPSpace {
      BHdouble WalkerWidth_{1.0};
      BHwalkerRepulsionZone RepulsionZone_;
    };

    BHwalkerAlgorithmType ChosenAlgorithm_{BHwalkerAlgorithmType::stnd};

    /// Repulsion energy (in eV) between the other walkers
    BHdouble WalkerRepulsion_{0.7};
    std::vector<interactionInOPSpace> OPspaceSettings_{{0.01, {}}, {1.0, {}}};
    /*std::vector<BHdouble> WalkerWidth_{0.01, 1.0};
    std::vector<BHwalkerRepulsionZone> RepulsionZone_{BHSettings::NumberOfOP};*/
    std::vector<unsigned> interactWith_{};
    std::string seedFname_{"seed.in"};
    std::vector<MoveNameAndString> MovesStrings_;
    // legacy options-unused
    BHdouble VariableTempHighTemp_{100.};
    BHdouble VariableTempDeltaTemp_{0.0};
    BHdouble VariableTempPeriodTemp_{0.0};
    BHdouble RestartEnergy_{-100.0};
    bool OPT_VariableTemp_{false};
    bool OPT_Restart_{false};
    // end of legacy options
    bool seeded_{false};
    BHWalkerSettings ();
    //    BHWalkerSettings (const BHwalkerAlgorithmType);
    BHWalkerSettings (const BHWalkerSettings &);
    BHWalkerSettings (const BHWalkerSettings &&) noexcept;
    BHWalkerSettings &operator= (BHWalkerSettings);
    void swap (BHWalkerSettings &);
  };

  bool isStandaloneAlgorithm (BHwalkerAlgorithmType algorithm);
  std::vector<std::unique_ptr<BHMove>>
    createListOfMoves (std::vector<BHWalkerSettings::MoveNameAndString>);
  std::vector<std::unique_ptr<BHMove>> createListOfSpecializedMoves (
    std::vector<BHWalkerSettings::MoveNameAndString>,
    const BHCluster &,
    const BHMetalParameters &);
  std::string printInputFileFromWalkerSettings (const BHWalkerSettings &ws);
  void printWalkerSettings (
    const BHWalkerSettings &ws, std::ostream &stream = std::cout);
  BHClusterAtoms getSeedFromSettings (const BHWalkerSettings &, rndEngine &rng);
  BHWalkerSettings createSettingsWithAllMoves (
    const BHwalkerAlgorithmType &, std::string fname = "");

  BHWalkerSettings loadBHWalkerSettings (std::string, bool = true);
  BHWalkerSettings loadBHWalkerSettingsNoPrintFile (std::string, bool = true);
  BHWalkerSettings loadBHWalkerSettings (std::istream &, bool = true);

  ///@brief This support class loads the BHWalkerSettings from a stream
  ///
  /// It is intended to be called by the loadBHWalkersSettings functions
  class BHWalkerSettingsLoader final : public BHWalkerSettings {
  public:
    friend BHWalkerSettings createSettingsWithAllMoves (
      const BHwalkerAlgorithmType &, std::string fname);
    friend BHWalkerSettings loadBHWalkerSettings (std::istream &, bool);

  private:
    BHWalkerSettingsLoader (std::istream &, bool = true);
    void loadFromFile (std::istream &);
    void parser_VariableTemp (const std::string &parsed);
    void parser_moves (const std::string &parsed);
    void parser_algorithm (const std::string &parsed);
  };

  namespace BHParsers {
    namespace BHWS {
      enum class TVARIABLE {
        VariableTemp,
        VariableTempHighTemp,
        VariableTempDeltaTemp,
        VariableTempPeriodTemp
      };

      static const std::array<const std::string, 4> CardTVARIABLE = {
        "OPT_VariableTemp",      // bool
        "VariableTempHighTemp",  // double
        "VariableTempDeltaTemp", // double
        "VariableTempPeriodTemp" // double
      };
      enum class ALGORITHM {
        algorithm,
        seed,
        repwalkers,
        WalkerWidth,
        widthwalker,
        restart,
        restartEnergy,
        lowRepulsion,
        highRepulsion,
        interactWith
      };
      static const std::array<const std::string, 10> CardALGORITHM = {
        "algorithm",     // string //0
        "seed",          // string //1
        "repwalkers",    // double //2
        "WalkerWidth",   // double, with obj //3
        "widthwalker",   // double, with obj //4
        "restart",       // bool //5
        "restartEnergy", // double //6
        "lowRepulsion",  // double, with obj //7
        "highRepulsion", // double, with obj //8
        "interactWith"   // unsigned, series of //9
      };

      static const std::array<const std::string, 2> PARAMETER = {"param", "p"};
    } // namespace BHWS
    static const std::array<const std::string, 4>
      _BHWSParser_DefaultStringsForCardTVARIABLE = {
        "false", //"OPT_VariableTemp_",//bool
        "350",   //"VariableTemp_HighTemp",//double
        "50",    //"VariableTemp_DeltaTemp",//double
        "100"    //"VariableTemp_PeriodTemp",//double
    };
  } // namespace BHParsers
} // namespace BH
#endif // BHWALKERSETTINGS_H
