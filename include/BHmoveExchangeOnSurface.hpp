/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
  @brief Declaration of move Exchange On Surface
   @file BHmoveExchangeOnSurface.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 12/4/2019
   @version 0.1
   creating the move
  */
#ifdef MOVE_PARSER
MoveParser (exchangeOnSurface, BHMoveExchangeOnSurface)
#else
#ifndef BHMOVEEXCHANGEONSURFACE_H
#define BHMOVEEXCHANGEONSURFACE_H
#include "BHmoveExchange.hpp"

namespace BH {
  /// \brief the exchange on surface move exchange tho random atoms on the
  /// surface
  class BHMoveExchangeOnSurface : public BHMoveExchange {
  public:
    BHMoveExchangeOnSurface ();
    ~BHMoveExchangeOnSurface () override;
    static std::string DefaultString ();
    std::unique_ptr<BHMove> clone () override;

  protected:
    bool selectAtoms (
      BHCluster &out,
      BHClusterAnalyser &,
      const BHMetalParameters &,
      rndEngine &) override;
    std::string printSettingsSpecialized () const override;
    std::string printSettingsSpecializedForInput () const override;
    bool parseSpecialized (const std::string &) override;
    unsigned int fewNeighbours_{MinimumNeighbourForFCCBulk};
  };

  namespace BHParsers {
    namespace BHMV {
      enum class EXCHANGEONSURFACEvariable { fewNeighbours_ };
      const std::array<std::string, 1> EXCHANGEONSURFACE = {
        "fewneighbours" // int
      };
    } // namespace BHMV
  }   // namespace BHParsers
} // namespace BH
#endif // BHMOVEEXCHANGEONSURFACE_H
#endif
