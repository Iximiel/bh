/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declarationn of class BHAtom, wraps BHvector with a std::string
   @file BHatom.hpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 21/3/2017
   @version 1.0
*/
#ifndef BHATOM_H
#define BHATOM_H
#include <iosfwd> //foward declaration so that iostream is included only if needed, needed for IO operators

#include "BHvector.hpp"
#include <string>
namespace BH {
  /// \brief Works like a vector, but stores the kind of atom, has some utiliies
  /// for the exchanges
  class BHAtom : public BHVector {
  public:
    /// Declares an empty atom
    BHAtom ();
    BHAtom (const BHAtom &);
    BHAtom (BHAtom &&) noexcept;
    /// Copy from another BHAtom
    BHAtom &operator= (const BHAtom &);
    BHAtom &operator= (BHAtom &&) noexcept;
    ~BHAtom () override;
    /// Create an atom in a given position
    BHAtom (std::string, const BHVector &);
    /// Create an atom in a given position
    BHAtom (std::string, const BHdouble, const BHdouble, const BHdouble);
    // operators and menageries thinked for the algorithm
    /// Copy the coordinates from another BHAtom.
    /**If the atoms are not of the same type trows and exception
     *Should be used as operator= in the walker to assign the temporary
     *cluster to the main one
     */
    BHAtom &operator<< (const BHAtom &);
    /// Get new coordinates from a BHVector
    BHAtom &operator= (const BHVector &);
    /// Get new coordinates from a temporary BHVector
    BHAtom &operator= (BHVector &&) noexcept;
    /// Increment the coordinates by a BHVector
    BHAtom &operator+= (const BHVector &);
    /// Decrement the coordinates by BHVector
    BHAtom &operator-= (const BHVector &);
    /// I need this operator for sorting atoms alphabetically
    /**returns "myType<other.myType"*/
    bool operator< (const BHAtom &) const;

    std::string Type () const;
    /// Swap position with another atom, but not the types
    /**If the two atoms are of the same type doesn't do anything and throw an
     * exeption*/
    void positionSwap (BHAtom &);

    void setType (std::string);

  private:
    /// The type of the atom
    /**It is intended to be for example:"Ag", "Cu", etc...*/
    std::string myType;
  };

  BHAtom operator+ (const BH::BHAtom &, const BH::BHVector &);
  BHAtom operator- (const BH::BHAtom &, const BH::BHVector &);
  // IO
  std::istream &operator>> (std::istream &, BHAtom &);
  std::ostream &operator<< (std::ostream &, const BHAtom &);
} // namespace BH

#endif // BHATOM_H
