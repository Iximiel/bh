/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of BHOrderParameter

   @file BHorderParameter.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 19/11/2017
   @version 0.1

   moved from BHsettings in order to separate the dependecies
*/
#ifndef BHORDERPARAMETER_H
#define BHORDERPARAMETER_H
#include <string>

namespace BH {
  /// Stores the type of order parameter in order to use it in the calculations
  struct BHOrderParameter {
    /// Defines the kind of the Order Parameter
    enum class BHOrderParameter_type {
      bonds,           ///< Percentile of bonds specified by option_
      surfaceBonds,    ///< Percentile of bonds  on surface specified by option_
      surfaceAtoms,    ///< Percentile of the surface occupied by atoms of type
                       ///< option_
      signature,       ///< CNA Signature
      aggregation,     ///< Aggregation analysis
      bondOrientation, ///< Bond Orientation analysis
      unset
    };
    enum BHOrderParameterMathOperator {
      /// no mathematical operation in postproduction
      none = 0,
      /// after caclulation extract and store the sqrt root as the OP
      doSqrt = 1 << 0,
      ///  after caclulation store the logarithm as the OP
      doLog = 1 << 1
    };

    BHOrderParameter_type OPtype_{
      BHOrderParameter_type::unset}; ///< Indicates the kind of the Order
                                     ///< Parameter
    BHOrderParameterMathOperator MathOperation_{
      BHOrderParameterMathOperator::none};
    /// Contains the informations that are required for specifying the type of
    /// OP from OPtype_
    /**option_ can be
     *
     *- 3 number like `555`, `442`, `444` etc.. to specify what kind of
     *signature use
     *- a small string like `Au`, `AgCu`, `AgAg` or `mix` to specify what kind
     *of bond or 'surfaceAtoms' use
     */
    std::string option_{};
    BHOrderParameter ();                              ///< Empty initializer
    BHOrderParameter (const BHOrderParameter &other); ///< Copy constructor
    /// Calls #initialize to initialize the option_ and OPtype_
    BHOrderParameter (const char input[]);
    /// Calls #initialize to initialize the option_ and OPtype_
    BHOrderParameter (const std::string input);
    /// Initializes option_ and OPtype_ with a simple parser on the input string
    void initialize (std::string input);
    bool operator== (const BHOrderParameter &) const;
  };
  /// convert int to BHOrderParameter, here for legacy motifs
  BHOrderParameter OPfromInt (int id);
  /// given a BHOrderParameter returns the name
  std::string OparName (const BHOrderParameter &OP);
} // namespace BH

#endif // BHORDERPARAMETER_H
