/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
// sugar for creating fast lambda for move constructor
#ifndef BHCONSTRUCTORS_HPP
#define BHCONSTRUCTORS_HPP
#include "BHminimizationAlgorithm.hpp"
#include "BHmove.hpp"
#include "BHwalker.hpp"

namespace BH {

  std::unique_ptr<BHMove>
  createMoveFromString (const std::string moveName, const std::string options);

  class BHConstructors {
    // the idea here is to have the list of moves ready to work and having the
    // possibility to add new moves without the need of recompiling the main
    // library
    using moveCreator = std::function<std::unique_ptr<BHMove> (
      const std::string &options)>;

  private:
    BHConstructors ();
    std::unordered_map<std::string, moveCreator> moveCreators_{};

  public:
    static BHConstructors &getConstructor ();
    std::unique_ptr<BHMove> createMoveFromString (
      const std::string moveName, const std::string options);
    BHMinimizationAlgorithm *getMinimizationAlgorithm (const BHSettings &);
    void addMoveToList (std::string moveName, moveCreator creatorFunction);
    /// This factory method creates the various kind of walkers
    BHWalker createWalker (
      BHWalkerSettings, const BHMetalParameters &bhmp, rndEngine &rng) const;
    /// This function will create alwais a standard walker regardless the
    /// algorithm selected in the inputfile
    BHWalker createStandardWalker (
      BHWalkerSettings, const BHMetalParameters &bhmp, rndEngine &rng) const;
    BHWalker createWalker (
      const BHWalkerSettings &,
      const BHMetalParameters &bhmp,
      BHClusterAtoms) const;
    /// This function will create alwais a standard walker regardless the
    /// algorithm selected in the inputfile
    BHWalker createStandardWalker (
      BHWalkerSettings, const BHMetalParameters &bhmp, BHClusterAtoms) const;
    BHConstructors (const BHConstructors &) = delete;
    BHConstructors (BHConstructors &&) = delete;
    BHConstructors &operator= (const BHConstructors &) = delete;
    BHConstructors &operator= (BHConstructors &&) = delete;
  };

  /// returns a function that creates and initialize a move
  template <typename MoveClass>
  auto createMoveCreator () {
    return [] (const std::string &options) -> std::unique_ptr<BHMove> {
      std::unique_ptr<BHMove> t{
        static_cast<BHMove *> (new MoveClass ())};
      t->parse (options);
      return t;
    };
  }

  /// It is a shortcut for calling BHConstructors::addMoveToList without
  /// specifying the functionfor initialize the move
  template <typename MoveClass>
  void addMoveToConstructorList (std::string moveName) {
    BHConstructors::getConstructor ().addMoveToList (
      moveName, createMoveCreator<MoveClass> ());
  }

} // namespace BH
#endif // BHCONSTRUCTORS_HPP
