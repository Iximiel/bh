/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of BHHisto

   @file BHhisto.hpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 1/4/2019
   @version 0.1

 */
#ifndef BHHISTO_HPP
#define BHHISTO_HPP

#include "BHmyMath.hpp"
#include <iosfwd>

namespace BH {
  class BHHisto {
  public:
    /// A struct that contains the information for the histograms
    class BHHistoPars {
      friend class BHHisto;

    public:
      BHHistoPars ();
      /// computes the index of the histogram
      inline unsigned long addr (BHdouble position) {
        return (position <= start)
                 ? 0
                 : ((position >= end)
                      ? nbins - 1
                      : static_cast<unsigned long> ((position - start) / step));
      }
      BHdouble getStart () const;
      void setStart (const BHdouble &value);
      BHdouble getEnd () const;
      void setEnd (const BHdouble &value);
      BHdouble getWidth () const;
      BHdouble getStep () const;
      unsigned long getNbins () const;
      void setNbins (unsigned long value);

    private:
      BHdouble start{0.0}, ///< Starting value of the first bin of the histogram
        end{1.0},          ///< Max parameter for histo
        width{1.0},        /// difference between #histo_start and #histo_end
        step{1.0};         ///< width of bars
      unsigned long nbins{1}; ///< number of bars
      void calcPars ();       ///< computes #histo_end and histo_width
    };
    /// this constructor does not initialize the histogram, if needed the user
    /// must call initializeHisto()
    BHHisto (
      const BHHistoPars &pars1,
      const BHHistoPars &pars2,
      const unsigned long &hrenew);
    BHHisto (const BHHisto &) = delete;
    BHHisto (BHHisto &&) = delete;
    BHHisto &operator= (const BHHisto &) = delete;
    BHHisto &operator= (BHHisto &&) = delete;
    ~BHHisto ();
    /// Gives the content of the bin with the selected coords
    inline int HistoBIN (const BHdouble &OP1, const BHdouble &OP2) {
      return histo_
        [histoParameters_[0].addr (OP1) * histoParameters_[1].nbins +
         histoParameters_[1].addr (OP2)];
    }
    /// Gives the content of the bin with the selected coords divided by
    /// #histoData_
    inline BHdouble NormHistoBIN (const BHdouble &OP1, const BHdouble &OP2) {
      return HistoBIN (OP1, OP2) / double (histoData_);
    }
    /// Gives the weight for the histo algorithm
    inline BHdouble getCalculatedHistoWeight (
      const BHdouble &OP1,
      const BHdouble &OP2,
      const BHdouble &old_OP1,
      const BHdouble &old_OP2) {
      return (HistoBIN (OP1, OP2) - HistoBIN (old_OP1, old_OP2)) /
             double (histoData_);
    }
    /// initialize histo if not initializated yet (acts only if histo_is a
    /// nullptr)
    void initializeHisto ();
    /// increment the content of the bin with the selected coords by 1 and
    /// #histoData_ by 1, if #histoData_==#renew_histo
    void IncrementBin (const BHdouble &OP1, const BHdouble &OP2);
    /// Plot the histogram in a file
    void PlotHistoMatrix (std::ostream &stream) const;
    void PlotHistoCoordinates (std::ostream &stream) const;
    bool HistoIsReady () const;

  protected:
    BHHistoPars histoParameters_[2];
    /// setting on when restart the histogram
    unsigned long histoRenew_;
    /// counts how many data is recorderd in the histogram
    unsigned long histoData_{0};
    /// the histogram is a vector of dimension:
    /// histoPars[0].histo_nbars*histoPars[1].histo_nbars, taking
    /// histoPars[0].histo_nbars as the row lenght
    int *histo_{nullptr};
  };
} // namespace BH
#endif // BHHISTO_HPP
