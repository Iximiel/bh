/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief The template function with the main minimization routine
   @file BHainTemplate.hpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 11/03/2020
*/
#ifndef BHMAINTEMPLATE_HPP
#define BHMAINTEMPLATE_HPP
#include "BHminimizationAlgorithms.hpp"
#include "BHsettings.hpp"
#include "BHversion.hpp"
#include <csignal>
#include <iostream>

namespace BH {
  template <typename BHEngine>
  int BHexecutor (int argc, char **argv) {
    std::string inputFile = "input_bh++.in";
    for (int i = 1; i < argc; i++) {
      if (std::string (argv[i]) == "-in") {
        if (i + 1 < argc) {
          ++i;
          inputFile = argv[i];
          break;
        }
      }
    }
    std::cout << BHEngine::staticLogo () << std::endl;

    std::cout << "Libraries: " << BHPP_LIBRARIES_MAJOR << "."
              << BHPP_LIBRARIES_MINOR << "." << BHPP_LIBRARIES_PATCHLEVEL
              << std::endl;
    std::cout << "Version " << BHPP_MAIN_MAJOR << "." << BHPP_MAIN_MINOR
              << std::endl;

    BHEngine::run = true;
    BHSettings settings =
      BHSettingsUtilities::loadBHSettings (inputFile.c_str ());
    // make a factory here
    BHEngine doer (settings);
    signal (SIGINT, [] (int) {
      std::cerr << "Caugh an INTERRUPT, finishing operations and closing"
                << std::endl;
      BHEngine::interruptRun (0);
    });
    doer.Processing ();
    return 0;
  }
} // namespace BH
#endif // BHMAINTEMPLATE_HPP
