/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
  @brief Declaration of move Exchange Separate
   @file BHmoveExchangeSeparate.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 15/4/2019
   @version 0.1
   creating the move
  */
#ifdef MOVE_PARSER
MoveParser (exchangeSeparate, BHMoveExchangeSeparate)
#else
#ifndef BHMOVEEXCHANGESEPARATE_H
#define BHMOVEEXCHANGESEPARATE_H
#include "BHmoveExchange.hpp"

namespace BH {
  /**
   * @brief The Move Exchange Separate aims at segragate atoms of different
   * kinds
   *
   * This move weights atoms to chose in order to make exchanges that favor
   * separation between different species:
   * - one for atoms that should stay inside of the cluster. The more neighbours
   * they have the lighter is their weight in the extraction list
   * - the second for atoms that we desire to be on the ouside of the cluster.
   * Then it updates #chosenAtom1_ and #chosenAtom2_ with random atoms each
   * selected from the two lists. The weight are calculated with
   * \f$(n_{NdiffNeigh}+1)^{esp}\f$, where _esp_ is #exchSep.exp, or in the case
   * of the first list when an atom is on the surface:
   * \f$(n_{NdiffNeigh}+1+n_{NNlimSup}-n_{NNeigh})^{esp}\f$ where
   * \f$n_{NNlimSup}\f$ is the number of neigbour minimum for an atom to be
   * considered in the bulk If #small_inside is _true_ the first list will be
   * composed of the light atoms.
   */
  class BHMoveExchangeSeparate : public BHMoveExchange {
  public:
    BHMoveExchangeSeparate ();
    ~BHMoveExchangeSeparate () override;
    static std::string DefaultString ();
    std::unique_ptr<BHMove> clone () override;

  protected:
    bool selectAtoms (
      BHCluster &out,
      BHClusterAnalyser &,
      const BHMetalParameters &,
      rndEngine &) override;
    std::string printSettingsSpecialized () const override;
    std::string printSettingsSpecializedForInput () const override;
    bool parseSpecialized (const std::string &) override;
    unsigned int fewNeighbours_{MinimumNeighbourForFCCBulk};
    BHdouble exponentialWeight_{2.0};
    bool smallInside_{true};
  };

  namespace BHParsers {
    namespace BHMV {
      enum class EXCHANGESEPARATEvariable {
        fewNeighbours_,
        exponentialWeight_,
        smallInside_
      };

      const std::array<std::string, 3> EXCHANGESEPARATE = {
        "fewneighbours", // int
        "weight",        // int
        "smallinside"    // int
      };
    } // namespace BHMV
  }   // namespace BHParsers
} // namespace BH
#endif // BHMOVEEXCHANGESEPARATE_H
#endif
