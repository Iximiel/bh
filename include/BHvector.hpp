/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declarationn of class BHVector with some 3D vector algebra
   @file BHvector.hpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 13/9/2017
   @version 1.1

   @date 28/03/2019
   @version 1.1.1

   some changes for documentation
*/
#ifndef BHVECTOR_H
#define BHVECTOR_H
#include "BHmyMath.hpp"
#include "BHrandom.hpp"
#include <iosfwd> //foward declaration so that iostream is included only if needed, needed for IO operators
namespace BH {
  class BHVector;
  BHVector operator* (const BHdouble, const BHVector &);
  /// Base class for forces and position of atoms
  class BHVector { // rule of 5: 3/5
    enum { x = 0, y = 1, z = 2 };

  public:
    /// Empty vector
    BHVector ();
    /// Constuctor with coordinates
    BHVector (const BHdouble X, const BHdouble Y, const BHdouble Z);
    BHVector (const BHVector &);
    BHVector (BHVector &&) noexcept;
    virtual ~BHVector ();
    // accessing members
    /// Returns the X component value
    BHdouble X () const;
    /// Returns the Y component value
    BHdouble Y () const;
    /// Returns the Z component value
    BHdouble Z () const;
    /// Returns the modulus
    BHdouble R () const;
    /// Returns the spherical theta coordinate
    BHdouble Theta () const;
    /// Returns the spherical phi coordinate
    BHdouble Phi () const;
    // setting members
    /// Sets the X component
    void X (BHdouble newX);
    /// Sets the Y component
    void Y (BHdouble newY);
    /// Sets the Z component
    void Z (BHdouble newZ);
    /// Set the X, Y and Z components
    void setXYZ (const BHdouble newX, const BHdouble newY, const BHdouble newZ);
    /// Set the X, Y and Z components from a three dimensional vector
    void setXYZ (const BHdouble *newXYZ);
    /// Copy the X, Y and Z components in to a 3 dim vector
    void copyXYZto (BHdouble *newXYZ);
    /// set X=Y=Z=0
    void zero ();
    /// Set the possibility to access/set members
    BHdouble &operator[] (const int);

    void myswap (BHVector &);
    void swap (BHVector &);

    // some basic "internal" operators
    BHVector &operator= (const BHVector &);
    BHVector &operator= (BHVector &&) noexcept;
    BHVector &operator+= (const BHVector &);
    BHVector &operator-= (const BHVector &);
    // BHVector& operator= (BHVector&&) noexcept;//move things need to be
    // understood
    /// Sum betwen vectors
    BHVector operator+ (const BHVector &) const;
    /// Inverse
    BHVector operator- () const;
    /// Subtraction betwen vectors
    BHVector operator- (const BHVector &) const;
    /// Cross product
    BHVector operator% (const BHVector &) const; // using modulo as vector
                                                 // product
    /// Scalar product
    BHdouble operator* (const BHVector &) const;
    /// Multiplication by a scalar
    BHVector operator* (const BHdouble) const;
    /// Division by a scalar
    BHVector operator/ (const BHdouble) const;
    BHVector &operator*= (const BHdouble);
    BHVector &operator/= (const BHdouble);
    /// distance from another vector
    BHdouble dist (const BHVector &) const;
    /// distance from another vector,squared
    BHdouble dist2 (const BHVector &) const;
    /// rotate around Z axis
    BHVector RotateAroundZ (const BHdouble);
    /// rotate around Y axis
    BHVector RotateAroundY (const BHdouble);
    /// rotate around X axis
    BHVector RotateAroundX (const BHdouble);
    /// create a new vector using spherical coords
    static BHVector RhoThetaPhi (BHdouble rho, BHdouble theta, BHdouble phi);
    /// create a random of lenght between rmin and rMAX
    static BHVector
    RandomVector (rndEngine &rng, BHdouble rMAX, BHdouble rmin = 0.0);
    /// outputs the versor of the distance betwenn from and to
    static BHVector
    directionVersorFromTo (const BHVector &from, const BHVector &to);
    /// outputs the versor of a given vector
    static BHVector directionVersor (const BHVector &);
    friend BHVector operator* (const BHdouble, const BHVector &);

  private:
    BHdouble coord_[3];
  };
} // namespace BH
// external operators&functions
///\fnMultiplication scalar by a vector
// BH::BHVector operator* (const BH::BHdouble , const BH::BHVector &);
///\fnModule of a vector
BH::BHdouble abs (const BH::BHVector &);
// IO operators
std::istream &operator>> (std::istream &, BH::BHVector &);
std::ostream &operator<< (std::ostream &, const BH::BHVector &);

#endif // BHVECTOR_H
