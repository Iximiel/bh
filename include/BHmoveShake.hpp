/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
 */
#ifdef MOVE_PARSER
MoveParser (shake, BHMoveShake)
#else
#ifndef BHMOVESHAKE_H
#define BHMOVESHAKE_H
#include "BHmove.hpp"
namespace BH {
  /**
   * @brief The Move Shake displaces all atoms by a random amount
   *
   *This move consists in displacing every atom of the cluster
   *within a spherical shell centered in its initial position
   *whose minimum and maximum radii are written in the [input file](@ref
   *parserBHWalkerSettings)
   */
  class BHMoveShake : public BHMove {
  public:
    BHMoveShake ();
    ~BHMoveShake () override;
    static std::string DefaultString ();
    std::unique_ptr<BHMove> clone () override;

  protected:
    bool doMoveAlgorithm (
      BHCluster &in,
      BHCluster &out,
      BHClusterAnalyser &,
      const BHMetalParameters &,
      rndEngine &) override;
    std::string printSettingsSpecialized () const override;
    std::string printSettingsSpecializedForInput () const override;
    bool parseSpecialized (const std::string &) override;
    bool parseAfter () override;
    BHdouble Rmin_{0.0}, Rmax_{0.0};
    BHdouble Rmin3_;
    BHRealRND rndRho_{0.0, 0.0};
    BHRealRND rndTheta_{-1., 1.}; //(goes in 1-2*rnd(0,1)
    BHRealRND rndPhi_{0, 2 * M_PI};
  };

  namespace BHParsers {
    namespace BHMV {
      enum class SHAKEvariable { Rmin_, Rmax_ };

      const std::array<std::string, 2> SHAKE = {
        "rmin", // double //0
        "rmax"  // double //1
      };
    } // namespace BHMV
  }   // namespace BHParsers
} // namespace BH
#endif // BHMOVESHAKE_H
#endif // MOVE_PARSER
