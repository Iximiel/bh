/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#ifndef BHBASINHOPPINGFLYINGLANDINGWBESTWALKER_HPP
#define BHBASINHOPPINGFLYINGLANDINGWBESTWALKER_HPP
#include "BHbasinHoppingFlyingLanding.hpp"
namespace BH {
  // this class sets up a bhFLH_1 simulation
  class BHBasinHoppingFlyingLandingWBestWalker : public BHAbstractBasinHopping {
  public:
    const char *logo () override;
    static const char *staticLogo ();
    BHBasinHoppingFlyingLandingWBestWalker (const BHSettings &settings);
    BHBasinHoppingFlyingLandingWBestWalker (
      BHBasinHoppingFlyingLandingWBestWalker &) = delete;
    BHBasinHoppingFlyingLandingWBestWalker (
      BHBasinHoppingFlyingLandingWBestWalker &&) = delete;
    BHBasinHoppingFlyingLandingWBestWalker &
    operator= (BHBasinHoppingFlyingLandingWBestWalker &) = delete;
    BHBasinHoppingFlyingLandingWBestWalker &
    operator= (BHBasinHoppingFlyingLandingWBestWalker &&) = delete;
    ~BHBasinHoppingFlyingLandingWBestWalker () override;
    const std::string BHStyleName () override;
  };
} // namespace BH
#endif // BHBASINHOPPINGFLYINGLANDINGWBESTWALKER_HPP
