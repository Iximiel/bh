/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief This file contains all the include to the walkers in the project
   @file BHwalkerAlgorithmss.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 29/3/2019
   @version 1.0

   Created
*/

#include "BHwalkerAlgorithmHistogram.hpp"
#include "BHwalkerAlgorithmObstacle.hpp"
#include "BHwalkerAlgorithmParallelExcited.hpp"
#include "BHwalkerAlgorithmRegionExcited.hpp"
#include "BHwalkerAlgorithmSelectiveParallelExcited.hpp"
#include "BHwalkerAlgorithmStandard.hpp"
