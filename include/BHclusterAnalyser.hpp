/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of the baseclass BHClusterAnalyser

   @file BHclusterAnalyser.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 25/6/2018
   @version 0.8
   Adding couple return to the CNA

   @date 6/3/2018
   @version 0.7

   Removed Island management from the Analysis class as a more standalone option

   @date 6/12/2017
   @version 0.6

   Moving the all transformation and the analysis possibilities (except for the
   moves) to this class

   @date 31/5/2017
   @version 0.5
*/
#ifndef BHCLUSTERANALYSIS_H
#define BHCLUSTERANALYSIS_H
#include <functional>
#include <iosfwd>
#include <unordered_map> //is faster than map for access by key
#include <vector>

#include "BHcluster.hpp"
#include "BHcouple.hpp"
#include "BHmetalParameters.hpp"
#include "BHorderParameter.hpp"
#include "BHtiming.hpp"

namespace BH {
  ///@todo move using statement for BHMetalNameToLabels and BHMetalLabelToNames
  /// to a separate header
  using BHMetalNameToLabels = std::unordered_map<std::string, unsigned>;
  using BHMetalLabelToNames = std::unordered_map<unsigned, std::string>;

  ///\brief This class is used to analise a single cluster
  ///
  /// This is a service class: a std::vector of BHOrderParameter is used to
  /// initilize the internal parameters, then each time operator() or
  /// ParameterAnalysis() is called on a BHCluster the class will return a
  /// std::vector of BHdouble with the values of the parameters analysed.

  class BHClusterAnalyser final {
  public:
    BHClusterAnalyser (
      const std::vector<BHOrderParameter> &opData,
      const BH::BHMetalParameters &bhmp);
    BHClusterAnalyser (const BHClusterAnalyser &) = delete;
    BHClusterAnalyser (BHClusterAnalyser &&) = delete;
    BHClusterAnalyser &operator= (const BHClusterAnalyser &) = delete;
    BHClusterAnalyser &operator= (BHClusterAnalyser &&) = delete;
    ~BHClusterAnalyser ();

    std::tuple<unsigned, unsigned, std::vector<unsigned>>
    ComputeNNlists (BHCluster &cluster, const BHMetalParameters &bhmp) const;
    /// calculates and returns the values of the order parameters
    std::vector<BHdouble>
    ParameterAnalysis (BHCluster &cluster, const BHMetalParameters &bhmp) const;
    /// calculates and returns the values of the order parameters
    std::vector<BHdouble>
    operator() (BHCluster &cluster, const BHMetalParameters &bhmp) const;
    /// returns the names of the parameters in the calculation order
    std::vector<BHOrderParameter> ParameterNames () const;
    void calcNeighbourhood (BHCluster &, const BHMetalParameters &bhmp)
      const; ///@todo Make static or put in BHClusterUtilities
    void calcNeighbourhood_tol (BHCluster &, const BHMetalParameters &bhmp)
      const; ///@todo Make static or put in BHClusterUtilities
    unsigned Npars () const;

  private:
    /// Calculates the order Parameters for "islands" analysis
    std::vector<BHdouble> IsleParameters (
      const BHCluster &cluster,
      const BHMetalParameters &bhmp,
      bool plotAllOnScreen = false);
    std::tuple<unsigned, std::vector<unsigned>, unsigned, std::vector<unsigned>>
    SurfaceAnalysis (
      const BHCluster &cluster, const BHMetalParameters &bhmp) const;

    ///@todo better names for DataContainer and BHOPHandler

    /// Contains the data for the BHClusterAnalyser calculations
    struct DataContainer {
      // calculation efforts
      // BHMetalNameToLabels PairLabels_; ///< Pair labels
      std::vector<BHdouble> cutOff_end2_{}, NNdist_tol2_{};
      std::vector<BHdouble> AggregationParameters_{};
      BHdouble Q6_{0.0}, Q4_{0.0};
      std::unordered_map<short, int> signatures_;
      unsigned NofCouples_{0};
      unsigned Nbounds_{0};
      unsigned MixedCounter_{0};
      std::vector<unsigned> BondsCounter_{};

      unsigned NSurf_{0};
      unsigned SurfaceMixedCounter_{0};
      std::vector<unsigned> SurfaceBondsCounter_{};
      std::vector<unsigned> AtomsOnSurf_{};
      bool NeedCNA_{true};
      bool NeedSURF_{true};
      bool NeedIsleParameters_{true};
      bool NeedQ4_{true};
      bool NeedQ6_{true};
      bool needSurfaceAnalysis_{true};
    };
    struct BHOPHandler {
      std::function<BHdouble (
        const BHCluster &cluster,
        const BHMetalParameters &bhmp,
        DataContainer &)>
        Action;
      std::function<BHdouble (const BHdouble &)> PostProduction;
    };
    const std::vector<BHOrderParameter> opData_;
    const std::vector<BHOPHandler> OPcalculators_;
    // mutable because I am a bad person :P
    // these mutable variablesare used only for calculating the performance of
    // the program
    mutable ms analysis_time{std::chrono::steady_clock::duration::zero ()};
    mutable int n_analysis{0};

    std::vector<BHdouble> cutOff_end2_;
    std::vector<BHdouble> NNdist_tol2_;

    void conditionalSurfaceAnalysis (
      DataContainer &data,
      const BHCluster &cluster,
      const BHMetalParameters &bhmp) const;

  public:
    void times (std::ostream &stream) const;
  };
} // namespace BH
#endif // BHCLUSTERANALYSIS_H
