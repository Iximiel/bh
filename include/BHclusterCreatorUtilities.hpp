/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief The declarations of the functions to create clusters
   @file BHclusterCreatorUtilities.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 9/1/2019
   @version 0.1

   separated from main

*/

#ifndef BHCLUSTERCREATORUTILITIES_H
#define BHCLUSTERCREATORUTILITIES_H

#include <vector>

#include "BHatom.hpp"

namespace BH {
  // octahedron
  namespace TruncatedOctahedron {
    unsigned impiledSquareAtoms (unsigned edgeBase);

    unsigned NatOctahedron (const unsigned edgeLenght);
    unsigned NatOctahedronSurf (const unsigned edgeLenght);
    unsigned NatTruncOct (const unsigned edgeLenght, const unsigned edgeCut);
    unsigned
    NatTruncOctSurf (const unsigned edgeLenght, const unsigned edgeCut);
    unsigned NofAtoms (const unsigned edgeLenght, const unsigned edgeCut);
    std::vector<BHAtom> truncatedOctahedron (
      const unsigned edgeLenght,
      const unsigned edgeCut,
      const char *atomsType,
      const double AtomRadius);
  } // namespace TruncatedOctahedron
  // icosahedron
  namespace Icosahedron {
    unsigned NatIcosahedron (const unsigned numberOfShells);
    unsigned NofAtoms (const unsigned numberOfShells);
    BH::BHVector createEdge (
      const unsigned I,
      const unsigned J,
      const unsigned L,
      const unsigned shell,
      BHVector *tvec);
    std::vector<BHAtom> icosahedron (
      const unsigned numberOfShells,
      const char *atomsType,
      const double AtomRadius);
  } // namespace Icosahedron
  // decahedron
  namespace MarksDecahedron {
    BH::BHVector shellForDeca (const unsigned thisShell, const unsigned column);
    int NatDecahedron (const int m, const int n, const int p);
    int NofAtoms (const int m, const int n, const int p);
    int getCutEdge_mp (const int m, const int p);
    int getCutEdge_sn (const int numberOfShells, const int n);
    int getNumberOfShells (const int cutEdge, const int n);
    int getM (const int cutEdge, const int p);
    std::vector<BHAtom> decahedron (
      const int m,
      const int n,
      const int p,
      const char *atomsType,
      const double AtomRadius,
      const double dilatation = 0.83);
  } // namespace MarksDecahedron
} // namespace BH
#endif // BHCLUSTERCREATORUTILITIES_H
