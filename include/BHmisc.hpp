/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Some utilities different

Here you can find function that where previously old main file used for testing
purposes

   @file BHmisc.hpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 17/01/2020
*/
#ifndef BHMISC_HPP
#define BHMISC_HPP
#include <iosfwd>

namespace BH {
  int printMemory (std::ostream &stream);
  int plotBHSettingsInputFile (std::ostream &stream, int, char **);
  int plotBHWalkerSettingsInputFile (std::ostream &, int, char **);
  int centerCluster (std::ostream &stream, int, char **);
  int energyCluster (std::ostream &stream, int, char **);
} // namespace BH
#endif // BHMISC_HPP
