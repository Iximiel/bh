/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of abstact class BHMinimizationAlgorithm

   This abstract lass acts as a wrapper for the minimization algorithms.

   @file BHminimizationAlgorithm .hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 12/11/2019
   @version 1.0

   base class is complete
*/
#ifndef BHMINIMIZATIONALGORITHM_HPP
#define BHMINIMIZATIONALGORITHM_HPP
#include "BHcluster.hpp"
#include "BHenergyCalculator.hpp"
#include "BHmyMath.hpp"

namespace BH {
  class BHMinimizationAlgorithm {
  public:
    BHMinimizationAlgorithm ();
    virtual ~BHMinimizationAlgorithm ();
    virtual BHdouble
    Minimization (BHdouble *x, const unsigned &nat, BHEnergyCalculator *) = 0;
    virtual BHdouble Minimization_tol (
      BHdouble *x, const unsigned &nat, BHEnergyCalculator *) = 0;
    virtual BHdouble MinimizeCluster (BHClusterAtoms &, BHEnergyCalculator *);
    virtual BHdouble
    MinimizeCluster_tol (BHClusterAtoms &, BHEnergyCalculator *);
  };
} // namespace BH
#endif // BHMINIMIZATIONALGORITHM_HPP
