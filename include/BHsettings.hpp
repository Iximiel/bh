/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of BHSettings

   @file BHsettings.hpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 21/3/2017
   @version 0.5

   @todo Make BHsettings a singleton
*/
#ifndef BHSETTINGS_H
#define BHSETTINGS_H
#include "BHenums.hpp"
#include "BHhisto.hpp"
#include "BHmyMath.hpp"
#include "BHorderParameter.hpp"
#include <array>
#include <iosfwd>
#include <string>
#include <vector>

namespace BH {

  struct walkerFiles {
    std::string flyingWalker{"defaultWalker.in"};
    std::string landingWalker{};
    std::string hikingWalker{};
  };

  /// Stores the settings for the simulation
  struct BHSettings {
    enum class walkersTurnover {
      ordinal = 1, ///< When set at every step evolves a different walker
      random = 2,  ///< When set at every step evolves a different walker chosen
                   ///< randomly
      change = 3   ///< When set walkers changes with freq=freq_walker_exchange
    };

    struct walkerFiles {
      std::string flyingWalker{"defaultWalker.in"};
      std::string landingWalker{};
      std::string hikingWalker{};
    };

    static walkersTurnover wTOfromInt (int);

    struct parameterSettingsForOutput {
      BHdouble paramMin{0.0};
      BHdouble paramMax{1.0};
      BHdouble interval{1.0};
      int partitioning{1};
      bool active{false};
    };

    struct OPSettings {
      BHOrderParameter OP_;
      parameterSettingsForOutput parametersForOutput_;
      BHHisto::BHHistoPars histoPars_;
    };

    static std::vector<BHOrderParameter>
    getOPsFromSettings (const std::vector<OPSettings> &);

    static constexpr int NumberOfOP = 2;
    // order is not in file appearence for question of padding(memeory
    // optimizing and fast access)
    unsigned RngSeed_{12345678};
    BHminimizationAlgorithmType minimizationAlgorithm_{
      BHminimizationAlgorithmType::LBFGSB};
    std::string namePotpar_{"MetalParameters.in"};
    // run settings
    unsigned int nMC_{0}; ///< Number of Monte Carlo steps

    std::vector<OPSettings> OPSettings_{
      {"mix", {0.0, 1.0, 1.0, 1, true}, {}},
      {"555", {0.0, 1.0, 1.0, 1, false}, {}}};
    unsigned long renewHisto_{0};
    /// if 0 histogram is not used
    BHdouble histoWeight_{0.0};

    BHdouble landingAlpha_{0.5};

    walkersTurnover walkersTurnover_{walkersTurnover::ordinal};

    /// the array of filenames for initialize the correct #BHWalker
    std::vector<walkerFiles> walkerFilenames_{1};
    std::string uniqueBestWalker_{};
    /// the name of the file where to output the histogram
    std::string HistogramOutFile_{"histo.out"};
    int freqWalkerExchange_{0};

    // data not in the file
    /// if False use 12345678 as a seed
    bool setRNGseed_{false};
    bool BWRestartsLanding_{false};
    // interaction potentials
    bool substrate_{false};
    bool cutOff_{false};
    // Output instructions
    /// Different output for every walker
    bool separateWalkerOutput_{false};
    /// plot the list of minima
    bool plotMinima_{true};
    /// if true ener_all.out will contain all minima found, if false only the
    /// accepted one
    bool analyzeAtEveryStep_{false};
    /// if true in during BH the algorithm saves data in an histogram
    bool saveHistogramData_{false};
  };

  void PlotHistoMatrix (
    std::ostream &stream, const BHSettings &, const BHHisto &histo);
  void PlotHistoCoordinates (
    std::ostream &stream, const BHSettings &, const BHHisto &histo);

  namespace BHSettingsUtilities {
    void setFlyingLandingMinimization (BHSettings &);
    void setFLMinimizationwBestWalker (BHSettings &);
    void setFLMinimizationwBestWalkerNoRestart (BHSettings &);
    void setFlyingLandingHikingMinimization (BHSettings &);
    void setFlyingLandingHikingMinimizationNoRebound (BHSettings &);

    void PlotHistoCoordinates (
      std::ostream &stream, const BHSettings &, const BHHisto &histo);
    std::string printInputFileFromData (const BHSettings &);
    void printData (std::ostream &stream, const BHSettings &);
    void PlotHistoMatrix (
      std::ostream &stream, const BHSettings &, const BHHisto &histo);

    /// this function exist because BHSettingsFileLoader is not meant to be
    /// stored directy by the user
    BHSettings loadBHSettings (std::string, bool verbose = true);

    BHSettings loadBHSettings (std::istream &, bool verbose = true);
  } // namespace BHSettingsUtilities

  class BHSettingsFileLoader final : public BHSettings {
  private:
    BHSettingsFileLoader (std::istream &, bool verbose = true);
    BHSettingsFileLoader () = delete;
    //~BHSettingsLoader ();//will be default construced by the compiler
    void parser_general (const std::string &);
    void parser_potential (const std::string &);
    void parser_run (const std::string &);
    void parser_goa (const std::string &);
    void parser_output (const std::string &);
    friend BHSettings
    BHSettingsUtilities::loadBHSettings (std::istream &, bool verbose);
  };

  /// A class to include and load the settings for the algorithm.
  /**This class loads and interprets a [settings file](@ref parserBHSettings).

  The settings loaded from this class regulate the basis of the algorithm and
          how the walker are treated*/
  namespace BHParsers {
    namespace BHS {
      enum class GENERAL { rng_seed, minimizationAlgorithm };
      static const std::array<const std::string, 2> CardGENERAL = {
        "rng_seed", // 0
        "minimizationAlgorithm"};
      enum class POTENTIAL { name_potpar, cut_off };
      static const std::array<const std::string, 2> CardPOTENTIAL = {
        "name_potpar", // 0
        "cut_off"      // 1
      };

      enum class RUN {
        n_mc,
      };

      static const std::array<const std::string, 1> CardRUN = {
        "n_mc" // 0
      };

      enum class GOA {
        histoWeight,
        OP,
        histo,
        renew_histo,
        freq_walker_exchange,
        alpha_flying,
        bestWalker,
        BWRestartsLanding
      };
      static const std::array<const std::string, 8> CardGOA = {
        "histoWeight",          // 0 double
        "OP",                   // 1 special
        "histo",                // 2 varies
        "renew_histo",          // 3 int
        "freq_walker_exchange", // 4 int
        "alpha_flying",         // 5 double
        "bestWalker",           // 6 string
        "BWRestartsLanding"};

      enum class GOAhisto { nbins, start, end };
      static const std::array<const std::string, 3> HISTO = {
        "nbins", "start", "end"};

      enum class OUTPUT {
        separateWalkerOutput,
        plotMinima,
        param_min,
        param_max,
        parameter_partitioning,
        analyzeAllMinima
      };

      static const std::array<const std::string, 6> CardOUTPUT = {
        "separateWalkerOutput",   // 0 bool
        "plotMinima",             // 1 bool
        "param_min",              // 2 double
        "param_max",              // 2 double
        "parameter_partitioning", // 4 int
        "analyzeAllMinima"        // 5 bool
      };
    } // namespace BHS
  }   // namespace BHParsers
} // namespace BH
#endif // BHSETTINGS_H
