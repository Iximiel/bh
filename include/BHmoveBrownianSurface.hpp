/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
  @brief Declaration of move Brownian for surface atoms
   @file BHmoveBrownianSurface.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 11/4/2019
   @version 0.1
   creating the move
  */
#ifdef MOVE_PARSER
MoveParser (brownianSurface, BHMoveBrownianSurface)
#else
#ifndef BHMOVEBROWNIANSURFACE_H
#define BHMOVEBROWNIANSURFACE_H
#include "BHenergyCalculator.hpp"
#include "BHmove.hpp"

namespace BH {
  /**
   * @brief The Move Brownian Surface evolves the surface of the system with a
   *small MD simulation
   *
   * BHMoveBrownianSurface evolves only the atoms of the system with less bonds,
   *it does a small number of steps by bringing the system to an high
   *temperature, then evolving it by simulating a brownian motion of the atoms,
   *influenced by the forces of the cluster
   */
  class BHMoveBrownianSurface : public BHMove {
  public:
    BHMoveBrownianSurface ();
    BHMoveBrownianSurface (const BHMoveBrownianSurface &);
    ~BHMoveBrownianSurface () override;
    void Specialize (const BHCluster &in, const BHMetalParameters &) override;
    static std::string DefaultString ();
    std::unique_ptr<BHMove> clone () override;

  protected:
    bool doMoveAlgorithm (
      BHCluster &in,
      BHCluster &out,
      BHClusterAnalyser &,
      const BHMetalParameters &,
      rndEngine &) override;
    std::string printSettingsSpecialized () const override;
    std::string printSettingsSpecializedForInput () const override;
    bool parseSpecialized (const std::string &) override;
    std::normal_distribution<double> Gaussian_{0.0, 1.0};
    unsigned NofAtoms_{0};
    unsigned NumberOfSteps_{1500};
    unsigned fewNeighbours_{MinimumNeighbourForFCCBulk};
    BHdouble BrownianTemperature_{1500};
    BHdouble cbltz_{1.38e-23};
    BHdouble Friction_{1e12};
    BHdouble TimeStep_{5e-15};
    BHdouble BrownianMass_{1.67e-25};
    BHdouble *x_{nullptr};
    BHdouble *v_{nullptr};
    BHdouble *gradient_{nullptr};
    BHEnergyCalculator *energyCalculator_{nullptr};
  };

  namespace BHParsers {
    namespace BHMV {
      enum class BROWNIANSURFACEvariable {
        NumberOfSteps_,
        BrownianTemperature_,
        BrownianTemperature_alt,
        Friction_,
        TimeStep_,
        TimeStep_alt,
        BrownianMass_,
        BrownianMass_alt,
        cbltz_,
        fewNeighbours_
      };
      const std::array<std::string, 10> BROWNIANSURFACE = {
        "steps", // 0 int
        "browniantemperature",
        "mdtemp",   // 1 2 //double
        "friction", // 3 double
        "browniantimestep",
        "mdts", // 4 5 double
        "brownianMass",
        "mdmass",        // 6 7 double
        "cbltz",         // 8 double
        "fewNeighbours", // 9int
      };
    } // namespace BHMV
  }   // namespace BHParsers
} // namespace BH
#endif // BHMOVEBROWNIANSURFACE_H
#endif
