/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#ifndef ENERGICALCULATOR_LJ12_6
#define ENERGICALCULATOR_LJ12_6
#include "BHenergyCalculator.hpp"
namespace BH {
  class BHEnergyCalculator_LJ12_6 : public BH::BHEnergyCalculator {
  public:
    BHEnergyCalculator_LJ12_6 (
      const BH::BHCluster &, const BHMetalParameters &);
    ~BHEnergyCalculator_LJ12_6 ();
    BH::BHdouble Energy (BH::BHdouble *x);
    BH::BHdouble Energy_tol (BH::BHdouble *x);
    BH::BHdouble Gradient (BH::BHdouble *x, BH::BHdouble *g);
    BH::BHdouble Gradient_tol (BH::BHdouble *x, BH::BHdouble *g);
    BHEnergyCalculator *clone () override;

  private:
    BH::BHdouble *sigma2_, *epsilonX4_, *epsilonX24_;
  };
} // namespace BH
#endif // ENERGICALCULATOR_LJ
