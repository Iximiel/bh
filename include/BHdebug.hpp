/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Utility header with some  shortcuts for standard I/O
   @file BHdebug.hpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 4/7/2017
   @version 1.0.1
*/

#ifndef BHDEBUG_HPP
#define BHDEBUG_HPP

#include <fstream>
#include <iostream>

using std::cerr;
using std::cin;
using std::cout;
using std::endl;
/*void DBFILE_LINE(){
  std::cout<<_FILE__<<": "<<__LINE__<<std::endl;
  }*/
/*
void pause(){
  cout << "Press Enter to continue.";
  std::cin.get();
}
*/
#endif // BHDEBUG_HPP
