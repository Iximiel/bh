/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of class BHEnergyCalculator_SMATB

   some variable are a double of the ones in BHMetalParameters, this class uses
   them in a way that wants to be efficient for calculation instead of
   classification
   @file BHenergyCalculator_SMATB.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 05/06/2017
   @version 0.5

   Interacting neighbours moved from here to base class

   @date 16/10/2017
   @version 0.1

   @date 17/10/2017
   @version 0.2

   energy defined

   @date 19/10/2017
   @version 0.4

   Gradient defined and working, divided base class from SMATB
*/

#ifndef BHENERGYCALCULATOR_SMATB_H
#define BHENERGYCALCULATOR_SMATB_H
#include "BHenergyCalculator.hpp"

namespace BH {
  /// Inherits from BHEnergyCalculator, calculates the energy with the SMATB
  /// potential
  class BHEnergyCalculator_SMATB final : public BHEnergyCalculator {
  public:
    BHEnergyCalculator_SMATB (const BHCluster &, const BHMetalParameters &);
    BHEnergyCalculator_SMATB (const BHEnergyCalculator_SMATB &);
    BHEnergyCalculator_SMATB (BHEnergyCalculator_SMATB &&) noexcept;
    ~BHEnergyCalculator_SMATB () override;
    /// calculates and returns the energy of the cluster
    /**
       coordinates must be given following the schematic:

       - x[3*i]=atom[i].X
       - x[3*i+1]=atom[i].Y
       - x[3*i+2]=atom[i].Z
    */
    BHdouble Energy (BHdouble *x) override;
    BHdouble Energy_tol (BHdouble *x) override;
    /// calculates and returns the energy of the cluster, assigns to g the value
    /// of the gradient
    /**
       coordinates must be given following the schematic:

       - x[3*i]=atom[i].X
       - x[3*i+1]=atom[i].Y
       - x[3*i+2]=atom[i].Z
    */
    BHdouble Gradient (BHdouble *x, BHdouble *g) override;
    BHdouble Gradient_tol (BHdouble *x, BHdouble *g) override;

    BHdouble Pressure (BHdouble *x, BHdouble *p) override;
    BHEnergyCalculator *clone () override;

  private:
    // calculation aid:
    BHdouble *den_;
    // maybe this coud evolve in a different matrix
    BHdouble **Fb_;
    // energy parameters
    BHdouble *p_;
    BHdouble *q_;
    BHdouble *a_;
    BHdouble *xi_;
    BHdouble *NNd_;
    BHdouble *cs_;
    BHdouble *ce_;
    BHdouble *ceSQ_;
    BHdouble *P5_;
    BHdouble *P4_;
    BHdouble *P3_;
    BHdouble *Q5_;
    BHdouble *Q4_;
    BHdouble *Q3_;
  };
} // namespace BH
#endif // BHENERGYCALCULATOR_SMATB_H
