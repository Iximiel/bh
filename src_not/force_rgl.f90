SUBROUTINE FORCE_RGL
  USE PARAMETERS
  USE FUNCTION_PAR
  USE BH_MOD
  implicit none
  ! Local variables

  Real(8) :: den(nat3d),frx(nat3d),fry(nat3d),frz(nat3d)
  Real(8) :: x(nat3d),y(nat3d),z(nat3d)
  Real(8) :: ebi,eri,eneri,for,forsudik,denik
  Real(8) :: fb(nat3d,nat3d)
  Real(8) :: xik(nat3d,nat3d),yik(nat3d,nat3d),zik(nat3d,nat3d),d(nat3d,nat3d)
  Real(8) :: dik0, espo, qsiexpq, aexpp
  Real(8) :: dikm,dikm2,dikm3,dikm4,dikm5
  Integer :: i,j,k,itypik

  !f(dabs(zat(1))>3275786477704)write(*,*)'0000000000 zat(1)=',zat(1)
!!$  write(*,*)'force_rgl'
!!$  write(*,*)'cutoff_start ',cutoff_start
!!$  write(*,*)'cutoff_end ',cutoff_end
!!$  write(*,*)'dist ',dist
!!$
!!$  write(*,*)a3(1),a3(2),a3(3)
!!$  write(*,*)a4(1),a4(2),a4(3)
!!$  write(*,*)a5(1),a5(2),a5(3)
!!$  write(*,*)x3(1),x3(2),x3(3)
!!$  write(*,*)x4(1),x4(2),x4(3)
!!$  write(*,*)x5(1),x5(2),x5(3)

  !     ener_debug = ener
  ener=0.d0
  !  write(*,*)nat3d

  do i=1,nat3d
     dfx(i)=fx(i)
     dfy(i)=fy(i)
     dfz(i)=fz(i)
     x(i)=xat(i)
     y(i)=yat(i)
     z(i)=zat(i)

     den(i)=0.d0
     frx(i)=0.d0
     fry(i)=0.d0
     frz(i)=0.d0
  enddo

  do 10 i=1,nat3d

     ebi=0.d0
     eri=0.d0
     eneri=0.d0
     !write(*,*)i,nvois(i)
     IF(nvois(i).gt.0) THEN
        do 20 j=1,nvois(i)
           k=ivois(j,i)
           if(k.gt.i) then
              if((itype(i).eq.1).and.(itype(k).eq.1)) then
                 itypik=1   ! stesso metallo A
              else if((itype(i).eq.2).and.(itype(k).eq.2)) then
                 itypik=2   ! stesso metallo B
              else
                 itypik=3  ! interazione A-B
              endif
              dik0=dist(itypik)

              xik(k,i)=x(k)-x(i)
              yik(k,i)=y(k)-y(i)
              zik(k,i)=z(k)-z(i)
              dispx(j,i)=xik(k,i)
              dispy(j,i)=yik(k,i)
              dispz(j,i)=zik(k,i)
              d(k,i)=dsqrt(xik(k,i)*xik(k,i)+yik(k,i)*yik(k,i)+zik(k,i)*zik(k,i))
              if (d(k,i).lt.cutoff_start(itypik)) then
                 espo=1.d0-d(k,i)/dik0
                 aexpp=a(itypik)*dexp(p(itypik)*espo)
                 for=2.d0*(p(itypik)/dik0)*aexpp
                 eri=eri+2.d0*aexpp
                 qsiexpq = qsi(itypik)*qsi(itypik) * dexp(2.d0*q(itypik)*espo)
                 fb(k,i) = (q(itypik)/dik0)*qsiexpq;
              else
                 dikm=d(k,i)-cutoff_end(itypik)
                 dikm2=dikm*dikm
                 dikm3=dikm2*dikm
                 dikm4=dikm3*dikm
                 dikm5=dikm4*dikm
                 qsiexpq =x5(itypik)*dikm5+x4(itypik)*dikm4+x3(itypik)*dikm3
                 for=-2.d0*(5.d0*a5(itypik)*dikm4+4.d0*a4(itypik)*dikm3+3.d0*a3(itypik)*dikm2)
                 eri=eri+2*(a5(itypik)*dikm5+a4(itypik)*dikm4+a3(itypik)*dikm3)
                 fb(k,i) = -((5.d0*x5(itypik)*dikm4+4.d0*x4(itypik)*dikm3+3.d0*x3(itypik)*dikm2))*qsiexpq;
                 qsiexpq = qsiexpq**2
              endif !k>i
              den(i)=qsiexpq+den(i)
              den(k)=qsiexpq+den(k)

              forsudik=for/d(k,i)

              frx(i)=frx(i)-forsudik*xik(k,i)
              fry(i)=fry(i)-forsudik*yik(k,i)
              frz(i)=frz(i)-forsudik*zik(k,i)

              frx(k)=frx(k)+forsudik*xik(k,i)
              fry(k)=fry(k)+forsudik*yik(k,i)
              frz(k)=frz(k)+forsudik*zik(k,i)
           endif!k>i
20      enddo
        ebi=dsqrt(den(i))
        den(i)=1.d0/ebi
        eneri=eri-ebi
        ener_atom(i)=eneri
        ener=ener+eneri
     ENDIF !exist neighbours	

10 enddo
  
  emetal=ener
  do 30 i=1,nat3d
     IF(nvois(i) .gt. 0) THEN !exist neighbours
        do 40 j=1,nvois(i)
           k=ivois(j,i)
           if(k.gt.i)then
              denik=fb(k,i)*(den(i)+den(k))/d(k,i)
              frx(i)=frx(i)+denik*xik(k,i)
              fry(i)=fry(i)+denik*yik(k,i)
              frz(i)=frz(i)+denik*zik(k,i)
              frx(k)=frx(k)-denik*xik(k,i)
              fry(k)=fry(k)-denik*yik(k,i)
              frz(k)=frz(k)-denik*zik(k,i)
           endif!k>i
40      enddo
     ENDIF ! no neighbours
     fx(i)=frx(i)
     fy(i)=fry(i)
     fz(i)=frz(i)
30 enddo

!!$  if(substrate == 'si') then
!!$     !   call force_met_mgo
!!$     e_met_mgo=0
!!$  else
  e_met_mgo=0
  is_nan = .false.
!!$  endif

  !write(*,*)'force_rgl, ener= ',ener
  !stop

END SUBROUTINE FORCE_RGL
