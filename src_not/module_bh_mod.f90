MODULE BH_MOD
  use iso_c_binding!in realta` e` uno statement per fortran 2003 ma se compila non mi lamento 
  Implicit None
  SAVE
  Integer(C_INT) :: nat3d    ! number of atoms in the cluster  
  Integer,        allocatable :: nvois(:), ivois(:,:)!n of neighbours per atom, neighbours list of atom  i
  Integer                     :: nwithvois!number of atoms with neighbours
  Integer,        allocatable :: hasvois(:)!indexes of atoms with neighbours
  Integer(C_INT), allocatable :: itype(:)!type of the atoms
  !Integer(C_INT), allocatable :: INT_label(:,:)!for each (j,i) contains the label of the interaction is faster only with smaller clusters
END MODULE BH_MOD
