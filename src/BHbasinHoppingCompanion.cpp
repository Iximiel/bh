/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Here the definitions the basin hopping algorithm

   @file BHbasinHopping.cpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 12/6/2019
   @version 0.13
   mke this class hinerith from an abstract class

   @date 1/5/2018
   @version 0.12
   restored original meaning for "true move", now the user setting for parameter
   are taken seriously, polished output files

   @date 26/4/2018
   @version 0.11
   added rew algorithm

   @date 10/3/2018
   @version 0.10.1

   adding separateg glo output per walker

   @date 10/3/2018
   @version 0.10

   added separated ouput ener_all per walker

   @date 20/6/2017
   @version 0.9.2
*/
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "BHbasinHoppingCompanion.hpp"
#include "BHclusterUtilities.hpp"
#include "BHworkWithFortran.hpp"
//#include "BHdebug.hpp"
#include "BHconstructors.hpp"
#include "BHmetalParameters.hpp"

// remember always to use wID when selecting a walker!!!
// it is more stylish and easy to read

/*
  instruction for a benchmark:
  /usr/bin/time -o tempo -p ../main
*/
namespace BH {
  const char *BasinHoppingCompanion::staticLogo () {
    return "  ___ _  _   _     _\n"
           " | _ ) || |_| |_ _| |_ __ \n"
           " | _ \\ __ |_   _|_   _/ _|\n"
           " |___/_||_| |_|   |_| \\__|\n"
           "                          \n";
  }
  const char *BasinHoppingCompanion::logo () { return staticLogo (); }
  BasinHoppingCompanion::BasinHoppingCompanion (const BHSettings &settings)
    : BHAbstractBasinHopping (settings) {
    settings_.separateWalkerOutput_ = true;
  }

  BasinHoppingCompanion::~BasinHoppingCompanion () = default;

  const std::string BasinHoppingCompanion::BHStyleName () {
    return "Companion";
  }

  void BasinHoppingCompanion::PreOutputInitialization () {
    for (unsigned wID = 0; wID < settings_.nWalkers_; ++wID) {
      std::stringstream fname;
      fname << "w" << std::setw (2) << std::setfill ('0') << wID;
      walkers_[wID].identificator = fname.str ();
    }
    for (unsigned wID = 0; wID < settings_.nWalkers_; ++wID) {
      std::stringstream fname;
      fname << "c" << std::setw (2) << std::setfill ('0') << wID;
      walkers_[wID + settings_.nWalkers_].identificator = fname.str ();
    }
  }

  void BasinHoppingCompanion::Initialization () {
    BHAbstractBasinHopping::Initialization ();
    std::cout
      << "\nCreating the companions walkers from file \"companion.in\"\n";
    BH::BHWalkerSettings companionWalkerSettings =
      BH::loadBHWalkerSettings ("companion.in");
    if (
      companionWalkerSettings.ChosenAlgorithm_ ==
      BH::BHwalkerAlgorithmType::obst) {
      throw "You cannot start a minimization using companion walkers as "
            "obstacles";
    }

    for (unsigned wID = 0; wID < settings_.nWalkers_; ++wID) {
      std::cout << "\n###############Creating companion for walker " << wID
                << std::endl;
      walkers_.push_back (BHConstructors::getConstructor ().createWalker (
        companionWalkerSettings, rndEngine_));
      // this should set up also the interaction labels
      walkers_.back ().copyAllConfigurations (walkers_[wID]);
      walkers_.back ().printSettings ();
      acceptedMovesInfo_.push_back ({0, 0});
    }
  }

  void BasinHoppingCompanion::MoveAcceptedPostProduction () {
    BHdouble newEne = walkers_[runnigWalker_].lastAcceptedEnergy ();
    // test if the walker has found the best energy until now
    if (
      (newEne - GloInfoGeneral_.energy) < MINIMUN_ENERGY_DIFFERENCE) { // 1e-05?

      if (runnigWalker_ < settings_.nWalkers_) {
        unsigned companionID = settings_.nWalkers_ + runnigWalker_;
        walkers_[companionID].copyAcceptedConfiguration (
          walkers_[runnigWalker_]);
      }
    }
  }
} // namespace BH
