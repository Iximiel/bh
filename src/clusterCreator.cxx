/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief A small utility to create nanoclusters
   @file clusterCreator.cxx
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 17/12/2018
   @version 0.1

   added truncated octahedra

   @date 20/12/2018
   @version 0.5

   added truncated decahedron and icosahedron

   @date 22/12/2018
   @version 1.0

   everithing works fine, needs some refirements

   @date 9/1/2019
   @version 1.1

   separated the function from main
*/
#include "BHatom.hpp"
#include "BHclusterCreatorUtilities.hpp"
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>

using namespace BH;
using namespace std;

int main (int /*argc*/, char ** /*argv*/) {
  int retval = 0;
  try {
    constexpr unsigned nMetals = 12;
    std::string MetalNames[nMetals] = {"Co", "Ni", "Cu", "Rh", "Pd", "Ag",
                                       "Ir", "Pt", "Au", "Ne", "Ar", "1A"};
    BH::BHdouble MetalRadii[nMetals] = {1.25,  1.245, 1.28, 1.345, 1.375, 1.445,
                                        1.355, 1.385, 1.44, 1.88,  1.91,  1.0};

    for (unsigned i = 0; i < nMetals; ++i) {
      std::cout << "[" << i << "]\t" << MetalNames[i] << "\t" << MetalRadii[i]
                << std::endl;
    }

    std::cout << "Select the metal that you want to use for the cluster: ";
    int metalSelected = 8;
    std::cin >> metalSelected;
    if (metalSelected < 0 || metalSelected > (nMetals - 1)) {
      throw "Metal index out of bonds";
    }
    std::cout << "Select the type of cluster that you want to "
                 "create:\n1\t(truncated) "
                 "octahedron\n2\ticosahedron\n3\tMark's decahedron\n:";
    unsigned kind = 1;
    std::cin >> kind;
    bool doPlot = true;
    std::vector<BHAtom> atoms;
    switch (kind) {
    case 1: {
      std::cout << "You have selected truncated octaheron:\n";
      unsigned nl = 7, ncut = 2;
      std::cout
        << "Please insert the length (in atoms) of the uncut octahedron: ";
      std::cin >> nl;
      std::cout << "Please insert the number of atoms to cut from vertices: ";
      std::cin >> ncut;

      std::cout << "Number of atoms in octaedron:             "
                << BH::TruncatedOctahedron::NatOctahedron (nl) << std::endl
                << "Surface atoms on the octaedron:           "
                << BH::TruncatedOctahedron::NatOctahedronSurf (nl) << std::endl
                << "Number of atoms in truncated octaedron:   "
                << BH::TruncatedOctahedron::NatTruncOct (nl, ncut) << std::endl
                << "Surface atoms on the truncated octaedron: "
                << BH::TruncatedOctahedron::NatTruncOctSurf (nl, ncut)
                << std::endl;

      atoms = BH::TruncatedOctahedron::truncatedOctahedron (
        nl, ncut, MetalNames[metalSelected].c_str (),
        MetalRadii[metalSelected]);
      // atoms =
      // BH::TruncatedOctahedron::truncatedOctahedron(nl,ncut,"Pt",1.35*sqrt(8.0));
    } break;
    case 2: {
      std::cout << "You have selected icosaheron:\n";
      unsigned ns = 1;
      std::cout << "Please insert the number of shells: ";
      std::cin >> ns;
      atoms = BH::Icosahedron::icosahedron (
        ns, MetalNames[metalSelected].c_str (), MetalRadii[metalSelected]);
      // atoms = BH::Icosahedron::icosahedron(ns,0,0,"Ag",1.44*sqrt(8.0));
    } break;
    case 3: {
      std::cout << "You have selected Mark's decahedron:\n";
      int m = 3, n = 1, p = 5;
      double dilatation = 0.83;
      std::cout << "Please insert the number of shells: ";
      std::cin >> m;
      std::cout << "The number of layers between the two piramids: ";
      std::cin >> n;
      std::cout << "Please insert p: ";
      std::cin >> p;

      if (n <= 0) {
        n = 1;
        std::cout << "n has ben forced to 1\n";
      }
      atoms = BH::MarksDecahedron::decahedron (
        m, n, p, MetalNames[metalSelected].c_str (), MetalRadii[metalSelected],
        dilatation);
      // atoms = BH::MarksDecahedron::decahedron(m, n, p, "Ag", 1.44*2.0,
      // dilatation);
    } break;
    default:
      doPlot = false;
    }
    if (doPlot) {
      std::string outfilename = "test.xyz";
      std::cout << "saving cluster to \"" << outfilename << "\"\n";
      std::ofstream ofile (outfilename);
      // std::cout << atoms.size() <<std::endl;
      ofile << atoms.size ()
            << std::endl
            //<<"jmolscript: select atomno<=13; color blue;select @26; color
            // red; select all;"
            << std::endl;
      for (const BHAtom &atom : atoms) {
        ofile << atom << std::endl;
      }
      ofile.close ();
    }
  } catch (const char *problem) {
    cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m" << endl;
    retval = 1;
  } catch (const string &problem) {
    cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m" << endl;
    retval = 1;
  }
  return retval;
}
