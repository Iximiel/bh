/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @file BHclusterUtilities.cpp
   @brief Declaration of the function in the namespace BHClusterUtilities

   These utilitess use the old and slow
   BHMetalParameters::getBHMP().calcNeighbourhood_tol routine instead of the new
   faster from BHclusterAnalysis

   @author Daniele Rapetti (iximiel@gmail.com)

   @date 17/4/2018
   @version 0.2

   adding #define SurfaceLimit

   @date 16/4/2018
   @version 0.2

   adding some functionality for surface atoms  in island workers

   @date 6/3/2018
   @version 0.1

   Separating these function from BHClusterAnalyser

*/
#include "BHclusterUtilities.hpp"
#include "BHtiming.hpp"

#include <algorithm>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>
#include <limits>
#include <map>

#include <queue>
#include <sstream>

using namespace std;
// the upper linimt of surface atoms

constexpr auto maxIgnore = std::numeric_limits<std::streamsize>::max ();

namespace BH {

  namespace BHClusterUtilities {
    BHClusterRandomizerData::Limits extraCtMinMax (std::string data) {
      BHdouble min, MAX;
      min = stod (data);
      data = data.substr (data.find_first_of (" ,\t"));
      MAX = stod (data.substr (data.find_first_of ("0123456789")));
      return {min, MAX};
    }

    BHClusterRandomizerData::BHClusterRandomizerData (string fname) {
      std::ifstream file (fname);
      if (!file.is_open ()) {
        throw "Problem opening \"" + fname + "\"";
      }
      std::string dummy;

      file >> N;
      file.ignore (maxIgnore, '\n'); // discard comments
      std::getline (file, dummy, '\n');
      dummy = dummy.substr (0, dummy.find_first_of ("#!"));
      std::stringstream ft (dummy);
      while (ft >> dummy) {
        nAtomByTypeAndN.push_back ({dummy, 0});
      }
      unsigned overflow = 0;
      for (unsigned i = 0; i < nAtomByTypeAndN.size (); ++i) {
        file >> nAtomByTypeAndN[i].NofAtoms;
        overflow += nAtomByTypeAndN[i].NofAtoms;
      }
      if (overflow != N) {
        throw "RandomCluster: Sum of atoms by type (" +
          std::to_string (overflow) +
          ") don't coincide with the desired atoms quantity: " +
          std::to_string (N);
      }
      file.ignore (maxIgnore, '\n'); // discard comments
      file >> dummy;
      randomClusterShape = (dummy[0] == 'c' || dummy[0] == 'C')
                             ? typeOfRandom::cartesian
                             : typeOfRandom::sphere; // c for "cartesian"
      file.ignore (maxIgnore, '\n');                 // discard comments

      switch (randomClusterShape) {
      case typeOfRandom::cartesian:
        std::getline (file, dummy, '\n');
        XLimits = extraCtMinMax (dummy);
        std::getline (file, dummy, '\n');
        YLimits = extraCtMinMax (dummy);
        std::getline (file, dummy, '\n');
        ZLimits = extraCtMinMax (dummy);
        break;
      case typeOfRandom::sphere:
        std::getline (file, dummy, '\n');
        XLimits = extraCtMinMax (dummy);
      }
    }

    std::istream &
    operator>> (std::istream &stream, BHClusterRandomizerData &data) {
      std::string dummy;
      stream >> data.N;
      stream.ignore (maxIgnore, '\n'); // discard comments
      std::getline (stream, dummy, '\n');
      dummy = dummy.substr (0, dummy.find_first_of ("#!"));
      std::stringstream ft (dummy);
      while (ft >> dummy) {
        data.nAtomByTypeAndN.push_back ({dummy, 0});
      }
      unsigned overflow = 0;
      for (unsigned i = 0; i < data.nAtomByTypeAndN.size (); ++i) {
        stream >> data.nAtomByTypeAndN[i].NofAtoms;
        overflow += data.nAtomByTypeAndN[i].NofAtoms;
      }
      if (overflow != data.N) {
        throw "RandomCluster: Sum of atoms by type (" +
          std::to_string (overflow) +
          ") don't coincide with the desired atoms quantity: " +
          std::to_string (data.N);
      }
      stream.ignore (maxIgnore, '\n'); // discard comments
      stream >> dummy;
      data.randomClusterShape =
        (dummy[0] == 'c' || dummy[0] == 'C')
          ? BHClusterRandomizerData::typeOfRandom::cartesian
          : BHClusterRandomizerData::typeOfRandom::sphere; // c for
                                                           // "cartesian"
      stream.ignore (maxIgnore, '\n');                     // discard comments

      switch (data.randomClusterShape) {
      case BHClusterRandomizerData::typeOfRandom::cartesian:
        std::getline (stream, dummy, '\n');
        data.XLimits = extraCtMinMax (dummy);
        std::getline (stream, dummy, '\n');
        data.YLimits = extraCtMinMax (dummy);
        std::getline (stream, dummy, '\n');
        data.ZLimits = extraCtMinMax (dummy);
        break;
      case BHClusterRandomizerData::typeOfRandom::sphere:
        std::getline (stream, dummy, '\n');
        data.XLimits = extraCtMinMax (dummy);
      }
      return stream;
    }

    BHClusterRandomizerData::BHClusterRandomizerData () = default;

    void plotRandomClusterData (const BHClusterRandomizerData hintsForRnd) {
      std::cout << "*****Random Cluster*****" << std::endl;
      std::cout << "Positioning " << hintsForRnd.N << " atoms" << std::endl;
      std::cout << "Of types:" << std::endl;
      for (const auto &atomsNumbers : hintsForRnd.nAtomByTypeAndN) {
        std::cout << "Type: " << atomsNumbers.Type << ", "
                  << atomsNumbers.NofAtoms << " atoms" << std::endl;
      }
      switch (hintsForRnd.randomClusterShape) {
      case BHClusterRandomizerData::typeOfRandom::cartesian:
        std::cout << "in a box with dimensions:" << std::endl;
        std::cout << "x: " << std::setw (3) << hintsForRnd.XLimits.min << " "
                  << std::setw (3) << hintsForRnd.XLimits.max << std::endl;
        std::cout << "y: " << std::setw (3) << hintsForRnd.YLimits.min << " "
                  << std::setw (3) << hintsForRnd.YLimits.max << std::endl;
        std::cout << "z: " << std::setw (3) << hintsForRnd.ZLimits.min << " "
                  << std::setw (3) << hintsForRnd.ZLimits.max << std::endl;
        break;
      case BHClusterRandomizerData::typeOfRandom::sphere:
        std::cout << "in a sphere of radius  between "
                  << hintsForRnd.XLimits.min << " and "
                  << hintsForRnd.XLimits.max << std::endl;
      }
    }

    BHClusterAtoms randomCluster (
      const BHClusterRandomizerData hintsForRnd, rndEngine &my_rnd_engine) {
      unsigned overflow = 0;
      for (const auto &atomsNumbers : hintsForRnd.nAtomByTypeAndN) {
        overflow += atomsNumbers.NofAtoms;
      }
      if (overflow != hintsForRnd.N) {
        throw "RandomCluster: Sum of atoms by type (" +
          std::to_string (overflow) +
          ") don't coincide with the desired atoms quantity: " +
          std::to_string (hintsForRnd.N);
      }

      BHClusterAtoms cluster (hintsForRnd.N);

      BHRealRND rn01 (0., 1.);
      std::function<BHVector (rndEngine &)> vectorMaker;

      switch (hintsForRnd.randomClusterShape) {
      case BHClusterRandomizerData::typeOfRandom::cartesian:
        vectorMaker =
          [&rn01, xdiff = (hintsForRnd.XLimits.max - hintsForRnd.XLimits.min),
           xmin = hintsForRnd.XLimits.min,
           ydiff = (hintsForRnd.YLimits.max - hintsForRnd.YLimits.min),
           ymin = hintsForRnd.YLimits.min,
           zdiff = (hintsForRnd.ZLimits.max - hintsForRnd.ZLimits.min),
           zmin = hintsForRnd.ZLimits.min] (rndEngine &rng) -> BHVector {
          BHVector toRet (
            rn01 (rng) * xdiff + xmin, rn01 (rng) * ydiff + ymin,
            rn01 (rng) * zdiff + zmin);
          return toRet;
        };
        break;
      case BHClusterRandomizerData::typeOfRandom::sphere:
        vectorMaker =
          [&rn01,
           rminCub = hintsForRnd.XLimits.min * hintsForRnd.XLimits.min *
                     hintsForRnd.XLimits.min,
           rMaxCub = hintsForRnd.XLimits.max * hintsForRnd.XLimits.max *
                     hintsForRnd.XLimits.max] (rndEngine &rng) -> BHVector {
          return BHVector::RhoThetaPhi (
            cbrt (rminCub + rn01 (rng) * (rMaxCub - rminCub)),
            acos (1 - 2 * rn01 (rng)), rn01 (rng) * 2. * M_PI);
        };
      }

      unsigned i = 0;
      for (unsigned t = 0; t < hintsForRnd.nAtomByTypeAndN.size (); ++t) {
        for (unsigned j = 0; j < hintsForRnd.nAtomByTypeAndN[t].NofAtoms; ++j) {
          cluster[i].setType (hintsForRnd.nAtomByTypeAndN[t].Type);
          cluster[i] = vectorMaker (my_rnd_engine);
          ++i;
        }
      }
      cluster.updateComposition ();
      return cluster;
    }

    namespace BHIslandWorkers {
      unsigned eatIsland (
        unsigned j, const BHCluster &cluster, std::vector<bool> &unvisited) {
        unsigned dim = 0;
        if (unvisited[j]) {
          ++dim;
          unvisited[j] = false;
          std::queue<unsigned> atomlist;
          atomlist.push (j);
          do {
            j = atomlist.front ();
            atomlist.pop ();
            for (const unsigned &n : cluster.getNNs (j)) {
              if (unvisited[n]) {
                unvisited[n] = false;
                ++dim;
                atomlist.push (n);
              }
            }
          } while (!atomlist.empty ());
        }
        return dim;
      }

      unsigned eatIsland_sameKind (
        unsigned j, const BHCluster &cluster, std::vector<bool> &unvisited) {
        unsigned dim = 0;
        if (unvisited[j]) {
          ++dim;
          unvisited[j] = false;
          std::queue<unsigned> atomlist;
          atomlist.push (j);
          do {
            j = atomlist.front ();
            atomlist.pop ();
            for (const unsigned &n : cluster.getNNs_same (j)) {
              if (unvisited[n]) {
                unvisited[n] = false;
                ++dim;
                atomlist.push (n);
              }
            }
          } while (!atomlist.empty ());
        }
        return dim;
      }

      vectorAtomID getElementsOfIsland (
        unsigned j, const BHCluster &cluster, std::vector<bool> &unvisited) {
        vectorAtomID elements;
        if (unvisited[j]) {
          elements.push_back (j);
          unvisited[j] = false;
          std::queue<unsigned> atomlist;
          atomlist.push (j);
          do {
            j = atomlist.front ();
            atomlist.pop ();
            for (const unsigned &n : cluster.getNNs (j)) {
              if (unvisited[n]) {
                unvisited[n] = false;
                elements.push_back (j);
                atomlist.push (n);
              }
            }
          } while (!atomlist.empty ());
        }
        return elements;
      }

      std::pair<unsigned, unsigned> eatIslandAndSurface (
        unsigned i, const BHCluster &cluster, std::vector<bool> &unvisited) {
        unsigned dim = 0, surfAtoms = 0;
        std::queue<unsigned> indexes;
        if (unvisited[i]) {
          ++dim;
          indexes.push (i);
          unvisited[i] = false;
        }
        while (!indexes.empty ()) {
          unsigned index = indexes.front ();
          indexes.pop ();
          for (const unsigned &n : cluster.getNNs (index)) {
            if (unvisited[n]) {
              unvisited[n] = false;
              indexes.push (n);
              ++dim;
              if (cluster.getNNs (n).size () < MinimumNeighbourForFCCBulk) {
                ++surfAtoms;
              }
            }
          }
        }
        return std::make_pair (dim, surfAtoms);
      }
    } // namespace BHIslandWorkers

    std::vector<IslandInfo>
    IslandsAnalysis (BHCluster &cluster, const BHMetalParameters &bhmp) {
      const unsigned NofAtoms_ = cluster.getNofAtoms ();
      std::vector<IslandInfo> islands;
      bhmp.calcNeighbourhood_tol (cluster);
      // workarray that stores the index of unvisited atoms
      std::vector<bool> unvisited (NofAtoms_, true);
      for (unsigned i (0); i < NofAtoms_; ++i) {
        if (unvisited[i]) {
          islands.emplace_back (
            IslandInfo{i, BHIslandWorkers::eatIsland (i, cluster, unvisited)});
        }
      }
      return islands;
    }

    unsigned CountIslands (BHCluster &cluster, const BHMetalParameters &bhmp) {
      return static_cast<unsigned> (IslandsAnalysis (cluster, bhmp).size ());
    }

    std::vector<IslandInfoSurf> IslandsAnalysisWithSurface (
      BHCluster &cluster, const BHMetalParameters &bhmp) {
      const unsigned NofAtoms_ = cluster.getNofAtoms ();
      std::vector<IslandInfoSurf> islands;
      bhmp.calcNeighbourhood_tol (cluster);
      // workarray that stores the index of unvisited atoms
      std::vector<bool> unvisited (NofAtoms_, true);
      for (unsigned i (0); i < NofAtoms_; ++i) {
        if (unvisited[i]) {
          auto dimAndSurf =
            BHIslandWorkers::eatIslandAndSurface (i, cluster, unvisited);
          islands.emplace_back (
            IslandInfoSurf{i, dimAndSurf.first, dimAndSurf.second});
        }
      }
      return islands;
    }

    void AlphabeticalReorder (BHClusterAtoms &cluster) {
      std::stable_sort (
        cluster.begin (), cluster.end (),
        [] (
          const BHAtom &me,
          const BHAtom &other) -> bool { // portland needs this
          return me.Type () < other.Type ();
        });
    }

    void
    MassesReorder (BHClusterAtoms &cluster, const BHMetalParameters &bhmp) {
      std::stable_sort (
        cluster.begin (), cluster.end (),
        [&] (
          const BHAtom &a,
          const BHAtom &b) -> bool { // using a lambda function to
                                     // declare a temporary operator<
          // this is inverted because i want first the massive atoms
          return bhmp.getMass (a.Type ()) > bhmp.getMass (b.Type ());
        });
    }

    /// Collapses the islands in one cluster
    /** This routine is useful after a random generation of the cluster in order
       to verify that there aren't any isolated minor cluster arond the wanted
       one.

        BHMetalParameters::calcNeighbourhood_tol is called within the function
       to ensure that the neighbours are calculated
    */
    void IslandsCollapser (
      BHCluster &cluster, const BHMetalParameters &bhmp, rndEngine &rng) {
      bhmp.calcNeighbourhood_tol (cluster);
      BHVector center = cluster.getGeometricCenter ();
      // workarray that stores the index of unvisited atoms
      std::vector<bool> unvisited (cluster.getNofAtoms (), true);
      // the index of the map is the first atom of an island and the stored
      // indexes are island's dimension
      // std::unordered_map<int, int> islands;
      unsigned greater_ind = 0, // index of the bigger island
        greater_dim = 0;        // dimension of the bigger island
      // In this vector are stored the
      vectorAtomID starting_atoms;
      for (unsigned int i (0); i < cluster.getNofAtoms (); i++) {
        if (unvisited[i]) {
          unsigned dim = BHIslandWorkers::eatIsland (i, cluster, unvisited);
          // std::cout << i << " " <<dim <<std::endl;
          starting_atoms.push_back (i);
          if (dim > greater_dim) {
            greater_ind = i;
            greater_dim = dim;
          }
        }
      }
      unvisited.flip (); // turns again unvisited all atoms

      // Having a list of island and their dimensions i can collapse
      // the smaller ones onto the bigger cluster.

      // This creates a list of the atoms in  the smaller islands
      vectorAtomID drifters;
      for (const unsigned i : starting_atoms) {
        if (i != greater_ind) {
          auto t = BHIslandWorkers::getElementsOfIsland (i, cluster, unvisited);
          drifters.insert (drifters.end (), t.begin (), t.end ());
        }
      }
      // The unvisited atoms are only atoms that are in the bigger island.
      // So we calculate the the atoms with the least number of neighbours on
      // the bigger island. We use vectorint::size_type for discarding warnings
      // I know this is inefficent, but has to be done ONLY one time
      vectorAtomID lonelyAtoms;
      vectorAtomID::size_type lessNeig = 0;
      //@todo repalace this output
      /* std::ofstream seedOut ("seedOut_.xyz");
       seedOut << cluster.getNofAtoms () << std::endl;
       seedOut << "jmolcomments" << std::endl;
       seedOut << cluster[0];
       for (unsigned int i = 1; i < cluster.getNofAtoms (); i++) {
         seedOut << '\n' << cluster[i];
       }
       seedOut << std::endl;
       seedOut.close ();*/
      while (lonelyAtoms.size () < drifters.size () &&
             lessNeig < MinimumNeighbourForFCCBulk) {
        // increases the minimun number of neighbours
        lessNeig++;
        for (unsigned int i (0); i < cluster.getNofAtoms (); i++) {
          if (unvisited[i]) {
            if (cluster.getNNs (i).size () == lessNeig) {
              unvisited[i] = false;
              lonelyAtoms.push_back (i);
            }
          }
        }
      }
      // !lonelyAtoms.empty () maybe is an overkill
      while (!drifters.empty () && !lonelyAtoms.empty ()) {
        // this is an arbitrary distance
        BHdouble NNdist = bhmp.getNNdist (0) * 2.0 / 3.0;
        unsigned toBeMoved = drifters.back ();
        drifters.pop_back ();
        BHIntRND rndneigh (0, int (lonelyAtoms.size ()) - 1);
        unsigned ind_newNeigbour = unsigned (rndneigh (rng));
        unsigned newNeigbour = lonelyAtoms[ind_newNeigbour];
        if (lonelyAtoms.size () > drifters.size ()) {
          lonelyAtoms.erase (lonelyAtoms.begin () + ind_newNeigbour);
        }
        // Atoms are are arbitrary placed in this position

        cluster[toBeMoved] =
          BHVector::directionVersorFromTo (center, cluster[newNeigbour]) *
            NNdist +
          cluster[newNeigbour];
        /*::RhoThetaPhi(cbrt(SphereSettings_.rho_inf3+rnd_rho(*rng)),
          acos(rnd_theta(*rng)),
          rnd_phi(*rng));*/
      }
    }

    BHClusterAtoms splitAndJuxtapose (
      const BHClusterAtoms &firstCluster,
      const BHClusterAtoms &secondCluster,
      splitAndJuxtaposePlane plane) {
      if (!firstCluster.testSameConfiguration (secondCluster, false)) {
        throw "splitAndJuxtapose the two clusters does not "
              "have the same configuration";
      }
      std::function<bool (const BHAtom &)> slice;

      switch (plane) {
      case xy:
        slice = [] (const BHAtom &atom) { return atom.Z () < 0; };
        break;
      case xz:
        slice = [] (const BHAtom &atom) { return atom.Y () < 0; };
        break;
      case yz:
        slice = [] (const BHAtom &atom) { return atom.X () < 0; };
      }
      BHVector firstCenter = firstCluster.getGeometricCenter (),
               secondCenter = secondCluster.getGeometricCenter ();
      std::vector<BHAtom> selectedAtoms;
      const unsigned nat = firstCluster.getNofAtoms ();
      for (unsigned i = 0; i < nat; ++i) {
        if (slice ((firstCluster[i] - firstCenter))) {
          selectedAtoms.push_back ((firstCluster[i] - firstCenter));
        }
        if (!slice ((secondCluster[i] - secondCenter))) {
          selectedAtoms.push_back ((secondCluster[i] - secondCenter));
        }
      }
      if (selectedAtoms.size () < nat) {
        size_t delta = nat - selectedAtoms.size ();
        std::function<BHAtom (const size_t &)> add;
        switch (plane) {
        case xy:
          add = [delta] (const size_t &index) {
            return BHAtom (
              "",
              cos (static_cast<double> (index) / static_cast<double> (delta)),
              sin (static_cast<double> (index) / static_cast<double> (delta)),
              0.0);
          };
          break;
        case xz:
          add = [delta] (const size_t &index) {
            return BHAtom (
              "",
              cos (static_cast<double> (index) / static_cast<double> (delta)),
              0.0,
              sin (static_cast<double> (index) / static_cast<double> (delta)));
          };
          break;
        case yz:
          add = [delta] (const size_t &index) {
            return BHAtom (
              "", 0.0,
              cos (static_cast<double> (index) / static_cast<double> (delta)),
              sin (static_cast<double> (index) / static_cast<double> (delta)));
          };
        }
        for (size_t i = 0u; i < delta; ++i) {
          selectedAtoms.push_back (add (i));
        }
      } else if (selectedAtoms.size () > nat) {
        selectedAtoms.resize (nat);
      }

      BHClusterAtoms toreturn (static_cast<unsigned> (nat));
      for (unsigned i = 0; i < nat; ++i) {
        toreturn[i] = selectedAtoms[i];
        toreturn[i].setType (firstCluster[i].Type ());
      }

      return toreturn;
    }

    std::string plotCenteredClusterWithData (
      const BHClusterAtoms &cluster,
      const BHClusterData &clusterData,
      const std::vector<BHOrderParameter> &OPnames) {
      std::stringstream data;
      BHVector center = cluster.getGeometricCenter ();
      data << cluster.getNofAtoms () << '\n'
           << cluster.Composition () << "\t#"
           << clusterData.plotDataForxyz (OPnames);
      for (unsigned int i = 0; i < cluster.getNofAtoms (); i++) {
        data << "\n" << (cluster[i] - center);
      }
      return data.str ();
    }

    std::string plotClusterWithData (
      const BHClusterAtoms &cluster,
      const BHClusterData &clusterData,
      const std::vector<BHOrderParameter> &OPnames) {
      std::stringstream data;
      data << cluster.getNofAtoms () << '\n'
           << cluster.Composition () << " "
           << clusterData.plotDataForxyz (OPnames)
           << " Lattice=\"0 0 0 10 10 10\" Properties=species:S:1:pos:R:3 ";
      //<< "\tProperties=species:S:1:pos:R:3";
      for (unsigned int i = 0; i < cluster.getNofAtoms (); i++) {
        data << "\n" << cluster[i];
      }
      return data.str ();
    }

    std::vector<BHCouple> calculateCNAsignatures (const BHCluster &cluster) {
      std::vector<BHCouple> couples;
      const size_t NofAtoms (cluster.getNofAtoms ());
      couples.reserve (NofAtoms * NofAtoms);
      // the first loop analises the number of common neighbours of a couple
      // and the number of common neighbours of ecah neighbours of the couple

      // loop on the first atom of the couple
      for (unsigned firstAtom = 0; firstAtom < NofAtoms - 1; ++firstAtom) {
        // loop on the NN of the first atom
        for (const unsigned &secondAtom : cluster.getNNs (firstAtom)) {
          if (secondAtom > firstAtom) { // so calculation are made only one time
            BHCouple cp (firstAtom, secondAtom);
            // loops on the NN of the couple to determine if there are some
            // neighbours in common
            for (const unsigned &neighOfFirst : cluster.getNNs (firstAtom)) {
              for (const unsigned &neighOfSecond :
                   cluster.getNNs (secondAtom)) {
                if (neighOfFirst == neighOfSecond) {
                  ++cp.r;
                  // If we find  more than one common neighbour we test if it
                  // is present in the neighbour lists of the other common
                  // neighbours
                  if (cp.r > 1) {
                    for (const unsigned &firstCommonNeigh : cp.commonNeighs) {
                      for (const unsigned &secondCommonNeigh :
                           cluster.getNNs (firstCommonNeigh)) {
                        // if neighOfFirst is one of the NN of
                        // firstCommonNeigh, there is a a bound between the
                        // common NN
                        if (neighOfFirst == secondCommonNeigh) {
                          cp.s++;
                        }
                      } // NNloop for common secondCommonNeigh
                    }   // NNloop for common firstCommonNeigh
                  }     // NN between common NN
                  // i push the cNN after the if so that it does not loop over
                  // itself to find if is a nn of itself a common neigh is
                  // found and stored in the array
                  cp.commonNeighs.push_back (neighOfFirst);
                } // if m==n
              }   // NNloop for secondAtom
            }     // NNloop for i
            couples.push_back (cp);
          } // neigh secondAtom>firstAtom
        }   // loop on the NN of firstAtom
      }     // loop on the atoms

      // now i have to calculate the max lenght of the chain of NN
      for (BHCouple &cp : couples) {
        size_t cNNsize = cp.commonNeighs.size ();
        if (cNNsize <= 1) {
          cp.t = 0;
        } else if (cNNsize == 2) {
          auto it = find (
            cluster.getNNs (cp.commonNeighs[1]).begin (),
            cluster.getNNs (cp.commonNeighs[1]).end (), cp.commonNeighs[0]);
          if (it != cluster.getNNs (cp.commonNeighs[1]).end ()) {
            cp.t = 1;
          }
        } else {
          // c++ in <algorithm> give a way to permutate an array, lexycally
          std::sort (cp.commonNeighs.begin (), cp.commonNeighs.end ());
          do {
            /*cout << "*";
            for(auto i:cp.commonNeigh)
            cout << i<<" ";
            cout <<endl;*/
            unsigned char lenght = 0;
            size_t last = cNNsize - 1;
            {
              // look if the last and the first elemente are NN
              auto commonNeigh = find (
                cluster.getNNs (cp.commonNeighs[last]).begin (),
                cluster.getNNs (cp.commonNeighs[last]).end (),
                cp.commonNeighs[0]);
              if (
                commonNeigh != cluster.getNNs (cp.commonNeighs[last]).end ()) {
                ++lenght;
              }
            }
            for (size_t i = 0; i < last; ++i) {
              // calculates the max lenght with this configuration
              if (cp.t > (lenght + (cNNsize - i - 1))) {
                break; // interrupt the loop if i can't find a greater cp.t
              }
              // cout <<i<<":"<< cp.commonNeigh[i] <<" "<<cp.commonNeigh[i-1];
              unsigned thisID = cp.commonNeighs[i];
              auto commonNeigh = find (
                cluster.getNNs (thisID).begin (),
                cluster.getNNs (thisID).end (), cp.commonNeighs[i + 1]);
              if (commonNeigh != cluster.getNNs (thisID).end ()) {
                ++lenght; // cout <<"true"<<endl;
              } else {
                if (lenght > cp.t) {
                  cp.t = lenght;
                }
                lenght = 0; // cout <<"false"<<endl;
              }
              // cout <<cp.commonNeigh.size()<<": "<< lenght <<endl;
            }
            if (lenght > cp.t) {
              cp.t = lenght;
            }
          } while (std::next_permutation (
            cp.commonNeighs.begin (), cp.commonNeighs.end ()));
        } // loop on couples
      }
      return couples;
      // for future purpose:
      // bulk atoms:
      // FCC 12 x 421
      // HCP  6 x 421, 6 x 422
      // ICO 12 x 555 (only the very center of the icosahedron)
      // BCC  8 x 666, 6 x 444
    }

    void printCNASignatures (const std::vector<BHCouple> &couples) {
      // NB: here there is some code duplication from
      // BHClusterAnalyser::extractCNASignatures
      std::map<short, int> signatures;
      // cout << "Couples: " << couples.size() <<endl;
      for (const BHCouple &cp : couples) {
        short signature = static_cast<short> (cp.r * 100 + cp.s * 10 + cp.t);
        // cp.debug();
        /*    if(signature==422)
            cout << cp <<endl;*/
        if (signatures.find (signature) == signatures.end ()) {
          signatures[signature] = 1;
        } else {
          ++signatures[signature];
        }
      }

      std::cout << "N of couples: " << couples.size () << std::endl;
      for (const auto &it : signatures) {
        cout << ((it.first == 0) ? "000" : std::to_string (it.first)) << "\t"
             << it.second << "\t->\t"
             << it.second / static_cast<double> (couples.size ()) << endl;
      }
    }

    std::unordered_map<short, int>
    extractCNASignatures (const std::vector<BHCouple> &couples) {
      std::unordered_map<short, int> signatures;
      for (const BHCouple &cp : couples) {
        short signature = static_cast<short> (cp.r * 100 + cp.s * 10 + cp.t);
        // cp.debug();
        if (signatures.find (signature) == signatures.end ()) {
          signatures[signature] = 1;
        } else {
          ++signatures[signature];
        }
      }
      /*
     if (plotAllOnScreen) {
      std::cout << "N of couples: " << couples.size () << std::endl;
      for (const auto &it : signatures) {
        cout << ((it.first == 0) ? "000" : std::to_string (it.first)) << "\t"
             << it.second << "\t->\t" << it.second / double(couples.size ())
             << endl;
      }
    }
    */
      return signatures;
    }

    void monoatomicTrajectory (
      string atomName,
      string fname,
      unsigned nat,
      BHdouble *x,
      std::string comment,
      bool append,
      BHdouble *v) {
      std::ofstream f (fname, (append) ? std::ios::app : std::ios::out);
      BHClusterAtoms cluster (nat);
      for (auto &atom : cluster) {
        atom.setType (atomName);
      }
      cluster.AssignFromVector (x);
      f << cluster.getNofAtoms () << '\n'
        << cluster.Composition () << " " << comment
        << " Lattice=\"0 0 0 10 10 10\" Properties=species:S:1:pos:R:3 ";
      //<< "\tProperties=species:S:1:pos:R:3";
      unsigned i = 0;
      for (auto &atom : cluster) {
        atom.setType (atomName);
        f << "\n" << atom;
        if (v)
          f << '\t' << v[3 * i] << '\t' << v[3 * i + 1] << '\t' << v[3 * i + 2];
        ++i;
      }

      f << std::endl;
    }

    std::string plotClusterWithComment (
      const BHClusterAtoms &cluster, std::string comment) {
      std::stringstream data;
      data << cluster.getNofAtoms () << '\n'
           << cluster.Composition () << " " << comment
           << " Lattice=\"0 0 0 10 10 10\" Properties=species:S:1:pos:R:3 ";
      //<< "\tProperties=species:S:1:pos:R:3";
      for (const auto &atom : cluster) {
        data << "\n" << atom;
      }
      return data.str ();
    }
  } // namespace BHClusterUtilities
} // namespace BH
