/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief This is the main file for testing the island collapser

   @file islemain.cxx
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 21/3/2017
   @version 1.0
*/
#include <fstream>
#include <iostream>

#include "BHclusterUtilities.hpp"
#include "BHconstructors.hpp"
#include "BHenergyCalculator_SMATB.hpp"
#include "BHmetalParameters.hpp"
#include "BHworkWithFortran.hpp"

using namespace std;
using namespace BH;

int main () {
  try {
    rndEngine my_rnd_engine (123456789);
    BHMetalParameters bhmp{"MetalParameters.in", true};
    BHWalkerSettings wl = loadBHWalkerSettings ("input_IsleWalker.in");
    BHCluster cluster (getSeedFromSettings (wl, my_rnd_engine));
    BHMinimizationAlgorithm *minimizator = new
#ifdef __DOMINIMIZATION_WITH_FIRE
      BHminimizationAlgorithm_FIRE ();
#else
      BHMinimizationAlgorithm_LBFGSB ();
#endif //__DOMINIMIZATION_WITH_FIRE
    BHEnergyCalculator_SMATB ec (cluster, bhmp);
    // cluster.initialize();
    // BHWWF::sendClusterToFortran(cluster);
    ofstream out ("out.xyz");
    out << cluster << endl;
    // this imitates the initialization routine
    {
      minimizator->MinimizeCluster (cluster, &ec);

      unsigned int n = BHClusterUtilities::CountIslands (cluster, bhmp);
      cout << "**********************************************\n";
      cout << "Number of clusters before collapsing: " << n << endl;
      // cluster->islands_collapser();island collapse in the initializer
      while (n > 1) {
        out << cluster << endl;
        BHClusterUtilities::IslandsCollapser (cluster, bhmp, my_rnd_engine);
        minimizator->MinimizeCluster_tol (cluster, &ec);
        n = BHClusterUtilities::CountIslands (cluster, bhmp);
      }
      cout << "Number of clusters after collapsing:  " << n << endl;
      cout << "**********************************************\n";
    }
    out << cluster << endl;
    out.close ();
    ///@todo add times for minimizations
    // cluster.times (std::cout);
    std::cout << std::endl;
    delete minimizator;
  } catch (const char *problem) {
    cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m" << endl;
  }
  return 0;
}
