/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief A small utility to create fragments of bulk crystal
   @file lattice.cxx
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 13/9/2017
   @version 0.1
*/
#include "BHcluster.hpp"
#include "BHclusterAnalyser.hpp"
#include "BHclusterUtilities.hpp"
#include "BHmetalParameters.hpp"
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>

using namespace BH;

void BCC (BHVector &a1, BHVector &a2, BHVector &a3) {
  a1 = BHVector::directionVersor ({-1, 1, 1});
  a2 = BHVector::directionVersor ({1, -1, 1});
  a3 = BHVector::directionVersor ({1, 1, -1});
}

void FCC (BHVector &a1, BHVector &a2, BHVector &a3) {
  a1 = BHVector::directionVersor ({0, 1, 1});
  a2 = BHVector::directionVersor ({1, 0, 1});
  a3 = BHVector::directionVersor ({1, 1, 0});
}

/// HCP vectors creator, needs a Base!
void HCP (BHVector &a1, BHVector &a2, BHVector &a3) {
  a1 = 0.5 * BHVector (1, -sqrt (3.), 0);
  a2 = 0.5 * BHVector (1, sqrt (3.), 0);
  a3 = BHVector (0, 0, sqrt (8. / 3.));
}

void analyzeAndPrintSignatures (
  BHCluster &cluster,
  const BHMetalParameters &bhmp,
  BHClusterAnalyser &analysis) {
  bhmp.setInteractionLabels (cluster);
  analysis.calcNeighbourhood_tol (cluster, bhmp);
  std::vector<BHCouple> couples =
    BHClusterUtilities::calculateCNAsignatures (cluster);
  BHClusterUtilities::printCNASignatures (couples);
}

int main (int /*argc*/, char ** /*argv*/) {
  BH::BHMetalParameters bhmp ("MetalParameters.in", true);
  std::string name = bhmp.getAtomName (0);
  std::cout << "Using: " << name << std::endl;
  const double NNdist = bhmp.getNNdist (name + name);
  // BCC lattice

  BHVector a1, a2, a3;

  // BCC routine:
  std::vector<BHAtom> atoms;
  BCC (a1, a2, a3);
  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 4; ++j) {
      for (int k = 0; k < 4; ++k) {
        // if(!((j+k)==2&&j!=k)&&!(i==1&&j==0)){
        atoms.emplace_back (
          name, NNdist * ((i - 1) * a1 + (j - 1) * a2 + (k - 1) * a3));
        //}
      }
    }
  }
  for (int i = 2; i < 4; ++i) {
    for (int j = 0; j < 4; ++j) {
      for (int k = 0; k < 4; ++k) {
        // if(!((j+k)==2&&j!=k)){
        atoms.emplace_back (name, NNdist * ((i - 1) * a1 + (j)*a2 + (k)*a3));
        //}
      }
    }
  }
  BHCluster BCC (
    BHClusterAtoms (static_cast<unsigned> (atoms.size ()), atoms.data ()));
  atoms.clear ();

  // FCC routine:
  FCC (a1, a2, a3);

  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 4; ++j) {
      for (int k = 0; k < 4; ++k) {
        // if(!((j+k)==2&&j!=k)&&!(i==1&&j==0)){
        atoms.emplace_back (
          name, NNdist * ((i - 1) * a1 + (j - 2) * a2 + (k - 1) * a3));
        //}
      }
    }
  }
  for (int i = 2; i < 4; ++i) {
    for (int j = 0; j < 4; ++j) {
      for (int k = 0; k < 4; ++k) {
        // if(!((j+k)==2&&j!=k)){
        atoms.emplace_back (
          name, NNdist * ((i - 1) * a1 + (j - 2) * a2 + (k - 2) * a3));
        //}
      }
    }
  }
  BHCluster FCC (
    BHClusterAtoms (static_cast<unsigned> (atoms.size ()), atoms.data ()));
  atoms.clear ();
  /// HCP routine
  HCP (a1, a2, a3);
  /*  BHVector b1 =NNdist*(1./3. * a1 + 2./3. * a2 + 1./4. * a3),
      b2 = NNdist*(2./3. * a1 + 1./3. * a2 + 3./4.* a3);*/
  BHVector b1 = {0, 0, 0},
           b2 = NNdist * 0.5 * (BHVector (1., 1 / (std::sqrt (3.)), 0) + a3);

  for (int i = 0; i < 4; ++i) {
    for (int j = 0; j < 4; ++j) {
      for (int k = 0; k < 2; ++k) {
        BHVector pos = NNdist * ((i - 1) * a1 + (j - 1) * a2 + (k)*a3);
        atoms.emplace_back (name, pos + b1);
        atoms.emplace_back (name, pos + b2);
      }
    }
  }

  BHCluster HCP (
    BHClusterAtoms (static_cast<unsigned> (atoms.size ()), atoms.data ()));
  atoms.clear ();

  std::vector<BHOrderParameter> OPs = {
    BHOrderParameter ("422"), BHOrderParameter ("421")};
  BHClusterAnalyser analysis (OPs, bhmp);
  std::cout << "\n***************\nCNA\n***************\n";

  std::cout << "\n***************HCP***************\n";
  analyzeAndPrintSignatures (HCP, bhmp, analysis);

  std::cout << "\n***************FCC***************\n";
  analyzeAndPrintSignatures (FCC, bhmp, analysis);

  std::cout << "\n***************BCC***************\n";
  analyzeAndPrintSignatures (BCC, bhmp, analysis);

  std::ofstream fBCC ("BCC.xyz");
  fBCC << BCC << std::endl;
  fBCC.close ();
  std::ofstream fFCC ("FCC.xyz");
  fFCC << FCC << std::endl;
  fFCC.close ();
  std::ofstream fHCP ("HCP.xyz");
  fHCP << HCP << std::endl;
  fHCP.close ();

  return 0;
}
