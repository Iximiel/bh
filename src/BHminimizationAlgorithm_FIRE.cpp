/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Definition of class BHMinimizationAlgorithm_FIRE

This class cointain the Fast Inertial Relaxation Engine (FIRE, see
http://users.jyu.fi/~pekkosk/resources/pdf/FIRE.pdf)

  @file BHminimizationAlgorithm_FIRE.cpp
  @author Daniele Rapetti (iximiel@gmail.com)

  @date 12/11/2019
  @version 0.1

  Standard minimization is done, needs some beef up the tol minimization

@date 16/03/2021
    @version 0.5

Following adding backtracking on P<0
https://doi.org/10.1016/j.commatsci.2020.109584

@date 8/06/2021
    @version 0.6

Changed leapfrog to velocity verlet, now FIRE follow more accurately
https://doi.org/10.1016/j.commatsci.2020.109584, the main difference is that the
integration happens before the eventual dt recalculation
*/

#include "BHminimizationAlgorithm_FIRE.hpp"
//#define FIREDBG
#ifdef FIREDBG
#include <iostream>

#include "BHclusterUtilities.hpp"
#endif

namespace BH {

  constexpr BHdouble UnitOfLength = 1e-10; // 1e-10 m = 1 Angstrom
  constexpr BHdouble UnitOfTime = 1e-12;   // 1e-12 s = 1 ps
  constexpr BHdouble UnitOfSpeed = UnitOfLength / UnitOfTime;
  constexpr BHdouble amu2Kg = 1.660539040e-27;
  constexpr BHdouble eV2J = 1.6021766208e-19;
  constexpr BHdouble fTol = 5e-6;
  // This convert sqrt([eV]/[amu]) to UnitOfSpeed
  // static const BHdouble kbTonAmuFact = /*std::sqrt(eV2J/amu2Kg)**/
  // UnitOfSpeed;
  // This simply convert the Kinetic energy in eV from [amu]*([A]/[ps])^2
  // static const BHdouble KinFact = amu2Kg / eV2J * (UnitOfSpeed *
  // UnitOfSpeed);
  // this multiplies gradient[eV/Ang] * dt[ps]/mass[amu] in order to convert in
  // [A]/[ps^2]
  constexpr BHdouble FdtOnMFact = eV2J / amu2Kg / (UnitOfSpeed * UnitOfSpeed);

  constexpr unsigned MaxStep = 4000;

  constexpr BHdouble startingDt = 1e-3; // in ps
  constexpr BHdouble increaseDt = 1.1;
  constexpr BHdouble decreaseDt = 0.5;
  constexpr BHdouble maxDt = startingDt * 10;
  constexpr BHdouble minDt = startingDt * 0.01;
  constexpr BHdouble alphaStart = 0.25;
  constexpr BHdouble alphaDecrease = 0.99;
  constexpr BHdouble arbitraryFireMass = 208.0;
  constexpr BHdouble dtOn2Fact = 0.5 * FdtOnMFact / arbitraryFireMass;
  constexpr unsigned positiveStepCountDelay = 20;

  BHMinimizationAlgorithm_FIRE::BHMinimizationAlgorithm_FIRE () = default;
  BHMinimizationAlgorithm_FIRE::~BHMinimizationAlgorithm_FIRE () = default;
  BHdouble BHMinimizationAlgorithm_FIRE::Minimization (
    BHdouble *x, const unsigned &nat, BHEnergyCalculator *calculator) {
    const unsigned N = 3 * nat;
    BHdouble *grad = new BHdouble[N];
    // initialize Gradient
    BHdouble f = calculator->Gradient (x, grad);
    BHdouble *v = new BHdouble[N];
    // initialize speed to 0;
    for (unsigned i = 0; i < N; i += 3) {
      v[i] = 0.0;
      v[i + 1] = 0.0;
      v[i + 2] = 0.0;
    }
    unsigned step = 0;

    // v is 0 at start
    BHdouble P = 0.0;
    BHdouble vv;
    BHdouble ff;

    BHdouble dt = startingDt;

    BHdouble dtOn2Force = dt * dtOn2Fact;
    BHdouble dtOn2;
    BHdouble alpha = alphaStart;
    unsigned positiveStepCount = 0;

    // working aids
    BHdouble vvOnff;
    BHdouble oneMinusAlpha;
#ifdef FIREDBG
    BHClusterUtilities::monoatomicTrajectory (
      "Ag", "fireTraj.xyz", nat, x, "F=0.0 V=0.0 P=0.0 dT=0.0 A=0.0", false, v);
#endif
    // using a velocity verlet
    while (step < MaxStep) {
      ++step;

      vv = 0.0;
      ff = 0.0;
      // velocity Verlet:
      // v(t+.5dt) and accumulating vv and ff for the mixing procedure
      // remeber: we are using the gradient, so -g=f
      for (unsigned i = 0; i < N; i += 3) {
        v[i] -= dtOn2Force * grad[i];
        v[i + 1] -= dtOn2Force * grad[i + 1];
        v[i + 2] -= dtOn2Force * grad[i + 2];
        vv += v[i] * v[i] + v[i + 1] * v[i + 1] + v[i + 2] * v[i + 2];
        ff += grad[i] * grad[i] + grad[i + 1] * grad[i + 1] +
              grad[i + 2] * grad[i + 2];
      }

      vvOnff = std::sqrt (vv / ff) * alpha;
      oneMinusAlpha = 1 - alpha;
      // rescaling v(t+.5dt) trough mixing and calculating x(t+dt)
      for (unsigned i = 0; i < N; i += 3) {
        v[i] = oneMinusAlpha * v[i] - grad[i] * vvOnff;
        v[i + 1] = oneMinusAlpha * v[i + 1] - grad[i + 1] * vvOnff;
        v[i + 2] = oneMinusAlpha * v[i + 2] - grad[i + 2] * vvOnff;
        x[i] += dt * v[i];
        x[i + 1] += dt * v[i + 1];
        x[i + 2] += dt * v[i + 2];
      }
      f = calculator->Gradient (x, grad);
      // convergence on function value value:
      /*double tol=std::abs (f - fOld) / (std::abs (f) + std::abs (fOld));
      if (std::abs (f - fOld) / (std::abs (f) + std::abs (fOld)) < fTol)
        break;
*/
      if (std::sqrt (ff) / N < fTol)
        break;
      P = 0.0;
      // updating to v(x+dt) and calculating P(t+dt) for the next step
      for (unsigned i = 0; i < N; i += 3) {
        v[i] -= dtOn2Force * grad[i];
        v[i + 1] -= dtOn2Force * grad[i + 1];
        v[i + 2] -= dtOn2Force * grad[i + 2];
        P -= grad[i] * v[i] + grad[i + 1] * v[i + 1] + grad[i + 2] * v[i + 2];
      }
#ifdef FIREDBG
      BHClusterUtilities::monoatomicTrajectory (
        "Ag", "fireTraj.xyz", nat, x,
        "F=" + std::to_string (std::sqrt (ff) / N) + " V=" +
          std::to_string (std::sqrt (vv) / N) + " P=" + std::to_string (P) +
          " dT=" + std::to_string (dt) + " A=" + std::to_string (alpha),
        true, v);
#endif
      if (P > 0.0) {
        ++positiveStepCount;
        if (positiveStepCount > positiveStepCountDelay) {
          dt = std::min (dt * increaseDt, maxDt);
          dtOn2Force = dt * dtOn2Fact;
          // dtOn2 = dt * 0.5;
          alpha *= alphaDecrease;
        }
      } else { // P<=0
        positiveStepCount = 0;
        dt = std::max (decreaseDt * dt, minDt);
        dtOn2Force = dt * dtOn2Fact;
        dtOn2 = dt * 0.5;
        alpha = alphaStart;
        // backtracking and zeroing the velocity
        for (unsigned i = 0; i < N; i += 3) {
          x[i] -= v[i] * dtOn2;
          x[i + 1] -= v[i + 1] * dtOn2;
          x[i + 2] -= v[i + 2] * dtOn2;
          v[i] = 0.0;
          v[i + 1] = 0.0;
          v[i + 2] = 0.0;
        }
      }
    }
    delete[] grad;
    delete[] v;
#ifdef FIREDBG
    cin.get ();
#endif
    return f;
  }

  BHdouble BHMinimizationAlgorithm_FIRE::Minimization_tol (
    BHdouble *x, const unsigned &nat, BHEnergyCalculator *calculator) {
    return Minimization (x, nat, calculator);
  }
} // namespace BH
