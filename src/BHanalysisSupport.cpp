/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Definitions for BHAnalysisSupport

   @file BHanalysisSupport.cpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 22/02/2018
   @version 0.2

   added the onefile mode

   @date 22/02/2018
   @version 0.1
*/

#include "BHanalysisSupport.hpp"

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <numeric>
#include <sstream>
#include <utility>

static const size_t BIGN = std::numeric_limits<std::streamsize>::max ();

namespace BH {

  BHAnalyzedClusterData::BHAnalyzedClusterData (
    std::string Name, BHdouble newE, int N_OPs)
    : Name_ (std::move (Name)),
      E_ (newE),
      OPs_ (size_t (N_OPs)) {}

  bool BHAnalyzedClusterData::operator== (const BHAnalyzedClusterData &x) {
    return Name_ == x.Name_;
  }

  BHAnalysisSupport::BHAnalysisSupport () = default;

  void BHAnalysisSupport::OrderDataByOP (const unsigned int &selOP) {
    std::sort (
      data_.begin (), data_.end (),
      [selOP] (const BHAnalyzedClusterData &i, const BHAnalyzedClusterData &j)
        -> bool { return i.OPs_[selOP] < j.OPs_[selOP]; });
  }

  /**this function loads a file that ust contain all the data needed for the
     analysis:

     The format of the file must be:
     ~~~
     filename Energy OP1.. OPn
     ~~~
     the number of OP is determined by reading the first line
  */
  void BHAnalysisSupport::loadDataFromFile (
    const std::string &energyFileName, bool reset) {
    // if reset is true wipe out the data
    if (reset) {
      data_.clear ();
    }
    // setting up the flags for the analysis
    ready1D_ = false;
    ready2D_ = false;
    // now we can add the clusters to the data:
    std::ifstream energyFile (energyFileName);
    if (!energyFile) {
      throw "cannot find \"" + energyFileName + "\"";
    }
    // determining the number of OPs:
    // the first line of analysis.out is the header:
    // name energy name_op1 name_op2...
    unsigned NOPs = 0;
    { // this isolates the used variables
      std::string dummy;
      getline (energyFile, dummy);
      std::stringstream ss;
      ss.str (dummy);
      while (ss) {
        std::string tmp;
        ss >> tmp;
        if (!tmp.empty ()) {
          ++NOPs;
        }
      }
      NOPs -= 2; // remove the 'name' column
    }

    while (energyFile.peek () != EOF) {
      std::string tName;
      BHdouble tEnergy;
      energyFile >> tName >> tEnergy;
      if (!tName.empty ()) {
        data_.emplace_back (tName, tEnergy, NOPs);
        for (unsigned i = 0; i < NOPs;
             ++i) { // REMEMBER COLUMN INDEX IS DIFFERENT
                    // FROM ARRAY INDEX (ai=ci-1)
          energyFile >> data_.back ().OPs_[i];
        }
        data_.back ().ready_ = true;
      }
      // std::cout << tName << " "<<data_.size() <<std::endl;
    }
    energyFile.close ();
  }

  /**this function loads the two files as how it was intended in the original
     ppanalyser main program, it is maintained for compatibility with legacy
     analysis.

     energyFile is the file with the energies in the format
     ~~~
     filename Energy other...
     ~~~
     OPfile is the file with the OPs, the format is
     ~~~
     filename OP1.. OPn
     ~~~
     the number of OP is determined by reading the first line
  */
  void BHAnalysisSupport::loadDataFromFiles (
    const std::string &energyFileName,
    const std::string &OPFileName,
    bool reset) {
    // if reset is true wipe out the data
    if (reset) {
      data_.clear ();
    }
    // setting up the flags for the analysis
    ready1D_ = false;
    ready2D_ = false;
    // determining the number of OPs:
    std::ifstream OPFile (OPFileName);
    if (!OPFile) {
      throw "cannot find \"" + OPFileName + "\"";
    }
    // the first line of analysis.out is the header:
    // it should be:
    // name name_op1 name_op2...
    int NOPs = 0;
    { // this isolates the used variables
      std::string dummy;
      getline (OPFile, dummy);
      std::stringstream ss;
      ss.str (dummy);
      while (ss) {
        std::string tmp;
        ss >> tmp;
        if (!tmp.empty ()) {
          ++NOPs;
        }
      }
      --NOPs; // remove the 'name' column
    }
    // now we can add the clusters to the data:
    std::ifstream energyFile (energyFileName);
    if (!energyFile) {
      throw "cannot find \"" + energyFileName + "\"";
    }
    energyFile.ignore (BIGN, '\n'); // ignores the header
    // we asre interested in the first two colums
    while (energyFile.peek () != EOF) {
      std::string tName;
      BHdouble tEnergy;
      energyFile >> tName >> tEnergy;
      data_.emplace_back (tName, tEnergy, NOPs);
      energyFile.ignore (BIGN, '\n'); // ignores the others data
    }
    energyFile.close ();
    // now that energies are loaded we can load OPs
    // and, if it is necessary delete unwanted datas
    size_t nmissing = 0;
    size_t NofClusters = data_.size ();
    // loading Order Parameters to Data_
    for (; NofClusters > 0 && OPFile.peek () != EOF; --NofClusters) {
      std::string dummy;
      OPFile >> dummy;
      BHAnalyzedClusterData dummydata (dummy);
      auto Known = std::find (data_.begin (), data_.end (), dummydata);
      if (Known != data_.end ()) {
        // REMEMBER COLUMN INDEX IS DIFFERENT FROM ARRAY INDEX (ai=ci-1)
        for (unsigned i = 0; i < unsigned (NOPs) - 1; ++i) {
          OPFile >> Known->OPs_[i];
        }
        Known->ready_ = true;
      } else {
        ++nmissing;
        std::cerr << "\033[1;35mWARNING: \033[0m\033[1m" << dummy
                  << " does not have OP data\033[0m" << std::endl;
      }
      OPFile.ignore (BIGN, '\n'); // ignores the others data
    }
    OPFile.close ();
    nmissing += NofClusters;
    if (nmissing > 0) {
      // putting in the back of the vector the clusters with non OP infos
      std::sort (
        data_.begin (), data_.end (),
        [] (const BHAnalyzedClusterData &i, const BHAnalyzedClusterData &j)
          -> bool { return i.ready_ && (!j.ready_); });
      std::cout << "Clusters with no OPs info: " << nmissing << std::endl;
      std::cout << "Size before deleting: " << data_.size ();

      data_.erase (data_.end () - static_cast<long> (nmissing), data_.end ());

      std::cout << ", size after deleting: " << data_.size () << std::endl;
    }
  }

  void BHAnalysisSupport::addCluster (
    const std::string &Name, BHdouble newE, unsigned N_OPs) {
    ready1D_ = false;
    ready2D_ = false;
    data_.emplace_back (Name, newE, N_OPs);
  }

  void BHAnalysisSupport::setClusterReadyForAnalysis (
    const unsigned dataID, const bool set) {
    ready1D_ = false;
    ready2D_ = false;
    data_.at (dataID).ready_ = set;
  }

  std::string &BHAnalysisSupport::getClusterName (const unsigned i) {
    ready1D_ = false;
    ready2D_ = false;
    return data_.at (i).Name_;
  }
  BHdouble &
  BHAnalysisSupport::getClusterOP (const unsigned dataID, const unsigned OPID) {
    ready1D_ = false;
    ready2D_ = false;
    // this is not efficient, but this is postprocessing code
    size_t oldDim = data_.at (dataID).OPs_.size ();
    if (OPID >= oldDim) {
      data_.at (dataID).OPs_.resize (OPID + 1);
    }
    return data_.at (dataID).OPs_[OPID];
  }

  size_t BHAnalysisSupport::getNofData () const { return data_.size (); }
  /*
    BHAnalyzedClusterData BHAnalysisSupport::getCluster(int i) const {
    return data_.at(i);
    }*/

  void BHAnalysisSupport::oneDimensionAnalysis (
    const unsigned OP_selected, const unsigned nbins) {
    std::cerr << "BHAnalysis::starting 1D analysis" << std::endl;
    size_t NofClusters (data_.size ());
    // this will order the data by the selected OP
    OrderDataByOP (OP_selected);
    // setting up the analysis
    oneDData_.bestMinima.clear ();
    // max value of an OP:1
    // min value:0
    oneDData_.step = 1. / nbins;
    oneDData_.OPsel = OP_selected;

    // std::cout << nbins << " " <<step <<std::endl;
    unsigned clusterID = 0;

    for (unsigned bin = 0; bin < nbins; ++bin) {
      double inferiorLim = oneDData_.step * (bin);
      if (bin == 0) {
        inferiorLim = -1;
      }
      double superiorLim = oneDData_.step * (bin + 1);
      double bestMinHere = std::numeric_limits<BHdouble>::max ();
      ///@todo: resolve the >= problem with a more double-style approach
      ///[(a-b)<tollerance]
      while (data_[clusterID].OPs_[OP_selected] > inferiorLim &&
             data_[clusterID].OPs_[OP_selected] <= superiorLim) {
        if (data_[clusterID].E_ < bestMinHere && data_[clusterID].ready_) {
          bestMinHere = data_[clusterID].E_;
          oneDData_.bestMinima[bin] = clusterID;
        }
        ++clusterID;
        if (clusterID >= NofClusters) {
          break;
        }
      }
      if (clusterID >= NofClusters) {
        break;
      }
    }
    // std::cerr << oneDData_.bestMinima.size();
    ready1D_ = true;
  }

  void
  BHAnalysisSupport::oneDimensionAnalysis2OutFile (const std::string &fname) {
    if (ready1D_) {
      std::ofstream outfile (fname);
      outfile << "name\tbin\tOPmin\tOPmax\tE\tOPval" << '\n';
      for (auto &data : oneDData_.bestMinima) {
        unsigned bin = data.first;
        unsigned minID = data.second;
        outfile << data_[minID].Name_ << "\t" << bin << std::fixed
                << std::setprecision (5) << "\t" << bin * oneDData_.step << "\t"
                << (bin + 1) * oneDData_.step << std::fixed
                << std::setprecision (9) << "\t" << data_[minID].E_ << "\t"
                << data_[minID].OPs_[oneDData_.OPsel] << '\n';
      }
    } else {
      throw "BHAnalysisSupport::oneDimensionAnalysis2OutFile Data is not ready";
    }
  }
  void BHAnalysisSupport::oneDimensionAnalysis2OutData (
    std::vector<BHdouble> &OPvals, std::vector<BHdouble> &Energies) {
    if (ready1D_) {
      // std::cerr << "BHAnalysis" << std::endl;
      for (auto &data : oneDData_.bestMinima) {
        unsigned minID = data.second;
        Energies.push_back (data_[minID].E_);
        OPvals.push_back (data_[minID].OPs_[oneDData_.OPsel]);
      }
    } else {
      throw "BHAnalysisSupport::oneDimensionAnalysis2OutData Data is not ready";
    }
  }

  void BHAnalysisSupport::twoDimensionAnalysis (
    const unsigned OP_selected[], const unsigned nbins[]) {
    std::cerr << "BHAnalysis::starting 2D analysis" << std::endl;
    auto NofClusters (data_.size ());
    // sorting the clusters on the first order parameter
    // this should accelerate the analysis
    OrderDataByOP (OP_selected[0]);
    unsigned startingID (0);

    twoDData_.bestMinima.clear ();
    twoDData_.OPsel[0] = OP_selected[0];
    twoDData_.OPsel[1] = OP_selected[1];

    twoDData_.step[0] = 1. / nbins[0];
    twoDData_.step[1] = 1. / nbins[1];
    // std::cout << step[0] << " " <<step[1] <<std::endl;
    for (unsigned bin0 (0); bin0 < nbins[0]; ++bin0) {
      BHdouble inferiorLim0 = twoDData_.step[0] * (bin0);
      if (bin0 == 0) {
        inferiorLim0 = -1;
      }
      BHdouble superiorLim0 = twoDData_.step[0] * (bin0 + 1);
      // std::cout <<startingID<<": " << data_[startingID].OPs[OP_selected[0]]
      // <<">=" <<inferiorLim0 <<std::endl;
      for (unsigned bin1 (0); bin1 < nbins[1]; ++bin1) {
        double inferiorLim1 = twoDData_.step[1] * (bin1);
        if (bin1 == 0) {
          inferiorLim0 = -1;
        }
        BHdouble superiorLim1 = twoDData_.step[1] * (bin1 + 1);
        // if true prints the bin position
        bool found = false;
        BHdouble bestMinHere = std::numeric_limits<double>::max ();
        unsigned bestID;
        for (unsigned clusterID (startingID);
             clusterID < NofClusters &&
             data_[clusterID].OPs_[OP_selected[0]] > inferiorLim0 &&
             data_[clusterID].OPs_[OP_selected[0]] <= superiorLim0;
             ++clusterID) {
          if (
            data_[clusterID].OPs_[OP_selected[1]] > inferiorLim1 &&
            data_[clusterID].OPs_[OP_selected[1]] <= superiorLim1) {
            if (data_[clusterID].E_ < bestMinHere) {
              found = true;
              bestID = clusterID;
              bestMinHere = data_[clusterID].E_;
            }
          }
        } // loop on clusters

        if (found) {
          twoDData_.bestMinima.emplace_back (bin0, bin1, bestID);
        }
      } // loop on bin1
      // advance the startID
      while (startingID < NofClusters &&
             data_[startingID].OPs_[OP_selected[0]] <= superiorLim0) {
        ++startingID;
      }

      // std::cout <<bin0 <<": "<< startingID <<", "<<superiorLim0<<std::endl;
    } // loop on bin0
    ready2D_ = true;
  }

  void
  BHAnalysisSupport::twoDimensionAnalysis2OutFile (const std::string &fname) {
    if (ready2D_) {
      std::ofstream outfile (fname);
      // the outputfile will have the format:
      // fname OPval1 OPVal2 Energy binID1 binID2 centerBIN1 centerBIN2
      outfile << "name\tOPval1\tOPval2\tE\tbin0\tbin1\tcbin1\tcbin2" << '\n';
      for (auto b0b1ID : twoDData_.bestMinima) {
        unsigned bin0 = std::get<0> (b0b1ID);
        unsigned bin1 = std::get<1> (b0b1ID);
        unsigned bestID = std::get<2> (b0b1ID);
        BHdouble centerbin0 = twoDData_.step[0] * (bin0 + 0.5),
                 centerbin1 = twoDData_.step[1] * (bin1 + 0.5);
        outfile << data_[bestID].Name_ << std::fixed << std::setprecision (9)
                << "\t" << data_[bestID].OPs_[twoDData_.OPsel[0]] << "\t"
                << data_[bestID].OPs_[twoDData_.OPsel[1]] << "\t"
                << data_[bestID].E_ << "\t" << bin0 << "\t" << bin1 << "\t"
                << centerbin0 << "\t" << centerbin1 << '\n';
      }
    } else {
      throw "BHAnalysisSupport::twoDimensionAnalysis2OutFile Data is not ready";
    }
  }

  void BHAnalysisSupport::twoDimensionAnalysis2OutData (
    std::vector<BHdouble> &OP1vals,
    std::vector<BHdouble> &OP2vals,
    std::vector<BHdouble> &Energies) {
    if (ready2D_) {
      // std::cerr << "BHAnalysis" << std::endl;
      for (auto b0b1ID : twoDData_.bestMinima) {
        unsigned bestID = std::get<2> (b0b1ID);
        OP1vals.push_back (data_[bestID].OPs_[twoDData_.OPsel[0]]);
        OP2vals.push_back (data_[bestID].OPs_[twoDData_.OPsel[1]]);
        Energies.push_back (data_[bestID].E_);
      }
    } else {
      throw "BHAnalysisSupport::twoDimensionAnalysis2OutData Data is not ready";
    }
  }
} // namespace BH
