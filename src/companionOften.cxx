/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief The main program with the BH algorithm with the companionv2 addon

   @file companionvw.cxx
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 21/3/2017
   @version 1.0

   @date 11/3/2020
   @version 1.1
*/
#include <iostream>

#include "BHbasinHoppingCompanionOften.hpp"
#include "BHdebug.hpp"
#include "BHmainTemplate.hpp"
#ifdef _OPENMP
#include <omp.h>
#endif

using namespace std;

int main (int argc, char **argv) {
  int retval = 0;
  try {
#ifdef _OPENMP
    cout << "***********************WARNING********************************\n"
         << "Compiled with openMP for minimization speedup\n"
         << "Paralelization add some variations in the order of the sums,\n"
         << "so two consecutive runs could give slightly different results.\n"
         << "In fact results are not reproducible\n"
         //<< "Number of threads: " << omp_get_num_threads() <<'\n'
         << "Max number of threads: " << omp_get_max_threads () << '\n'
         << "**************************************************************"
         << endl;
#endif
    /*
        const char BHpplogo[] = "  ___ _  _   _     _\n"
                                " | _ ) || |_| |_ _| |_ __ \n"
                                " | _ \\ __ |_   _|_   _/ _|\n"
                                " |___/_||_| |_|   |_| \\__|\n"
                                "                          \n";
    std::cout << BHpplogo << std::endl;*/
    // select between Doom:
    //        http://patorjk.com/software/taag/#p=display&f=Doom&t=BH%2B%2B
    // and   Stop
    //        http://patorjk.com/software/taag/#p=display&f=Stop&t=BH%2B%2B
    retval = BH::BHexecutor<BH::BasinHoppingCompanionOften> (argc, argv);
  } catch (const char *problem) {
    cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m" << endl;
    retval = 1;
  } catch (const string &problem) {
    cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m" << endl;
    retval = 1;
  }
  return retval;
}
