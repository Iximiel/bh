/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @file BHvector.cpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @version 1.1
   @date 13/9/2017
   @brief Definition of class BHVector
*/

#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>

#include "BHvector.hpp"
namespace BH {
  // constructors
  BHVector::BHVector () = default;

  BHVector::BHVector (
    const BHdouble newX, const BHdouble newY, const BHdouble newZ)
    : coord_{newX, newY, newZ} {}

  BHVector::BHVector (const BHVector &obj)
    : coord_{obj.coord_[x], obj.coord_[y], obj.coord_[z]} {}
  BHVector::BHVector (BHVector &&obj) noexcept
    : coord_{obj.coord_[x], obj.coord_[y], obj.coord_[z]} {}

  BHVector &BHVector::operator= (const BHVector &other) {
    if (this != &other) { // protect against invalid self-assignment
      coord_[x] = other.coord_[x];
      coord_[y] = other.coord_[y];
      coord_[z] = other.coord_[z];
    }
    // by convention, always return *this
    return *this;
  }

  BHVector &BHVector::operator= (BHVector &&other) noexcept {
    if (this != &other) { // protect against invalid self-assignment
      coord_[x] = other.coord_[x];
      coord_[y] = other.coord_[y];
      coord_[z] = other.coord_[z];
    }
    // by convention, always return *this
    return *this;
  }

  BHVector::~BHVector () = default;

  BHVector &BHVector::operator+= (const BHVector &other) {
    if (this != &other) { // protect against invalid self-assignment
      coord_[x] += other.coord_[x];
      coord_[y] += other.coord_[y];
      coord_[z] += other.coord_[z];
    }
    // by convention, always return *this
    return *this;
  }

  BHVector &BHVector::operator-= (const BHVector &other) {
    if (this != &other) { // protect against invalid self-assignment
      coord_[x] -= other.coord_[x];
      coord_[y] -= other.coord_[y];
      coord_[z] -= other.coord_[z];
    }
    // by convention, always return *this
    return *this;
  }

  // accessing members
  BHdouble BHVector::X () const { return coord_[x]; }
  BHdouble BHVector::Y () const { return coord_[y]; }
  BHdouble BHVector::Z () const { return coord_[z]; }

  BHdouble BHVector::R () const { return abs (*this); }
  BHdouble BHVector::Theta () const {
    return std::atan (
      sqrt (coord_[x] * coord_[x] + coord_[y] * coord_[y]) / coord_[z]);
  }
  BHdouble BHVector::Phi () const {
    // atan2 keeps track of the quadrant
    return std::atan2 (coord_[y], coord_[x]);
  }
  // setting members
  void BHVector::X (BHdouble newX) { coord_[x] = newX; }
  void BHVector::Y (BHdouble newY) { coord_[y] = newY; }
  void BHVector::Z (BHdouble newZ) { coord_[z] = newZ; }

  void BHVector::setXYZ (
    const BHdouble newX, const BHdouble newY, const BHdouble newZ) {
    coord_[x] = newX;
    coord_[y] = newY;
    coord_[z] = newZ;
  }

  void BHVector::setXYZ (const BHdouble *newXYZ) {
    coord_[x] = newXYZ[x];
    coord_[y] = newXYZ[y];
    coord_[z] = newXYZ[z];
  }

  void BHVector::copyXYZto (BHdouble *XYZ) {
    XYZ[x] = coord_[x];
    XYZ[y] = coord_[y];
    XYZ[z] = coord_[z];
  }

  void BHVector::zero () {
    coord_[x] = 0;
    coord_[y] = 0;
    coord_[z] = 0;
  }

  BHdouble &BHVector::operator[] (const int i) {
    if (i >= 3 || i <= -1) {
      throw "BHVector::asked a wrong coordinate";
    }
    return coord_[i];
  }

  // various operators
  void BHVector::myswap (BHVector &other) {
    std::swap (coord_[x], other.coord_[x]);
    std::swap (coord_[y], other.coord_[y]);
    std::swap (coord_[z], other.coord_[z]);
  }

  void BHVector::swap (BHVector &other) {
    std::swap (coord_[x], other.coord_[x]);
    std::swap (coord_[y], other.coord_[y]);
    std::swap (coord_[z], other.coord_[z]);
  }

  BHVector BHVector::operator+ (const BHVector &other) const {
    return BHVector (
      coord_[x] + other.coord_[x], coord_[y] + other.coord_[y],
      coord_[z] + other.coord_[z]);
  }

  BHVector BHVector::operator- (const BHVector &other) const {
    return BHVector (
      coord_[x] - other.coord_[x], coord_[y] - other.coord_[y],
      coord_[z] - other.coord_[z]);
  }

  BHVector BHVector::operator- () const {
    return BHVector (-coord_[x], -coord_[y], -coord_[z]);
  }

  BHVector BHVector::operator% (const BHVector &other) const {
    BHdouble tx = coord_[y] * other.coord_[z] - coord_[z] * other.coord_[y];
    BHdouble ty = coord_[z] * other.coord_[x] - coord_[x] * other.coord_[z];
    BHdouble tz = coord_[x] * other.coord_[y] - coord_[y] * other.coord_[x];
    return BHVector (tx, ty, tz);
  }

  BHdouble BHVector::operator* (const BHVector &other) const {
    return (coord_[x] * other.coord_[x]) + (coord_[y] * other.coord_[y]) +
           (coord_[z] * other.coord_[z]);
  }

  BHVector BHVector::operator* (const BHdouble scalar) const {
    return BHVector (
      coord_[x] * scalar, coord_[y] * scalar, coord_[z] * scalar);
  }

  BHVector BHVector::operator/ (const BHdouble scalar) const {
    return BHVector (
      coord_[x] / scalar, coord_[y] / scalar, coord_[z] / scalar);
  }

  BHVector &BHVector::operator*= (const BHdouble scalar) {
    coord_[x] *= scalar;
    coord_[y] *= scalar;
    coord_[z] *= scalar;
    return *this;
  }

  BHVector &BHVector::operator/= (const BHdouble scalar) {
    coord_[x] /= scalar;
    coord_[y] /= scalar;
    coord_[z] /= scalar;
    return *this;
  }

  BHdouble BHVector::dist (const BHVector &other) const {
    return abs (*this - other);
  }

  BHdouble BHVector::dist2 (const BHVector &other) const {
    BHdouble dx = coord_[x] - other.coord_[x], dy = coord_[y] - other.coord_[y],
             dz = coord_[z] - other.coord_[z];
    return (dx * dx + dy * dy + dz * dz);
  }

  BHVector BHVector::RhoThetaPhi (BHdouble rho, BHdouble theta, BHdouble phi) {
    BHdouble thex = rho * sin (theta) * cos (phi);
    BHdouble they = rho * sin (theta) * sin (phi);
    BHdouble thez = rho * cos (theta);
    return BHVector (thex, they, thez);
  }

  BHVector
  BHVector::RandomVector (rndEngine &rng, BHdouble rMAX, BHdouble rmin) {
    static BHRealRND rndRho_ (0, 1.0);
    static BHRealRND rndTheta_ (-1, 1.); //(goes in 1-2*rnd(0,1)
    static BHRealRND rndPhi_ (0, 2 * M_PI);
    BHdouble rmin3 = rmin * rmin * rmin;
    BHdouble rMAX3 = rMAX * rMAX * rMAX;
    return RhoThetaPhi (
      cbrt (rmin3 - (rMAX3 - rmin3) * rndRho_ (rng)), acos (rndTheta_ (rng)),
      rndPhi_ (rng));
  }

  BHVector
  BHVector::directionVersorFromTo (const BHVector &from, const BHVector &to) {
    BHVector diff = to - from;
    return diff / abs (diff);
  }

  BHVector BHVector::directionVersor (const BHVector &theVec) {
    return theVec / abs (theVec);
  }

  BHVector BHVector::RotateAroundZ (const BHdouble theta) {
    BHdouble s = sin (theta);
    BHdouble c = cos (theta);
    BHVector toreturn (
      coord_[x] * c - coord_[y] * s, coord_[x] * s + coord_[y] * c, coord_[z]);
    return toreturn;
  }
  BHVector BHVector::RotateAroundY (const BHdouble theta) {
    BHdouble s = sin (theta);
    BHdouble c = cos (theta);
    BHVector toreturn (
      coord_[x] * c + coord_[z] * s, coord_[y], coord_[z] * c - coord_[x] * s);
    return toreturn;
  }
  BHVector BHVector::RotateAroundX (const BHdouble theta) {
    BHdouble s = sin (theta);
    BHdouble c = cos (theta);
    BHVector toreturn (
      coord_[x], coord_[y] * c - coord_[z] * s, coord_[y] * s + coord_[z] * c);
    return toreturn;
  }

  BHVector operator* (const BHdouble scalar, const BHVector &vector) {
    //  namespace BH{
    return (vector * scalar);
    //}
  }

} // namespace BH
// IO
std::istream &operator>> (std::istream &stream, BH::BHVector &obj) {
  BH::BHdouble td;
  stream >> td;
  obj.X (td);
  stream >> td;
  obj.Y (td);
  stream >> td;
  obj.Z (td);
  return stream;
}

std::ostream &operator<< (std::ostream &stream, const BH::BHVector &obj) {
  stream << obj.X () << " " << obj.Y () << " " << obj.Z ();
  return stream;
}

BH::BHdouble abs (const BH::BHVector &toabs) {
  return std::sqrt (toabs * toabs);
}
