/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @file signatureAnalyzer.cxx
   @brief Main program for signature Analysis

   This program needs two files in order to work properly.
   - *input_analysis.in* formatted:
   ~~~
   NumberOfOrderParametersToBeAnalyzed
   nameOfOP1
   nameOfOP2
   nameOfOP3
   ...
   ~~~
   - *MetalParameters.in* with the metal parameters

   SignatureAnalyser has two working mode:
   - to use **Trajectory analysis** add as an argumente the name of the file
   with the trajectory. Must be a list of cluster in xyz format
   - to use **list analysis** do an `ls *filestobeanalysed* > input_files.in`
   and then launch the program without any argument

   @author Daniele Rapetti (iximiel@gmail.com)

   @date 21/11/2017
   @version 0.9.1

   added documentation and a more flexible verbosity for future

   @date 20/4/2017
   @version 0.9
*/
#include <fstream>
#include <iomanip>
#include <iostream>

#include "BHclusterAnalyser.hpp"
#include "BHmetalParameters.hpp"

using namespace std;
using namespace BH;

void AnalyzeAndPlot (
  BH::BHClusterAnalyser &analyzer,
  BHCluster &cluster,
  const BH::BHMetalParameters &bhmp,
  std::ostream &stream,
  bool verbosity = false) {
  bhmp.setInteractionLabels (cluster);
  auto OPs = analyzer.ParameterAnalysis (cluster, bhmp);
  for (unsigned i = 0; i < analyzer.Npars (); ++i) {
    if (verbosity) {
      std::cout << '\t' << std::fixed << std::setprecision (9) << OPs[i];
    }
    stream << '\t' << std::fixed << std::setprecision (9) << OPs[i];
  }
  stream << std::endl;
  if (verbosity) {
    std::cout << std::endl;
  }
}

int main (int argc, char **argv) {
  try {
    bool verbosity = true;
    std::ifstream settings ("input_analysis.in");
    // organized as:
    /*N
      Ndefinitions of OP
    */
    if (!settings)
      throw "cannot find \"input_analysis.in\"";
    unsigned NOP;
    settings >> NOP;
    std::string getter;
    getline (settings, getter);
    std::vector<BHOrderParameter> opData (NOP);
    for (unsigned i = 0; i < NOP; ++i) {
      getline (settings, getter);
      size_t comments_pos = getter.find_first_of ("*#!");
      if (comments_pos != std::string::npos)
        getter.erase (comments_pos);
      opData[i].initialize (getter);
    }
    settings.close ();
    BH::BHMetalParameters bhmp ("MetalParameters.in", true);

    BHClusterAnalyser analyzer (opData, bhmp);
    std::ofstream results ("analysis.out");
    if (argc >= 2) {
      std::cout << "frame";
      results << "frame";
    } else {
      std::cout << "filename";
      results << "filename";
    }
    for (unsigned i = 0; i < NOP; ++i) {
      std::cout << '\t' << OparName (opData[i]);
      results << '\t' << OparName (opData[i]);
    }
    std::cout << '\n';
    results << std::endl;
    if (argc >= 2) {
      std::ifstream trajectory (argv[1]);
      if (!trajectory) {
        std::string err = "cannot find \"" + std::string (argv[1]) + "\"";
        throw err;
      }
      int frame = 0;
      do {
        if (verbosity) {
          std::cout << frame;
        }
        results << frame;
        ++frame;
        BHClusterAtoms tmpcluster;
        trajectory >> tmpcluster;
        BHCluster mycluster (tmpcluster);
        AnalyzeAndPlot (analyzer, mycluster, bhmp, results, verbosity);
        if ((frame % 100) == 0 && !verbosity) {
          std::cout << "I have just analyzed the cluster in the frame: "
                    << frame << '\r' << std::flush;
        }
        std::string dummy;
        getline (trajectory, dummy); // gets the \n
      } while (trajectory.peek () != EOF);

    } else {
      std::ifstream fileList ("input_files.in");
      if (!fileList)
        throw "cannot find \"input_files.in\"";
      // list of file, one per row, produced by ls > input_files.in for example
      int count (0);
      while (fileList) {
        std::string fileName;
        getline (fileList, fileName);
        std::ifstream cluster (fileName);
        if (cluster) {
          if (verbosity) {
            std::cout << fileName;
          }
          results << fileName;
          BHCluster mycluster (fileName);
          AnalyzeAndPlot (analyzer, mycluster, bhmp, results, verbosity);
          ++count;
          if ((count % 100) == 0 && !verbosity) {
            std::cout << "I have just analyzed the cluster in file: "
                      << fileName << '\r' << std::flush;
          }
        }
        cluster.close ();
      }
    }
    std::cout << std::endl;
  } catch (const char *problem) {
    std::cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m"
              << std::endl;
  }

  return 0;
}
