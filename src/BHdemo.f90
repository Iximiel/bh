function func_old (x,y,g)
  use iso_c_binding

  Implicit None
  
  Real(C_DOUBLE), Intent(in) :: x,y
  Real(C_double), Intent(out) :: g(2)
  Real(C_double) :: func_old
  
  g(1) = -4*(x - 2)**2*x*dexp(-x**2 - (y + 1)**2) &
       - 6*(5*x**3 + 5*y**3 -x)*x*dexp(-x**2 - y**2) &
       + 2./3.*(x + 1)*dexp(-(x + 1)**2 - y**2) &
       + 4*(x -2)*dexp(-x**2 - (y + 1)**2) + 3*(15*x**2 - 1)*dexp(-x**2 - y**2)

  g(2) =  -4*(x - 2)**2*(y+ 1)*dexp(-x**2 - (y + 1)**2) &
       - 6*(5*x**3 + 5*y**3 - x)*y*dexp(-x**2 - y**2) &
       + 45*y**2*dexp(-x**2 - y**2) + 2./3.*y*dexp(-(x + 1)**2 - y**2)

  func_old=2*(2-x)**2*exp(-(x**2)-(y+1)**2) &
       -15*(x/5-x**3-y**3)*exp(-x**2- y**2)-1./3.*exp(-(x+1)**2-y**2)
  return
end function func_old

function func (x,y,g)
  use iso_c_binding

  Implicit None
  
  Real(C_DOUBLE), Intent(in) :: x,y
  Real(C_double), Intent(out) :: g(2)
  Real(C_double) :: func

  g(1)=1.3d0*cos(1.3d0*x)*cos(0.9d0*y) -0.2d0*y*sin(0.2d0*x*y) &
       -0.8d0*sin(0.8d0*x)*sin(1.9d0*y)

  g(2)=1.9*cos(0.8d0*x)*cos(1.9d0*y) -0.2d0*x*sin(0.2d0*x*y) &
       -0.9d0*sin(1.3*x)*sin(0.90*y)

  func = sin(1.3d0*x)*cos(0.9d0*y)+cos(0.8d0*x)*sin(1.9d0*y)+cos(x*y*0.2d0)
  return
end function func

Subroutine demoBH(data,val,val_min)bind(C,name="demoBH")
  use iso_c_binding
  !USE PARAMETERS
  !USE BH_MODULE
  !USE FUNCTION_PAR 

  Implicit None

  Real(8), Parameter :: ftoll=1.e-7_8

  Real(C_DOUBLE), Intent(in) :: data(2)
  Real(C_DOUBLE) :: data_i(2)
  Real(C_DOUBLE), Intent(out) :: val,val_min
  Real(C_double) :: func
  ! Needed by the minimization routine setulb()
  Character(60) :: task, csave
  Logical :: lsave(4)
  !number_of_parameters = 3*nat3D
  !Integer :: n, m, iprint, nbd(number_of_parameters), &
  !     iwa(3*number_of_parameters)
  Integer :: n, m, iprint, nbd(2), iwa(2)
  Integer :: isave(44)
  Real(8) :: f, factr, pgtol
  Real(8) :: dsave(29)
  Real(8), Allocatable :: wa(:)
  Real(8) :: l(2)
  Real(8) :: u(2)
  Real(8) :: g(2)!gradiente
  ! local 
  Integer, Parameter :: itmax=2000 !The maximum number of calls to setulb
  Real(8), Parameter :: epsi_lbfgsb=1.e-50_8 !Close to zero, used to check that 
  !setulb does not return the same energy
  !as in the previous step
  Real(8) :: rtol,funzval_old,df,funzval
  Integer :: iter !It counts the number of calls to Setulb()


  n = 2
  data_i=data
  iprint = -1 ! We suppress the default output.
  ! We suppress both code-supplied stopping tests because the
  ! user is providing his own stopping criteria.
  factr=1.0d0 !1.0d+7
  pgtol=0.d0 !1.0d-5

  ! We now specify nbd which defines the bounds on the variables:
  !                l   specifies the lower bounds,
  !                u   specifies the upper bounds.
  !     nbd(i)=0 if x(i) is unbounded,
  !            1 if x(i) has only a lower bound,
  !            2 if x(i) has both lower and upper bounds, and
  !            3 if x(i) has only an upper bound.

  !l(1:n)=0.000000000000001d0
  !u(1:n)=0.999999999999999d0
  nbd(1:n)=0 !unbounded minimization

  ! settaggio delle dimensioni del problema e delle 'limited memory corrections'
  ! N.B. valori raccomandati dagli autori per m: 3 <= m <= 20

  m=7 ! m is the number of limited memory corrections

  ! allocations/initializations
  Allocate(wa(2*m*n+4*n+12*m*m+12*m))
  iter=0
  iwa=0
  wa=0.d0
  isave=0
  dsave=0.d0
  lsave=.False.

!  write(*,*) data
  !do i=1,N
   !  write(*,*) data(i),data(i+N),data(i+2*N)
  !enddo
  !stop
  !serve per la routine di minimizzazione che vuole argomenti dbl-prc
  !call BHbigvoi(data)
  !call cpu_time(start)
  funzval = func(data(1),data(2),g)
  val = funzval
  !call cpu_time(finish)
  !print '("mygradientTime = ",f6.3," seconds.")',finish-start
  !write(*,*)funzval
  !!furbescamente faccio in modo che force_rgl mi ritorni automaticamente il gradiente, su un unico array
  funzval_old = funzval

  !We start the iteration by initializing task.
  task = 'START'
  !------- the beginning of the loop ----------
  DO
     IF(iter.Gt.itmax)EXIT!!!while loop f90 style
     iter=iter+1
     f = funzval
     call setulb(n,m,data,l,u,nbd,f,g,factr,pgtol,wa,iwa,task,iprint,&
          csave,lsave,isave,dsave)

     !in origine qua gli atomi venivano mandato al modulo

     !
     !call BHbigvoi(data)
     funzval=func(data(1),data(2),g)
     !write(*,*) f, funzval,f-funzval

     IF (task(1:5) .eq. 'NEW_X') THEN
        ! the minimization routine has returned with a new iterate.
        ! At this point have the opportunity of stopping the iteration 
        ! or observing the values of certain parameters
        df=abs(funzval_old-funzval)
        rtol=2._8*df/(abs(funzval_old)+abs(funzval))
        funzval_old=funzval
        !   se la tolleranza e' soddisfatta o il numero massimo
        !   di iterazioni e' raggiunto il migliore individuo
        !   trovato sino ad ora viene restituito in P0 e funzval
        If((iter.Ge.itmax).and.(df.gt.epsi_lbfgsb)) Then
           Deallocate(wa)
           open (22,file='errors_bhdemo.out',status='unknown',position='append')
           write(22,*)'WARNING:'
           !write(22,*)'At step ',ind_mc, 
           write(22,*)'the minimization exits with iter > itmax but task=NEW_X'
           write(22,*)data_i
           close(22)
           val_min = func(data(1),data(2),g)
           Return
        Else If((rtol.Lt.ftoll).and.(df.gt.epsi_lbfgsb)) Then
           Deallocate(wa)
           val_min = func(data(1),data(2),g)
           Return
        EndIf ! chiude If(rtol.Lt.ftoll) Then
     ENDIF ! chiude else if (task(1:5) .eq. 'NEW_X') then   
  ENDDO!while(iter.Lt.itmax)
  !if i got here iter>itmax and setulb did not returned 
  Deallocate(wa)
  open (22,file='errors_bhdemo.out',status='unknown',position='append')
  write(22,*)'WARNING:'
  !write(22,*)'At step ',ind_mc
  write(22,*)'the minimization exits with iter > itmax '
  write(22,*)data_i
  close(22)
  val_min = func(data(1),data(2),g)
End Subroutine demoBH


