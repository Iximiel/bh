/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#include <sstream>

#include "BHmoveBrownian.hpp"
#include "BHparsers.hpp"

#include "BHwalker.hpp" //thi is needed for the energy calculatior definition

#ifndef USESMATBGAUSS
#include "BHenergyCalculator_SMATB.hpp"
using EnergyCalculator = BH::BHEnergyCalculator_SMATB;
#else
#include "BHenergyCalculator_SMATB_GAUSS.hpp"
using EnergyCalculator = BH::BHEnergyCalculator_SMATB_GAUSS;
#endif

namespace BH {
  BHMoveBrownian::BHMoveBrownian () : BHMove ("brownian") {}

  BHMoveBrownian::BHMoveBrownian (const BHMoveBrownian &other)
    : BHMove (other),
      NofAtoms_{other.NofAtoms_},
      NumberOfSteps_{other.NumberOfSteps_},
      BrownianTemperature_{other.BrownianTemperature_},
      cbltz_{other.cbltz_},
      Friction_{other.Friction_},
      TimeStep_{other.TimeStep_},
      BrownianMass_{other.BrownianMass_},
      x_{new BHdouble[3 * NofAtoms_]},
      v_{new BHdouble[3 * NofAtoms_]},
      gradient_{new BHdouble[3 * NofAtoms_]},
      energyCalculator_ (other.energyCalculator_->clone ()) {}

  BHMoveBrownian::~BHMoveBrownian () {
    delete[] x_;
    delete[] v_;
    delete[] gradient_;
    delete energyCalculator_;
  }

  std::string BHMoveBrownian::DefaultString () {
    return "prob = 1.0, accTemp = 1500, steps = 500, brownianTemperature = "
           "2000";
  }

  std::unique_ptr<BHMove> BHMoveBrownian::clone () {
    return std::unique_ptr<BHMove>{new BHMoveBrownian (*this)};
  }

  std::string BHMoveBrownian::printSettingsSpecialized () const {
    std::stringstream ss;
    ss << "steps: " << NumberOfSteps_
       << ", BrownTemp: " << BrownianTemperature_;
    return ss.str ();
  }

  std::string BHMoveBrownian::printSettingsSpecializedForInput () const {
    std::stringstream ss;
#define outputwriter(variable)                                                 \
  ", " << BHParsers::BHMV::BROWNIAN[static_cast<size_t> (                      \
            BHParsers::BHMV::BROWNIANvariable::variable)]                      \
       << " = " << variable
    ss << outputwriter (NumberOfSteps_) << outputwriter (BrownianTemperature_);
#undef outputwriter
    return ss.str ();
  }

  void BHMoveBrownian::Specialize (
    const BHCluster &in, const BHMetalParameters &bhmp) {
    NofAtoms_ = in.getNofAtoms ();
    delete[] x_;
    x_ = new BHdouble[3 * NofAtoms_];
    delete[] v_;
    v_ = new BHdouble[3 * NofAtoms_];
    delete[] gradient_;
    gradient_ = new BHdouble[3 * NofAtoms_];
    delete[] energyCalculator_;
    energyCalculator_ = new EnergyCalculator (in, bhmp);
  }

  bool BHMoveBrownian::doMoveAlgorithm (
    BHCluster &in,
    BHCluster &out,
    BHClusterAnalyser &,
    const BHMetalParameters &,
    rndEngine &rng) {
    /*      factv=dsqrt(cbltz*temp_brow/BrownianMass_)
     fact_bro_1=dsqrt(2.d0*cbltz*temp_brow*Friction_*TimeStep__bro/(BrownianMass_))*1d10
     !Angs/sec fact_bro_2=(16.d0/arete(1))*TimeStep__bro/BrownianMass_*/
    // some constants:
    const BHdouble fact_v =
                     sqrt (cbltz_ * BrownianTemperature_ / BrownianMass_),
                   fact_b1 = sqrt (
                               2. * (cbltz_ * BrownianTemperature_) *
                               (Friction_ * TimeStep_) / BrownianMass_) *
                             1e10, // convert to Angs/sec
      fact_b2 = 16. * TimeStep_ /
                BrownianMass_; // in the original BH here is divided by arete,
                               // but i have discarded that feature
    /*cout << fact_v <<endl
  << fact_b1 <<endl
  << fact_b2 <<endl;*/
    for (unsigned int j = 0; j < NofAtoms_; j++) {
      x_[3 * j] = in[j].X ();
      x_[3 * j + 1] = in[j].Y ();
      x_[3 * j + 2] = in[j].Z ();
      v_[3 * j] = fact_v * Gaussian_ (rng);
      v_[3 * j + 1] = fact_v * Gaussian_ (rng);
      v_[3 * j + 2] = fact_v * Gaussian_ (rng);
    }
    // then i proceed with the evolution of the system:

    energyCalculator_->Neighbours_for_tol (x_);
    for (unsigned int step = 0; step < NumberOfSteps_;
         step++) { // this cannot be a parallel loop
      if (step % 500 == 0) {
        energyCalculator_->Neighbours_for_tol (x_);
      }
      energyCalculator_->Gradient_tol (x_, gradient_);

      // this is an Euler-Cromer evolution:
      for (unsigned int i = 0; i < NofAtoms_;
           i++) { // this can be parallel but i need a way to protect rng
        v_[3 * i] += fact_b1 * Gaussian_ (rng) -
                     (Friction_ * TimeStep_) * v_[3 * i] -
                     fact_b2 * gradient_[3 * i];
        v_[3 * i + 1] += fact_b1 * Gaussian_ (rng) -
                         (Friction_ * TimeStep_) * v_[3 * i + 1] -
                         fact_b2 * gradient_[3 * i + 1];
        v_[3 * i + 2] += fact_b1 * Gaussian_ (rng) -
                         (Friction_ * TimeStep_) * v_[3 * i + 2] -
                         fact_b2 * gradient_[3 * i + 2];
        x_[3 * i] += v_[3 * i] * TimeStep_;
        x_[3 * i + 1] += v_[3 * i + 1] * TimeStep_;
        x_[3 * i + 2] += v_[3 * i + 2] * TimeStep_;
      }
      // think about other types of evolution
      /*/DELETE
   for (unsigned int i=0; i<NofAtoms_; i++) {//this can be parallel but i need a
   way to protect rng Atoms_[i].X(x[3*i]); Atoms_[i].Y(x[3*i+1]);
   Atoms_[i].Z(x[3*i+2]);
   }
   std::ofstream f("ani.xyz",std::ofstream::out|std::ofstream::app);
   f << *this <<'\n';
   f.close();
   //DELETE
   */
    }
    out.AssignFromVector (x_);
    return true;
  }
  bool BHMoveBrownian::parseSpecialized (const std::string &parsed) {
    bool toreturn = false;
    try {
#define inputgetter(variable)                                                  \
  BHParsers::parse (                                                           \
    BHParsers::BHMV::BROWNIAN[static_cast<size_t> (                            \
                                BHParsers::BHMV::BROWNIANvariable::variable)]  \
      .c_str (),                                                               \
    parsed, variable)
#define inputgetteralt(variable)                                               \
  BHParsers::parse (                                                           \
    BHParsers::BHMV::BROWNIAN                                                  \
      [static_cast<size_t> (BHParsers::BHMV::BROWNIANvariable::variable##alt)] \
        .c_str (),                                                             \
    parsed, variable)
      toreturn |= inputgetter (NumberOfSteps_);
      toreturn |= inputgetter (BrownianTemperature_);
      toreturn |= inputgetteralt (BrownianTemperature_);
      toreturn |= inputgetter (Friction_);
      toreturn |= inputgetter (TimeStep_);
      toreturn |= inputgetteralt (TimeStep_);
      toreturn |= inputgetter (BrownianMass_);
      toreturn |= inputgetteralt (BrownianMass_);
      toreturn |= inputgetter (cbltz_);
#undef inputgetter
#undef inputgetteralt
    } catch (const std::invalid_argument & /*ia*/) {
      throw typeName () + "::Invalid argument: \"" + parsed + "\"";
    }
    return toreturn;
  }
} // namespace BH
