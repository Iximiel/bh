/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#include "BHmoveShake.hpp"
#include "BHdebug.hpp"
#include "BHparsers.hpp"
#include <algorithm>
#include <iomanip>
#include <sstream>

namespace BH {
  BHMoveShake::BHMoveShake ()
    : BHMove ("shake"),
      Rmin3_ (Rmin_ * Rmin_ * Rmin_) {}

  BHMoveShake::~BHMoveShake () = default;

  std::string BHMoveShake::printSettingsSpecialized () const {
    std::stringstream ss;
    ss << "Rmin->Rmax: " << Rmin_ << "->" << Rmax_;
    return ss.str ();
  }

  std::string BHMoveShake::printSettingsSpecializedForInput () const {
    std::stringstream ss;
#define outputwriter(variable)                                                 \
  ", " << BHParsers::BHMV::SHAKE[static_cast<size_t> (                         \
            BHParsers::BHMV::SHAKEvariable::variable)]                         \
       << " = " << variable
    ss << outputwriter (Rmin_) << outputwriter (Rmax_);
#undef outputwriter
    return ss.str ();
  }

  std::string BHMoveShake::DefaultString () {
    return "prob = 1.0, accTemp = 300, Rmin = 0, Rmax = 1.4";
  }

  std::unique_ptr<BHMove> BHMoveShake::clone () {
    return std::unique_ptr<BHMove>{new BHMoveShake (*this)};
  }

  bool BHMoveShake::doMoveAlgorithm (
    BHCluster &in,
    BHCluster &out,
    BHClusterAnalyser &,
    const BHMetalParameters &,
    rndEngine &rng) {
    unsigned int NAt = in.getNofAtoms ();
    for (unsigned int i = 0; i < NAt;
         i++) { // uniformly distribuited in a sphere
      out[i] = in[i] + BHVector::RhoThetaPhi (
                         cbrt (Rmin3_ + (rndRho_) (rng)),
                         acos (rndTheta_ (rng)), rndPhi_ (rng));
    }
    return true;
  }

  bool BHMoveShake::parseAfter () {
    if (Rmax_ <= Rmin_) {
      throw typeName () + "::Rmax is less or equal to Rmin";
    }
    Rmin3_ = Rmin_ * Rmin_ * Rmin_;
    BHdouble Rdiff = Rmax_ * Rmax_ * Rmax_ - Rmin3_;
    rndRho_.param (BHRealRND::param_type{0, Rdiff});
    return true;
  }

  bool BHMoveShake::parseSpecialized (const std::string &parsed) {
    bool toreturn = false;
    try {
#define inputgetter(variable)                                                  \
  BHParsers::parse (                                                           \
    BHParsers::BHMV::SHAKE[static_cast<size_t> (                               \
                             BHParsers::BHMV::SHAKEvariable::variable)]        \
      .c_str (),                                                               \
    parsed, variable)
      toreturn |= inputgetter (Rmin_);
      toreturn |= inputgetter (Rmax_);
#undef inputgetter
    } catch (const std::invalid_argument & /*ia*/) {
      throw typeName () + "::Invalid argument: \"" + parsed + "\"";
    }
    return toreturn;
  }
} // namespace BH
