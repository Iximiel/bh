/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Here the definitions the basin hopping algorithm flying/landong with
best walkers

@file BHbasinHoppingFlyingLandingWBestWalker.cpp
    @author Daniele Rapetti (iximiel@gmail.com)

    @date 05/01/2020
    @version 1.0
    adding the best walker to the standar FL bh algorithm
        */

#include "BHclusterUtilities.hpp"
#include "BHconstructors.hpp"
#include "BHmetalParameters.hpp"
#include <iomanip>
#include <iostream>
#include <sstream>

#include "BHbasinHoppingFLallBW.hpp"
namespace BH {

  const char *BHBasinHoppingFlyingLandingAllWBestWalker::staticLogo () {
    return " ______  _     _  ___ _       _           \n"
           "(____  \\| |   | |/ __) |     | |          \n"
           " ____)  ) |__ | | |__| | ____| | _  _ _ _ \n"
           "|  __  (|  __)| |  __) |/ _  | || \\| | | |\n"
           "| |__)  ) |   | | |  | ( ( | | |_) ) | | |\n"
           "|______/|_|   |_|_|  |_|\\_||_|____/ \\____|\n";
  }

  const char *BHBasinHoppingFlyingLandingAllWBestWalker::logo () {
    return staticLogo ();
  }

  BHBasinHoppingFlyingLandingAllWBestWalker::
    BHBasinHoppingFlyingLandingAllWBestWalker (const BHSettings &settings)
    : BHBasinHoppingFlyingLanding (settings) {}

  BHBasinHoppingFlyingLandingAllWBestWalker::
    ~BHBasinHoppingFlyingLandingAllWBestWalker () = default;

  const std::string BHBasinHoppingFlyingLandingAllWBestWalker::BHStyleName () {
    return "Flying-Landing-bestWalker";
  }
  void BHBasinHoppingFlyingLandingAllWBestWalker::PreOutputInitialization () {
    BHBasinHoppingFlyingLanding::PreOutputInitialization ();
    for (unsigned wID = 0; wID < settings_.nWalkers_; ++wID) {
      std::stringstream fname;
      fname << "bw" << std::setw (2) << std::setfill ('0') << wID;
      outputDifferentiator_.emplace_back (fname.str ());
    }
  }

  void BHBasinHoppingFlyingLandingAllWBestWalker::Initialization () {
    // this will call the base initialization an dhte F/L initialization
    BHBasinHoppingFlyingLanding::Initialization ();
    // now walkers_.size()==2*settings_.nWalkers_

    std::cout << "\nCreating the hiking walker from file \"bestWalker.in\"\n";
    BHWalkerSettings bestWalkerSettings =
      loadBHWalkerSettings ("bestWalker.in");
    if (
      bestWalkerSettings.ChosenAlgorithm_ == BH::BHwalkerAlgorithmType::obst) {
      throw "You cannot start a minimization using the hiking walkers as an"
            "obstacles";
    }
    for (unsigned wID = 0; wID < settings_.nWalkers_; ++wID) {
      std::cout << "\n###############Creating hiking walker for companion "
                << wID << std::endl;

      walkers_.push_back (BHConstructors::getConstructor ().createWalker (
        bestWalkerSettings, rndEngine_));
      // this should set up also the interaction labels
      walkers_.back ().copyAllConfigurations (walkers_[wID]);
      walkers_.back ().printSettings ();
      walkers_.back ().moveAccepted ();
      walkers_.back ().setDepth (2);
      acceptedMovesInfo_.push_back ({0, 0});
    }
  }

  void
  BHBasinHoppingFlyingLandingAllWBestWalker::MoveAcceptedPostProduction () {
    BHdouble newEne = walkers_[runnigWalker_].lastAcceptedEnergy ();
    if (
      (GloInfoPerWalker_[runnigWalker_].mcStep == mcStep_) &&
      //(runnigWalker_ < settings_.nWalkers_)
      walkers_[runnigWalker_].depth_ == 0) {
      unsigned companionID = settings_.nWalkers_ + runnigWalker_;
      unsigned hikingWalkerID = companionID + settings_.nWalkers_;
      BHdouble deltaFlying = EnergyAtDive_[runnigWalker_] - newEne;
      BHdouble deltaLanding = EnergyAtDive_[runnigWalker_] -
                              walkers_[companionID].lastAcceptedEnergy ();
      BHdouble eps = std::pow (
        std::abs (deltaFlying / deltaLanding), settings_.landingAlpha_);
      if (LandingRestart_ (rndEngine_) < eps) {
        if (
          walkers_[companionID].lastAcceptedEnergy () <
          walkers_[hikingWalkerID].lastAcceptedEnergy ()) {
          walkers_[hikingWalkerID].copyAcceptedConfiguration (
            walkers_[companionID]);
        }
        EnergyAtDive_[runnigWalker_] = newEne;
        walkers_[companionID].copyAcceptedConfiguration (
          walkers_[runnigWalker_]);
      }
    }
  }
} // namespace BH
