/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief The implementation of the functions to create clusters
   @file BHclusterCreatorUtilities.cpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 9/1/2019
   @version 0.1

   separated from main

*/
#include <cmath>
#include <iostream>

#include "BHclusterCreatorUtilities.hpp"

namespace BH {
  namespace TruncatedOctahedron {
    unsigned impiledSquareAtoms (unsigned edgeBase) {
      // sum i from 1 to N  (N-i)^2 on wolfram alpha
      // sums up to natd o tatocd in the original divided by 2
      // should be inlined?
      unsigned number =
        edgeBase * (1 - 3 * edgeBase + 2 * edgeBase * edgeBase) / 6;
      return number;
    }

    unsigned NatOctahedron (const unsigned edgeLenght) {
      // atoms in the pure octahedron
      unsigned toret =
        edgeLenght * edgeLenght + 2 * impiledSquareAtoms (edgeLenght);
      return toret;
    }

    unsigned NatOctahedronSurf (const unsigned edgeLenght) {
      unsigned toret =
        (edgeLenght - 1) * (edgeLenght - 1) + edgeLenght * edgeLenght;
      return toret;
    }

    unsigned NatTruncOct (const unsigned edgeLenght, const unsigned edgeCut) {
      unsigned toret = NatOctahedron (edgeLenght) -
                       6 * (impiledSquareAtoms (edgeCut) + edgeCut * edgeCut);
      return toret;
    }

    unsigned NofAtoms (const unsigned edgeLenght, const unsigned edgeCut) {
      return NatTruncOct (edgeLenght, edgeCut);
    }

    unsigned
    NatTruncOctSurf (const unsigned edgeLenght, const unsigned edgeCut) {
      unsigned toret =
        NatOctahedronSurf (edgeLenght) -
        ((edgeCut == 0)
           ? 0
           : (3 * (edgeCut * edgeCut + (edgeCut - 1) * (edgeCut - 1))));
      return toret;
    }

    std::vector<BHAtom> truncatedOctahedron (
      const unsigned edgeLenght,
      const unsigned edgeCut,
      const char *atomsType,
      const double AtomRadius) {
      const BHdouble retLenght = AtomRadius * 2.0 * sqrt (2.0);
      if (edgeLenght < edgeCut) {
        throw "edgeLenght < edgeCut";
      }

      /*    std::cout << "Number of atoms in octaedron:             " <<
         NatOctahedron     <<std::endl
            << "Surface atoms on the octaedron:           " << NatOctahedronSurf
         <<std::endl
            << "Number of atoms in truncated octaedron:   " << NatTrubcOct
         <<std::endl
            << "Surface atoms on the truncated octaedron: " << NatTrubcOctSurf
         <<std::endl;*/
      std::vector<BHAtom> toreturn (
        NatTruncOct (edgeLenght, edgeCut), BHAtom (atomsType, 0., 0., 0.));
      int kmax = static_cast<int> (edgeLenght - edgeCut);

      unsigned atomNum = 0;
      double shift = 0.0;
      int ijmax = static_cast<int> (edgeLenght);
      int l1 = edgeCut;
      int l4 = edgeLenght * 2 - edgeCut - 2;
      int l3 = edgeCut - edgeLenght + 1;
      int l2 = edgeLenght - edgeCut - 1;
      for (int k = 0; k < kmax; ++k) {
        for (int j = 0; j < ijmax; ++j) {
          for (int i = 0; i < ijmax; ++i) {
            int sp = i + j + k;
            int sm = i - j;
            if ((sp >= l1 && sp <= l4) && (sm >= l3 && sm <= l2)) {
              toreturn[atomNum].setXYZ (
                (i + shift) / sqrt (2.0), (j + shift) / sqrt (2.0), k / 2.0);
              toreturn[atomNum++] *= retLenght;
              if (k != 0) {
                toreturn[atomNum].setXYZ (
                  (i + shift) / sqrt (2.0), (j + shift) / sqrt (2.0), -k / 2.0);
                toreturn[atomNum++] *= retLenght;
              }
            }
          }
        }
        shift += 0.5;
        --ijmax;
      }
      return toreturn;
    }
  } // namespace TruncatedOctahedron

  namespace Icosahedron {
    BHVector createEdge (
      const unsigned I,
      const unsigned J,
      const unsigned L,
      const unsigned shell,
      BHVector *tvec) {
      BHVector supportVec =
        tvec[I] + (tvec[J] - tvec[I]) * (double (L + 1) / double (shell));
      return supportVec;
    }

    unsigned NatIcosahedron (const unsigned numberOfShells) {
      unsigned ntot =
        1 + (10 * numberOfShells * numberOfShells + 15 * numberOfShells + 11) *
              numberOfShells / 3;
      return ntot;
    }

    unsigned NofAtoms (const unsigned numberOfShells) {
      return NatIcosahedron (numberOfShells);
    }

    std::vector<BHAtom> icosahedron (
      const unsigned numberOfShells,
      const char *atomsType,
      const double AtomRadius) {
      const BHdouble retLenght = AtomRadius * 2.0 * sqrt (2.0);
      unsigned ntot = NatIcosahedron (numberOfShells);
      // std::cout <<ntot <<std::endl;
      std::vector<BHAtom> toreturn (ntot, BHAtom (atomsType, 0., 0., 0.));
      // todo:make this more efficient
      double tdef = (sqrt (5.0) + 1.0) / 2.0;
      double xdef = sqrt (2.0) / sqrt (1 + tdef * tdef);
      double rapp = sqrt (2.0 * (1.0 - tdef / (tdef * tdef + 1.0)));
      // can be looped, but I think that explicit will be faster
      toreturn[1].setXYZ (tdef, 1.0, 0.0);
      toreturn[2].setXYZ (tdef, -1.0, 0.0);
      toreturn[3].setXYZ (-tdef, 1.0, 0.0);
      toreturn[4].setXYZ (-tdef, -1.0, 0.0);
      toreturn[5].setXYZ (1.0, 0.0, tdef);
      toreturn[6].setXYZ (-1.0, 0.0, tdef);
      toreturn[7].setXYZ (1.0, 0.0, -tdef);
      toreturn[8].setXYZ (-1.0, 0.0, -tdef);
      toreturn[9].setXYZ (0.0, tdef, 1.0);
      toreturn[10].setXYZ (0.0, tdef, -1.0);
      toreturn[11].setXYZ (0.0, -tdef, 1.0);
      toreturn[12].setXYZ (0.0, -tdef, -1.0);
      /* cabalistic old loops
         for (int i=1;i<5;++i){
         toreturn[i  ].Z(0.0);
         toreturn[i+4].Y(0.0);
         toreturn[i+8].X(0.0);
         }

         for (int i:{1,2}) {
         toreturn[i  ].X(tdef);
         toreturn[i+2].X(-tdef);
         toreturn[i+4].X(1.0);
         toreturn[i+6].X(-1.0);
         toreturn[i+8].Y(tdef);
         toreturn[i+10].Y(-tdef);
         }
         for (int i=1;i<4;i+=2) {
         toreturn[i  ].Y(1.0);
         toreturn[i+1].Y(-1.0);
         toreturn[i+4].Z(tdef);
         toreturn[i+5].Z(-tdef);
         toreturn[i+8].Z(1.0);
         toreturn[i+9].Z(-1.0);
         }*/

      for (unsigned i = 1; i < 13; ++i) {
        toreturn[i] *= xdef * 0.5;
      }

      if (numberOfShells > 1) {
        double epsilon = 1e-2;
        // if i set them to bool they create a segmentation fault
        int naret[12][12], nfacet[12][12][12];
        // support vectors for caclulations
        BHVector tvec[12], supportVec;
        /*int ifacet=0,
          iaret=0,
          inatf=0;*/
        unsigned atomNum = 13;
        unsigned I, J;

        double dij2, dik2, djk2, delta;
        for (unsigned shell = 2; shell <= numberOfShells; ++shell) {
          double dssn2 = double (shell) * rapp / sqrt (2.0);
          dssn2 *= dssn2;
          for (unsigned i = 0; i < 12; ++i) { // vertex loop
            tvec[i] = double (shell) * toreturn[i + 1];
            toreturn[atomNum++] = tvec[i];
          }
          for (int i = 0; i < 12; ++i) {
            for (int j = (i + 1); j < 12; ++j) {
              naret[i][j] = false;
              for (int k = (j + 1); k < 12; ++k) {
                nfacet[i][j][k] = false;
              }
            }
          }

          for (unsigned i = 0; i < 12; ++i) {
            for (unsigned j = (i + 1); j < 12; ++j) {
              dij2 = tvec[i].dist2 (tvec[j]);
              delta = std::abs (dij2 - dssn2);
              if (delta > epsilon) {
                continue;
              }
              for (unsigned k = (j + 1); k < 12; ++k) {
                djk2 = tvec[k].dist2 (tvec[j]);
                delta = std::abs (djk2 - dssn2);
                if (delta > epsilon) {
                  continue;
                }
                dik2 = tvec[i].dist2 (tvec[k]);
                delta = std::abs (dik2 - dssn2);
                if (delta > epsilon) {
                  continue;
                }

                if (!nfacet[i][j][k]) {
                  nfacet[i][j][k] = true;

                  for (std::pair<unsigned, unsigned> p :
                       {std::make_pair (i, j), std::make_pair (i, k),
                        std::make_pair (j, k)}) {
                    I = p.first;
                    J = p.second;
                    if (!naret[I][J]) {
                      naret[I][J] = true;
                      for (unsigned l = 0; l < shell - 1; ++l) {
                        toreturn[atomNum++] = createEdge (I, J, l, shell, tvec);
                      }
                    }
                  }

                  for (unsigned l = 1; l < shell - 1; ++l) {
                    for (unsigned ll = 0; ll < l; ++ll) {
                      supportVec = createEdge (i, j, l, shell, tvec);
                      toreturn[atomNum++] =
                        supportVec +
                        (createEdge (i, k, l, shell, tvec) - supportVec) *
                          ((ll + 1) / double (l + 1));
                    }
                  }
                }
              }
            }
          }
        }
      }

      for (unsigned i = 0; i < ntot; ++i) {
        toreturn[i] *= retLenght;
      }

      return toreturn;
    }
  } // namespace Icosahedron

  namespace MarksDecahedron {
    BHVector shellForDeca (const int thisShell, const int column) {
      constexpr double angmin = -2.0 * 0.314159;
      static const double stepx = -2.0 * sin (angmin);
      constexpr double ang[4] = {
        2.0 * 3.14159 / 5.0,
        4.0 * 3.14159 / 5.0, // 2*2*3.14159/5.;
        6.0 * 3.14159 / 5.0, // 3*2*3.14159/5.;
        8.0 * 3.14159 / 5.0  // 4*2*3.14159/5.;
      };
      // this routine expects that columns is at maximum thisShell*5+4
      const int dx = (column % (thisShell + 1));
      BHVector toreturn (
        (thisShell + 1) * sin (angmin) + dx * stepx,
        -(thisShell + 1) * cos (angmin), 0.0);
      const int angId = /*static_cast<int>*/ (column / (thisShell + 1)) - 1;
      if (angId >= 0) {
        toreturn = toreturn.RotateAroundZ (ang[angId]);
      }
      return toreturn;
    }

    int NatDecahedron (const int m, const int n, const int p) {
      int ntot = ((30 * p * p * p - 135 * p * p + 207 * p - 102) +
                  (5 * m * m * m + (30 * p - 45) * m * m +
                   (60 * (p * p - 3 * p) + 136) * m) +
                  n * (15 * m * m + (60 * p - 75) * m +
                       3 * (10 * p * p - 30 * p) + 66)) /
                   6 -
                 1;
      return ntot;
    }
    int NofAtoms (const int m, const int n, const int p) {
      return NatDecahedron (m, n, p);
    }

    int getCutEdge_mp (const int m, const int p) { return m + 2 * p - 3; }

    int getNumberOfShells (const int cutEdge, const int n) {
      return cutEdge + n - 1;
    }

    int getM (const int cutEdge, const int p) { return cutEdge + 3 - 2 * p; }

    int getCutEdge_sn (const int numberOfShells, const int n) {
      return numberOfShells + 1 - n;
    }

    std::vector<BHAtom> decahedron (
      const int m,
      const int n,
      const int p,
      const char *atomsType,
      const double AtomRadius,
      const double dilatation) {
      // To you that are reading this function:
      // this was copied from a fortran code
      // every calculation here depends on the aproximation int to double
      // this function begin to the  "it works" category
      // then must be refactored
      // this is crazy
      const BHdouble retLenght = AtomRadius * 2.0;
      const int cutEdge = (getCutEdge_mp (m, p));
      const int numberOfShells = (getNumberOfShells (cutEdge, n));
      /*
        std::vector<BHAtom> decahedron(const int & numberOfShells, const int &
        n, const int & p, const char* atomsType, const double &retLenght){ const
        int cutEdge = getCutEdge_sn(numberOfShells,n); const int m       =
        getM(cutEdge,p);
      */
      const int ntot = NatDecahedron (m, n, p);
      /*
        std::cout << (30*p*p*p-135*p*p+207*p-102)/6. <<'\n'
        << ((5*m*m*m+(30*p-45)*m*m+(60*(p*p-3*p)+136)*m))/6.<<'\n'
        << (n*(15*m*m+(60*p-75)*m + 3*(10*p*p-30*p)+66))/6. <<'\n'
        <<  ")/6-1;"<<'\n';
      */
      if (ntot < 1) {
        throw "It is impossible to create a Mark's decahedron with this "
              "parameters combination";
      }
      /*std::cout << "Total number of atoms:" << ntot <<"\n"
        << "Number of shells:     " << numberOfShells <<"\n"
        << "m n p:                " << m <<" " << n << " "<< p <<"\n"
        << "Cut Number:           " << cutEdge <<std::endl;*/
      // placing atoms int the cluster
      std::vector<BHAtom> toreturn (
        size_t (ntot), BHAtom (atomsType, 0., 0., 0.));
      unsigned atomNum = 0;
      for (int shell = 0; shell <= numberOfShells; ++shell) {
        toreturn[atomNum++].setXYZ (
          0.0, 0.0,
          double (-numberOfShells / 2 + shell) -
            double (numberOfShells % 2) * 0.5);
      }

      double zshell;
      BHVector supportV;
      for (int shell = 0; shell < cutEdge - p + 1; ++shell) {
        zshell = ((numberOfShells - shell - 1) % 2) * 0.5 -
                 static_cast<int> ((numberOfShells - shell) * 0.5);
        for (int col = 0; col < 5 * (shell + 1); ++col) {
          supportV = dilatation * shellForDeca (shell, col);
          for (int atmID = 0; atmID < numberOfShells - shell; ++atmID) {
            toreturn[atomNum] = supportV;
            toreturn[atomNum++].Z (zshell + atmID);
          }
        }
      }

      int shift = 1;
      for (int shell = cutEdge - p + 1; shell < cutEdge; ++shell) {
        int colmax = shell + 1 - shift;
        zshell = ((numberOfShells - shell - 1) % 2) * 0.5 -
                 /*static_cast<int>*/ ((numberOfShells - shell) / 2);
        for (int i = 0; i < 5; ++i) {
          for (int col = shift + i; col <= colmax + i; ++col) {
            supportV = dilatation * shellForDeca (shell, col + i * shell);
            for (int atmID = 0; atmID < numberOfShells - shell; ++atmID) {
              toreturn[atomNum] = supportV;
              toreturn[atomNum++].Z (zshell + atmID);
            }
          }
        }
        shift += 1;
      }

      // scaling the positions to the reticole:
      for (auto &v : toreturn) {
        v *= retLenght;
      }
      return toreturn;
    }
  } // namespace MarksDecahedron
} // namespace BH
