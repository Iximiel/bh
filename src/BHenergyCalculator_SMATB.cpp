/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Definition of class BHEnergyCalculator_SMATB
   @file BHenergyCalculator_SMATB.cpp
   @author Daniele Rapetti (iximiel@gmail.com)


   @date 05/06/2017
   @version 1.2

   Interacting neighbours moved from here to base class
   Solved a nasty bug (energy summed 2 times in the vutoff regime) in Gradient

   @date 2/11/2017
   @version 1.1

   Corrected Bug on Tol: wrong neighbours were considered in the routine

   @date 23/10/2017
   @version 1.0

   Added Energy_tol form minimization
   @date 19/10/2017
   @version 0.4

   Gradient defined and working, divided base class from SMATB

   @date 17/10/2017
   @version 0.3

   energy defined and added a possible inheritance for different potential

   @date 16/10/2017
   @version 0.1

   constructor
*/
#include <cmath>
#include <cstring>
#include <iostream>
#include <map>

#include "BHenergyCalculator_SMATB.hpp"

// debugging:
// using std::cout;
// using std::endl;
namespace BH {
  BHEnergyCalculator_SMATB::BHEnergyCalculator_SMATB (
    const BHCluster &theCluster, const BHMetalParameters &bhmp)
    : BHEnergyCalculator (theCluster, bhmp),
      den_ (new BHdouble[nAtoms_]),
      Fb_ (new BHdouble *[nAtoms_]),
      p_ (new BHdouble[nInteractions_]),
      q_ (new BHdouble[nInteractions_]),
      a_ (new BHdouble[nInteractions_]),
      xi_ (new BHdouble[nInteractions_]),
      NNd_ (new BHdouble[nInteractions_]),
      cs_ (new BHdouble[nInteractions_]),
      ce_ (new BHdouble[nInteractions_]),
      ceSQ_ (new BHdouble[nInteractions_]),
      P5_ (new BHdouble[nInteractions_]),
      P4_ (new BHdouble[nInteractions_]),
      P3_ (new BHdouble[nInteractions_]),
      Q5_ (new BHdouble[nInteractions_]),
      Q4_ (new BHdouble[nInteractions_]),
      Q3_ (new BHdouble[nInteractions_]) {
    //    for (unsigned int j = 0; j < nAtoms_; ++j) {Fb_[j] = new
    //    BHdouble[nAtoms_];}
    {
      Fb_[0] = new BHdouble[nAtoms_ * nAtoms_];
      unsigned position = nAtoms_;
      for (unsigned int j = 1; j < nAtoms_; ++j) {
        Fb_[j] = &Fb_[0][position];
        position += nAtoms_;
      }
    }

    for (const auto &corrispondence : intName2ID_) {
      std::string MPindex = corrispondence.first;
      int i = corrispondence.second;
      // std::cout << MPindex << " -> " << i << std::endl;
      BHSMATBInteraction parameters =
        bhmp.giveSMATBInteractionParameters (MPindex);
      p_[i] = parameters.p;
      q_[i] = parameters.q;
      a_[i] = parameters.a;
      xi_[i] = parameters.qsi;
      NNd_[i] = parameters.NNdist;
      cs_[i] = parameters.cutOff_start;
      ce_[i] = parameters.cutOff_end;
      P3_[i] = parameters.P_par3;
      P4_[i] = parameters.P_par4;
      P5_[i] = parameters.P_par5;
      Q3_[i] = parameters.Q_par3;
      Q4_[i] = parameters.Q_par4;
      Q5_[i] = parameters.Q_par5;
      // bhmp.giveMetalPars ( MPindex, p_[i], q_[i],
      // a_[i], xi_[i], NNd_[i], cs_[i], ce_[i]);
      ceSQ_[i] = ce_[i] * ce_[i];
      IntNeighDistTol2_[i] = (ce_[i] + 1.0) * (ce_[i] + 1.0);
      /*std::cout <<MPindex<< '\n';
        std::cout << p_[i] << " " << q_[i] << " " << a_[i]
        << " " << xi_[i] << " " << NNd_[i]
        << " " << cs_[i] << " " << ce_[i] <<std::endl
        <<  ce2_[i] << " " << IntNeighDistTol2_[i] << std::endl;*/

      // bhmp.givePolyPars (MPindex, P5_[i], P4_[i],
      // P3_[i], Q5_[i], Q4_[i], Q3_[i]);
      /*
            std::cout << P5_[i] << " " << P4_[i] << " " << P3_[i]
                      << "\n" << Q5_[i] << " " << Q4_[i]
                      << " " << Q3_[i]  << std::endl;*/
    }
  }

  BHEnergyCalculator_SMATB::BHEnergyCalculator_SMATB (
    BHEnergyCalculator_SMATB &&other) noexcept
    : BHEnergyCalculator (std::move (other)),
      den_ (other.den_),
      Fb_ (other.Fb_),
      p_ (other.p_),
      q_ (other.q_),
      a_ (other.a_),
      xi_ (other.xi_),
      NNd_ (other.NNd_),
      cs_ (other.cs_),
      ce_ (other.ce_),
      ceSQ_ (other.ceSQ_),
      P5_ (other.P5_),
      P4_ (other.P4_),
      P3_ (other.P3_),
      Q5_ (other.Q5_),
      Q4_ (other.Q4_),
      Q3_ (other.Q3_) {
    other.den_ = nullptr;
    other.Fb_ = nullptr;
    other.p_ = nullptr;
    other.q_ = nullptr;
    other.a_ = nullptr;
    other.xi_ = nullptr;
    other.NNd_ = nullptr;
    other.cs_ = nullptr;
    other.ce_ = nullptr;
    other.ceSQ_ = nullptr;
    other.P5_ = nullptr;
    other.P4_ = nullptr;
    other.P3_ = nullptr;
    other.Q5_ = nullptr;
    other.Q4_ = nullptr;
    other.Q3_ = nullptr;
  }

  BHEnergyCalculator_SMATB::BHEnergyCalculator_SMATB (
    const BHEnergyCalculator_SMATB &other)
    : BHEnergyCalculator (other),
      den_ (new BHdouble[nAtoms_]),
      Fb_ (new BHdouble *[nAtoms_]),
      p_ (new BHdouble[nInteractions_]),
      q_ (new BHdouble[nInteractions_]),
      a_ (new BHdouble[nInteractions_]),
      xi_ (new BHdouble[nInteractions_]),
      NNd_ (new BHdouble[nInteractions_]),
      cs_ (new BHdouble[nInteractions_]),
      ce_ (new BHdouble[nInteractions_]),
      ceSQ_ (new BHdouble[nInteractions_]),
      P5_ (new BHdouble[nInteractions_]),
      P4_ (new BHdouble[nInteractions_]),
      P3_ (new BHdouble[nInteractions_]),
      Q5_ (new BHdouble[nInteractions_]),
      Q4_ (new BHdouble[nInteractions_]),
      Q3_ (new BHdouble[nInteractions_]) {
    {
      Fb_[0] = new BHdouble[nAtoms_ * nAtoms_];
      unsigned position = nAtoms_;
      for (unsigned int j = 1; j < nAtoms_; ++j) {
        Fb_[j] = &Fb_[0][position];
        position += nAtoms_;
      }
    }

    for (unsigned i = 0; i < nInteractions_; ++i) {
      p_[i] = other.p_[i];
      q_[i] = other.q_[i];
      a_[i] = other.a_[i];
      xi_[i] = other.xi_[i];
      NNd_[i] = other.NNd_[i];
      cs_[i] = other.cs_[i];
      ce_[i] = other.ce_[i];
      P3_[i] = other.P3_[i];
      P4_[i] = other.P4_[i];
      P5_[i] = other.P5_[i];
      Q3_[i] = other.Q3_[i];
      Q4_[i] = other.Q4_[i];
      Q5_[i] = other.Q5_[i];

      ceSQ_[i] = other.ceSQ_[i];
      // it is already udated in the base copy ctor
      // IntNeighDistTol2_[i] = other.IntNeighDistTol2_[i];
    }
  }

  BHEnergyCalculator_SMATB::~BHEnergyCalculator_SMATB () {
    // std::cout << "~BHEnergyCalculator_SMATB()" <<std::endl;
    delete[] den_;

    if (Fb_) {
      // for (unsigned int j = 0; j < nAtoms_; ++j) {delete[] Fb_[j]; }
      delete[] Fb_[0];
    }

    delete[] Fb_;

    delete[] p_;
    delete[] q_;
    delete[] a_;
    delete[] xi_;
    delete[] NNd_;
    delete[] cs_;
    delete[] ce_;
    delete[] ceSQ_;
    delete[] P5_;
    delete[] P4_;
    delete[] P3_;
    delete[] Q5_;
    delete[] Q4_;
    delete[] Q3_;
  }
  /// This Function returns the caclulation of pressure NOT DIVIDED BY THE
  /// VOLUME OF EACH ATOM, is up to the user to de do the last operation
  BHdouble BHEnergyCalculator_SMATB::Pressure (BHdouble *x, BHdouble *p) {
    // std::vector<BHdouble> den(nAtoms_,0);
    memset (
      den_, 0,
      nAtoms_ * sizeof (den_[0])); // should empty the array efficiently
    memset (
      p, 0,
      nAtoms_ * sizeof (BHdouble)); // should empty the array efficiently
    BHdouble /*ebi,*/ eri;
    BHdouble xik, yik, zik, dik, dik2, x_i, y_i, z_i;
    BHdouble /*dik0,*/ espo, qsiexpq, aexpp, dikm, dikm2, dikm3, dikm4, dikm5,
      pressBik, pressRik;
    BHdouble *pressB = new BHdouble[nAtoms_];
    memset (pressB, 0, nAtoms_ * sizeof (BHdouble));
    BHdouble energy (0);
    unsigned int INTID, i, k, mytype, IDi, IDk;
    for (i = 0; i < nAtoms_; ++i) {
      eri = 0;
      IDi = 3 * i;
      // BHdouble
      x_i = x[IDi];
      y_i = x[IDi + 1];
      z_i = x[IDi + 2];
      mytype = nTypes_ * typeList_[i];
      for (k = i + 1; k < nAtoms_; ++k) {
        // INTID=intID_[TAI(i,k)];
        INTID = intType_[mytype + typeList_[k]];
        // std :: cout <<mytype + typeList_[k]<<" "<< INTID << std::endl;
        // BHdouble
        IDk = 3 * k;
        xik = x[IDk] - x_i;
        yik = x[IDk + 1] - y_i;
        zik = x[IDk + 2] - z_i;
        // BHdouble
        dik2 = xik * xik + yik * yik + zik * zik;
        if (dik2 < ceSQ_[INTID]) {
          // BHdouble
          dik = sqrt (dik2);
          if (dik < cs_[INTID]) {
            // dik0=NNd_[INTID];
            espo = 1.0 - dik / NNd_[INTID]; // dik0;
            aexpp = exp (p_[INTID] * espo) * a_[INTID];
            eri += 2.0 * aexpp;
            qsiexpq = (xi_[INTID] * xi_[INTID]) * exp (2.0 * q_[INTID] * espo);
            pressBik = -q_[INTID] * qsiexpq / NNd_[INTID];
            pressRik = p_[INTID] * aexpp / NNd_[INTID];
          } else {
            dikm = dik - ce_[INTID];
            dikm2 = dikm * dikm;
            dikm3 = dikm2 * dikm;
            dikm4 = dikm3 * dikm;
            dikm5 = dikm4 * dikm;
            qsiexpq =
              Q5_[INTID] * dikm5 + Q4_[INTID] * dikm4 + Q3_[INTID] * dikm3;
            eri += 2.0 * (P5_[INTID] * dikm5 + P4_[INTID] * dikm4 +
                          P3_[INTID] * dikm3);

            pressRik =
              (5.0 * P5_[INTID] * dikm4 + 4.0 * P4_[INTID] * dikm3 +
               3.0 * P3_[INTID] * dikm2);

            pressBik =
              qsiexpq * (5.0 * Q5_[INTID] * dikm4 + 4.0 * Q4_[INTID] * dikm3 +
                         3.0 * Q3_[INTID] * dikm2);
            qsiexpq = qsiexpq * qsiexpq;
          }
          pressBik *= dik;
          pressRik *= dik;

          pressB[i] += pressBik;
          pressB[k] += pressBik;

          p[i] += pressRik;
          p[k] += pressRik;

          den_[i] += qsiexpq;
          den_[k] += qsiexpq;
        }
      }

      // ebi=sqrt(den[i]);
      // den[i]=1.0/ebi;
      // energy=energy+(eri-ebi);
      energy += (eri - sqrt (den_[i]));
      p[i] += pressB[i] / sqrt (den_[i]);
    }
    delete[] pressB;
    return energy;
  }

  BHEnergyCalculator *BHEnergyCalculator_SMATB::clone () {
    return new BHEnergyCalculator_SMATB (*this);
  }

  BHdouble BHEnergyCalculator_SMATB::Energy (BHdouble *x) {
    // std::vector<BHdouble> den(nAtoms_,0);
    memset (
      den_, 0,
      nAtoms_ * sizeof (den_)); // should empty the array efficiently
    BHdouble /*ebi,*/ eri;
    BHdouble xik, yik, zik, dik, dik2, x_i, y_i, z_i;
    BHdouble /*dik0,*/ espo, qsiexpq, aexpp, dikm, /*dikm2,*/ dikm3, dikm4,
      dikm5;
    BHdouble energy (0);
    unsigned int INTID, i, k, mytype, IDi, IDk;
    for (i = 0; i < nAtoms_; ++i) {
      eri = 0;
      IDi = 3 * i;
      // BHdouble
      x_i = x[IDi];
      y_i = x[IDi + 1];
      z_i = x[IDi + 2];
      mytype = nTypes_ * static_cast<unsigned> (typeList_[i]);
      for (k = i + 1; k < nAtoms_; ++k) {
        // INTID=intID_[TAI(i,k)];
        INTID = intType_[mytype + typeList_[k]];
        // std :: cout <<mytype + typeList_[k]<<" "<< INTID << std::endl;
        // BHdouble
        IDk = 3 * k;
        xik = x[IDk] - x_i;
        yik = x[IDk + 1] - y_i;
        zik = x[IDk + 2] - z_i;
        // BHdouble
        dik2 = xik * xik + yik * yik + zik * zik;
        if (dik2 < ceSQ_[INTID]) {
          // BHdouble
          dik = sqrt (dik2);
          if (dik < cs_[INTID]) {
            // dik0=NNd_[INTID];
            espo = 1.0 - dik / NNd_[INTID]; // dik0;
            aexpp = exp (p_[INTID] * espo) * a_[INTID];
            eri += 2.0 * aexpp;
            qsiexpq = (xi_[INTID] * xi_[INTID]) * exp (2.0 * q_[INTID] * espo);
          } else {
            dikm = dik - ce_[INTID];
            // dikm2=dikm*dikm;
            dikm3 = dikm * dikm * dikm;
            dikm4 = dikm3 * dikm;
            dikm5 = dikm4 * dikm;
            qsiexpq =
              Q5_[INTID] * dikm5 + Q4_[INTID] * dikm4 + Q3_[INTID] * dikm3;
            eri += 2.0 * (P5_[INTID] * dikm5 + P4_[INTID] * dikm4 +
                          P3_[INTID] * dikm3);
            qsiexpq = qsiexpq * qsiexpq;
          }
          den_[i] += qsiexpq;
          den_[k] += qsiexpq;
        }
      }

      // ebi=sqrt(den[i]);
      // den[i]=1.0/ebi;
      // energy=energy+(eri-ebi);
      energy += (eri - sqrt (den_[i]));
    }
    return energy;
  }

  BHdouble BHEnergyCalculator_SMATB::Gradient (BHdouble *x, BHdouble *g) {
    // std::vector<BHdouble> den(nAtoms_,0);
    memset (
      den_, 0,
      nAtoms_ * sizeof (den_[0])); // should empty the array efficiently
    memset (g, 0, n3Atoms_ * sizeof (g[0]));
    memset (nNeigh_, 0, nAtoms_ * sizeof (nNeigh_[0]));
    // cout << sizeof(nNeigh_) <<" " << sizeof(g) <<endl;
    BHdouble ebi, eri;
    BHdouble xik, yik, zik, dik, dik2, x_i, y_i, z_i,
      /*dik0,*/ espo, qsiexpq, aexpp, dikm, dikm2, dikm3, dikm4, dikm5;
    BHdouble energy (0), F_on_dik;
    BHdouble Fr;
    unsigned int INTID, i, j, k, mytype, IDi, IDk;
#pragma omp parallel for default(shared)			\
  private(i,k,INTID,x_i,y_i,z_i,mytype,IDi,IDk,			\
	  xik,yik,zik,dik2,dik,espo,aexpp,qsiexpq,Fr,		\
	  ebi,eri,dikm,dikm2,dikm3,dikm4,dikm5,F_on_dik)	\
  reduction(+:energy)
    for (i = 0; i < nAtoms_; ++i) {
      // nNeigh_[i]=0;
      eri = 0;
      IDi = i * 3;
      x_i = x[IDi];
      y_i = x[IDi + 1];
      z_i = x[IDi + 2];
      mytype = nTypes_ * static_cast<unsigned> (typeList_[i]);
      for (
#ifdef _OPENMP
        k = 0;
#else
        k = i + 1;
#endif //_OPENMP
        k < nAtoms_; ++k) {
#ifdef _OPENMP
        if (k == i)
          continue;
#endif //_OPENMP
       // INTID=intID_[TAI(i,k)];
        IDk = 3 * k;
        INTID = intType_[mytype + typeList_[k]];
        // std :: cout <<mytype + typeList_[k]<<" "<< INTID << std::endl;
        xik = x[IDk] - x_i;
        yik = x[IDk + 1] - y_i;
        zik = x[IDk + 2] - z_i;

        dik2 = xik * xik + yik * yik + zik * zik;
        if (dik2 < ceSQ_[INTID]) {
          dik = sqrt (dik2);
          NeighList_[i][nNeigh_[i]] = k;
          if (dik < cs_[INTID]) {
            // dik0=NNd_[INTID];
            espo = 1.0 - dik / NNd_[INTID]; // dik0;
            aexpp = exp (p_[INTID] * espo) * a_[INTID];
            Fr = (2.0 * aexpp) * (p_[INTID] / NNd_[INTID]);
#ifdef _OPENMP
            eri += aexpp;
#else
            eri += 2.0 * aexpp;
#endif //_OPENMP
            qsiexpq = (xi_[INTID] * xi_[INTID]) * exp (2.0 * q_[INTID] * espo);
            Fb_[i][nNeigh_[i]] = qsiexpq * q_[INTID] / NNd_[INTID];
          } else {
            dikm = dik - ce_[INTID];
            dikm2 = dikm * dikm;
            dikm3 = dikm2 * dikm;
            dikm4 = dikm3 * dikm;
            dikm5 = dikm4 * dikm;
            qsiexpq =
              Q5_[INTID] * dikm5 + Q4_[INTID] * dikm4 + Q3_[INTID] * dikm3;
            Fr = -2.0 * (5.0 * P5_[INTID] * dikm4 + 4.0 * P4_[INTID] * dikm3 +
                         3.0 * P3_[INTID] * dikm2);
#ifdef _OPENMP
            eri +=
              (P5_[INTID] * dikm5 + P4_[INTID] * dikm4 + P3_[INTID] * dikm3);
#else
            eri += 2.0 * (P5_[INTID] * dikm5 + P4_[INTID] * dikm4 +
                          P3_[INTID] * dikm3);
#endif //_OPENMP
            Fb_[i][nNeigh_[i]] =
              -(5.0 * Q5_[INTID] * dikm4 + 4.0 * Q4_[INTID] * dikm3 +
                3.0 * Q3_[INTID] * dikm2) *
              qsiexpq;
            qsiexpq = qsiexpq * qsiexpq;
          }
          ++nNeigh_[i];
          den_[i] += qsiexpq;
          F_on_dik = Fr / dik;

          g[IDi] += F_on_dik * xik;
          g[IDi + 1] += F_on_dik * yik;
          g[IDi + 2] += F_on_dik * zik;
#ifndef _OPENMP
          den_[k] += qsiexpq;
          g[IDk] -= F_on_dik * xik;
          g[IDk + 1] -= F_on_dik * yik;
          g[IDk + 2] -= F_on_dik * zik;
#endif
        }
      }
      ebi = sqrt (den_[i]);

      den_[i] = 1.0 / ebi;
      energy = energy + (eri - ebi);
    }

    /*double sum=0;
      for ( i=0; i< nAtoms_; ++i) {
      IDi = 3*i;
      std:: cout << i <<"->"<< g[IDi+0]<<" "<< g[IDi+1]<<" "<< g[IDi+2]
      <<"\t\t" << nNeigh_[i] <<'\n';
      for (j=0;j<3;j++) {
      sum+=g[IDi+j]*g[IDi+j];
      }
      }
      std:: cout << sum <<std::endl;
      std:: cout << g[0]<<" "<< g[1]<<" "<< g[2]<<std::endl;
      std::    cin.get();*/

    // shared(x,g,den_,Fb_NeighList_,)
#pragma omp parallel for default(shared) private(                              \
  i, j, k, x_i, y_i, z_i, IDi, IDk, xik, yik, zik, dik, F_on_dik)
    for (i = 0; i < nAtoms_; ++i) {
      IDi = 3 * i;
      x_i = x[IDi];
      y_i = x[IDi + 1];
      z_i = x[IDi + 2];
      // mytype = nTypes_*typeList_[i];
      for (j = 0; j < nNeigh_[i]; ++j) {
        k = NeighList_[i][j]; /*
                                #ifdef _OPENMP
                                if (k<=i) continue;
                                #endif //_OPENMP   */
        IDk = 3 * k;
        // INTID = intType_[mytype + typeList_[k]];
        // std :: cout <<mytype + typeList_[k]<<" "<< INTID << std::endl;
        xik = x[IDk] - x_i;
        yik = x[IDk + 1] - y_i;
        zik = x[IDk + 2] - z_i;

        dik = sqrt (xik * xik + yik * yik + zik * zik);
        // fb(j,i) so this won't run on a "distant" address
        // F_on_dik used for bounding forces
        F_on_dik = Fb_[i][j] * (den_[i] + den_[k]) / dik;

        g[IDi] -= F_on_dik * xik;
        g[IDi + 1] -= F_on_dik * yik;
        g[IDi + 2] -= F_on_dik * zik;
#ifndef _OPENMP
        g[IDk] += F_on_dik * xik;
        g[IDk + 1] += F_on_dik * yik;
        g[IDk + 2] += F_on_dik * zik;
#endif
      }
    }
    /*
     for ( i=0; i< nAtoms_; ++i) {
      IDi = 3*i;
          x_i = x[IDi  ];
          y_i = x[IDi+1];
          z_i = x[IDi+2];
          std::cout << i+1<<"("<< ((typeList_[i]==0)?"Ag":"Cu")
     <<")["<<nNeigh_[i]<<"]:: "; for ( j=0; j < nNeigh_[i]; ++j) { k =
     NeighList_[i][j]; IDk = 3*k;
            //INTID = intType_[mytype + typeList_[k]];
            //std :: cout <<mytype + typeList_[k]<<" "<< INTID << std::endl;
            xik=x[IDk  ]-x_i;
            yik=x[IDk+1]-y_i;
            zik=x[IDk+2]-z_i;
            dik=sqrt(xik*xik+yik*yik+zik*zik);
          std::cout << k+1<<"("<< ((typeList_[k]==0)?"Ag":"Cu") <<"["<< dik
     <<"]"<<"), ";
          }
          std::cout << "\n";
        }
    */
    /*      double sum=0;
             for ( i=0; i< nAtoms_; ++i) {
             IDi = 3*i;
             for (j=0;j<3;j++) {
             sum+=g[IDi+j]*g[IDi+j];
             }
             }
             std:: cout << sum <<std::endl;
             std:: cout << g[0]<<" "<< g[1]<<" "<< g[2]<<std::endl;
             std::    cin.get();
 */
    return energy;
  }

  BHdouble BHEnergyCalculator_SMATB::Energy_tol (BHdouble *x) {
    // std::vector<BHdouble> den(nAtoms_,0);
    memset (
      den_, 0,
      nAtoms_ * sizeof (den_)); // should empty the array efficiently
    BHdouble eri;
    BHdouble xik, yik, zik, dik, dik2, x_i, y_i, z_i,
      /*dik0,*/ espo, qsiexpq, aexpp, dikm, dikm2, dikm3, dikm4, dikm5;
    BHdouble energy (0);
    unsigned int INTID, i, j, k, mytype, IDi, IDk;

    for (i = 0; i < nAtoms_; ++i) {
      eri = 0;
      IDi = 3 * i;
      x_i = x[IDi];
      y_i = x[IDi + 1];
      z_i = x[IDi + 2];
      mytype = nTypes_ * static_cast<unsigned> (typeList_[i]);
      for (j = 0; j < nNeigh_[i]; ++j) {
        k = NeighList_[i][j];
        INTID = intType_[mytype + typeList_[k]];
        IDk = 3 * k;
        xik = x[IDk] - x_i;
        yik = x[IDk + 1] - y_i;
        zik = x[IDk + 2] - z_i;

        dik2 = xik * xik + yik * yik + zik * zik;
        if (dik2 < ceSQ_[INTID]) {
          dik = sqrt (dik2);
          if (dik < cs_[INTID]) {
            // dik0=NNd_[INTID];
            espo = 1.0 - dik / NNd_[INTID]; // dik0;
            aexpp = exp (p_[INTID] * espo) * a_[INTID];
            eri += 2.0 * aexpp;
            // add xi2_ the squared xi
            qsiexpq = (xi_[INTID] * xi_[INTID]) * exp (2.0 * q_[INTID] * espo);
          } else {
            dikm = dik - ce_[INTID];
            dikm2 = dikm * dikm;
            dikm3 = dikm2 * dikm;
            dikm4 = dikm3 * dikm;
            dikm5 = dikm4 * dikm;
            qsiexpq =
              Q5_[INTID] * dikm5 + Q4_[INTID] * dikm4 + Q3_[INTID] * dikm3;
            eri += 2.0 * (P5_[INTID] * dikm5 + P4_[INTID] * dikm4 +
                          P3_[INTID] * dikm3);
            qsiexpq = qsiexpq * qsiexpq;
          }
          den_[i] += qsiexpq;
          den_[k] += qsiexpq;
        }
      }
      energy = energy + (eri - sqrt (den_[i]));
    }

    return energy;
  }

  BHdouble BHEnergyCalculator_SMATB::Gradient_tol (BHdouble *x, BHdouble *g) {
    // std::vector<BHdouble> den(nAtoms_,0);
    memset (
      den_, 0,
      nAtoms_ * sizeof (den_)); // should empty the array efficiently
    memset (g, 0, n3Atoms_ * sizeof (g));

    BHdouble ebi, eri;
    BHdouble xik, yik, zik, dik, dik2, x_i, y_i, z_i,
      /*dik0,*/ espo, qsiexpq, aexpp, dikm, dikm2, dikm3, dikm4, dikm5;
    BHdouble energy (0), F_on_dik;
    BHdouble Fr;
    unsigned int INTID, i, j, k, mytype, IDi, IDk;
    for (i = 0; i < nAtoms_; ++i) {
      eri = 0;
      IDi = 3 * i;
      x_i = x[IDi];
      y_i = x[IDi + 1];
      z_i = x[IDi + 2];
      mytype = nTypes_ * static_cast<unsigned> (typeList_[i]);
      for (j = 0; j < nNeigh_[i]; ++j) {
        k = NeighList_[i][j];
        INTID = intType_[mytype + typeList_[k]];
        IDk = 3 * k;
        xik = x[IDk] - x_i;
        yik = x[IDk + 1] - y_i;
        zik = x[IDk + 2] - z_i;

        dik2 = xik * xik + yik * yik + zik * zik;
        if (dik2 < ceSQ_[INTID]) {
          dik = sqrt (dik2);
          if (dik < cs_[INTID]) {
            // dik0=NNd_[INTID];
            espo = 1.0 - dik / NNd_[INTID]; // dik0;
            aexpp = exp (p_[INTID] * espo) * a_[INTID];
            Fr = (2.0 * p_[INTID] / NNd_[INTID]) * aexpp;
            eri += 2.0 * aexpp;
            // add xi2_ the squared xi
            qsiexpq = (xi_[INTID] * xi_[INTID]) * exp (2.0 * q_[INTID] * espo);
            Fb_[i][j] = qsiexpq * (q_[INTID] / NNd_[INTID]);
          } else {
            dikm = dik - ce_[INTID];
            dikm2 = dikm * dikm;
            dikm3 = dikm2 * dikm;
            dikm4 = dikm3 * dikm;
            dikm5 = dikm4 * dikm;
            qsiexpq =
              Q5_[INTID] * dikm5 + Q4_[INTID] * dikm4 + Q3_[INTID] * dikm3;
            Fr = -2.0 * (5.0 * P5_[INTID] * dikm4 + 4.0 * P4_[INTID] * dikm3 +
                         3.0 * P3_[INTID] * dikm2);
            eri += 2.0 * (P5_[INTID] * dikm5 + P4_[INTID] * dikm4 +
                          P3_[INTID] * dikm3);
            Fb_[i][j] = -(
                          (5.0 * Q5_[INTID] * dikm4 + 4.0 * Q4_[INTID] * dikm3 +
                           3.0 * Q3_[INTID] * dikm2)) *
                        qsiexpq;
            qsiexpq = qsiexpq * qsiexpq;
          }
          den_[i] += qsiexpq;
          den_[k] += qsiexpq;

          F_on_dik = Fr / dik;

          g[IDi] += F_on_dik * xik;
          g[IDi + 1] += F_on_dik * yik;
          g[IDi + 2] += F_on_dik * zik;
          g[IDk] -= F_on_dik * xik;
          g[IDk + 1] -= F_on_dik * yik;
          g[IDk + 2] -= F_on_dik * zik;
        }
      }

      ebi = sqrt (den_[i]);

      den_[i] = 1.0 / ebi;
      energy = energy + (eri - ebi);
    }

    for (i = 0; i < nAtoms_; ++i) {
      IDi = 3 * i;
      x_i = x[IDi];
      y_i = x[IDi + 1];
      z_i = x[IDi + 2];
      mytype = nTypes_ * static_cast<unsigned> (typeList_[i]);
      for (j = 0; j < nNeigh_[i]; ++j) {
        k = NeighList_[i][j];
        INTID = intType_[mytype + typeList_[k]];
        // std :: cout <<mytype + typeList_[k]<<" "<< INTID << std::endl;
        IDk = 3 * k;
        xik = x[IDk] - x_i;
        yik = x[IDk + 1] - y_i;
        zik = x[IDk + 2] - z_i;

        dik2 = xik * xik + yik * yik + zik * zik;
        if (dik2 < ceSQ_[INTID]) {
          dik = sqrt (dik2);
          // F_on_dik used for bounding forces
          F_on_dik = Fb_[i][j] * (den_[i] + den_[k]) / dik;

          g[IDi] -= F_on_dik * xik;
          g[IDi + 1] -= F_on_dik * yik;
          g[IDi + 2] -= F_on_dik * zik;
          g[IDk] += F_on_dik * xik;
          g[IDk + 1] += F_on_dik * yik;
          g[IDk + 2] += F_on_dik * zik;
        }
      }
    }
    return energy;
  }
} // namespace BH
