/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Definition for BHSettings

   @file BHsettings.cpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 22/11/2017
   @version 0.6.1

   Small modifies in the parsers

   @date 4/7/2017
   @version 0.6

   @date 31/03/2020
   Updated the parsers for input files

   @todo need a way to prepare automatically an output file based on fefault
   values
*/
#include <fstream>
#include <iostream>
#include <sstream>

#include "BHmetalParameters.hpp"
#include "BHparsers.hpp"
#include "BHsettings.hpp"

///@todo: add a protection on order parameters
using std::cout;
using std::endl;
#define SETTINGSVERBOSE
namespace BH {

  void printData (std::ostream &stream, const BHSettings &settings) {
#ifdef SETTINGSVERBOSE
    stream << "*****COMMON SETTINGS*****" << endl;
    // stream <<"Seeded start:\t\t" << (seeded?"yes":"no") << endl;
    stream << "AutoRNGSeed:\t\t" << (settings.setRNGseed_ ? "yes" : "no")
           << endl;
    stream << "RNGSeed:\t\t" << settings.RngSeed_ << endl;
    stream << "Cut off:\t\t" << (settings.cutOff_ ? "yes" : "no") << endl;

    stream << "Potential filename:\t" << settings.namePotpar_ << endl;
    stream << "Montecarlo steps:\t" << settings.nMC_ << endl;

    stream << "Histo weight:\t\t" << settings.histoWeight_ << endl;

    stream << "Order parameters:\n\tfirst:\t"
           << OparName (settings.OPSettings_[0].OP_) << "\n\tsecond:\t"
           << OparName (settings.OPSettings_[1].OP_) << endl;
    for (size_t OPid = 0; OPid < settings.NumberOfOP; ++OPid) {
      stream << "Histo " << OPid
             << " options:\n\tstart\tstep\tend\tbars:" << endl;
      stream << "\t" << settings.OPSettings_[OPid].histoPars_.getStart ()
             << "\t" << settings.OPSettings_[OPid].histoPars_.getStep () << "\t"
             << settings.OPSettings_[OPid].histoPars_.getEnd () << "\t"
             << settings.OPSettings_[OPid].histoPars_.getNbins () << endl;
    }

    stream << "histo_.histoRenew:\t\t" << settings.renewHisto_ << endl;
    stream << "Landing walkers update exponent:" << settings.landingAlpha_
           << endl;
    stream << "Walker calc behavior:\t";
    switch (settings.walkersTurnover_) {
    case BHSettings::walkersTurnover::ordinal:
      stream << "ordinal";
      break;
    case BHSettings::walkersTurnover::random:
      stream << "random";
      break;
    case BHSettings::walkersTurnover::change:
      stream << "change";
      break;
    }
    stream << endl;
    stream << "Freq to change walkers:\t" << settings.freqWalkerExchange_
           << endl;
    stream << "Number of walker:\t" << settings.walkerFilenames_.size ()
           << endl;
    for (unsigned i = 0; i < settings.walkerFilenames_.size ();
         ++i) { // get the walkers info
      stream << "Walker " << i << ": "
             << settings.walkerFilenames_[i].flyingWalker;
      if (settings.walkerFilenames_[i].landingWalker != "") {
        stream << " with landing: "
               << settings.walkerFilenames_[i].landingWalker;
        if (settings.walkerFilenames_[i].hikingWalker != "") {
          stream << " and with hiking: "
                 << settings.walkerFilenames_[i].hikingWalker;
        }
      }
      stream << std::endl;
    }
    if (settings.uniqueBestWalker_ != "") {
      stream << "Best Walker : " << settings.uniqueBestWalker_ << std::endl;
      stream << "Best Walker restart the Landing Walkers:\t"
             << (settings.BWRestartsLanding_ ? "yes" : "no") << endl;
    }
    stream << "Walker private output:\t"
           << (settings.separateWalkerOutput_ ? "yes" : "no") << endl;
    stream << "Print the minima list:\t"
           << (settings.plotMinima_ ? "yes" : "no") << endl;
    stream << "Analyze all the minima:\t"
           << (settings.analyzeAtEveryStep_ ? "yes" : "no") << endl;
    stream << "Parameter limits:\n";
    for (size_t OPid = 0; OPid < settings.NumberOfOP; ++OPid) {
      stream << "For \"" + OparName (settings.OPSettings_[OPid].OP_) << "\":"
             << settings.OPSettings_[OPid].parametersForOutput_.paramMin
             << "...["
             << settings.OPSettings_[OPid].parametersForOutput_.interval
             << " x "
             << settings.OPSettings_[OPid].parametersForOutput_.partitioning
             << "]..."
             << settings.OPSettings_[OPid].parametersForOutput_.paramMax
             << endl;
    }
#endif // SETTINGSVERBOSE
  }

  BHSettings::walkersTurnover BHSettings::wTOfromInt (int t) {
    switch (t) {
    case 1:
      return BHSettings::walkersTurnover::ordinal;
    case 2:
      return BHSettings::walkersTurnover::random;
    case 3:
      return BHSettings::walkersTurnover::change;
    default:
      return BHSettings::walkersTurnover::ordinal;
    }
  }

  std::vector<BHOrderParameter>
  BHSettings::getOPsFromSettings (const std::vector<OPSettings> &data) {
    std::vector<BHOrderParameter> OPs;
    for (const auto &t : data) {
      OPs.push_back (t.OP_);
    }
    return OPs;
  }

  namespace BHSettingsUtilities {

    std::string printInputFileFromData (const BHSettings &settings) {
      std::stringstream ss;

#define fastOutputWriter(str, variable) str << " = " << variable << '\n'

#define fasterOutputWriter(str, index, variable)                               \
  BHParsers::BHS::Card##str[static_cast<size_t> (BHParsers::BHS::str::index)]  \
    << " = " << variable << '\n'

#define fasterHISTOWriter(index, variable)                                     \
  BHParsers::BHS::HISTO[static_cast<size_t> (BHParsers::BHS::GOAhisto::index)] \
    << " = " << variable << '\n'

      ss << "#GENERAL\n";
      ss << fasterOutputWriter (GENERAL, rng_seed, settings.RngSeed_);
      ss << fasterOutputWriter (
        GENERAL, minimizationAlgorithm,
        strFromMinimizationAlgorithm (settings.minimizationAlgorithm_));

      ss << "#POTENTIAL\n";
      ss << fasterOutputWriter (POTENTIAL, name_potpar, settings.namePotpar_);

      ss << "#RUN\n";
      ss << fasterOutputWriter (RUN, n_mc, settings.nMC_);

      ss << "#GOA\n";
      for (size_t OPid = 0; OPid < settings.NumberOfOP; ++OPid) {
        ss << fastOutputWriter (
          BHParsers::BHS::CardGOA[static_cast<size_t> (
            BHParsers::BHS::GOA::OP)] +
            std::to_string (OPid),
          OparName (settings.OPSettings_[OPid].OP_));
      }
      ss << fasterOutputWriter (
        GOA, freq_walker_exchange, settings.freqWalkerExchange_);
      ss << fasterOutputWriter (GOA, alpha_flying, settings.landingAlpha_);
      ss << fasterOutputWriter (GOA, histoWeight, settings.histoWeight_);
      if (settings.uniqueBestWalker_ != "") {
        ss << fasterOutputWriter (GOA, bestWalker, settings.uniqueBestWalker_);
        ss << fasterOutputWriter (
          GOA, BWRestartsLanding,
          ((settings.BWRestartsLanding_) ? "true" : "false"));
      }
      for (size_t OPid = 0; OPid < settings.NumberOfOP; ++OPid) {
        std::string histoNum = BHParsers::BHS::CardGOA[static_cast<size_t> (
                                 BHParsers::BHS::GOA::histo)] +
                               std::to_string (OPid) + ".";
        ss << histoNum
           << fasterHISTOWriter (
                nbins, settings.OPSettings_[OPid].histoPars_.getNbins ());
        ss << histoNum
           << fasterHISTOWriter (
                start, settings.OPSettings_[OPid].histoPars_.getStart ());
        ss << histoNum
           << fasterHISTOWriter (
                end, settings.OPSettings_[OPid].histoPars_.getEnd ());
      }

      ss << "#WALKERS " << settings.walkerFilenames_.size () << '\n';
      for (const auto &wi : settings.walkerFilenames_) {
        ss << wi.flyingWalker;
        if (wi.landingWalker != "")
          ss << " " << wi.landingWalker;
        if (wi.hikingWalker != "")
          ss << " " << wi.hikingWalker;
        ss << '\n';
      }

      ss << "#OUTPUT\n"
         << fasterOutputWriter (
              OUTPUT, separateWalkerOutput,
              ((settings.separateWalkerOutput_) ? "true" : "false"));
      ss << fasterOutputWriter (
        OUTPUT, analyzeAllMinima,
        ((settings.analyzeAtEveryStep_) ? "true" : "false"));
      ss << fasterOutputWriter (
        OUTPUT, plotMinima, ((settings.plotMinima_) ? "true" : "false"));

      for (size_t OPid = 0; OPid < settings.NumberOfOP; ++OPid) {
        std::string OPnum =
          std::string (
            (settings.plotMinima_ &&
             settings.OPSettings_[OPid].parametersForOutput_.active)
              ? ""
              : "#") +
          "OP" + std::to_string (OPid) + "_";
        ss << OPnum
           << fasterOutputWriter (
                OUTPUT, param_min,
                settings.OPSettings_[OPid].parametersForOutput_.paramMin);
        ss << OPnum
           << fasterOutputWriter (
                OUTPUT, param_max,
                settings.OPSettings_[OPid].parametersForOutput_.paramMax);
        ss << OPnum
           << fasterOutputWriter (
                OUTPUT, parameter_partitioning,
                settings.OPSettings_[OPid].parametersForOutput_.partitioning);
      }

      //<<_BHSParser_BHS::CardPOTENTIAL[static_cast<size_t>(BHParsers::BHS::POTENTIAL::name_potpar)]
#undef fastOutputWriter
#undef fasterHISTOWriter
#undef fasterOutputWriter
      return ss.str ();
    }

    void PlotHistoMatrix (
      std::ostream &stream, const BHSettings &settings, const BHHisto &histo) {
      if (histo.HistoIsReady ()) {
        stream << "#Histogram:\n";
        for (size_t OPid = 0; OPid < settings.NumberOfOP; ++OPid) {
          stream << "#" << OparName (settings.OPSettings_[OPid].OP_) << " "
                 << settings.OPSettings_[OPid].histoPars_.getStart () << ", "
                 << settings.OPSettings_[OPid].histoPars_.getEnd () << ", "
                 << settings.OPSettings_[OPid].histoPars_.getStep () << "\n";
        }
      }
    }

    void PlotHistoCoordinates (
      std::ostream &stream, const BHSettings &settings, const BHHisto &histo) {
      if (histo.HistoIsReady ()) {
        stream << "#Histogram:\n";
        for (size_t OPid = 0; OPid < settings.NumberOfOP; ++OPid) {
          stream << "#" << OparName (settings.OPSettings_[OPid].OP_) << " "
                 << settings.OPSettings_[OPid].histoPars_.getStart () << ", "
                 << settings.OPSettings_[OPid].histoPars_.getEnd () << ", "
                 << settings.OPSettings_[OPid].histoPars_.getStep () << "\n";
        }
      }
    }

    void setFlyingLandingMinimization (BHSettings &settings) {
      settings.separateWalkerOutput_ = true;
      for (auto &wfn : settings.walkerFilenames_) {
        wfn.landingWalker = "landing:companion.in";
        wfn.hikingWalker = "";
      }
      settings.uniqueBestWalker_ = "";
      settings.separateWalkerOutput_ = true;
    }

    void setFLMinimizationwBestWalker (BHSettings &settings) {
      for (auto &wfn : settings.walkerFilenames_) {
        wfn.landingWalker = "landing:companion.in";
        wfn.hikingWalker = "";
      }
      settings.BWRestartsLanding_ = true;
      settings.uniqueBestWalker_ = "bestWalker.in";
      settings.separateWalkerOutput_ = true;
    }

    void setFLMinimizationwBestWalkerNoRestart (BHSettings &settings) {
      for (auto &wfn : settings.walkerFilenames_) {
        wfn.landingWalker = "landing:companion.in";
        wfn.hikingWalker = "";
      }
      settings.BWRestartsLanding_ = false;
      settings.uniqueBestWalker_ = "bestWalker.in";
      settings.separateWalkerOutput_ = true;
    }

    void setFlyingLandingHikingMinimization (BHSettings &settings) {
      settings.separateWalkerOutput_ = true;
      for (auto &wfn : settings.walkerFilenames_) {
        wfn.landingWalker = "landing:companion.in";
        wfn.hikingWalker = "hikingR:bestWalker.in";
      }
      settings.uniqueBestWalker_ = "";
      settings.separateWalkerOutput_ = true;
    }

    void setFlyingLandingHikingMinimizationNoRebound (BHSettings &settings) {
      settings.separateWalkerOutput_ = true;
      for (auto &wfn : settings.walkerFilenames_) {
        wfn.landingWalker = "landing:companion.in";
        wfn.hikingWalker = "hiking:bestWalker.in";
      }
      settings.uniqueBestWalker_ = "";
      settings.separateWalkerOutput_ = true;
    }

    BHSettings loadBHSettings (std::string filename, bool verbose) {
      std::ifstream f (filename); // open the file with settings
      if (!f.good ()) {
        throw std::string ("BHSettings::Invalid Filename: ").append (filename);
      }
      BHSettings settings = loadBHSettings (f, verbose);
      auto hasPath = filename.find_last_of ("\\/");
      if (hasPath == std::string::npos) {
        hasPath = 0;
      } else {
        ++hasPath;
      }
      std::string processedFilename = filename.substr (hasPath) + ".processed";
      std::ofstream processedOptions (processedFilename);
      processedOptions << printInputFileFromData (settings) << std::endl;
      // cout << "RICORDATI DI TOGLIERE EXIT"<<endl;
      // exit(0);
      return settings;
    }

    BHSettings loadBHSettings (std::istream &stream, bool verbose) {
      return BHSettingsFileLoader (stream, verbose);
    }
  } // namespace BHSettingsUtilities

  BHSettingsFileLoader::BHSettingsFileLoader (
    std::istream &stream, bool verbose) {
    std::string getter, comments;
    enum parsers {
      general,
      potential,
      run,
      goa,
      output,
      walkers,
      out_scope
    } to_parse = out_scope;
    while (stream) {
      getline (stream, getter);
      switch (getter[0]) { // eliminates comments and eventually
                           // selects card
      case '*':            // comments can start with *
        [[fallthrough]];
      case '#': // comments can start with #
        [[fallthrough]];
      case '!': // comments can start with !
      {
        if (getter.find ("GENERAL") != std::string::npos) {
          to_parse = general;
        } else if (getter.find ("POTENTIAL") != std::string::npos) {
          to_parse = potential;
        } else if (getter.find ("RUN") != std::string::npos) {
          to_parse = run;
        } else if (getter.find ("GOA") != std::string::npos) {
          to_parse = goa;
        } else if (getter.find ("OUTPUT") != std::string::npos) {
          to_parse = output;
        } else if (getter.find ("WALKERS") != std::string::npos) {
          /*WALKERS is a special card: it does not follow the other
           *pasers rules it will cycle on nwalkers names and save
           *them into an array
           */
          to_parse = walkers;

          unsigned nWalkers_ =
            unsigned (stoi (getter.substr (getter.find ("WALKERS") + 7)));

          walkerFilenames_.resize (nWalkers_);
          // get the walkers info
          for (unsigned i = 0; i < nWalkers_; ++i) {
            getline (stream, getter);
            std::stringstream ss (getter);
            ss >> walkerFilenames_[i].flyingWalker;
            if (!ss.eof ()) {
              // load landing walker, can be prepended by an option
              // separaded by colon, example "landing:walker.in"
              ss >> walkerFilenames_[i].landingWalker;
              if (!ss.eof ()) {
                // load hiking walker, can be prepended by an option
                // separaded by colon, example "hiking:walker.in"
                ss >> walkerFilenames_[i].hikingWalker;
              }
            }
            // eliminates comments and eventually selects card
            switch (walkerFilenames_[i].flyingWalker[0]) {
            case '*': // comments can start with *
              [[fallthrough]];
            case '#': // comments can start with #
              [[fallthrough]];
            case '!': // comments can start with !
              --i;
              break;
            default: {
            }
            }
          }
        }
        break;
      default:
        size_t comments_pos = getter.find_first_of ("*#!");
        if (comments_pos != std::string::npos) {
          getter.erase (comments_pos);
        }
        // cout << to_parse << " " << getter << endl;
        switch (to_parse) {
        case general:
          parser_general (getter);
          break;
        case potential:
          parser_potential (getter);
          break;
        case run:
          parser_run (getter);
          break;
        case goa:
          parser_goa (getter);
          break;
        case output:
          parser_output (getter);
          break;
        case walkers:
          [[fallthrough]];
        case out_scope:
          [[fallthrough]];
        default:
          break;
        }
      }
      }
    }

    if (walkerFilenames_.size () == 1) {
      separateWalkerOutput_ = false;
    }

    for (auto &ops : OPSettings_) {
      ops.parametersForOutput_.interval = (ops.parametersForOutput_.paramMax -
                                           ops.parametersForOutput_.paramMin) /
                                          ops.parametersForOutput_.partitioning;
    }
    if (verbose) {
      printData (std::cout, *this);
    }

    if (nMC_ <= 0) {
      throw "BHS: Montecarlo step not set or negative";
    }
  }

  // for parsers: i given more importance to the default value for
  // bools: if default is true only 0 or "false" will set the variable
  // to false, and viceversa only 1 or "true" will set the fariable to
  // true if the default is false, any other value leaves the variable
  // to the default value
  void BHSettingsFileLoader::parser_general (const std::string &parsed) {
    try {
      bool parsedCorrectly = false;
      parsedCorrectly |= BHParsers::parse (
        BHParsers::BHS::CardGENERAL[static_cast<size_t> (
                                      BHParsers::BHS::GENERAL::rng_seed)]
          .c_str (),
        parsed, RngSeed_);
      std::string strAlgo;
      bool gotAlgotithm = BHParsers::parse (
        BHParsers::BHS::CardGENERAL
          [static_cast<size_t> (BHParsers::BHS::GENERAL::minimizationAlgorithm)]
            .c_str (),
        parsed, strAlgo);
      if (gotAlgotithm) {
        parsedCorrectly = true;
        minimizationAlgorithm_ = minAlgorithmFromStr (strAlgo);
      }

      if (parsed == "") {
        parsedCorrectly = true;
      }
      if (!parsedCorrectly) {
        throw "BHS::Invalid option in RUN card: \"" + parsed + "\"";
      }

    } catch (const std::invalid_argument &) {
      throw "BHS::Invalid argument in GENERAL card: \"" + parsed + "\"";
    }
  }

  void BHSettingsFileLoader::parser_potential (const std::string &parsed) {
    try {
      bool parsedCorrectly = false;
      parsedCorrectly |= BHParsers::parse (
        BHParsers::BHS::CardPOTENTIAL[static_cast<size_t> (
                                        BHParsers::BHS::POTENTIAL::name_potpar)]
          .c_str (),
        parsed, namePotpar_);
      parsedCorrectly |= BHParsers::parse (
        BHParsers::BHS::CardPOTENTIAL[static_cast<size_t> (
                                        BHParsers::BHS::POTENTIAL::cut_off)]
          .c_str (),
        parsed, cutOff_);
      std::cout << namePotpar_ << std::endl;
      if (parsed == "") {
        parsedCorrectly = true;
      }
      if (!parsedCorrectly) {
        throw "BHS::Invalid option in POTENTIAL card: \"" + parsed + "\"";
      }
    } catch (const std::invalid_argument &) {
      throw "BHS::Invalid argument in POTENTIAL card: \"" + parsed + "\"";
    }
  }

  void BHSettingsFileLoader::parser_run (const std::string &parsed) {
    try {
      bool parsedCorrectly = false;
      parsedCorrectly |= BHParsers::parse (
        BHParsers::BHS::CardRUN[static_cast<size_t> (BHParsers::BHS::RUN::n_mc)]
          .c_str (),
        parsed, nMC_);
      if (parsed == "") {
        parsedCorrectly = true;
      }
      if (!parsedCorrectly) {
        throw "BHS::Invalid option in RUN card: \"" + parsed + "\"";
      }
    } catch (const std::invalid_argument &) {
      throw "BHS::Invalid argument in RUN card: \"" + parsed + "\"";
    }
  }

  void BHSettingsFileLoader::parser_goa (const std::string &parsed) {
    try {
      bool parsedCorrectly = false;
      parsedCorrectly |= BHParsers::parse (
        BHParsers::BHS::CardGOA[static_cast<size_t> (
                                  BHParsers::BHS::GOA::histoWeight)]
          .c_str (),
        parsed, separateWalkerOutput_);
      bool got = false;
      for (size_t OPid = 0; OPid < NumberOfOP && (!parsedCorrectly); ++OPid) {
        std::string OPname;
        bool foundIP = BHParsers::parse (
          (BHParsers::BHS::CardGOA[static_cast<size_t> (
             BHParsers::BHS::GOA::OP)] +
           std::to_string (OPid))
            .c_str (),
          parsed, OPname);
        if (foundIP) {
          OPSettings_[OPid].OP_.initialize (OPname);
          parsedCorrectly = true;
          break;
        }
        std::string histoNum = BHParsers::BHS::CardGOA[static_cast<size_t> (
                                 BHParsers::BHS::GOA::histo)] +
                               std::to_string (OPid) + ".";
        unsigned long tl;
        got = BHParsers::parse (
          (histoNum + BHParsers::BHS::HISTO[static_cast<size_t> (
                        BHParsers::BHS::GOAhisto::nbins)])
            .c_str (),
          parsed, tl);
        if (got) {
          OPSettings_[OPid].histoPars_.setNbins (tl);
          break;
        }
        double td;
        got = BHParsers::parse (
          (histoNum + BHParsers::BHS::HISTO[static_cast<size_t> (
                        BHParsers::BHS::GOAhisto::start)])
            .c_str (),
          parsed, td);
        if (got) {
          OPSettings_[OPid].histoPars_.setStart (td);
          break;
        }
        got = BHParsers::parse (
          (histoNum + BHParsers::BHS::HISTO[static_cast<size_t> (
                        BHParsers::BHS::GOAhisto::end)])
            .c_str (),
          parsed, td);
        if (got) {
          OPSettings_[OPid].histoPars_.setEnd (td);
          break;
        }
      }
      parsedCorrectly |= got;
      parsedCorrectly |= BHParsers::parse (
        BHParsers::BHS::CardGOA[static_cast<size_t> (
                                  BHParsers::BHS::GOA::renew_histo)]
          .c_str (),
        parsed, renewHisto_);
      parsedCorrectly |= BHParsers::parse (
        BHParsers::BHS::CardGOA[static_cast<size_t> (
                                  BHParsers::BHS::GOA::freq_walker_exchange)]
          .c_str (),
        parsed, freqWalkerExchange_);
      parsedCorrectly |= BHParsers::parse (
        BHParsers::BHS::CardGOA[static_cast<size_t> (
                                  BHParsers::BHS::GOA::alpha_flying)]
          .c_str (),
        parsed, landingAlpha_);
      parsedCorrectly |= BHParsers::parse (
        BHParsers::BHS::CardGOA[static_cast<size_t> (
                                  BHParsers::BHS::GOA::bestWalker)]
          .c_str (),
        parsed, uniqueBestWalker_);
      parsedCorrectly |= BHParsers::parse (
        BHParsers::BHS::CardGOA[static_cast<size_t> (
                                  BHParsers::BHS::GOA::BWRestartsLanding)]
          .c_str (),
        parsed, BWRestartsLanding_);
      if (parsed == "") {
        parsedCorrectly = true;
      }
      if (!parsedCorrectly) {
        throw "BHS::Invalid option in GOA card: \"" + parsed + "\"";
      }

    } catch (const std::invalid_argument & /*ia*/) {
      throw "BHS::Invalid argument in GOA card: \"" + parsed + "\"";
    }
  }

  void BHSettingsFileLoader::parser_output (const std::string &parsed) {
    try {
      bool parsedCorrectly = false;
      parsedCorrectly |= BHParsers::parse (
        BHParsers::BHS::CardOUTPUT
          [static_cast<size_t> (BHParsers::BHS::OUTPUT::separateWalkerOutput)]
            .c_str (),
        parsed, separateWalkerOutput_);
      parsedCorrectly |= BHParsers::parse (
        BHParsers::BHS::CardOUTPUT[static_cast<size_t> (
                                     BHParsers::BHS::OUTPUT::plotMinima)]
          .c_str (),
        parsed, plotMinima_);
      for (size_t OPid = 0; OPid < NumberOfOP; ++OPid) {
        std::string parSpec = "OP" + std::to_string (OPid) + "_";
        bool OPparsed = false;
        OPparsed |= BHParsers::parse (
          (parSpec + BHParsers::BHS::CardOUTPUT[static_cast<size_t> (
                       BHParsers::BHS::OUTPUT::param_min)])
            .c_str (),
          parsed, OPSettings_[OPid].parametersForOutput_.paramMin);
        OPparsed |= BHParsers::parse (
          (parSpec + BHParsers::BHS::CardOUTPUT[static_cast<size_t> (
                       BHParsers::BHS::OUTPUT::param_max)])
            .c_str (),
          parsed, OPSettings_[OPid].parametersForOutput_.paramMax);
        OPparsed |= BHParsers::parse (
          (parSpec + BHParsers::BHS::CardOUTPUT[static_cast<size_t> (
                       BHParsers::BHS::OUTPUT::parameter_partitioning)])
            .c_str (),
          parsed, OPSettings_[OPid].parametersForOutput_.partitioning);
        parsedCorrectly |= OPparsed;
        OPSettings_[OPid].parametersForOutput_.active |= OPparsed;
      }
      parsedCorrectly |= BHParsers::parse (
        BHParsers::BHS::CardOUTPUT[static_cast<size_t> (
                                     BHParsers::BHS::OUTPUT::analyzeAllMinima)]
          .c_str (),
        parsed, analyzeAtEveryStep_);
      if (parsed == "") {
        parsedCorrectly = true;
      }
      if (!parsedCorrectly) {
        throw "BHS::Invalid option in OUTPUT card: \"" + parsed + "\"";
      }
    } catch (const std::invalid_argument & /*ia*/) {
      throw "BHS::Invalid argument in OUTPUT card: \"" + parsed + "\": \"" +
        parsed + "\"";
    }
  }
} // namespace BH
