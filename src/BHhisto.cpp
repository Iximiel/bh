/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#include "BHhisto.hpp"

#include <cmath>
#include <cstring>
#include <iostream>
#include <limits>
#include <vector>

namespace BH {
  BHHisto::BHHisto (
    const BHHistoPars &pars1,
    const BHHistoPars &pars2,
    const unsigned long &hrenew)
    : histoParameters_{pars1, pars2},
      histoRenew_ (hrenew) {}

  BHHisto::~BHHisto () { delete[] histo_; }

  void BHHisto::initializeHisto () {
    if (!histo_) {
      histoData_ = 0;
      histo_ = new int[histoParameters_[0].nbins * histoParameters_[1].nbins];
      memset (
        histo_, 0,
        histoParameters_[0].nbins * histoParameters_[1].nbins *
          sizeof (histo_[0]));
      /*for (unsigned i = 0; i < histoParameters_[0].nbins *
      histoParameters_[1].nbins; i++) { histo_[i] = 0; // all bins are
      initialized to 0
      }*/
    }
  }

  void BHHisto::IncrementBin (const BHdouble &OP1, const BHdouble &OP2) {
    if (!histo_) {
      initializeHisto ();
    }
    // reinitialize the histogram after histoRenew_ updates
    if (histoData_ >= histoRenew_) {
      memset (
        histo_, 0,
        histoParameters_[0].nbins * histoParameters_[1].nbins *
          sizeof (histo_[0]));
      /*
      for (unsigned i = 0; i < histoParameters_[0].nbins *
      histoParameters_[1].nbins; i++) { histo_[i] = 0; // all bins are
      initialized to 0
      }*/
      histoData_ = 0;
    }
    ++histo_
      [histoParameters_[0].addr (OP1) * histoParameters_[1].nbins +
       histoParameters_[1].addr (OP2)];
    ++histoData_;
  }

  bool BHHisto::HistoIsReady () const { return histo_; }

  void BHHisto::PlotHistoMatrix (std::ostream &stream) const {
    if (histo_) {
      for (unsigned i = 0; i < histoParameters_[0].nbins; i++) {
        for (unsigned j = 0; j < histoParameters_[1].nbins; j++) {
          stream << histo_[i * histoParameters_[1].nbins + j] << " ";
        }
        stream << "\n";
      }
      stream << std::flush;
    }
  }

  void BHHisto::PlotHistoCoordinates (std::ostream &stream) const {
    if (histo_) {
      for (unsigned i = 0; i < histoParameters_[0].nbins; i++) {
        BHdouble ipos =
          i * histoParameters_[0].step + histoParameters_[0].start;
        for (unsigned j = 0; j < histoParameters_[1].nbins; j++) {
          if (histo_[i * histoParameters_[1].nbins + j] > 0) {
            stream << ipos << "\t"
                   << j * histoParameters_[1].step + histoParameters_[1].start
                   << "\t" << histo_[i * histoParameters_[1].nbins + j] << "\n";
          }
        }
      }
    }
  }

  BHHisto::BHHistoPars::BHHistoPars () = default;

  BHdouble BHHisto::BHHistoPars::getStart () const { return start; }

  void BHHisto::BHHistoPars::setStart (const BHdouble &value) {
    start = value;
    calcPars ();
  }

  BHdouble BHHisto::BHHistoPars::getEnd () const { return end; }

  void BHHisto::BHHistoPars::setEnd (const BHdouble &value) {
    end = value;
    calcPars ();
  }

  BHdouble BHHisto::BHHistoPars::getWidth () const { return width; }

  BHdouble BHHisto::BHHistoPars::getStep () const { return step; }

  unsigned long BHHisto::BHHistoPars::getNbins () const { return nbins; }

  void BHHisto::BHHistoPars::setNbins (unsigned long value) {
    nbins = value;
    calcPars ();
  }

  void BHHisto::BHHistoPars::calcPars () {
    width = end - start;
    step = width / static_cast<double> (nbins);
  }

} // namespace BH
