/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief A small utility that calculates the energy of a configuratation

   @file calculateEnergy.cxx
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 29/3/2018
   @version 1.0

   @todo compact this executable in the mover one
*/

#include <fstream>
#include <iomanip>
#include <iostream>

#include "BHcluster.hpp"
#include "BHenergyCalculator_SMATB.hpp"
#include "BHmetalParameters.hpp"
#include "BHminimizationAlgorithm_LBFGSB.hpp"

int main (int argc, char **argv) {
  int retval = 0;
  try {
    std::string error = "";
    if (argc < 2) {
      error = "You must specify the name of the file with the configuration as "
              "first argument";
    }
    if (argc < 3) {
      if (argc < 2) {
        error += "\n";
      }
      error += "You must specify the name of the file with the MetalParameters "
               "as second argument\n";
      error += "You can also specify -m to minimize the cluster, the output "
               "will be saved in a file called \"minimized_yourfilename\"\n";
      error +=
        "You can also specify -f to calculate the forces on the cluster\n";
      error +=
        "You can also specify -p to calculate the pressure on the cluster";
      throw error;
    }
    bool calculate_forces = false, do_minimization = false,
         calculate_pressure = false;
    if (argc > 3 && std::string (argv[3]) == "-f") {
      calculate_forces = true;
    }
    if (argc > 3 && std::string (argv[3]) == "-m") {
      do_minimization = true;
    }
    if (argc > 3 && std::string (argv[3]) == "-p") {
      calculate_pressure = true;
    }
    std::string fname{argv[1]};
    std::string bhmpFile{argv[2]};
    BH::BHMetalParameters bhmp{bhmpFile, true};

    std::cout << "Hello, this is energyCalculator\n"
              << "I will calculate the energy";
    if (calculate_forces) {
      std::cout << " and the forces";
    }
    if (do_minimization) {
      std::cout << " and minimize";
    }
    if (calculate_pressure) {
      std::cout << " and the pressure map";
    }
    std::cout << "\nfor the configuration in the file \033[1m" << fname
              << "\033[0m\nUsing data from File \033[1m" << bhmpFile
              << "\033[0m" << std::endl;

    BH::BHCluster cluster (fname);
    BH::BHEnergyCalculator_SMATB energy (cluster, bhmp);
    BH::BHdouble *x = new BH::BHdouble[3 * cluster.getNofAtoms ()];
    for (unsigned i = 0; i < cluster.getNofAtoms (); ++i) {
      x[3 * i] = cluster[i].X ();
      x[3 * i + 1] = cluster[i].Y ();
      x[3 * i + 2] = cluster[i].Z ();
    }
    if (calculate_forces) {
      BH::BHdouble *g = new BH::BHdouble[3 * cluster.getNofAtoms ()];
      std::cout << "***********************************************\n";
      std::cout << "The energy of the configuration is\033[1m "
                << energy.Gradient (x, g) << " \033[0m eV" << std::endl;
      std::cout << "The forces are:\n";
      std::cout << std::setprecision (9);
      std::cout << std::fixed;
      for (unsigned i = 0; i < cluster.getNofAtoms (); ++i) {
        std::cout << std::setw (5) << cluster[i].Type () << std::setw (14)
                  << cluster[i].X () << std::setw (14) << cluster[i].Y ()
                  << std::setw (14) << cluster[i].Z () << std::setw (14)
                  << -g[3 * i] << std::setw (14) << -g[3 * i + 1]
                  << std::setw (14) << -g[3 * i + 2] << "\n";
      }
      std::cout << std::flush;
      delete[] g;
    } else if (do_minimization) {
      BH::BHMinimizationAlgorithm_LBFGSB minimizer;
      std::cout << "***********************************************\n";
      std::cout << "The energy of the configuration is\033[1m "
                << energy.Energy (x) << " \033[0m eV" << std::endl;
      std::cout << "***********************************************\n";
      std::cout << "!\tMinimized energy:\033[1m "
                << minimizer.Minimization (x, cluster.getNofAtoms (), &energy)
                << " \033[0m eV" << std::endl;
      std::ofstream outfile ("minimized_" + fname);
      if (outfile) {
        outfile << cluster.getNofAtoms () << "\n\n";
        outfile << std::setprecision (9);
        outfile << std::fixed;
        for (unsigned i = 0; i < cluster.getNofAtoms (); ++i) {
          outfile << std::setw (5) << cluster[i].Type () << std::setw (14)
                  << x[3 * i] << std::setw (14) << x[3 * i + 1]
                  << std::setw (14) << x[3 * i + 2] << "\n";
        }
      } else {
        std::cout << "Unable to open \"minimized_" << fname << "\"\n";
      }
    } else if (calculate_pressure) {
      BH::BHdouble *press = new BH::BHdouble[cluster.getNofAtoms ()];
      std::cout << "***********************************************\n";
      std::cout << "The energy of the configuration is \033[1m"
                << energy.Pressure (x, press) << "\033[0m eV" << std::endl;
      std::string baseFCC = cluster.Composition (0);
      std::cout << "Will be used the volume of the unit cell of the FCC "
                << baseFCC << "\n";
      std::cout << "Plotting the pressure map in \"pressure_" << fname
                << "\"\n";
      // BH::BHdouble volume= 1;//BH::BHMetalParameters::getBHMP(false)
      BH::BHdouble ret = bhmp.getRadius (baseFCC) * 2.0 * sqrt (2.0);
      BH::BHdouble volume = 160.2 * 4.0 / (3.0 * ret * ret * ret);
      // pres_atom(ind)=pres_atom(ind)*4.d0/(3.d0*arete(1)**3)*160.2
      std::ofstream outfile ("pressure_" + fname);
      if (outfile) {
        outfile << cluster.getNofAtoms () << "\n\n";
        outfile << std::setprecision (9);
        outfile << std::fixed;
        for (unsigned i = 0; i < cluster.getNofAtoms (); ++i) {
          outfile << std::setw (5) << cluster[i].Type () << std::setw (14)
                  << cluster[i].X () << std::setw (14) << cluster[i].Y ()
                  << std::setw (14) << cluster[i].Z () << std::setw (14)
                  << press[i] * volume << "\n";
        }
      } else {
        std::cout << "Unable to open \"pressure_" << fname << "\"\n";
      }
      delete[] press;
    } else {
      std::cout << "***********************************************\n";
      std::cout << "The energy of the configuration is \033[1m"
                << energy.Energy (x) << "\033[0m eV" << std::endl;
    }
    delete[] x;
  } catch (const char *problem) {
    std::cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m\n";
    retval = 1;
  } catch (const std::string &problem) {
    std::cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m\n";
    retval = 1;
  }
  return retval;
}
