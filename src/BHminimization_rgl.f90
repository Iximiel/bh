!*******************************************************************************
! Data iind Modifica: 30 novembre 2001                                       *
!                                                                              *
! Autore: ARNALDO RAPALLO                                                      *
!                                                                              *
! Consiglio Nazionale delle Ricerche (CNR):                                    *
! Istituto di Studi Chimico Fisici di Macromolecole Sintetiche e Naturali(IMAG)*
! Via de Marini 6                                                              *
! CAP 16149 Genova (GE)                                                        *
! tel.     +390106475868                                                       *
! Fax.     +390106475880                                                       *
! e-mail   rapallo@imag.ge.cnr.it                                              *
!*******************************************************************************

! DRIVER PER L'USO DI 'setulb'; ROUTINE DI MINIMIZZAZIONE DA MCS ANL. GOV.
! pacchetto di nome 'Lbfgsb.2.1.tar.gz' scaricabile da
! http://www.ece.nwu.edu/~nocedal/lbfgsb.html
!=========================================================================
!>This function pass the x array of the atoms positions to fortran and modifies it so that it will became the array of the minimized cluster
REAL(C_DOUBLE) FUNCTION BHminimization(data)bind(C,name="BHminimization")
  use iso_c_binding
  !USE PARAMETERS
  USE BHMODULE
  !USE FUNCTION_PAR 

  Implicit None

  Real(C_DOUBLE), Intent(inout) :: data(3*nat3D)
  ! Needed by the minimization routine setulb()
  Character(60) :: task, csave
  Logical :: lsave(4)
  !number_of_parameters = 3*nat3D
  !Integer :: n, m, iprint, nbd(number_of_parameters), &
  !     iwa(3*number_of_parameters)
  Integer :: n, m, iprint, nbd(3*nat3d), iwa(9*nat3d)
  Integer :: isave(44)
  Real(8) :: f, factr, pgtol
  Real(8) :: dsave(29)
  Real(8), Allocatable :: wa(:)
  Real(8) :: l(3*nat3d)
  Real(8) :: u(3*nat3d)
  Real(8) :: g(3*nat3d)!gradiente
  ! local 
  Integer, Parameter :: itmax=2000 !The maximum number of calls to setulb
  Real(8), Parameter :: epsi_lbfgsb=1.e-50_8 !Close to zero, used to check that 
  !setulb does not return the same energy
  !as in the previous step
  Real(8) :: rtol,funzval_old,df,funzval
  Integer :: iter !It counts the number of calls to Setulb()

  !real :: start, finish
  !!debugging
  ! integer::i
  !  write(*,*) "sto minimizzando"
  n = 3*nat3D

  iprint = -1 ! We suppress the default output.
  ! We suppress both code-supplied stopping tests because the
  ! user is providing his own stopping criteria.
  factr=1.0d0 !1.0d+7
  pgtol=0.d0 !1.0d-5

  ! We now specify nbd which defines the bounds on the variables:
  !                l   specifies the lower bounds,
  !                u   specifies the upper bounds.
  !     nbd(i)=0 if x(i) is unbounded,
  !            1 if x(i) has only a lower bound,
  !            2 if x(i) has both lower and upper bounds, and
  !            3 if x(i) has only an upper bound.

  !l(1:n)=0.000000000000001d0
  !u(1:n)=0.999999999999999d0
  nbd(1:n)=0 !unbounded minimization

  ! settaggio delle dimensioni del problema e delle 'limited memory corrections'
  ! N.B. valori raccomandati dagli autori per m: 3 <= m <= 20

  m=7 ! m is the number of limited memory corrections

  ! allocations/initializations
  Allocate(wa(2*m*n+4*n+12*m*m+12*m))
  iter=0
  iwa=0
  wa=0.d0
  isave=0
  dsave=0.d0
  lsave=.False.

  !do i=1,N
  !  write(*,*) data(i),data(i+N),data(i+2*N)
  !enddo
  !stop
  !serve per la routine di minimizzazione che vuole argomenti dbl-prc
  !call BHbigvoi(data)
  !call cpu_time(start)
  !call BHGradient(data,g,funzval)
  !call cpu_time(finish)
  !print '("mygradientTime = ",f6.3," seconds.")',finish-start
  !write(*,*)funzval
  !!furbescamente faccio in modo che force_rgl mi ritorni automaticamente il gradiente, su un unico array
  funzval_old = BHEnergy(data)
  open (22,file='minimization_fortran.out',status='unknown',position='append')
  !write(22,*)"NAtoms:",nat3D
  !We start the iteration by initializing task.
  task = 'START'
  DO

     call setulb(n,m,data,l,u,nbd,f,g,factr,pgtol,wa,iwa,task,iprint,&
          csave,lsave,isave,dsave)
     !write(22,*) task
     IF (task(1:2) .eq. 'FG') THEN
        f=BHGradient(data,g)
     ELSE IF (task(1:5) .eq. 'NEW_X') THEN
        ! the minimization routine as returned with a new iterate.
        ! At this point have the opportunity of stopping the iteration 
        ! or observing the values of certain parameters
        funzval=BHEnergy(data)
        df=abs(funzval_old-funzval)
        rtol=2.0_8*df/(abs(funzval_old)+abs(funzval))
        !write(22,*)funzval, funzval_old,df,rtol,epsi_lbfgsb
        funzval_old=funzval
        !   se la tolleranza e' soddisfatta o il numero massimo
        !   di iterazioni e' raggiunto il migliore individuo
        !   trovato sino ad ora viene restituito in P0 e funzval
        If((isave(30).Ge.itmax).and.(df.gt.epsi_lbfgsb)) Then
           !write(22,*)"*"
           task='STOP'
           !open (22,file='errors_bh.out',status='unknown',position='append')
           !write(22,*)"fortr_tol: the minimization exits with iter > itmax but task=NEW_X"
        Else If((rtol.Lt.ftoll).and.(df.gt.epsi_lbfgsb)) Then
           !write(22,*)"#"
           !write(22,*)"fortr:  tollerancepassed?"
           task='STOP'
        EndIf ! chiude If(rtol.Lt.ftoll) Then
     ELSEIF (task(1:6) .eq. 'ERROR') THEN
        !write(22,*)"fortr:",task
        task='STOP'
     ELSEIF (task(1:4) .eq. 'ABNO') THEN
        !write(22,*)"fortr:",task
        task='STOP'
     ELSEIF (task(1:4) .eq. 'WARN') THEN
        !write(22,*)"fortr:",task
        task='STOP'
     ELSEIF (task(1:4) .eq. 'CONV') THEN
        !the routine has achieved convergence
        task='STOP'
     ELSE IF (task(1:4) .eq. 'STOP') THEN
        EXIT
     ENDIF ! chiude else if (task(1:5) .eq. 'NEW_X') then

  ENDDO!while(iter.Lt.itmax)
  !if i got here iter>itmax and setulb did not returned 
  Deallocate(wa)
  !write(22,*)"steps: ", isave(30)
  !flush(0)
  close(22)
  BHminimization = BHEnergy(data)
END FUNCTION BHminimization


