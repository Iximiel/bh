/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Here the definitions the basin hopping algorithm

   @file BHbasinHopping.cpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 12/6/2019
   @version 0.13
   mke this class hinerith from an abstract class

   @date 1/5/2018
   @version 0.12
   restored original meaning for "true move", now the user setting for parameter
   are taken seriously, polished output files

   @date 26/4/2018
   @version 0.11
   added rew algorithm

   @date 10/3/2018
   @version 0.10.1

   adding separateg glo output per walker

   @date 10/3/2018
   @version 0.10

   added separated ouput ener_all per walker

   @date 20/6/2017
   @version 0.9.2
*/
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "BHbasinHopping.hpp"
#include "BHclusterUtilities.hpp"
#include "BHworkWithFortran.hpp"
//#include "BHdebug.hpp"
#include "BHconstructors.hpp"
#include "BHmetalParameters.hpp"

// remember always to use wID when selecting a walker!!!
// it is more stylish and easy to read

/*
  instruction for a benchmark:
  /usr/bin/time -o tempo -p ../main
*/
namespace BH {
  const char *BasinHopping::staticLogo () {
    return " ______  _     _             \n"
           "(____  \\| |   | |  _     _   \n"
           " ____)  ) |__ | |_| |_ _| |_ \n"
           "|  __  (|  __)| (_   _|_   _)\n"
           "| |__)  ) |   | | |_|   |_|  \n"
           "|______/|_|   |_|            \n";
  }
  const char *BasinHopping::logo () { return staticLogo (); }
  BasinHopping::BasinHopping (const BHSettings &settings)
    : BHAbstractBasinHopping (settings) {}

  BasinHopping::~BasinHopping () = default;

  const std::string BasinHopping::BHStyleName () { return "BasinHopping"; }
} // namespace BH
