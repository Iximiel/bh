/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Definitions of BHOrderParameter

@file BHorderParameter.cpp
  @author Daniele Rapetti (iximiel@gmail.com)

@date 05/03/2021
    @version 1.0

    Updated the parser and added Q4 and Q6 compatibility

  @date 19/11/2017
  @version 0.1

  moved from BHsettings in order to separate the dependecies
    */
#include "BHorderParameter.hpp"
#include <algorithm>
#include <iostream>
#include <regex>
#include <tuple>
#include <utility>

namespace BH {
  BHOrderParameter::BHOrderParameter () = default;

  BHOrderParameter::BHOrderParameter (const BHOrderParameter &other) = default;

  BHOrderParameter::BHOrderParameter (const char input[]) {
    initialize (std::string (input));
  }

  BHOrderParameter::BHOrderParameter (const std::string input) {
    initialize (input);
  }
  /**values for input:
   * - `signature rst` or `rst` where `r`, `s` and `t` are number -> this
   * BHOrderParameter is a 'signature rst'
   * - `XxYy` are two atomic species, or the same atomic species -> this
   * BHOrderParameter is a 'bonds XxYy'
   * - - if `surf` is present (like `surf[ace][B] AgAu`) -> this
   * BHOrderParameter is a 'surface bonds XxYy'
   * - `Xx` is an atomic species  -> this BHOrderParameter is a 'surface Xx'
   * - `mix`-> this BHOrderParameter is a 'bonds mix'
   * - - `surf[ace][B] mix`-> this BHOrderParameter is a 'surface bonds mix'
   */
  void BHOrderParameter::initialize (std::string input) {
    std::string inputLowered;
    std::transform (
      input.begin (), input.end (), inputLowered.begin (),
      [] (unsigned char c) { return std::tolower (c); });
    // signature regex: /[0-9][0-9][0-9]/
    // Q* regex: /[qQ][46]/
    // sqrt : /[sS][qQ][rR][tT]/
    // log /[lL][oO][gG]/
    // std::regex sqwr("sqrt",std::regex::icase); icase ignore the case
    std::smatch RegexMatch;

    for (std::tuple<std::regex, BHOrderParameterMathOperator> t :
         {std::make_tuple (
            std::regex ("sqrt", std::regex::icase),
            BHOrderParameterMathOperator::doSqrt),
          std::make_tuple (
            std::regex ("log", std::regex::icase),
            BHOrderParameterMathOperator::doLog)}) {
      if (std::regex_search (input, RegexMatch, std::get<0> (t))) {
        input = RegexMatch.prefix ();
        input += RegexMatch.suffix ();
        MathOperation_ = static_cast<BHOrderParameterMathOperator> (
          MathOperation_ | std::get<1> (t));
      }
    }
    if (std::regex_search (input, RegexMatch, std::regex ("[0-9][0-9][0-9]"))) {
      OPtype_ = BHOrderParameter_type::signature;
      option_ = RegexMatch.str (0);
#ifdef BOOSTFOUND
    } else if (std::regex_search (input, RegexMatch, std::regex ("[qQ][46]"))) {
      OPtype_ = BHOrderParameter_type::bondOrientation;
      option_ = RegexMatch.str (0)[1];
#endif // BOOSTFOUND
    } else {
      size_t tofind;
      OPtype_ = BHOrderParameter_type::bonds;
      option_ = input; // coud be also "mix"
      /// Atom types ca be separated by spaces or `,.-_`.
      // These few lines will delete all the separator characters
      tofind = option_.find_first_of (" ,.-_");
      while (tofind != std::string::npos) {
        option_.erase (tofind, 1);
        tofind = option_.find_first_of (" ,.-_");
      }
      // now i will delete all the option keywords that i can find in the input,
      // in option_ should remain only the atom types
      for (std::tuple<std::regex, BHOrderParameter_type> t :
           /// bonds can be specified by using `bonds`.
           {std::make_tuple (
              std::regex ("bonds", std::regex::icase),
              BHOrderParameter_type::bonds),
            /// To specify surface you can use `surface`, `surfB`, or `surf`.
            std::make_tuple (
              std::regex ("surf(ace|)(b|)", std::regex::icase),
              BHOrderParameter_type::surfaceBonds),
            /// To ask aggregation analisys you can use 'islands', 'island',
            /// 'aggregation', 'aggr'
            std::make_tuple (
              std::regex ("island(s|)", std::regex::icase),
              BHOrderParameter_type::aggregation),
            std::make_tuple (
              std::regex ("aggr(egation|)", std::regex::icase),
              BHOrderParameter_type::aggregation)}) {
        if (std::regex_search (option_, RegexMatch, std::get<0> (t))) {
          OPtype_ = std::get<1> (t);
          option_ = RegexMatch.prefix ();
          option_ += RegexMatch.suffix ();
        }
      }
      // island are protected in BHclusterAnalyser
      // If it find less than 4 letter after removing all separation and finds
      // that theall specifications looks for surface atoms
      if (
        option_.size () < 4 && option_ != "mix" &&
        OPtype_ != BHOrderParameter_type::aggregation) {
        OPtype_ = BHOrderParameter_type::surfaceAtoms;
      }
    }
  }

  bool BHOrderParameter::operator== (const BHOrderParameter &x) const {
    // std :: cout <<OPtype_ <<"==" <<  x.OPtype_ <<","<< option_ <<"=="<<
    // x.option_<< std:: endl;
    return OPtype_ == x.OPtype_ && option_ == x.option_;
  }

  std::string OparName (const BHOrderParameter &Opar) {
    std::string toReturn;
    if (Opar.MathOperation_ == BHOrderParameter::doSqrt) {
      toReturn = "sqrt_";
    } else if (Opar.MathOperation_ == BHOrderParameter::doLog) {
      toReturn = "log_";
    }
    switch (Opar.OPtype_) {
    case BHOrderParameter::BHOrderParameter_type::bonds:
      toReturn += "bonds_" + Opar.option_;
      break;
    case BHOrderParameter::BHOrderParameter_type::signature:
      toReturn += "sign_" + Opar.option_;
      break;
    case BHOrderParameter::BHOrderParameter_type::surfaceBonds:
      toReturn += "surfB_" + Opar.option_;
      break;
    case BHOrderParameter::BHOrderParameter_type::surfaceAtoms:
      toReturn += "surface_" + Opar.option_;
      break;
    case BHOrderParameter::BHOrderParameter_type::aggregation:
      toReturn += "aggr_" + Opar.option_;
      break;
    case BHOrderParameter::BHOrderParameter_type::bondOrientation:
      toReturn += "Q" + Opar.option_;
      break;
    default:
      toReturn = "OP_err";
    }
    return toReturn;
  }
  // those are the old defaults, here for  compatiblity sake, works only with
  // AgCu clusters
  BHOrderParameter OPfromInt (int id) {
    switch (id) {
    case 1:
      return BHOrderParameter ("AgCu");
    case 2:
      return BHOrderParameter ("AgAg");
    case 3:
      return BHOrderParameter ("CuCu");
    case 4:
      return BHOrderParameter ("555");
    case 5:
      return BHOrderParameter ("422");
    case 6:
      return BHOrderParameter ("AgCu"); // default was fractionsubstrate
    default:
      return BHOrderParameter ("AgCu");
    }
  }
} // namespace BH
