/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @file BHclusterAtoms.cpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @brief Definition of class BHClusterAtoms

   @version 0.10
   @date 7/2/2018

   Now it can load xyz files even if they have more than 4 fields per atoms,
   simply ingoring the field past the 4th. THe modify has been done in the
   constructor for BHClusterAtoms and in   std::istream& operator>>
   (std::istream& stream, BHClusterAtoms& obj)

   @version 0.9
   @date 4/7/2017

*/
#include <fstream>
#include <iostream>
#include <sstream>
// for sorting
#include <algorithm>
#include <vector>

#include "BHclusterAtoms.hpp"

namespace BH {
  BHClusterAtoms::BHClusterAtoms () = default;

  BHClusterAtoms::BHClusterAtoms (const unsigned int NAtoms)
    : NofAtoms_ (NAtoms),
      Atoms_ (new BHAtom[NofAtoms_]) {}

  BHClusterAtoms::BHClusterAtoms (const unsigned int NAtoms, BHAtom *newAtoms)
    : NofAtoms_ (NAtoms),
      Atoms_ (new BHAtom[NofAtoms_]) {
    for (unsigned int i = 0; i < NofAtoms_; ++i) {
      Atoms_[i] = newAtoms[i];
    }
    updateComposition ();
  }

  BHClusterAtoms::BHClusterAtoms (std::vector<BHAtom> newAtoms)
    : NofAtoms_ (static_cast<unsigned> (newAtoms.size ())),
      Atoms_ (new BHAtom[NofAtoms_]) {
    for (unsigned int i = 0; i < NofAtoms_; ++i) {
      Atoms_[i] = newAtoms[i];
    }
    updateComposition ();
  }

  BHClusterAtoms::BHClusterAtoms (const BHClusterAtoms &other)
    : NofAtoms_ (other.NofAtoms_),
      Atoms_ (new BHAtom[NofAtoms_]),
      additionalInfos_ (other.additionalInfos_),
      Composition_ (other.Composition_) {
    for (unsigned int i = 0; i < NofAtoms_; i++) {
      Atoms_[i] = other.Atoms_[i];
    }
    updateComposition ();
  }

  BHClusterAtoms::BHClusterAtoms (BHClusterAtoms &&other) noexcept
    : NofAtoms_ (other.NofAtoms_),
      Atoms_ (nullptr),
      additionalInfos_ (std::move (other.additionalInfos_)),
      Composition_ (std::move (other.Composition_)) {
    Atoms_ = other.Atoms_;
    other.Atoms_ = nullptr;
    updateComposition ();
  }

  BHClusterAtoms::BHClusterAtoms (std::string fname) {
    std::ifstream file (fname);
    if (file.good ()) {
      file >> NofAtoms_;
      file.ignore (
        std::numeric_limits<std::streamsize>::max (),
        '\n'); // moves the comment line
      std::getline (file, additionalInfos_);
      Atoms_ = new BHAtom[NofAtoms_];
      std::string t;
      for (unsigned int i = 0; i < NofAtoms_; i++) {
        std::getline (file, t);
        std::stringstream atom_info (t);
        atom_info >> Atoms_[i];
        // std::cout << Atoms_[i]<<std::endl;
      }
      // std::cout << "caricato"<<std::endl;
      updateComposition ();
    } else {
      throw "BHClusterAtoms file " + fname + " not valid";
    }
  }

  BHClusterAtoms::~BHClusterAtoms () { delete[] Atoms_; }

  BHClusterAtoms &BHClusterAtoms::operator= (const BHClusterAtoms &other) {
    if (this != &other) { // protect against invalid self-assignment
      Composition_ = other.Composition_;
      additionalInfos_ = other.additionalInfos_;
      if (NofAtoms_ != other.NofAtoms_) {
        NofAtoms_ = other.NofAtoms_;
        // true if different from nullptr
        delete[] Atoms_;
        Atoms_ = new BHAtom[NofAtoms_];
      }
      for (unsigned int i = 0; i < NofAtoms_; i++) {
        Atoms_[i] = other.Atoms_[i];
      }
    }
    return *this;
  }

  BHClusterAtoms &BHClusterAtoms::operator= (BHClusterAtoms &&other) noexcept {
    if (this != &other) { // protect against invalid self-assignment
      NofAtoms_ = other.NofAtoms_;
      delete[] Atoms_;
      Atoms_ = other.Atoms_;
      other.Atoms_ = nullptr;
      Composition_ = std::move (other.Composition_);
      additionalInfos_ = std::move (other.additionalInfos_);
      updateComposition ();
    }
    return *this;
  }

  BHClusterAtoms &BHClusterAtoms::operator<< (const BHClusterAtoms &other) {
    if (this != &other) { // protect against invalid self-assignment
      if (NofAtoms_ != other.NofAtoms_) {
        throw "BHClusterAtoms_operator<<_differentSize";
      }
      for (unsigned int i = 0; i < NofAtoms_; i++) {
        Atoms_[i] << other.Atoms_[i];
      }
    }
    return *this;
  }
  void BHClusterAtoms::AssignFromVector (BHdouble *x) {
    for (unsigned int i = 0; i < NofAtoms_; ++i) {
      Atoms_[i].setXYZ ((x + i * 3));
    }
  }

  void BHClusterAtoms::AssignToVector (BHdouble *x) const {
    for (unsigned int i = 0; i < NofAtoms_; ++i) {
      Atoms_[i].copyXYZto ((x + i * 3));
    }
  }

  BHAtom *BHClusterAtoms::begin () const noexcept { return &Atoms_[0]; }

  BHAtom *BHClusterAtoms::end () const noexcept { return &Atoms_[NofAtoms_]; }

  BHAtom const *BHClusterAtoms::cbegin () const noexcept { return &Atoms_[0]; }

  BHAtom const *BHClusterAtoms::cend () const noexcept {
    return &Atoms_[NofAtoms_];
  }

  BHAtom &BHClusterAtoms::operator[] (const unsigned i) { return Atoms_[i]; }
  BHAtom &BHClusterAtoms::operator() (const unsigned i) { return Atoms_[i]; }
  const BHAtom &BHClusterAtoms::operator[] (const unsigned i) const {
    return Atoms_[i];
  }
  const BHAtom &BHClusterAtoms::operator() (const unsigned i) const {
    return Atoms_[i];
  }

  BHVector BHClusterAtoms::getGeometricCenter () const {
    BHVector center (0.0, 0.0, 0.0);
    for (unsigned int i = 0; i < NofAtoms_; ++i) {
      center += Atoms_[i];
    }
    center /= BHdouble (NofAtoms_);
    return center;
  }

  BHdouble BHClusterAtoms::getGeometricSphereRadius () const {
    BHVector center (0.0, 0.0, 0.0);
    for (unsigned int i = 0; i < NofAtoms_; ++i) {
      center += Atoms_[i];
    }
    center /= BHdouble (NofAtoms_);
    BHdouble radius = 0;
    for (unsigned int i = 0; i < NofAtoms_; ++i) {
      BHdouble t = center.dist (Atoms_[i]);
      if (t > radius) {
        radius = t;
      }
    }
    return radius;
  }

  // getters
  unsigned BHClusterAtoms::getNofAtoms () const { return NofAtoms_; }
  unsigned int BHClusterAtoms::NofSpecies () const {
    return static_cast<unsigned> (Composition_.size ());
  }
  std::string BHClusterAtoms::Composition () const {
    std::string toret;
    if (!Composition_.empty ()) {
      toret = Composition_[0];
      for (size_t i = 1; i < Composition_.size (); ++i) {
        toret += ' ' + Composition_[i];
      }
    }
    return toret;
  }
  /// NB:no protection from errors!!!
  std::string BHClusterAtoms::Composition (const unsigned i) const {
    return Composition_.at (i);
  }
  void BHClusterAtoms::updateComposition () {
    Composition_.clear ();
    for (unsigned int i = 0; i < NofAtoms_; ++i) {
      if (
        std::find (
          Composition_.begin (), Composition_.end (), Atoms_[i].Type ()) ==
        Composition_.end ()) {
        Composition_.push_back (Atoms_[i].Type ());
      }
    }
  }

  bool BHClusterAtoms::testSameConfiguration (
    const BHClusterAtoms &testcluster, bool verbose) const {
    bool toreturn = true;
    if (NofAtoms_ != testcluster.NofAtoms_) {
      if (verbose) {
        std::cout << "testSameConfiguration:: different number of atoms"
                  << std::endl;
      }
      return false;
    }
    for (unsigned int i = 0; i < NofAtoms_; i++) {
      if (Atoms_[i].Type () != testcluster[i].Type ()) {
        toreturn = false;
        if (verbose) {
          std::cout
            << "testSameConfiguration:: different kind of atoms, for ID " << i
            << ": " << Atoms_[i].Type () << ", " << testcluster[i].Type ()
            << std::endl;
        }
      }
    }
    return toreturn;
  }

  void BHClusterAtoms::setAdditionalInfos (const std::string &newInfos) {
    additionalInfos_ = newInfos;
  }

  std::string BHClusterAtoms::getAdditionalInfos () const {
    return additionalInfos_;
  }

  // assigns
  void BHClusterAtoms::setNofAtoms (unsigned newN) {
    if (newN != NofAtoms_) {
      NofAtoms_ = newN;
      delete[] Atoms_;
      Atoms_ = new BHAtom[NofAtoms_];
    }
  }

  void BHClusterAtoms::take_positions (const BHClusterAtoms &other) {
    if (this != &other) { // protect against self-assignment wich is a time loss
      if (NofAtoms_ != other.NofAtoms_) {
        throw "BHClusterAtoms::take_positions: Trying to take_position from a "
              "different sized cluster";
      }
      for (unsigned int i = 0; i < NofAtoms_; i++) {
        Atoms_[i] << other.Atoms_[i];
      }
    }
  }

  BHClusterAtoms BHClusterAtoms::getScaledCluster (
    BHClusterAtoms cluster, const BHdouble &scale) {
    for (unsigned int i = 0; i < cluster.NofAtoms_; i++) {
      cluster.Atoms_[i] *= scale;
    }
    return cluster;
  }

  BHClusterAtoms
  BHClusterAtoms::getCenteredCluster (BHClusterAtoms cluster, BHVector center) {
    /*::operator<<(std::cout ,cluster);
    std::cout <<std::endl;*/
    center -= cluster.getGeometricCenter ();
    for (unsigned int i = 0; i < cluster.NofAtoms_; i++) {
      cluster.Atoms_[i] += center;
    }
    return cluster;
  }
  /*
  //IO
  std::istream& operator>> (std::istream& stream, BHClusterAtoms& obj){
    return ::operator>>(stream,obj);
  }
  std::ostream& operator<< (std::ostream& stream, const BHClusterAtoms& obj) {
    return ::operator<<(stream,obj);
  }
*/
} // namespace BH
// IO
std::ostream &operator<< (std::ostream &stream, const BH::BHClusterAtoms &obj) {
  stream << obj.getNofAtoms () << '\n';
  if (obj.getAdditionalInfos ().empty ()) {
    stream << obj.Composition ();
  } else {
    stream << obj.getAdditionalInfos ();
  }

  for (unsigned int i = 0; i < obj.getNofAtoms (); i++) {
    stream << '\n' << obj[i];
  }
  return stream;
}

std::istream &operator>> (std::istream &stream, BH::BHClusterAtoms &obj) {
  unsigned int N;
  stream >> N;
  obj.setNofAtoms (N);
  std::string dummy;
  stream.ignore (
    std::numeric_limits<std::streamsize>::max (),
    '\n'); // moves the comment line
  std::getline (stream, dummy);
  obj.setAdditionalInfos (dummy);
  //  stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');//ignores
  //  the comment line
  for (unsigned int i = 0; i < N; i++) {
    std::getline (stream, dummy);
    std::stringstream atom_info (dummy);
    atom_info >> obj[i];
  }
  obj.updateComposition ();
  return stream;
}
