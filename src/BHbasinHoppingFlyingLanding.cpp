/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Here the definitions the basin hopping algorithm

   @file BHbasinHoppingFlyingLanding.cpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 10/9/2019
   @version 0.1
   mke this class hinerith from an abstract class
*/
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "BHbasinHoppingFlyingLanding.hpp"
#include "BHclusterUtilities.hpp"
#include "BHworkWithFortran.hpp"
//#include "BHdebug.hpp"
#include "BHconstructors.hpp"
#include "BHenums.hpp"
#include "BHmetalParameters.hpp"
// remember always to use wID when selecting a walker!!!
// it is more stylish and easy to read

/*
  instruction for a benchmark:
  /usr/bin/time -o tempo -p ../main
*/
namespace BH {
  const char *BHBasinHoppingFlyingLanding::staticLogo () {
    return " ______  _     _  ___ _ \n"
           "(____  \\| |   | |/ __) |\n"
           " ____)  ) |__ | | |__| |\n"
           "|  __  (|  __)| |  __) |\n"
           "| |__)  ) |   | | |  | |\n"
           "|______/|_|   |_|_|  |_|\n";
  }
  const char *BHBasinHoppingFlyingLanding::logo () { return staticLogo (); }
  BHBasinHoppingFlyingLanding::BHBasinHoppingFlyingLanding (
    const BHSettings &settings)
    : BHAbstractBasinHopping (settings) {
    BHSettingsUtilities::setFlyingLandingMinimization (settings_);
    std::ofstream fileout ("input_bh++.in.bhFL.processed");
    fileout << BHSettingsUtilities::printInputFileFromData (settings_)
            << std::endl;
  }

  BHBasinHoppingFlyingLanding::~BHBasinHoppingFlyingLanding () = default;

  const std::string BHBasinHoppingFlyingLanding::BHStyleName () {
    return "Flying-Landing";
  }

} // namespace BH
