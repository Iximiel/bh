/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief A small ulitilty to shift the cluster in order to center it on a given
   place I've decided to give the cluster as an argument and the center as an
   external file in order to simplify the use of this utility in a script If
   there isn't a file called center.in it simply tells where the center is then
   exits with an exception

   @file centerer.cxx
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 4/7/2016
   @version 1.0
*/

#include <fstream>
#include <iomanip>
#include <iostream>

#include "BHclusterAtoms.hpp"
//#include "BHclusterUtilities.hpp"

using namespace BH;

int main (int argc, char **argv) {
  try {
    if (argc == 1) {
      std::cout << "USAGE:\n"
                << "create a file named 'center.in' with the coordinates where "
                   "you'd like to center the cluster.\n"
                << "Then launch" << argv[0]
                << "with the xyz file of the centered cluster as argument.\n\n"
                << "Would you like to create 'center.in'?\n"
                << "Type 'y' if you want:";
      auto typed = std::cin.get ();
      if (typed == 'y' || typed == 'Y') {
        std::ofstream center ("center.in");
        center << "0\t0\t0" << std::endl;
        center.close ();
      }
    } else {
      std::ifstream cluster (argv[1]);
      if (!cluster) {
        throw "cannot open '" + std::string (argv[1]) + "'";
      }

      std::ifstream center ("center.in");
      if (!center) {
        throw "cannot open 'center.in'";
      }
      BHVector theCenter (0, 0, 0);
      center >> theCenter;

      BHClusterAtoms mycluster;
      cluster >> mycluster;

      mycluster = BHClusterAtoms::getCenteredCluster (mycluster, theCenter);
      std::cout << "Saved the moved cluster in: "
                << "centered_" << std::string (argv[1]) << std::endl;
      std::ofstream outcluster ("centered_" + std::string (argv[1]));
      outcluster << std::setprecision (9) << mycluster << std::endl;
      outcluster.close ();
    }
  } catch (const char *problem) {
    std::cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m"
              << std::endl;
    return 1;
  } catch (const std::string &problem) {
    std::cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m"
              << std::endl;
    return 1;
  }
  return 0;
}
