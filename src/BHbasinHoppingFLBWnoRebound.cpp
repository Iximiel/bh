/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Here the definitions the basin hopping algorithm flying/landong with
best walkers

@file BHbasinHoppingFlyingLandingWBestWalker.cpp
    @author Daniele Rapetti (iximiel@gmail.com)

    @date 05/01/2020
    @version 1.0
    adding the best walker to the standar FL bh algorithm
        */

#include "BHclusterUtilities.hpp"
#include "BHconstructors.hpp"
#include "BHmetalParameters.hpp"
#include <iomanip>
#include <iostream>
#include <sstream>

#include "BHbasinHoppingFLBWnoRebound.hpp"
namespace BH {

  const char *BHBasinHoppingFlyingLandingWBWnoRestart::staticLogo () {
    return " ______  _     _  ___ _ _           \n"
           "(____  \\| |   | |/ __) | |          \n"
           " ____)  ) |__ | | |__| | | _  _ _ _ \n"
           "|  __  (|  __)| |  __) | || \\| | | |\n"
           "| |__)  ) |   | | |  | | |_) ) | | |\n"
           "|______/|_|   |_|_|  |_|____/ \\____|\n";
  }

  const char *BHBasinHoppingFlyingLandingWBWnoRestart::logo () {
    return staticLogo ();
  }

  BHBasinHoppingFlyingLandingWBWnoRestart::
    BHBasinHoppingFlyingLandingWBWnoRestart (const BHSettings &settings)
    : BHAbstractBasinHopping (settings) {
    BH::BHSettingsUtilities::setFLMinimizationwBestWalkerNoRestart (settings_);
    std::ofstream fileout ("input_bh++.in.bhFLH2.processed");
    fileout << BHSettingsUtilities::printInputFileFromData (settings_)
            << std::endl;
  }

  BHBasinHoppingFlyingLandingWBWnoRestart::
    ~BHBasinHoppingFlyingLandingWBWnoRestart () = default;

  const std::string BHBasinHoppingFlyingLandingWBWnoRestart::BHStyleName () {
    return "Flying-Landing-bestWalker";
  }
} // namespace BH
