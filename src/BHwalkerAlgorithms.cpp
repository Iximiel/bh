/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/** @brief Definitions of the known algorithms
 * @file BHwalkerAlgorithms.cpp
 * @author Daniele Rapetti (iximiel@gmail.com)
 *
 * @date 20/11/2019
 * @version 0.1
 * List of the known algorithms:
 */
#include "BHwalker.hpp"
#include <vector>
namespace BH {

  // BHWalkerHistogram
  bool HistoAlgorithm (
    std::vector<BHWalker *>,
    BHWalker::BHWalkerAlgorithmSupport &support,
    rnd_engine &rng) {
    BHdouble EnergyModifier = 0.0;
    BHdouble oldOP0 = lastAcceptedOP0_, oldOP1 = lastAcceptedOP1_;
    BeAnalyzed (support.Analyzer);
    EnergyModifier = -support.histo.getCalculatedHistoWeight (
      lastAcceptedOP0_, lastAcceptedOP1_, oldOP0, oldOP1);

    return verifyStep (rng, EnergyModifier);
  }
} // namespace BH
