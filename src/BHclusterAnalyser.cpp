/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Definition of BHClusterAnalyser

@file BHclusterAnalyser.cpp
  @author Daniele Rapetti (iximiel@gmail.com)

@date 05/03/2021
    @version 0.7

Changed the process for calculating the OPs and added Q4 and Q6

  @date 25/6/2018
  @version 0.6

Adding couple return to the CNA

  @date 31/5/2017
  @version 0.5

  */
#include "BHclusterAnalyser.hpp"

#include <algorithm>
#include <map>

#include "BHclusterUtilities.hpp"
#include "BHdebug.hpp"

#ifdef BOOSTFOUND
#include <boost/math/special_functions/spherical_harmonic.hpp>
#include <complex>
#endif // BOOSTFOUND

#ifdef ANALYZERDEBUG
#include <iostream>
#endif // ANALYZERDEBUG

namespace BH {
  // control the number of bonds between atoms on surface
  constexpr unsigned short few_neigh =
    10; // if 9 or less neighbours the atom is considered to be in surface
#ifdef BOOSTFOUND
  template <int l>
  inline BHdouble
  squaredQ (const int m, const BHCluster &cluster, const unsigned atomID) {
    std::complex<BHdouble> Qlm = {0.0, 0.0};
    BHdouble theta, phi;
    BHVector t;
    for (auto &neighbour : cluster.getNNs (atomID)) {
      t = cluster[atomID] - cluster[neighbour];
      theta = t.Theta ();
      phi = t.Phi ();
      Qlm += boost::math::spherical_harmonic (l, m, theta, phi);
    }
    Qlm /= static_cast<BHdouble> (cluster.getNNs (atomID).size ());
    BHdouble r = Qlm.real ();
    BHdouble i = Qlm.imag ();
    return r * r + i * i;
  }
  template <int l>
  BHdouble Qcalculator (const BHCluster &cluster, const unsigned atomID) {
    /// @cite 10.1103/PhysRevB.28.784
    BHdouble sum = 0;
    for (int m = -l; m <= l; ++m) {
      sum += squaredQ<l> (m, cluster, atomID);
    }
    return sqrt ((4 * M_PI) / (2 * l + 1) * sum);
  }
#endif // BOOSTFOUND

  BHClusterAnalyser::BHClusterAnalyser (
    const std::vector<BHOrderParameter> &opData, const BHMetalParameters &bhmp)
    : opData_ (opData),
      OPcalculators_ (
        // OPCalculators is a const vector: i'm using a lambda to initialize
        // it
        [this, &bhmp] (const std::vector<BHOrderParameter> &OPd) {
          std::vector<BHOPHandler> OPHlist;
          for (unsigned j = 0; j < OPd.size (); ++j) {
            // std::cerr << j << " " << OparName(opData[j]) << std::endl;
            BHOPHandler OPH;
            switch (OPd[j].OPtype_) {
            case BHOrderParameter::BHOrderParameter_type::signature: {
              short signature = static_cast<short> (std::stoi (OPd[j].option_));
              OPH.Action = [signature] (
                             const BHCluster &cluster,
                             const BHMetalParameters &,
                             DataContainer &data) -> BHdouble {
                if (data.NeedCNA_) {
                  data.NeedCNA_ = false;
                  std::vector<BHCouple> couples =
                    BHClusterUtilities::calculateCNAsignatures (cluster);
                  data.NofCouples_ =
                    static_cast<typeof (data.NofCouples_)> (couples.size ());
                  data.signatures_ =
                    BHClusterUtilities::extractCNASignatures (couples);
                }
                return data.signatures_[signature] /
                       static_cast<BHdouble> (data.NofCouples_);
              };
            } break;
            case BHOrderParameter::BHOrderParameter_type::surfaceAtoms: {
              auto typeID = bhmp.getSlabels (OPd[j].option_);
              OPH.Action = [this, typeID] (
                             const BHCluster &cluster,
                             const BHMetalParameters &BHMP,
                             DataContainer &data) -> BHdouble {
                conditionalSurfaceAnalysis (data, cluster, BHMP);
                return data.AtomsOnSurf_[typeID] /
                       static_cast<BHdouble> (data.NSurf_);
              };
            } break;
            case BHOrderParameter::BHOrderParameter_type::surfaceBonds: {
              std::string lowerOPT = OPd[j].option_;
              std::transform (
                lowerOPT.begin (), lowerOPT.end (), lowerOPT.begin (),
                ::tolower);
              if (lowerOPT.find ("mix") != std::string::npos) {
                OPH.Action = [this] (
                               const BHCluster &cluster,
                               const BHMetalParameters &BHMP,
                               DataContainer &data) -> BHdouble {
                  conditionalSurfaceAnalysis (data, cluster, BHMP);
                  return data.SurfaceMixedCounter_ /
                         static_cast<BHdouble> (data.Nbounds_);
                };
              } else {
                // cout << opData[j].option_ <<" " <<
                // opData[j].option_.size()<< endl;
                unsigned INTid = bhmp.getPlabels (OPd[j].option_);
                OPH.Action = [this, INTid] (
                               const BHCluster &cluster,
                               const BHMetalParameters &BHMP,
                               DataContainer &data) -> BHdouble {
                  conditionalSurfaceAnalysis (data, cluster, BHMP);
                  return data.SurfaceBondsCounter_[INTid] /
                         static_cast<BHdouble> (data.Nbounds_);
                };
              }
            } break;
            case BHOrderParameter::BHOrderParameter_type::bonds: {
              std::string lowerOPT = OPd[j].option_;
              std::transform (
                lowerOPT.begin (), lowerOPT.end (), lowerOPT.begin (),
                ::tolower);
              if (lowerOPT.find ("mix") != std::string::npos) {
                OPH.Action = [] (
                               const BHCluster &, const BHMetalParameters &,
                               DataContainer &data) -> BHdouble {
                  // these data are already been calculated
                  return data.MixedCounter_ /
                         static_cast<BHdouble> (data.Nbounds_);
                };
              } else {
                // cout << opData[j].option_ <<" " <<
                // opData[j].option_.size()<< endl;
                unsigned INTid = bhmp.getPlabels (OPd[j].option_);
                OPH.Action = [INTid] (
                               const BHCluster &, const BHMetalParameters &,
                               DataContainer &data) -> BHdouble {
                  return data.BondsCounter_[INTid] /
                         static_cast<BHdouble> (data.Nbounds_);
                };
              }
            } break;
            case BHOrderParameter::BHOrderParameter_type::aggregation: {
              auto typeID = bhmp.getSlabels (OPd[j].option_);
              OPH.Action = [this, typeID] (
                             const BHCluster &cluster,
                             const BHMetalParameters &BHMP,
                             DataContainer &data) -> BHdouble {
                if (data.NeedIsleParameters_) {
                  data.NeedIsleParameters_ = false;
                  data.AggregationParameters_ =
                    IsleParameters (cluster, BHMP, false);
                }
                return data.AggregationParameters_[typeID];
              };
            } break;
            case BHOrderParameter::BHOrderParameter_type::unset:
              throw "BHClusterAnalyser: OP" + std::to_string (j) + " is unset";
#ifdef BOOSTFOUND
            case BHOrderParameter::BHOrderParameter_type::bondOrientation: {
              int L = std::stoi (OPd[j].option_);
              if (L == 4) {
                OPH.Action = [] (
                               const BHCluster &cluster,
                               const BHMetalParameters &,
                               DataContainer &data) -> BHdouble {
                  if (data.NeedQ4_) {
                    data.NeedQ4_ = false;
                    data.Q4_ = 0;
                    const unsigned NofAtoms = cluster.getNofAtoms ();
                    for (unsigned i = 0; i < NofAtoms; ++i) {
                      data.Q4_ += Qcalculator<4> (cluster, i);
                    }
                    data.Q4_ /= NofAtoms;
                  }
                  return data.Q4_;
                };
                break;
              } else if (L == 6) {
                OPH.Action = [] (
                               const BHCluster &cluster,
                               const BHMetalParameters &,
                               DataContainer &data) -> BHdouble {
                  if (data.NeedQ6_) {
                    data.NeedQ6_ = false;
                    data.Q6_ = 0;
                    const unsigned NofAtoms = cluster.getNofAtoms ();
                    for (unsigned i = 0; i < NofAtoms; ++i) {
                      data.Q6_ += Qcalculator<6> (cluster, i);
                    }
                    data.Q6_ /= NofAtoms;
                  }
                  return data.Q6_;
                };
              }
              break;
            }
#endif // BOOSTFOUND
            default:
              throw "BHClusterAnalyser: OP" + std::to_string (j) +
                " is not compatible";
            }
            if (OPd[j].MathOperation_ == BHOrderParameter::none) {
              OPH.PostProduction = [] (const BHdouble &x) { return x; };
            } else if (OPd[j].MathOperation_ == BHOrderParameter::doSqrt) {
              OPH.PostProduction = [] (const BHdouble &x) { return sqrt (x); };
            } else if (OPd[j].MathOperation_ == BHOrderParameter::doLog) {
              OPH.PostProduction = [] (const BHdouble &x) { return log (x); };
            }
            OPHlist.push_back (OPH);
#ifdef ANALYZERDEBUG
            std::cerr << Opar_name (opData[j]) << " : ";
            std::cerr << sPars_[i] << sPars_[i + 1] << sPars_[i + 2] << "->"
                      << int (sPars_[i]) << " " << int (sPars_[i + 1]) << " "
                      << int (sPars_[i + 2]) << std::endl;
#endif // ANALYZERDEBUG
          }
          return OPHlist;
        }(opData)),
      cutOff_end2_ (bhmp.getNofInteractions ()),
      NNdist_tol2_ (bhmp.getNofInteractions ()) {
    // Stores the interaction labels from BHMP for faster referencing times
    for (unsigned i = 0; i < cutOff_end2_.size (); ++i) {
      BHdouble cutOffEnd = bhmp.getCutOffEnd (i);
      cutOff_end2_[i] = cutOffEnd * cutOffEnd;
      NNdist_tol2_[i] = bhmp.getD2tol (i);
    }
  }

  unsigned BHClusterAnalyser::Npars () const {
    return static_cast<unsigned> (OPcalculators_.size ());
  }

  void BHClusterAnalyser::times (std::ostream &stream) const {
    stream << "analysis:\t" << n_analysis << ",\t";
    timing (stream, analysis_time);
    stream << '\n';
  }

  BHClusterAnalyser::~BHClusterAnalyser () = default;

  std::vector<BHdouble> BHClusterAnalyser::IsleParameters (
    const BHCluster &cluster,
    const BHMetalParameters &bhmp,
    bool plotAllOnScreen) {
    const unsigned NofAtoms = cluster.getNofAtoms ();
    const unsigned species = cluster.NofSpecies ();
    // Neigbours should have been precalculated

    // the index of the map is the first atom of an island and the stored inexes
    // are island's dimension std::unordered_map<int,int> islands;
    vectorAtomID biggestsIsland_perKind (species, 0),
      NofAtom_perKind (species, 0);
    // workarray that stores the index of unvisited atoms
    std::vector<bool> unvisited (NofAtoms, true);
    for (unsigned speciesID = 0; speciesID < species; ++speciesID) {
      for (unsigned i (0); i < NofAtoms; ++i) {
        if (
          unvisited[i] &&
          cluster[i].Type () == cluster.Composition (speciesID)) {
          unsigned dim =
            BHClusterUtilities::BHIslandWorkers::eatIsland_sameKind (
              i, cluster, unvisited);
          // the island compreends the visited atoms of this kind;
          NofAtom_perKind[speciesID] += dim;
          if (dim > biggestsIsland_perKind[speciesID]) {
            biggestsIsland_perKind[speciesID] = dim;
          }
        }
      }
    }

    std::vector<BHdouble> AggregationParameters (bhmp.getNofMetals ());
    for (unsigned speciesID = 0; speciesID < species; ++speciesID) {
      AggregationParameters[bhmp.getSlabels (cluster.Composition (speciesID))] =
        static_cast<BHdouble> (biggestsIsland_perKind[speciesID]) /
        NofAtom_perKind[speciesID];
    }

    if (plotAllOnScreen) {
      for (unsigned speciesID (0); speciesID < species; ++speciesID) {
        std::cout << cluster.Composition (speciesID) << ":\t"
                  << biggestsIsland_perKind[speciesID] << std::endl;
      }
    }
    return AggregationParameters;
  }

  std::tuple<unsigned, unsigned, std::vector<unsigned>>
  BHClusterAnalyser::ComputeNNlists (
    BHCluster &cluster, const BHMetalParameters &bhmp) const {
    //@todo computeNNlistAndcountMix o better name that returns mixecounter and
    // BondsCounter
    unsigned MixedCounter = 0;
    // NB:: this function produces oreded NN lists
    std::vector<unsigned> BondsCounter (bhmp.getNofInteractions (), 0);

    const unsigned NofAtoms (cluster.getNofAtoms ());

    // building up the NN_tol list
    // instead of calling calcNeighbourhood_tol(); i do this loop
    unsigned Nbounds = 0;
    cluster.clear_NN ();
    for (unsigned i = 0; i < NofAtoms; ++i) {
      for (unsigned j = i + 1; j < NofAtoms; ++j) {
        unsigned INTindex = cluster.INTlab (i, j);
        // cout <<Atoms_[i].Type()+Atoms_[j].Type()<< index << ": ";
        BHdouble d2 = cluster[i].dist2 (cluster[j]);
        // cout << d2 << "<->"<<NNdist_tol2[index]<< " ";
        if (d2 < NNdist_tol2_[INTindex]) {
          ++Nbounds;
          cluster.addNNcouple (i, j);
          ++BondsCounter[INTindex];
          // if (cluster[i].Type () == cluster[j].Type ()) {
          if (cluster.SAMElab (i, j) == 0) {
            cluster.addNNcouple_same (i, j);
          } else {
            ++MixedCounter;
            cluster.addNNcouple_diff (i, j);
          }
        }
      }
      assert (std::is_sorted (
        cluster.getNNs (i).begin (), cluster.getNNs (i).end ()));
    }
    return {Nbounds, MixedCounter, BondsCounter};
  }

  std::vector<BHdouble> BHClusterAnalyser::ParameterAnalysis (
    BHCluster &cluster, const BHMetalParameters &bhmp) const {
    n_analysis++;
    tp start = std::chrono::steady_clock::now ();
    DataContainer data;
    std::tie (data.Nbounds_, data.MixedCounter_, data.BondsCounter_) =
      ComputeNNlists (cluster, bhmp);
    std::vector<BHdouble> OPs (OPcalculators_.size ());
    for (unsigned i = 0; i < OPcalculators_.size (); ++i) {
      OPs[i] = OPcalculators_[i].PostProduction (
        OPcalculators_[i].Action (cluster, bhmp, data));
    }
    tp stop = std::chrono::steady_clock::now ();
    analysis_time += stop - start;
#ifdef ANALYZERDEBUG
    cerr << "###Iteration: " << n_analysis << '\n'
         << "Number of Bounds: " << Nbounds_ << '\n'
         << "Surface atoms: " << Nsurf_ << '\n';

    for (int j = 0; j < Npars_; ++j) {
      cerr << "Order Parameter " << j << ": " << OPs.value[j] << '\n';
    }
#endif // ANALYZERDEBUG
    return OPs;
  }

  std::vector<BHdouble> BHClusterAnalyser::operator() (
    BHCluster &cluster, const BHMetalParameters &bhmp) const {
    return ParameterAnalysis (cluster, bhmp);
  }

  std::vector<BHOrderParameter> BHClusterAnalyser::ParameterNames () const {
    return opData_;
  }

  void BHClusterAnalyser::calcNeighbourhood_tol (
    BHCluster &cluster, const BHMetalParameters &bhmp) const {
    cluster.clear_NN ();
    for (unsigned i = 0; i < cluster.getNofAtoms (); i++) {
      for (unsigned j = i + 1; j < cluster.getNofAtoms (); j++) {
        unsigned index =
          bhmp.getPlabels (cluster[i].Type () + cluster[j].Type ());
        BHdouble d = cluster[i].dist2 (cluster[j]);
        if (d < NNdist_tol2_[index]) {
          cluster.addNNcouple (j, i);
          if (cluster[i].Type () == cluster[j].Type ()) {
            cluster.addNNcouple_same (j, i);
          } else {
            cluster.addNNcouple_diff (j, i);
          }
        }
      }
    }
  }

  std::tuple<unsigned, std::vector<unsigned>, unsigned, std::vector<unsigned>>
  BHClusterAnalyser::SurfaceAnalysis (
    const BHCluster &cluster, const BHMetalParameters &bhmp) const {
    // surface analysis:
    const unsigned NofAtoms = cluster.getNofAtoms ();
    unsigned NSurf = 0;
    std::vector<unsigned> AtomsOnSurf (bhmp.getNofMetals (), 0);
    unsigned SurfaceMixedCounter = 0;
    std::vector<unsigned> SurfaceBondsCounter (bhmp.getNofInteractions (), 0);

    for (unsigned i (0); i < NofAtoms; ++i) {
      // atom on surface
      if (cluster.getNNs (i).size () < few_neigh) {
        ++NSurf;
        ++AtomsOnSurf[bhmp.getSlabels (cluster[i].Type ())];
        for (const unsigned &j : cluster.getNNs (i)) {
          if (j > i) {
            if (cluster.getNNs (j).size () < few_neigh) {
              unsigned INTindex = cluster.INTlab (i, j);
              BHdouble d2 = cluster[i].dist2 (cluster[j]);
              if (d2 < NNdist_tol2_[INTindex]) {
                ++SurfaceBondsCounter[INTindex];
                if (cluster.SAMElab (i, j) == 1) {
                  ++SurfaceMixedCounter;
                }
              } // if d<
            }   // if (cluster.getNNs(j].size()<few_neigh)
          }     // if j>i
        }       // for on cluster.getNNs(i)
      }
    }
    return {NSurf, AtomsOnSurf, SurfaceMixedCounter, SurfaceBondsCounter};
  }

  void BHClusterAnalyser::conditionalSurfaceAnalysis (
    BHClusterAnalyser::DataContainer &data,
    const BHCluster &cluster,
    const BHMetalParameters &bhmp) const {
    if (data.needSurfaceAnalysis_) {
      data.needSurfaceAnalysis_ = false;
      std::tie (
        data.NSurf_, data.AtomsOnSurf_, data.SurfaceMixedCounter_,
        data.SurfaceBondsCounter_) = SurfaceAnalysis (cluster, bhmp);
    }
  }

} // namespace BH
