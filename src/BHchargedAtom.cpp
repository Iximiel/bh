/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Definition of class BHChargedAtom
   @file BHchargedAtom.cpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 19/6/2017
   @version 0.1
*/
#include "BHchargedAtom.hpp"

#include "BHdebug.hpp"
#include <utility>

namespace BH {
  BHChargedAtom::BHChargedAtom () = default;
  BHChargedAtom::BHChargedAtom (const BHChargedAtom &) = default;
  BHChargedAtom &BHChargedAtom::operator= (BHChargedAtom &&) noexcept = default;
  BHChargedAtom::~BHChargedAtom () = default;
  BHChargedAtom::BHChargedAtom (
    std::string newType,
    BHdouble newX,
    BHdouble newY,
    BHdouble newZ,
    BHdouble newcharge)
    : BHAtom (std::move (newType), newX, newY, newZ),
      charge_ (newcharge) {}

  BHChargedAtom &BHChargedAtom::operator= (const BHChargedAtom &other) {
    if (this != &other) {
      charge_ = other.charge_;
      BHAtom::operator= (other);
    }
    return *this;
  }

  // getting members
  BHdouble BHChargedAtom::Charge () const { return charge_; }
  // setting members
  void BHChargedAtom::Charge (const BHdouble &newCharge) {
    charge_ = newCharge;
  }
} // namespace BH
std::istream &operator>> (std::istream &stream, BH::BHChargedAtom &obj) {
  std::string tname;
  stream >> tname;
  obj.setType (tname);
  BH::BHdouble td;
  stream >> td;
  obj.X (td);
  stream >> td;
  obj.Y (td);
  stream >> td;
  obj.Z (td);
  stream >> td;
  obj.Charge (td);
  return stream;
}
std::ostream &operator<< (std::ostream &stream, const BH::BHChargedAtom &obj) {
  std::cout.setf (std::ios_base::fixed);
  stream << obj.Type () << "\t" << obj.X () << "\t" << obj.Y () << "\t"
         << obj.Z () << "\t" << obj.Charge ();
  std::cout.unsetf (std::ios_base::fixed);
  return stream;
}
