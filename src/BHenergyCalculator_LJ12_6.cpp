/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#include "BHenergyCalculator_LJ12_6.hpp"
#include <cmath>
#include <cstring> //memset
#include <iostream>

#include "BHmetalParameters.hpp"
namespace BH {

  BHEnergyCalculator_LJ12_6::BHEnergyCalculator_LJ12_6 (
    const BHCluster &cluster, const BHMetalParameters &bhmp)
    : BHEnergyCalculator (cluster, bhmp),
      sigma2_ (new BHdouble[nInteractions_]),
      epsilonX4_ (new BHdouble[nInteractions_]),
      epsilonX24_ (new BHdouble[nInteractions_]) {
    for (auto corrispondence : INT_MP2ID) {
      auto MPindex = corrispondence.first;
      auto i = corrispondence.second;
      // using MetalParameters in an inappropriate way

      BHLJInteraction intvalues = bhmp.giveLJInteractionParameters (MPindex);
      sigma2_[i] = intvalues.sigma * intvalues.sigma;
      IntNeighDistTol2_[i] = sigma2_[i] * 8;
      BHdouble epsilon = intvalues.epsilon * 8.621738e-5; // conversion to eV
      // epsilon = epsilonInK * kBJ;// in J
      // epsilon = epsilonInK * 1.380648780669e-11;// in pJ
#ifndef _OPENMP
      /*if not using openMP we are using a triangular matrix
        so the energy sum does not count two times the energy
        so epsilonX4_ will NOT take into account the 1/2 in the sum
      */
      epsilonX4_[i] = 4 * epsilon;
      epsilonX24_[i] = 6 * epsilonX4_[i];
#else
      /*wth openMP we are not using triangular matrix,
        so energy need to be calculated halved
        so epsilonX4 will take into account the 1/2 from the sum
      */
      epsilonX4_[i] = 2 * epsilon;
      epsilonX24_[i] = 2 * 6 * epsilonX4_[i];
#endif
      std::cout << "Interaction " << bhmp.getPairName (MPindex) << ":\n"
                << "Sigma: " << intvalues.sigma << "\n"
                << "EPS: " << epsilon * 8.621738e-5 << std::endl;
      /*<<  sigma2_[i] << std::endl
        <<  epsilonX4_[i] << std::endl
        <<  epsilonX24_[i] << std::endl;*/
      // std::cout <<i <<" " << epsilonX4_[i]<<" "<< epsilonX24_[i]<<std::endl;
    }
  }

  BHEnergyCalculator_LJ12_6::~BHEnergyCalculator_LJ12_6 () {
    delete[] sigma2_;
    delete[] epsilonX4_;
    delete[] epsilonX24_;
  }

  BHdouble BHEnergyCalculator_LJ12_6::Energy (BHdouble *x) {
    unsigned int i, k, ID, INTID, myType;
    double x_i, y_i, z_i, xik, yik, zik;
    double energy = 0, sigma2onR2, sigma6onR6;
#pragma omp parallel for default(none)				\
  private(i, k, ID,INTID, myType,x_i, y_i, z_i, xik, yik,zik,	\
	  sigma2onR2, sigma6onR6)				\
  shared(x)							\
  reduction(+:energy)
    for (i = 0; i < nAtoms_; ++i) {
      ID = 3 * i;
      x_i = x[ID];
      y_i = x[ID + 1];
      z_i = x[ID + 2];
      myType = nTypes_ * typeList_[i];
      for (
#ifndef _OPENMP
        k = i + 1;
#else
        k = 0;
#endif
        k < nAtoms_; ++k) {
#ifdef _OPENMP
        if (k == i)
          continue;
#endif
        INTID = intType_[myType + typeList_[k]];
        ID = 3 * k; // updating memoization
        xik = x_i - x[ID];
        yik = y_i - x[ID + 1];
        zik = z_i - x[ID + 2];
        // i don't do sigma over r, because i need to do a square root and the
        // elevate it to a even power
        sigma2onR2 = sigma2_[INTID] / (xik * xik + yik * yik + zik * zik);
        sigma6onR6 = sigma2onR2 * sigma2onR2 * sigma2onR2;
        energy += epsilonX4_[INTID] * sigma6onR6 * (sigma6onR6 - 1);
      }
    }
    return energy;
  }
  BHdouble BHEnergyCalculator_LJ12_6::Energy_tol (BHdouble *x) {
    return Energy (x);
  }
  BHdouble BHEnergyCalculator_LJ12_6::Gradient (BHdouble *x, BHdouble *g) {
    unsigned int i, k, ID, IDk, INTID, myType;
    double x_i, y_i, z_i, xik, yik, zik;
    double rr;
    double energy = 0, gradientONr, sigma2onR2, sigma6onR6;
#pragma omp parallel for default(shared) private(i)
    for (i = 0; i < n3Atoms_; ++i) {
      g[i] = 0;
    }

#pragma omp parallel for default(none)				\
  private(i, k, ID,IDk,INTID, myType,x_i, y_i, z_i,gradientONr,	\
	  rr, xik, yik,zik,sigma2onR2, sigma6onR6)		\
  shared(x,g)							\
  reduction(+:energy)
    for (i = 0; i < nAtoms_; ++i) {
      ID = 3 * i;
      x_i = x[ID];
      y_i = x[ID + 1];
      z_i = x[ID + 2];
      myType = nTypes_ * typeList_[i];
      for (
#ifndef _OPENMP
        k = i + 1;
#else
        k = 0;
#endif
        k < nAtoms_; ++k) {
#ifdef _OPENMP
        if (k == i)
          continue;
#endif
        INTID = intType_[myType + typeList_[k]];
        IDk = 3 * k; // updating memoization
        xik = x_i - x[IDk];
        yik = y_i - x[IDk + 1];
        zik = z_i - x[IDk + 2];
        // i don't do sigma over r, because i need to do a square root and the
        // elevate it to a even power
        rr = (xik * xik + yik * yik + zik * zik);
        sigma2onR2 = sigma2_[INTID] / rr;
        sigma6onR6 = sigma2onR2 * sigma2onR2 * sigma2onR2;
        energy += epsilonX4_[INTID] * sigma6onR6 * (sigma6onR6 - 1);
        gradientONr =
          epsilonX24_[INTID] * sigma6onR6 * (1 - 2 * sigma6onR6) / rr;
        // std::cout << i << " " << k << " " << gradientONr <<std::endl;
        g[ID] += gradientONr * xik;
        g[ID + 1] += gradientONr * yik;
        g[ID + 2] += gradientONr * zik;
#ifndef _OPENMP
        g[IDk] -= gradientONr * xik;
        g[IDk + 1] -= gradientONr * yik;
        g[IDk + 2] -= gradientONr * zik;
#endif
      }
    }
    // std::cout << energy <<std::endl;
    // std::cin.get();
    return energy;
  }

  BHdouble BHEnergyCalculator_LJ12_6::Gradient_tol (BHdouble *x, BHdouble *g) {
    unsigned int i, j, k, ID, IDk, INTID, myType;
    double x_i, y_i, z_i, xik, yik, zik;
    double rr, cutoff;
    double energy = 0, gradientONr, sigma2onR2, sigma6onR6;
    memset (g, 0, n3Atoms_ * sizeof (g[0]));
    /*
      #pragma omp parallel for default(shared)	\
      private(i)
      for ( i=0; i< n3Atoms_; ++i) {
      g[i] =0;
      }
      #pragma omp parallel for default(none)				\
      private(i, k, ID,IDk,INTID, myType,x_i, y_i, z_i,gradientONr,	\
      rr, xik, yik,zik,sigma2onR2, sigma6onR6)		\
      shared(x,g)							\
      reduction(+:energy)//*/
    for (i = 0; i < nAtoms_; ++i) {
      ID = 3 * i;
      x_i = x[ID];
      y_i = x[ID + 1];
      z_i = x[ID + 2];
      myType = nTypes_ * typeList_[i];
      for (j = 0; j < nNeigh_[i]; ++j) {
        k = NeighList_[i][j];
        INTID = intType_[myType + typeList_[k]];
        IDk = 3 * k; // updating memoization
        xik = x_i - x[IDk];
        yik = y_i - x[IDk + 1];
        zik = z_i - x[IDk + 2];
        // i don't do sigma over r, because i need to do a square root and the
        // elevate it to a even power
        rr = (xik * xik + yik * yik + zik * zik);
        sigma2onR2 = sigma2_[INTID] / rr;
        sigma6onR6 = sigma2onR2 * sigma2onR2 * sigma2onR2;
        gradientONr =
          epsilonX24_[INTID] * sigma6onR6 * (1 - 2 * sigma6onR6) / rr;
        if (rr > IntNeighDistTol2_[INTID] - 1.0) {
          cutoff = (2 * rr - 2 * IntNeighDistTol2_[INTID] + 1.5);
          cutoff = 1 / 2. - 15. / 16. * cutoff *
                              (1 - cutoff * cutoff *
                                     (2. / 3. - 1. / 5. * cutoff * cutoff));
          if (rr > IntNeighDistTol2_[INTID] - 0.5)
            cutoff = 0;
          energy += epsilonX4_[INTID] * sigma6onR6 * (sigma6onR6 - 1) * cutoff;
          gradientONr *= cutoff;
        } else {
          energy += epsilonX4_[INTID] * sigma6onR6 * (sigma6onR6 - 1);
        }
        // std::cout << i << " " << k << " " << gradientONr <<std::endl;
        g[ID] += gradientONr * xik;
        g[ID + 1] += gradientONr * yik;
        g[ID + 2] += gradientONr * zik;
        //#ifndef _OPENMP
        g[IDk] -= gradientONr * xik;
        g[IDk + 1] -= gradientONr * yik;
        g[IDk + 2] -= gradientONr * zik;
        //#endif
      }
    }
    // std::cout << energy <<std::endl;
    // std::cin.get();
    return energy;
  }

  BHEnergyCalculator *BHEnergyCalculator_LJ12_6::clone () {
    return new BHEnergyCalculator_LJ12_6 (*this);
  }

} // namespace BH
