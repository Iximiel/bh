/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#include <sstream>

#include "BHmoveBonds.hpp"
#include "BHparsers.hpp"

namespace BH {
  BHMoveBonds::BHMoveBonds () : BHMove ("bonds") {}

  BHMoveBonds::~BHMoveBonds () = default;

  std::string BHMoveBonds::DefaultString () {
    return "prob = 1.0, accTemp = 300, Rmin = 0, Rmax = 1.4";
  }

  std::unique_ptr<BHMove> BHMoveBonds::clone () {
    return std::unique_ptr<BHMove>{new BHMoveBonds (*this)};
  }

  std::string BHMoveBonds::printSettingsSpecialized () const {
    std::stringstream ss;
    ss << "Rmin->Rmax: " << Rmin_ << "->" << Rmax_;
    return ss.str ();
  }

  std::string BHMoveBonds::printSettingsSpecializedForInput () const {
    std::stringstream ss;
#define outputwriter(variable)                                                 \
  ", " << BHParsers::BHMV::BONDS[static_cast<size_t> (                         \
            BHParsers::BHMV::BONDSvariable::variable)]                         \
       << " = " << variable
    ss << outputwriter (Rmin_) << outputwriter (Rmax_);
#undef outputwriter
    return ss.str ();
  }

  bool BHMoveBonds::doMoveAlgorithm (
    BHCluster &in,
    BHCluster &out,
    BHClusterAnalyser &Analyzer,
    const BHMetalParameters &bhmp,
    rndEngine &rng) {
    Analyzer.calcNeighbourhood_tol (in, bhmp);
    out << in;
    size_t NofAtoms = in.getNofAtoms ();
    vectorAtomID::size_type lessNeig[2] = {20, 21};
    for (unsigned i = 0; i < NofAtoms; i++) {
      auto i_neighs = in.getNNs (i).size ();
      if (i_neighs < lessNeig[0]) {
        // now the second least neighbours_number is the prevoius least one
        lessNeig[1] = lessNeig[0];
        lessNeig[0] = i_neighs;
      } else if (i_neighs < lessNeig[1] && i_neighs != lessNeig[0]) {
        lessNeig[1] = i_neighs;
      }
    }
    // make a list with the IDs of the atoms for each least n_of_neighbours
    vectorAtomID lonelyAtoms[2];

    for (unsigned i = 0; i < NofAtoms; i++) {
      if (lessNeig[0] == in.getNNs (i).size ()) {
        lonelyAtoms[0].push_back (i);
      } else if (lessNeig[1] == in.getNNs (i).size ()) {
        lonelyAtoms[1].push_back (i);
      }
    }

    // NB: the distribution runs on the indexes of the atoms IDs stored in
    // lonelyAtoms[*]
    BHULongRND firstAtm (0, lonelyAtoms[0].size () - 1);
    unsigned chosen = lonelyAtoms[0][firstAtm (rng)];
    unsigned newneigh;

    // if the least number of neighbours is 1 or the first list of atoms is made
    // of only one atom
    if (lessNeig[0] == 1 || lonelyAtoms[0].size () == 1) {
      // select an atom from the second list
      BHULongRND secondAtm (0, lonelyAtoms[1].size () - 1);
      newneigh = lonelyAtoms[1][secondAtm (rng)];
    } else { // otherwise select the second atom from the first list
      BHULongRND secondAtm (0, lonelyAtoms[0].size () - 1);
      do { ///@todo adda a protection: to see if it is the only atom that it si
           /// moving!
        newneigh = lonelyAtoms[0][firstAtm (rng)];
      } while (newneigh == chosen);
    }
    out[chosen] = BHVector::RandomVector (rng, Rmax_, Rmin_) + in[newneigh];
    return true;
  }
  bool BHMoveBonds::parseAfter () {
    if (Rmax_ <= Rmin_) {
      throw typeName () + "::Rmax is less or equal to Rmin";
    }
    return true;
  }

  bool BHMoveBonds::parseSpecialized (const std::string &parsed) {
    bool toreturn = false;
    try {
#define inputgetter(variable)                                                  \
  BHParsers::parse (                                                           \
    BHParsers::BHMV::BONDS[static_cast<size_t> (                               \
                             BHParsers::BHMV::BONDSvariable::variable)]        \
      .c_str (),                                                               \
    parsed, variable)
      toreturn |= inputgetter (Rmin_);
      toreturn |= inputgetter (Rmax_);
#undef inputgetter
    } catch (const std::invalid_argument & /*ia*/) {
      throw typeName () + "::Invalid argument: \"" + parsed + "\"";
    }
    return toreturn;
  }
} // namespace BH
