/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief A small utility to show the cut off in action

   @file plotenergy.cxx
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 10/7/2017
   @version 1.0
*/

#include <cmath>
#include <fstream>
#include <iostream>

#include "BHcluster.hpp"
#include "BHmetalParameters.hpp"
#include "BHworkWithFortran.hpp"
extern "C" {
void demoBH (double myxy[], double &E, double &E_min);
}

using namespace BH;

double bisect (double start, double end, double *x, const int &coord);
double BHenergy_minimized (double *x, const int &Nat);
int main () {
  const int Nx = 200;
  const int Ny = 200;
  const double xstart = -5;
  const double xstop = 5;
  const double ystart = -4;
  const double ystop = 5;
  std::ofstream f ("energysplot");
  std::ofstream g ("energysplot_min");
  const double ystep = (ystop - ystart) / Ny;
  const double xstep = (xstop - xstart) / Nx;
  //  f << "dist\tEnergy\tEnNoCut" <<std::endl;
  /*  std::cout <<Nx<< std::endl
            << Ny<< std::endl
            << xstart<< std::endl
            << xstop<< std::endl
            << ystart<< std::endl
            << ystop<< std::endl
            << ystep << std::endl
            << xstep << std::endl;*/
  double x[2] = {0, 0};

  for (int j = 0; j < Ny; ++j) {
    x[1] = ystart + ystep * j;
    // std::cout <<std:: endl<< x[1] << ": ";
    //    std::cout  <<ystart+ystep*j<<"\t";
    for (int i = 0; i < Nx; ++i) {
      double En, E_m;
      x[0] = xstart + xstep * i;
      x[1] = ystart + ystep * j;
      // std::cout << x[0] << " ";
      demoBH (x, En, E_m);
      f << En << '\t';
      g << E_m << '\t';
    }
    f << std::endl;
    g << std::endl;
  }

  f.close ();
  g.close ();
  return 0;
}

double bisect (double start, double end, double *x, const int &coord) {
  double target = BHWWF::BHenergy (x);
  double a = start, b = end, c = b;
  x[coord] = c;
  double set = BHWWF::BHenergy (x) - target;
  do {
    if (set > 0)
      a = c;
    else
      b = c;
    c = (a + b) / 2.;
    x[coord] = c;
    set = BHWWF::BHenergy (x) - target;
  } while (std::abs (a - c) > 1e-5);
  return c;
}

double BHenergy_minimized (double *x, const int &Nat) {
  double *alt = new double[3 * Nat];
  for (int i = 0; i < 3 * Nat; ++i) {
    alt[i] = x[i];
  }
  double lastMoveEnergy_;
  lastMoveEnergy_ = BHWWF::BHminimization (alt);
  return lastMoveEnergy_;
}

void original () {
  static const int Nat = 2;
  BHCluster mycluster (Nat);
  BHdouble x[3 * Nat] = {
    0, 0, 0, 0,
    0, 0
    /*1.4,1.4,0,
      0,0,0,//,//i'm moving this
      1.4,-1.4,0,
      0,0,-1.8,
      -1.4,1.4,0,
      -1.4,-1.4,0*/
  };
  for (int i = 0; i < Nat; i++) {
    mycluster[i] = {BHMetalParameters::getBHMP ().getAtomName (0), 0, 0, 0};
  }

  // int INTcode=BHMetalParameters::getBHMP().getPlabels(0,0);

  BHMetalParameters::getBHMP ().calcNeighINT (mycluster);
  BHWWF::sendClusterToFortran (mycluster);

  const int N = 5000;
  const double ce = 5.005626833874055;
  static const int coord = 2; // coordinate on which apply the variation
  // a small bisection to find a good start point
  x[coord] = ce;
  const double start = bisect (0, ce - 1, x, coord);

  std::ofstream f ("energyplot");
  const double step = (ce - start) / N;
  f << "dist\tEnergy\tEnNoCut" << std::endl;
  int first = -int (0.15 * N), last = int (N * 1.15);
  for (int i = first; i < last; i++) {
    x[coord] = start + step * i;
    f << x[coord] << '\t' << BHWWF::BHenergy (x) << '\t'
      << BHWWF::BHenergy_noCutOff (x) << std::endl;
  }
}
