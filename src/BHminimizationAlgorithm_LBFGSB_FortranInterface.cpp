/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of class BHMinimizationAlgorithm_LBFGSB_FortranInterface
This class uses the LBFGS driver written in frortran from the BHWWF namespace


  @file BHminimizationAlgorithm_LBFGSB_FortranInterface.cpp
  @author Daniele Rapetti (iximiel@gmail.com)

  @date 12/11/2019
  @version 1.0


    */
#include <cmath>
#include <cstring>

#include "BHminimizationAlgorithm_LBFGSB_FortranInterface.hpp"
#include "BHworkWithFortran.hpp"

namespace BH {

  BHMinimizationAlgorithm_LBFGSB_FortranInterface::
    BHMinimizationAlgorithm_LBFGSB_FortranInterface () = default;

  BHMinimizationAlgorithm_LBFGSB_FortranInterface::
    ~BHMinimizationAlgorithm_LBFGSB_FortranInterface () = default;

  BHdouble BHMinimizationAlgorithm_LBFGSB_FortranInterface::Minimization (
    BHdouble *x,
    const unsigned & /*nat*/,
    BHEnergyCalculator * /*calculator*/) {
    return BHWWF::BHminimization (x);
  }

  BHdouble BHMinimizationAlgorithm_LBFGSB_FortranInterface::Minimization_tol (
    BHdouble *x,
    const unsigned & /*nat*/,
    BHEnergyCalculator * /*calculator*/) {
    return BHWWF::BHminimization_tol (x);
  }
  BHdouble BHMinimizationAlgorithm_LBFGSB_FortranInterface::MinimizeCluster (
    BHClusterAtoms &cluster, BHEnergyCalculator *calculator) {
    throw "BHMinimizationAlgorithm_LBFGSB_FortranInterface is not usable as "
          "now";
    unsigned nat = cluster.getNofAtoms ();
    ///@todo make this correct
    BHMetalParameters bhmp{};
    BHWWF::sendClusterToFortran (cluster, bhmp);
    BHdouble *x = new BHdouble[3 * nat];
    cluster.AssignToVector (x);
    BHdouble energy = Minimization (x, nat, calculator);
    cluster.AssignFromVector (x);
    delete[] x;
    return energy;
  }
  BHdouble
  BHMinimizationAlgorithm_LBFGSB_FortranInterface::MinimizeCluster_tol (
    BHClusterAtoms &cluster, BHEnergyCalculator *calculator) {
    throw "BHMinimizationAlgorithm_LBFGSB_FortranInterface is not usable as "
          "now";
    unsigned nat = cluster.getNofAtoms ();
    ///@todo make this correct
    BHMetalParameters bhmp{};
    BHWWF::sendClusterToFortran (cluster, bhmp);
    BHdouble *x = new BHdouble[3 * nat];
    cluster.AssignToVector (x);
    BHdouble energy = Minimization_tol (x, nat, calculator);
    cluster.AssignFromVector (x);
    delete[] x;
    return energy;
  }
} // namespace BH
