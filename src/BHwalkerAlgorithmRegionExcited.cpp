/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Definition of function BHWalkerRegionExcited

   @file BHwalkerRegionExcited.cpp
   @author Daniele Rapetti (iximiel@gmail.com)

  @date 21/11/2019
  @version 0.1

  creation
  @date 25/11/2019
  @version 1.0

  was a class, now is a function

    */
#include "BHwalkerAlgorithmRegionExcited.hpp"
#include "BHwalker.hpp"

namespace BH {
  namespace BHWalkerAlgorithms {
    BHdouble BHWalkerAlgorithmRegionExcited (
      BHWalker &workon,
      std::vector<BHWalker> & /*walkers*/,
      const BHMetalParameters &,
      BHWalkerSupport::BHWalkerAlgorithmSupport & /*parameters*/,
      rndEngine & /*rng*/) {
      return (workon.isInForbiddenRegion ()) ? -workon.getWalkerRepulsion ()
                                             : 0.0;
    }
  } // namespace BHWalkerAlgorithms
} // namespace BH
