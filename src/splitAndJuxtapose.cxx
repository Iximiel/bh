/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief A small utility that calculates the energy of a configuratation

   @file calculateEnergy.cxx
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 29/3/2018
   @version 1.0

   @todo compact this executable in the mover one
*/

#include <fstream>
#include <iomanip>
#include <iostream>

#include "BHclusterAtoms.hpp"
#include "BHclusterUtilities.hpp"

int main (int argc, char **argv) {
  int retval = 0;
  try {
    std::string error = "";
    if (argc < 3) {
      error = "You must specify the name of two configuurations as arguments";
      throw error;
    }
    BH::BHClusterAtoms first (argv[1]);
    BH::BHClusterAtoms second (argv[2]);
    BH::BHVector fcenter = first.getGeometricCenter ();
    BH::BHVector scenter = second.getGeometricCenter ();

    for (unsigned i = 0; i < first.getNofAtoms (); ++i) {
      first[i] -= fcenter;
      second[i] -= scenter;
    }
    std::ofstream result ("result.xyz");

    result
      << first << '\n'
      << second << '\n'
      << BH::BHClusterUtilities::splitAndJuxtapose (
           first, second, BH::BHClusterUtilities::splitAndJuxtaposePlane::xy)
      << std::endl
      << BH::BHClusterUtilities::splitAndJuxtapose (
           first, second, BH::BHClusterUtilities::splitAndJuxtaposePlane::xz)
      << std::endl
      << BH::BHClusterUtilities::splitAndJuxtapose (
           first, second, BH::BHClusterUtilities::splitAndJuxtaposePlane::yz)
      << std::endl;
    result.close ();
  } catch (const char *problem) {
    std::cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m\n";
    retval = 1;
  } catch (const std::string &problem) {
    std::cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m\n";
    retval = 1;
  }
  return retval;
}
