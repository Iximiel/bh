/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Definition of class BHAtom
   @file BHatom.cpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 4/7/2017
   @version 1.1
*/
#include <iomanip>
#include <utility>

#include "BHatom.hpp"
#include "BHdebug.hpp"

namespace BH {

  BHAtom::BHAtom () = default;
  BHAtom::BHAtom (const BHAtom &) = default;
  BHAtom::BHAtom (BHAtom &&) noexcept = default;
  BHAtom &BHAtom::operator= (BHAtom &&) noexcept = default;
  BHAtom::~BHAtom () = default;
  BHAtom::BHAtom (std::string newType, const BHVector &coords)
    : BHVector (coords),
      myType (std::move (newType)) {}

  BHAtom::BHAtom (
    std::string newType,
    const BHdouble newX,
    const BHdouble newY,
    const BHdouble newZ)
    : BHVector (newX, newY, newZ),
      myType (std::move (newType)) {}

  BHAtom &BHAtom::operator<< (const BHAtom &other) {
    if (this != &other) {
      if (myType != other.myType) {
        throw "\"operator <<\": Different atoms types in setting coordinates";
      }
      BHVector::operator= (other);
    }
    return *this;
  }

  BHAtom &BHAtom::operator= (const BHAtom &other) {
    if (this != &other) {
      myType = other.myType;
      BHVector::operator= (other);
    }
    return *this;
  }

  BHAtom &BHAtom::operator= (const BHVector &vect) {
    if (this != &vect) {
      BHVector::operator= (vect);
    }
    return *this;
  }

  BHAtom &BHAtom::operator= (BHVector &&vect) noexcept {
    if (this != &vect) {
      BHVector::operator= (vect);
    }
    return *this;
  }

  BHAtom &BHAtom::operator+= (const BHVector &vect) {
    if (this != &vect) {
      BHVector::operator+= (vect);
    }
    return *this;
  }

  BHAtom &BHAtom::operator-= (const BHVector &vect) {
    if (this != &vect) {
      BHVector::operator-= (vect);
    }
    return *this;
  }

  // Sorts in alphabetical order by type
  bool BHAtom::operator< (const BHAtom &other) const {
    return myType < other.myType;
  }

  void BHAtom::positionSwap (BHAtom &other) {
    if (myType != other.myType) {
      BHVector::myswap (other);
    } else {
      throw "SWAPSAMETYPE";
    }
  }
  // getting members
  std::string BHAtom::Type () const { return myType; }
  // setting members
  void BHAtom::setType (std::string newType) { myType = std::move (newType); }

  BHAtom operator+ (const BH::BHAtom &x, const BH::BHVector &y) {
    BH::BHAtom toret = x;
    toret += y;
    return toret;
  }

  BHAtom operator- (const BH::BHAtom &x, const BH::BHVector &y) {
    BH::BHAtom toret = x;
    toret -= y;
    return toret;
  }

  // IO
  std::istream &operator>> (std::istream &stream, BHAtom &obj) {
    std::string tname;
    stream >> tname;
    obj.setType (tname);
    BHdouble td;
    stream >> td;
    obj.X (td);
    stream >> td;
    obj.Y (td);
    stream >> td;
    obj.Z (td);
    return stream;
  }
  std::ostream &operator<< (std::ostream &stream, const BHAtom &thisAtom) {
    stream.setf (std::ios::left);
    /*auto precision = stream.precision ();
   stream << std::setw (6) << thisAtom.Type ();
    stream.setf (std::ios::right);
    stream.precision (9);
    stream <<" "<< std::setw (15) << thisAtom.X () <<" "<< std::setw (15) <<
   thisAtom.Y ()
           <<" "<< std::setw (15) << thisAtom.Z ();

    stream.precision (precision);
    stream.unsetf (std::ios::adjustfield);*/
    char dummy[55];
    sprintf (
      dummy, "%-6s %15.9g %15.9g %15.9g", thisAtom.Type ().c_str (),
      thisAtom.X (), thisAtom.Y (), thisAtom.Z ());
    stream << dummy;
    return stream;
  }
} // namespace BH
