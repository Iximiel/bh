!>This module contains the energy caclulators and the data necessary to calculate the energy
MODULE BHMODULE
  use iso_c_binding!in realta` e` uno statement per fortran 2003 ma se compila non mi lamento

  Implicit None
  SAVE
  !from Parameters
  Integer, Parameter :: maxpar=5600
  Integer, Parameter :: nmax0=maxpar,nvmax0=maxpar
  ! parametro di tolleranza sulla determinazione di un minimo locale
  Real(8), Parameter :: ftoll=1.e-7_8
  !From module BH_mod
  Integer(C_INT) :: nat3d    ! number of atoms in the cluster  
  Integer,        allocatable :: nvois(:), ivois(:,:)!n of neighbours per atom, neighbours list of atom  i
  Integer(C_INT), allocatable :: itype(:)!type of the atoms
  Integer(C_INT), allocatable :: intType(:)!table with the interaction IDs
  !From Module Parameters
  Integer(C_INT) :: Ntypes,Nint !different types of atoms and interactions
  !private parameters
  Real(C_DOUBLE),allocatable :: ecoh(:),rat(:),mass(:)
  !interaction parameter
  Real(C_DOUBLE), allocatable :: p(:), q(:), a(:), qsi(:)
  Real(C_DOUBLE), allocatable :: a5(:), a4(:), a3(:), x5(:), x4(:), x3(:)
  Real(C_DOUBLE), allocatable :: dist(:)
  Real(C_DOUBLE), allocatable :: cutoff_start(:),&!<the cut off of the exponential, where it became a polinomy
       & cutoff_end(:)!<where the polinom goes to zero
  Real(C_DOUBLE), allocatable :: cutoff_end2(:), cutoff_end_tol2(:)!cutoff_end2 is here for bigvoi
CONTAINS
  !>For each atom a list of neighbours is calculated with some tolerance and is also calculated a list of atoms WITH neighbours, to do smaller loops.
  !!Neighbours are considered only if they have a index greater than the protagonist atom's one.
  !!It is intended to be used with #bhtolgradient().
  SUBROUTINE BHtolvoi(x)BIND(C,name="BHSetNeigh_tol")
    
    implicit none
    Real(C_DOUBLE),intent(in)::x(3*Nat3D)
    Integer ::i,j,INTID,mytype
    Real(C_DOUBLE) :: dij2,xij,yij,zij

    !fortran does array operation by itself
    nvois = 0
    !set the number of atoms with neighbours
    Do i=1,Nat3D-1
       mytype=(itype(i)-1)*Ntypes !goes to the correct row ot the "table"
       do j=i+1,Nat3D
          INTID=intType(mytype+itype(j))

          xij=x(3*j-2)-x(3*i-2)
          yij=x(3*j-1)-x(3*i-1)
          zij=x(3*j)  -x(3*i)

          dij2=xij*xij+yij*yij+zij*zij

          if (dij2.lt.cutoff_end_tol2(INTID)) then!there are atoms in the tolerance list
             nvois(i) = nvois(i)+1
             ivois(nvois(i),i) = j
          endif
       enddo
    endDo
!!$do i=1, 10
!!$   write(*,*)"atom:",i,nvois(i)
!!$   write(*,*)ivois(1:nvois(i),i)
!!$enddo
!!$stop
  END SUBROUTINE BHtolvoi

  Real(C_DOUBLE) FUNCTION BHTolEnergy(myx)
    
    implicit none
    !inout
    Real(C_DOUBLE),intent(in) ::myx(3*Nat3D)
    !Local variables
    Real(C_DOUBLE) :: den(Nat3D)
    Real(C_DOUBLE) :: eri
    Real(C_DOUBLE) :: xik,yik,zik,dik,dik2, x_i,y_i,z_i
    Real(C_DOUBLE) :: dik0, espo, qsiexpq, aexpp
    Real(C_DOUBLE) :: dikm,dikm2,dikm3,dikm4,dikm5
    Integer :: n_k,INTID,mytype
    Integer :: i,k!indexes of the atoms

    !fortran can do array operation by itself
    den=0.d0
    BHTolEnergy=0

    do 10 i=1,nat3D
       eri=0.d0
       x_i = myx(3*i-2)
       y_i = myx(3*i-1)
       z_i = myx(3*i)
       mytype=(itype(i)-1)*Ntypes !goes to the correct row ot the "table"

       do 20 n_k= 1, nvois(i)
          k = ivois(n_k,i)

          INTID=intType(mytype+itype(k))

          xik=myx(3*k-2)-x_i
          yik=myx(3*k-1)-y_i
          zik=myx(3*k)  -z_i
          dik2=xik*xik+yik*yik+zik*zik
          if (dik2.lt.cutoff_end2(INTID)) then
             dik=dsqrt(dik2)
             if (dik.lt.cutoff_start(INTID)) then
                dik0=dist(INTID)
                espo=1.d0-dik/dik0
                aexpp=dexp(p(INTID)*espo)*a(INTID)
                eri=eri+2.d0*aexpp
                qsiexpq = (qsi(INTID)*qsi(INTID)) * dexp(2.d0*q(INTID)*espo)
             else
                dikm=dik-cutoff_end(INTID)
                dikm2=dikm*dikm
                dikm3=dikm2*dikm
                dikm4=dikm3*dikm
                dikm5=dikm4*dikm
                qsiexpq = (x5(INTID)*dikm5+x4(INTID)*dikm4+x3(INTID)*dikm3)**2
                eri=eri+2*(a5(INTID)*dikm5+a4(INTID)*dikm4+a3(INTID)*dikm3)
             endif
             den(i)=qsiexpq+den(i)
             den(k)=qsiexpq+den(k)

          endif
20     enddo

       BHTolEnergy=BHTolEnergy+(eri-dsqrt(den(i)))
10  enddo

  END FUNCTION BHTolEnergy

  !>Gradient of the SMATB energy calculated by using the data precalculated in #bhtolvoi().
  Real(C_DOUBLE) FUNCTION BHTolGradient(myx,frx)BIND(C,name="BHGradient_tol")
!!!!!!!
!!!!!!!NB: forces are returned with inverted sign (gradient)
!!!!!!!
    implicit none
    !inout
    Real(C_DOUBLE),intent(in) ::myx(3*Nat3D)
    Real(C_DOUBLE),intent(out)::frx(3*Nat3D)
    ! Local variables
    Real(C_DOUBLE) :: den(Nat3D)
    Real(C_DOUBLE) :: ebi,eri,for,forsudik,denik
    !Real(C_DOUBLE) :: fb(Nat3D-1,Nat3D-1)
    !i don't expect that an atom will have more neighbours than the number of atoms with neighbours
    Real(C_DOUBLE) :: fb(Nat3D,Nat3D)!give errors on runtime
    Real(C_DOUBLE) :: xik,yik,zik,dik,dik2, x_i,y_i,z_i
    Real(C_DOUBLE) :: dik0, espo, qsiexpq, aexpp
    Real(C_DOUBLE) :: dikm,dikm2,dikm3,dikm4,dikm5
    Integer :: n_k,INTID, mytype
    Integer :: i,k!indexes of the atoms
    BHTolGradient=0.d0
    !fortran can do array operation by itself
    den=0.d0
    frx=0.d0
    fb(:,:) = 0.d0

    do 10 i=1,nat3D
       eri=0.d0
       x_i = myx(3*i-2)
       y_i = myx(3*i-1)
       z_i = myx(3*i)
       mytype=(itype(i)-1)*Ntypes !goes to the correct row ot the "table"

       do 20 n_k= 1, nvois(i)
          k = ivois(n_k,i)
          INTID=intType(mytype+itype(k))

          xik=myx(3*k-2)-x_i
          yik=myx(3*k-1)-y_i
          zik=myx(3*k)  -z_i
          dik2=xik*xik+yik*yik+zik*zik
          if (dik2.lt.cutoff_end2(INTID)) then
             dik=dsqrt(dik2)
             if (dik.lt.cutoff_start(INTID)) then
                dik0=dist(INTID)
                espo=1.d0-dik/dik0
                aexpp=dexp(p(INTID)*espo)*a(INTID)
                for=(2.d0*p(INTID)/dik0)*aexpp
                eri=eri+2.d0*aexpp
                qsiexpq = (qsi(INTID)*qsi(INTID)) * dexp(2.d0*q(INTID)*espo)
                fb(n_k,i) = (q(INTID)/dik0)*qsiexpq;
             else
                dikm=dik-cutoff_end(INTID)
                dikm2=dikm*dikm
                dikm3=dikm2*dikm
                dikm4=dikm3*dikm
                dikm5=dikm4*dikm
                qsiexpq =x5(INTID)*dikm5+x4(INTID)*dikm4+x3(INTID)*dikm3
                for=-2.d0*(5.d0*a5(INTID)*dikm4+4.d0*a4(INTID)*dikm3+3.d0*a3(INTID)*dikm2)
                eri=eri+2*(a5(INTID)*dikm5+a4(INTID)*dikm4+a3(INTID)*dikm3)
                fb(n_k,i) = -((5.d0*x5(INTID)*dikm4+4.d0*x4(INTID)*dikm3+3.d0*x3(INTID)*dikm2))*qsiexpq;
                qsiexpq = qsiexpq**2
             endif
             den(i)=qsiexpq+den(i)
             den(k)=qsiexpq+den(k)

             forsudik=for/dik

             frx(3*i-2)=frx(3*i-2)+forsudik*xik
             frx(3*i-1)=frx(3*i-1)+forsudik*yik
             frx(3*i)  =frx(3*i)  +forsudik*zik

             frx(3*k-2)=frx(3*k-2)-forsudik*xik
             frx(3*k-1)=frx(3*k-1)-forsudik*yik
             frx(3*k)  =frx(3*k)  -forsudik*zik
          endif
20     enddo
       ebi=dsqrt(den(i))
       den(i)=1.d0/ebi
       BHTolGradient=BHTolGradient+(eri-ebi)
10  enddo

    do 30 i=1,Nat3D!i'm pretty sure that won't extist an atom with a greather index than Nat3D
       x_i = myx(3*i-2)
       y_i = myx(3*i-1)
       z_i = myx(3*i)
       mytype=(itype(i)-1)*Ntypes !goes to the correct row ot the "table"
       do 40 n_k=1,nvois(i)
          k=ivois(n_k,i)
          INTID=intType(mytype+itype(k))
          xik=myx(3*k-2)-x_i
          yik=myx(3*k-1)-y_i
          zik=myx(3*k)  -z_i
          
          dik=dsqrt(xik*xik+yik*yik+zik*zik)
          if (dik.lt.cutoff_end(INTID)) then             
             !fb(n_k,n_i) so this won't run on a "distant" address
             denik=fb(n_k,i)*(den(i)+den(k))/dik
             
             frx(3*i-2)=frx(3*i-2)-denik*xik
             frx(3*i-1)=frx(3*i-1)-denik*yik
             frx(3*i)  =frx(3*i)  -denik*zik

             frx(3*k-2)=frx(3*k-2)+denik*xik
             frx(3*k-1)=frx(3*k-1)+denik*yik
             frx(3*k)  =frx(3*k)  +denik*zik

          endif
40     enddo
30  enddo

  END FUNCTION BHTolGradient
  !>Create a list of neighbours for each atoms
  SUBROUTINE BHbigvoi(x)
    !ho eliminato tutto cio` che proviene da met-oxi
    implicit none
    Real(8),intent(in)::x(3*Nat3D)
    Integer ::i,j,INTID,mytype
    Real(8) :: dij2,xij,yij,zij
    !fortran does array operation by itself
    nvois = 0

    do i=1,Nat3D-1
       mytype=(itype(i)-1)*Ntypes !goes to the correct row ot the "table"
       do j=i+1,Nat3D
          INTID=intType(mytype+itype(j))
          
          xij=x(3*j-2)-x(3*i-2)
          yij=x(3*j-1)-x(3*i-1)
          zij=x(3*j)  -x(3*i)

          dij2=xij*xij+yij*yij+zij*zij

          if (dij2.lt.cutoff_end2(INTID)) then
             nvois(i) = nvois(i)+1
             ivois(nvois(i),i) = j
             !in order to not add an if(k.gt.i) in BHgradient_rgl i won't count the neighs with a lower index
             !nvois(j) = nvois(j)+1
             !ivois(nvois(j),j) = i
          endif
       enddo

       if (nvois(i).ge.nvmax0) then
          write (*,530) i,nvois(i)
          stop
       endif


    enddo
    ! stop
530 format (' troppi vicini in bigvoi  ',2i5)

    return
  END SUBROUTINE BHbigvoi
  !>Calculate the gradient of the energy, and update the neighbours list for each atoms
  REAL(C_DOUBLE) FUNCTION BHgradient(myx,frx)BIND(C,name="BHGradient")
!!!!!!!
!!!!!!!NB: forces are returned with inverted sign (gradient)
!!!!!!!
    implicit none
    !inout
    Real(C_DOUBLE),intent(in), dimension(3*Nat3D) ::myx
    Real(C_DOUBLE),intent(out), dimension(3*Nat3D)::frx
    ! Local variables
    Real(C_DOUBLE), dimension(Nat3D) :: den
    Real(C_DOUBLE) :: ebi,eri,for,forsudik,denik
    Real(C_DOUBLE), dimension(Nat3D-1,Nat3D-1) :: fb
    !Real(C_DOUBLE) :: xik(Nat3D,Nat3D),yik(Nat3D,Nat3D),zik(Nat3D,Nat3D),d(Nat3D,Nat3D)
    Real(C_DOUBLE) :: xik,yik,zik,dik,dik2,x_i,y_i,z_i
    Real(C_DOUBLE) :: espo, qsiexpq, aexpp!, dik0
    Real(C_DOUBLE) :: dikm,dikm2,dikm3,dikm4,dikm5
    Integer :: i,j,k,INTID,mytype

    !f(dabs(zat(1))>3275786477704)write(*,*)'0000000000 zat(1)=',zat(1)
!!$  write(*,*)'gradient_rgl'
!!$  write(*,*)'cutoff_start ',cutoff_start
!!$  write(*,*)'cutoff_end ',cutoff_end
!!$  write(*,*)'dist ',dist
!!$
!!$  write(*,*)'a3:',a3
!!$  write(*,*)'a4:',a4
!!$  write(*,*)'a5:',a5
!!$  write(*,*)'x3:',x3
!!$  write(*,*)'x4:',x4
!!$  write(*,*)'x5:',x5
!!$  write(*,*)Nat3D
!!$  stop

    BHgradient=0.d0
    !fortran can do array operation by itself
    frx=0.d0
    den=0.d0
!!$  do  i=1,Nat3D
!!$     write(*,*)i,",",nvois(i),":",ivois(1:nvois(i),i)
!!$  enddo
!!$  stop
    nvois = 0

    do 10 i=1,Nat3D
       eri=0.d0
       x_i = myx(3*i-2)
       y_i = myx(3*i-1)
       z_i = myx(3*i)
       mytype=(itype(i)-1)*Ntypes !goes to the correct row ot the "table"
       do 21 k= i+1, Nat3D
          
          INTID=intType(mytype+itype(k))
          xik=myx(3*k-2)-x_i
          yik=myx(3*k-1)-y_i
          zik=myx(3*k)  -z_i

          dik2=xik*xik+yik*yik+zik*zik
          If (dik2.lt.cutoff_end2(INTID)) Then
             nvois(i) = nvois(i)+1
             ivois(nvois(i),i) = k
             dik=dsqrt(dik2)
             if (dik.lt.cutoff_start(INTID)) then
                !dik0=dist(INTID)
                !espo=1.d0-dik/dik0
                espo=1.d0-dik/dist(INTID)
                aexpp=dexp(p(INTID)*espo)*a(INTID)
                for=(2.d0*p(INTID)/dist(INTID))*aexpp
                eri=eri+2.d0*aexpp!!!ompdifference
                qsiexpq = (qsi(INTID)*qsi(INTID)) * dexp(2.d0*q(INTID)*espo)
                !fb(j,i) so this won't run on a "distant" address
                fb(nvois(i),i) = (q(INTID)/dist(INTID))*qsiexpq;
             else
                dikm=dik-cutoff_end(INTID)
                dikm2=dikm*dikm
                dikm3=dikm2*dikm
                dikm4=dikm3*dikm
                dikm5=dikm4*dikm
                qsiexpq =x5(INTID)*dikm5+x4(INTID)*dikm4+x3(INTID)*dikm3
                for=-2.d0*(5.d0*a5(INTID)*dikm4+4.d0*a4(INTID)*dikm3+3.d0*a3(INTID)*dikm2)
                eri=eri+2*(a5(INTID)*dikm5+a4(INTID)*dikm4+a3(INTID)*dikm3)!!!ompdifference
                fb(nvois(i),i) = -((5.d0*x5(INTID)*dikm4+4.d0*x4(INTID)*dikm3+3.d0*x3(INTID)*dikm2))*qsiexpq;
                qsiexpq = qsiexpq**2
             endif
             den(i)=qsiexpq+den(i)
             den(k)=qsiexpq+den(k)!!!ompdifference
             forsudik=for/dik

             frx(3*i-2)=frx(3*i-2)+forsudik*xik
             frx(3*i-1)=frx(3*i-1)+forsudik*yik
             frx(3*i)  =frx(3*i)  +forsudik*zik

             frx(3*k-2)=frx(3*k-2)-forsudik*xik!!!ompdifference
             frx(3*k-1)=frx(3*k-1)-forsudik*yik!!!ompdifference
             frx(3*k)  =frx(3*k)  -forsudik*zik!!!ompdifference

          EndIf

21     enddo
       ebi=dsqrt(den(i))
       den(i)=1.d0/ebi
       !eneri=eri-ebi
       !ener_atom(i)=eneri
       BHgradient=BHgradient+(eri-ebi)
       !write(*,*)i,nvois(i)
10  enddo
    do 31 i=1,Nat3D-1!i'm pretty sure that won't exist an atom with a greater index than Nat3D
       x_i = myx(3*i-2)
       y_i = myx(3*i-1)
       z_i = myx(3*i)
       IF(nvois(i) .gt. 0) THEN !exist neighbours
          do 41 j=1,nvois(i)
             k=ivois(j,i)

             xik=myx(3*k-2)-x_i
             yik=myx(3*k-1)-y_i
             zik=myx(3*k)  -z_i

             dik=dsqrt(xik*xik+yik*yik+zik*zik)
             !fb(j,i) so this won't run on a "distant" address
             denik=fb(j,i)*(den(i)+den(k))/dik

             frx(3*i-2)=frx(3*i-2)-denik*xik
             frx(3*i-1)=frx(3*i-1)-denik*yik
             frx(3*i)  =frx(3*i)  -denik*zik

             frx(3*k-2)=frx(3*k-2)+denik*xik
             frx(3*k-1)=frx(3*k-1)+denik*yik
             frx(3*k)  =frx(3*k)  +denik*zik

41        enddo
       ENDIF ! no neighbours
31  enddo
  END FUNCTION BHgradient

  Real(C_DOUBLE) FUNCTION BHenergy(myx)BIND(C,name="BHenergy")
!!!!!!!
!!!!!!!NB: forces are returned with inverted sign (gradient)
!!!!!!!
    implicit none
    !inout
    Real(C_DOUBLE),intent(in) ::myx(3*Nat3D)
    ! Local variables
    Real(C_DOUBLE) :: den(Nat3D)
    Real(C_DOUBLE) :: ebi,eri
    Real(C_DOUBLE) :: xik,yik,zik,dik,dik2,x_i,y_i,z_i
    Real(C_DOUBLE) :: espo, qsiexpq, aexpp!, dik0
    Real(C_DOUBLE) :: dikm,dikm2,dikm3,dikm4,dikm5
    Integer :: i,k,INTID, mytype

    !f(dabs(zat(1))>3275786477704)write(*,*)'0000000000 zat(1)=',zat(1)
!!$  write(*,*)'gradient_rgl'
!!$  write(*,*)'cutoff_start ',cutoff_start
!!$  write(*,*)'cutoff_end ',cutoff_end
!!$  write(*,*)'dist ',dist
!!$
!!$  write(*,*)'a3:',a3
!!$  write(*,*)'a4:',a4
!!$  write(*,*)'a5:',a5
!!$  write(*,*)'x3:',x3
!!$  write(*,*)'x4:',x4
!!$  write(*,*)'x5:',x5
!!$  write(*,*)Nat3D
!!$  stop
    !     ener_debug = BHenergy
    BHenergy=0.d0
    !fortran can do array operation by itself
    den=0.d0
!!$  do  i=1,Nat3D
!!$     write(*,*)i,",",nvois(i),":",ivois(1:nvois(i),i)
!!$  enddo
!!$  stop
    nvois = 0

    do 10 i=1,Nat3D
       eri=0.d0
       x_i = myx(3*i-2)
       y_i = myx(3*i-1)
       z_i = myx(3*i)       
       mytype=(itype(i)-1)*Ntypes !goes to the correct row ot the "table"

       do 21 k= i+1, Nat3D
          INTID=intType(mytype+itype(k))

          xik=myx(3*k-2)-x_i
          yik=myx(3*k-1)-y_i
          zik=myx(3*k)  -z_i

          dik2=xik*xik+yik*yik+zik*zik
          If (dik2.lt.cutoff_end2(INTID)) Then
             nvois(i) = nvois(i)+1
             ivois(nvois(i),i) = k
             dik=dsqrt(dik2)
             if (dik.lt.cutoff_start(INTID)) then
                !dik0=dist(INTID)
                !espo=1.d0-dik/dik0
                espo=1.d0-dik/dist(INTID)
                aexpp=dexp(p(INTID)*espo)*a(INTID)
                eri=eri+2.d0*aexpp!!!ompdifference
                qsiexpq = (qsi(INTID)*qsi(INTID)) * dexp(2.d0*q(INTID)*espo)
             else
                dikm=dik-cutoff_end(INTID)
                dikm2=dikm*dikm
                dikm3=dikm2*dikm
                dikm4=dikm3*dikm
                dikm5=dikm4*dikm
                qsiexpq =x5(INTID)*dikm5+x4(INTID)*dikm4+x3(INTID)*dikm3
                eri=eri+2*(a5(INTID)*dikm5+a4(INTID)*dikm4+a3(INTID)*dikm3)!!!ompdifference
                qsiexpq = qsiexpq**2
             endif
             den(i)=qsiexpq+den(i)
             den(k)=qsiexpq+den(k)!!!ompdifference
          EndIf

21     enddo
       ebi=dsqrt(den(i))
       den(i)=1.d0/ebi
       !eneri=eri-ebi
       !ener_atom(i)=eneri
       BHenergy=BHenergy+(eri-ebi)
       !write(*,*)i,nvois(i)
10  enddo
  END FUNCTION BHenergy

  Real(C_DOUBLE) FUNCTION BHenergy_noCutOff(myx)BIND(C,name="BHenergy_noCutOff")
    
    implicit none
    !inout
    Real(C_DOUBLE),intent(in) ::myx(3*Nat3D)
    ! Local variables
    Real(C_DOUBLE) :: den(Nat3D)
    Real(C_DOUBLE) :: ebi,eri

    Real(C_DOUBLE) :: xik,yik,zik,dik,dik2,x_i,y_i,z_i
    Real(C_DOUBLE) :: dik0, espo, qsiexpq, aexpp
    Integer :: i,k,INTID,mytype

    BHenergy_noCutOff=0.d0
    !fortran can do array operation by itself
    den=0.d0

    nvois = 0

    do 10 i=1,Nat3D
       eri=0.d0
       x_i = myx(3*i-2)
       y_i = myx(3*i-1)
       z_i = myx(3*i)
       mytype=(itype(i)-1)*Ntypes !goes to the correct row ot the "table"
       do 21 k= i+1, Nat3D
          INTID=intType(mytype+itype(k))

          xik=myx(3*k-2)-x_i
          yik=myx(3*k-1)-y_i
          zik=myx(3*k)  -z_i

          dik2=xik*xik+yik*yik+zik*zik
          !If (dik2.lt.cutoff_end2(INTID)) Then
          nvois(i) = nvois(i)+1
          ivois(nvois(i),i) = k
          dik=dsqrt(dik2)
          dik0=dist(INTID)
          espo=1.d0-dik/dik0
          aexpp=dexp(p(INTID)*espo)*a(INTID)
          eri=eri+2.d0*aexpp!!!ompdifference
          qsiexpq = (qsi(INTID)*qsi(INTID)) * dexp(2.d0*q(INTID)*espo)
          den(i)=qsiexpq+den(i)
          den(k)=qsiexpq+den(k)!!!ompdifference
          !EndIf
21     enddo
       ebi=dsqrt(den(i))
       den(i)=1.d0/ebi
       BHenergy_noCutOff=BHenergy_noCutOff+(eri-ebi)
10  enddo
  END FUNCTION BHenergy_noCutOff


  !!returns the forces
  SUBROUTINE BHforces(myx,frx)bind(C,name="BHforces")
    
    implicit none
    !inout
    Real(C_DOUBLE),intent(in) ::myx(3*Nat3D)
    Real(C_DOUBLE),intent(out)::frx(3*Nat3D)
    ! Local variables
    Real(C_DOUBLE) :: den(Nat3D)
    Real(C_DOUBLE) :: ebi,for,forsudik,denik
    Real(C_DOUBLE) :: fb(Nat3D-1,Nat3D-1)
    Real(C_DOUBLE) :: xik,yik,zik,dik,dik2,x_i,y_i,z_i
    Real(C_DOUBLE) :: dik0, espo, qsiexpq, aexpp
    Real(C_DOUBLE) :: dikm,dikm2,dikm3,dikm4,dikm5
    Integer :: i,j,k,INTID,mytype
    !fortran can do array operation by itself
    den=0.d0
    frx=0.d0
    nvois = 0

    do 10 i=1,Nat3D
       x_i = myx(3*i-2)
       y_i = myx(3*i-1)
       z_i = myx(3*i)
       mytype=(itype(i)-1)*Ntypes !goes to the correct row ot the "table"
       do 21 k= i+1, Nat3D
          INTID=intType(mytype+itype(k))

          xik=myx(3*k-2)-x_i
          yik=myx(3*k-1)-y_i
          zik=myx(3*k)  -z_i

          dik2=xik*xik+yik*yik+zik*zik
          if (dik2.lt.cutoff_end2(INTID)) then
             nvois(i) = nvois(i)+1
             ivois(nvois(i),i) = k
             dik=dsqrt(dik2)
             if (dik.lt.cutoff_start(INTID)) then
                dik0=dist(INTID)
                espo=1.d0-dik/dik0
                aexpp=dexp(p(INTID)*espo)*a(INTID)
                for=(2.d0*p(INTID)/dik0)*aexpp
                qsiexpq = (qsi(INTID)*qsi(INTID)) * dexp(2.d0*q(INTID)*espo)
                fb(nvois(i),i) = (q(INTID)/dik0)*qsiexpq;
             else
                dikm=dik-cutoff_end(INTID)
                dikm2=dikm*dikm
                dikm3=dikm2*dikm
                dikm4=dikm3*dikm
                dikm5=dikm4*dikm
                qsiexpq =x5(INTID)*dikm5+x4(INTID)*dikm4+x3(INTID)*dikm3
                for=-2.d0*(5.d0*a5(INTID)*dikm4+4.d0*a4(INTID)*dikm3+3.d0*a3(INTID)*dikm2)
                fb(nvois(i),i) = -((5.d0*x5(INTID)*dikm4+4.d0*x4(INTID)*dikm3+3.d0*x3(INTID)*dikm2))*qsiexpq;
                qsiexpq = qsiexpq**2
             endif
             den(i)=qsiexpq+den(i)
             den(k)=qsiexpq+den(k)

             forsudik=for/dik

             frx(3*i-2)=frx(3*i-2)-forsudik*xik
             frx(3*i-1)=frx(3*i-1)-forsudik*yik
             frx(3*i)  =frx(3*i)  -forsudik*zik

             frx(3*k-2)=frx(3*k-2)+forsudik*xik
             frx(3*k-1)=frx(3*k-1)+forsudik*yik
             frx(3*k)  =frx(3*k)  +forsudik*zik
          endif
21     enddo
       ebi=dsqrt(den(i))
       den(i)=1.d0/ebi
10  enddo

    do 31 i=1,Nat3D-1!i'm pretty sure that won't extist an atom with a greather index than Nat3D
       x_i = myx(3*i-2)
       y_i = myx(3*i-1)
       z_i = myx(3*i)
       IF(nvois(i) .gt. 0) THEN !exist neighbours
          do 41 j=1,nvois(i)
             k=ivois(j,i)

             xik=myx(3*k-2)-x_i
             yik=myx(3*k-1)-y_i
             zik=myx(3*k)  -z_i

             dik=dsqrt(xik*xik+yik*yik+zik*zik)

             denik=fb(j,i)*(den(i)+den(k))/dik

             frx(3*i-2)=frx(3*i-2)+denik*xik
             frx(3*i-1)=frx(3*i-1)+denik*yik
             frx(3*i)  =frx(3*i)  +denik*zik

             frx(3*k-2)=frx(3*k-2)-denik*xik
             frx(3*k-1)=frx(3*k-1)-denik*yik
             frx(3*k)  =frx(3*k)  -denik*zik
41        enddo
       ENDIF ! no neighbours
31  enddo
  END SUBROUTINE BHforces

END MODULE BHMODULE
