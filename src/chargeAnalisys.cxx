/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#include "BHchargedAtom.hpp"
#include "BHcluster.hpp"
#include "BHmetalParameters.hpp"
#include <cmath>
#include <fstream>
#include <iostream>
#include <string>

using std::cout;
using std::endl;
// using std::cin;
using std::cerr;

int main (int /*argc*/, char **argv) {
  try {
    BH::BHMetalParameters bhmp ("MetalParameters.in", true);
    std::ifstream file (argv[1]);
    unsigned N;
    file >> N;
    std::string dummy;
    // get rid of the comment
    getline (file, dummy);
    getline (file, dummy);
    // reading the file in format
    //
    // type x y z charge
    //
    BH::BHChargedAtom *chAtm = new BH::BHChargedAtom[N];
    BH::BHCluster cluster (N);
    for (unsigned i = 0; i < N; i++) {
      file >> chAtm[i];
      // cout << chAtm[i] <<endl;
      cluster[i] = chAtm[i];
    }
    // cout << cluster << endl;
    bhmp.setInteractionLabels (cluster);
    bhmp.calcNeighbourhood_tol (cluster);

    std::cout.setf (std::ios_base::fixed);
    std::ofstream of ("dist_charges.out");
    std::ofstream ofs ("dist_charges_same.out");
    std::ofstream ofd ("dist_charges_differnt.out");
    BH::BHVector pos (0, 0, 0), neg (0, 0, 0), dip (0, 0, 0);
    BH::BHdouble posch = 0, negch = 0;
    for (unsigned i = 0; i < N; i++) {
      if (chAtm[i].Charge () > 0) {
        pos += chAtm[i].Charge () * chAtm[i];
        posch += chAtm[i].Charge ();
      } else {
        neg += chAtm[i].Charge () * chAtm[i];
        negch += chAtm[i].Charge ();
      }
      dip += chAtm[i].Charge () * chAtm[i];
      for (auto &j : cluster.getNNs (i)) {
        if (j > i && (chAtm[i].Type () == chAtm[j].Type ())) {
          cout << chAtm[i].Type () << i << "<>" << chAtm[j].Type () << j << '\t'
               << std::abs (chAtm[i].Charge () - chAtm[j].Charge ()) << '\t'
               << chAtm[i].dist (chAtm[j]) << endl;
          of << i << "<>" << j << '\t'
             << std::abs (chAtm[i].Charge () - chAtm[j].Charge ()) << '\t'
             << chAtm[i].dist (chAtm[j]) << endl;
          ofs << i << "<>" << j << '\t'
              << std::abs (chAtm[i].Charge () - chAtm[j].Charge ()) << '\t'
              << chAtm[i].dist (chAtm[j]) << endl;
        }
        if (j > i && (chAtm[i].Type () != chAtm[j].Type ())) {
          cout << chAtm[i].Type () << i << "<>" << chAtm[j].Type () << j << '\t'
               << std::abs (chAtm[i].Charge () - chAtm[j].Charge ()) << '\t'
               << chAtm[i].dist (chAtm[j]) << endl;
          of << i << "<>" << j << '\t'
             << std::abs (chAtm[i].Charge () - chAtm[j].Charge ()) << '\t'
             << chAtm[i].dist (chAtm[j]) << endl;
          ofd << i << "<>" << j << '\t'
              << std::abs (chAtm[i].Charge () - chAtm[j].Charge ()) << '\t'
              << chAtm[i].dist (chAtm[j]) << endl;
        }
      }
    }
    std::cout.unsetf (std::ios_base::fixed);
    pos /= posch;
    neg /= negch;
    std::cout << "pos: " << pos << endl;
    std::cout << "neg: " << neg << endl;
    std::cout << "d: " << (pos - neg) << endl;
    std::cout << "dip: " << dip << "=" << abs (dip) << "*(" << (dip / abs (dip))
              << ')' << endl;
    delete[] chAtm;
  } catch (const char *problem) {
    cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m" << endl;
  } catch (const std::string &problem) {
    cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem.c_str () << "\033[0m"
         << endl;
  }
  return 0;
}
