/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Test energy in order to see if everitying gives the same answer

   test file for BHEnergyCalculator

   @file energytest.cxx
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 17/10/2017
   @version 0.1.5


   @date 2/11/2017
   @version 1.0

   I think that now does all what is needed to be done
*/

#include <cmath>
#include <iomanip>
#include <iostream>
#include <string>

#include "BHcluster.hpp"
#include "BHenergyCalculator_SMATB.hpp"
#include "BHmetalParameters.hpp"
#include "BHminimizationAlgorithm_FIRE.hpp"
#include "BHminimizationAlgorithm_LBFGSB.hpp"
#include "BHtiming.hpp"
#include "BHworkWithFortran.hpp"

enum methods { mtdEnergies, mtdMinimization };

using std::cin;
using std::cout;
using std::endl;
using std::flush;

using namespace BH;

void EnergyTimer (
  std::string fname, const BHMetalParameters &bhmp, const unsigned long ntests);
ms MinTimerFor (
  std::string fname,
  double &E,
  const BHMetalParameters &bhmp,
  bool printN = false);
ms MinTimerFor_tol (
  std::string fname,
  double &E,
  const BHMetalParameters &bhmp,
  bool printN = false);
ms MinTimerCpp (
  std::string fname,
  double &E,
  const BHMetalParameters &bhmp,
  bool printN = false);
ms MinTimerFIRECpp (
  std::string fname,
  double &E,
  const BHMetalParameters &bhmp,
  bool printN = false);
ms MinTimerCpp_tol (
  std::string fname,
  double &E,
  const BHMetalParameters &bhmp,
  bool printN = false);

int main (int, char **) {
  std::cout << "What do you want to do?\n"
            << "[1]\tTest Energies\n"
            << "[2]\tTest Minimizations\n"
            << "Input:[1-2] ";
  methods chosen;
  unsigned long method (1);
  std::string methodStr;
  getline (std::cin, methodStr);
  try {
    method = (methodStr.empty ()) ? method : std::stoul (methodStr);
    if (method >= 2)
      chosen = mtdMinimization;
    else
      chosen = mtdEnergies;

  } catch (...) {
    chosen = mtdMinimization;
  }

  std::cout << "Input number of tests: ";
  unsigned long ntests (10);
  std::string ntestStr;
  getline (std::cin, ntestStr);
  try {
    ntests = (ntestStr.empty ()) ? ntests : std::stoul (ntestStr);

  } catch (...) {
    ntests = 1;
  }

  switch (chosen) {
  case mtdEnergies:
    cout << "\nI will make a test with the Energy routines.";
    break;
  case mtdMinimization:
    cout << "\nI will make a test with the Minimization routines.";
    break;
  }
  cout << "\nI will make " << ntests << " test for each routine." << std::endl;

  BHMetalParameters bhmp ("MetalParameters.in", true);

  cout << std::setprecision (8);
  std::string fnames[] = /*{"Ag27Cu7.xyz",   "Ag140Cu40_noopt.xyz",
                          "Ag140Cu40.xyz", "shake1415.xyz",
                          "shake923.xyz",  "923.xyz",
                          "1289.xyz",      "1415.xyz"};*/
    {"shake_unminimized.xyz", "brownian_unminimized.xyz"};
  //{ "Ag140Cu40.xyz", "shake_unminimized.xyz", "Ag147_r.xyz", "Ag180.xyz",
  //"Ag7Cu6.xyz", "glo026.xyz", "min009.xyz", "Ag147.xyz", "Ag2Cu2.xyz",
  //"glo021.xyz",  "glo035.xyz"};
  for (const std::string &s : fnames) {
    cout << '\n' << s << ":\n";
    switch (chosen) {
    case mtdEnergies:
      EnergyTimer (s.c_str (), bhmp, ntests);
      break;
    case mtdMinimization:

      ms tFor = std::chrono::steady_clock::duration::zero (),
         tCpp = std::chrono::steady_clock::duration::zero ();
      double E;
      for (unsigned i = 0; i < ntests; ++i) {
        tFor += MinTimerFor (s.c_str (), E, bhmp, i == 0);
        cout << "FORTRAN:\t" << i + 1 << '/' << ntests << "\t\tE=" << E << "\r"
             << std::flush;
      }
      cout << "FORTRAN:\t";
      timing (cout, tFor);
      cout << "\t E= " << E << endl;

      tFor = std::chrono::steady_clock::duration::zero ();
      for (unsigned i = 0; i < ntests; ++i) {
        tFor += MinTimerFor_tol (s.c_str (), E, bhmp, false);
        cout << "FORTRANtol:\t" << i + 1 << '/' << ntests << "\t\tE=" << E
             << "\r" << std::flush;
      }
      cout << "FORTRANtol:\t";
      timing (cout, tFor);
      cout << "\t E= " << E << endl;

      for (unsigned i = 0; i < ntests; ++i) {
        tCpp += MinTimerCpp (s.c_str (), E, bhmp, false);
        cout << "C++:\t\t" << i + 1 << '/' << ntests << "\t\tE=" << E << "\r"
             << std::flush;
      }
      cout << "C++:\t\t";
      timing (cout, tCpp);
      cout << "\t E= " << E << endl;

      tCpp = std::chrono::steady_clock::duration::zero ();
      for (unsigned i = 0; i < ntests; ++i) {
        tCpp += MinTimerCpp_tol (s.c_str (), E, bhmp, false);
        cout << "C++tol:\t\t" << i + 1 << '/' << ntests << "\t\tE=" << E << "\r"
             << std::flush;
      }
      cout << "C++tol:\t\t";
      timing (cout, tCpp);
      cout << "\t E= " << E << endl;

      tCpp = std::chrono::steady_clock::duration::zero ();
      for (unsigned i = 0; i < ntests; ++i) {
        tCpp += MinTimerFIRECpp (s.c_str (), E, bhmp, false);
        cout << "C++FIRE:\t" << i + 1 << '/' << ntests << "\t\tE=" << E << "\r"
             << std::flush;
      }
      cout << "C++FIRE:\t";
      timing (cout, tCpp);
      cout << "\t E= " << E << endl;
      break;
    }
  }

  return 0;
}

void EnergyTimer (
  std::string fname,
  const BHMetalParameters &bhmp,
  const unsigned long ntests) {
  BHCluster c (fname);
  unsigned NofAtoms_ = c.getNofAtoms ();
  cout << "*********\t" << NofAtoms_ << " Atoms" << endl;
  BHdouble *x = new BHdouble[3 * NofAtoms_],
           *gfor = new BHdouble[3 * NofAtoms_],
           *gforTol = new BHdouble[3 * NofAtoms_],
           *gcpp = new BHdouble[3 * NofAtoms_],
           *gcppTol = new BHdouble[3 * NofAtoms_],
           *g = new BHdouble[3 * NofAtoms_];

  for (unsigned j = 0; j < NofAtoms_; j++) {
    x[3 * j] = c[j].X ();
    x[3 * j + 1] = c[j].Y ();
    x[3 * j + 2] = c[j].Z ();
  }

  tp start, stop;
  ms for_time = std::chrono::steady_clock::duration::zero (),
     cpp_time = std::chrono::steady_clock::duration::zero ();
  BHdouble for_energy, cpp_energy;

  bhmp.setInteractionLabels (c);
  BHWWF::sendClusterToFortran (c, bhmp);

  for_energy = BHWWF::BHGradient (x, gfor);
  std::cout << "fortran:\t" << for_energy << '\t' << flush;

  for_time = std::chrono::steady_clock::duration::zero ();
  start = std::chrono::steady_clock::now ();
  for (unsigned i = 0; i < ntests; i++) {
    BHWWF::BHGradient (x, gfor);
  }
  stop = std::chrono::steady_clock::now ();
  for_time += stop - start;
  timing (std::cout, for_time);
  std::cout << std::endl;

  BHWWF::BHSetNeigh_tol (x);
  BHWWF::BHGradient_tol (x, gforTol);
  std::cout << "fortran_tol:\t" << for_energy << '\t' << flush;
  for_time = std::chrono::steady_clock::duration::zero ();
  start = std::chrono::steady_clock::now ();
  for (unsigned i = 0; i < ntests; i++) {
    BHWWF::BHGradient_tol (x, g);
  }
  stop = std::chrono::steady_clock::now ();

  for_time += stop - start;
  timing (std::cout, for_time);
  std::cout << std::endl;

  BHEnergyCalculator_SMATB EC (c, bhmp);

  cpp_energy = EC.Gradient (x, gcpp);
  std::cout << "cpp:\t\t" << cpp_energy << '\t' << flush;

  start = std::chrono::steady_clock::now ();
  for (unsigned i = 0; i < ntests; i++) {
    EC.Gradient (x, gcpp);
  }
  stop = std::chrono::steady_clock::now ();

  cpp_time += stop - start;

  timing (std::cout, cpp_time);
  std::cout << std::endl;
  // tol
  cpp_time = std::chrono::steady_clock::duration::zero ();
  EC.Neighbours_for_tol (x);
  cpp_energy = EC.Gradient_tol (x, gcppTol);
  std::cout << "cpp_tol:\t" << cpp_energy << '\t' << flush;
  start = std::chrono::steady_clock::now ();
  for (unsigned i = 0; i < ntests; i++) {
    EC.Gradient_tol (x, g);
  }
  stop = std::chrono::steady_clock::now ();

  cpp_time += stop - start;
  timing (std::cout, cpp_time);
  std::cout << std::endl;

  BHdouble diffTol = 0, diffCpp = 0, diffCppTol = 0;
  for (unsigned j = 0; j < NofAtoms_; j++) {
    BHdouble gx = gfor[3 * j] - gcpp[3 * j],
             gy = gfor[3 * j + 1] - gcpp[3 * j + 1],
             gz = gfor[3 * j + 2] - gcpp[3 * j + 2];
    diffCpp += sqrt (gx * gx + gy * gy + gz * gz);

    gx = gfor[3 * j] - gforTol[3 * j];
    gy = gfor[3 * j + 1] - gforTol[3 * j + 1];
    gz = gfor[3 * j + 2] - gforTol[3 * j + 2];
    diffTol += sqrt (gx * gx + gy * gy + gz * gz);

    gx = gfor[3 * j] - gcppTol[3 * j];
    gy = gfor[3 * j + 1] - gcppTol[3 * j + 1];
    gz = gfor[3 * j + 2] - gcppTol[3 * j + 2];
    diffCppTol += sqrt (gx * gx + gy * gy + gz * gz);
  }
  diffCpp /= NofAtoms_;
  diffCppTol /= NofAtoms_;
  diffTol /= NofAtoms_;
  cout << "\n\t\tTol\t\tCpp\t\tCppTol";
  cout << "\ngradient error:\t" << diffTol << '\t' << diffCpp << '\t'
       << diffCppTol << endl;
  delete[] x;
  delete[] g;
  delete[] gfor;
  delete[] gforTol;
  delete[] gcpp;
  delete[] gcppTol;
}

// MinTimer
ms MinTimerFor (
  std::string fname, double &E, const BHMetalParameters &bhmp, bool printN) {
  BHCluster c (fname);
  unsigned NofAtoms_ = c.getNofAtoms ();
  if (printN)
    cout << "*********\t" << NofAtoms_ << " Atoms" << endl;
  BHdouble *x = new BHdouble[3 * NofAtoms_];

  tp start, stop;
  ms for_time;

  bhmp.setInteractionLabels (c);
  BHWWF::sendClusterToFortran (c, bhmp);

  for (unsigned j = 0; j < NofAtoms_; j++) {
    x[3 * j] = c[j].X ();
    x[3 * j + 1] = c[j].Y ();
    x[3 * j + 2] = c[j].Z ();
  }
  start = std::chrono::steady_clock::now ();
  E = BHWWF::BHminimization (x);
  stop = std::chrono::steady_clock::now ();
  for_time = stop - start;

  delete[] x;
  return for_time;
}

ms MinTimerFor_tol (
  std::string fname, double &E, const BHMetalParameters &bhmp, bool printN) {
  BHCluster c (fname);
  unsigned NofAtoms_ = c.getNofAtoms ();
  if (printN)
    cout << "*********\t" << NofAtoms_ << " Atoms" << endl;
  BHdouble *x = new BHdouble[3 * NofAtoms_];

  tp start, stop;
  ms for_time;

  bhmp.setInteractionLabels (c);
  BHWWF::sendClusterToFortran (c, bhmp);

  for (unsigned j = 0; j < NofAtoms_; j++) {
    x[3 * j] = c[j].X ();
    x[3 * j + 1] = c[j].Y ();
    x[3 * j + 2] = c[j].Z ();
  }
  start = std::chrono::steady_clock::now ();
  E = BHWWF::BHminimization_tol (x);
  stop = std::chrono::steady_clock::now ();
  for_time = stop - start;

  delete[] x;
  return for_time;
}

ms MinTimerCpp_tol (
  std::string fname, double &E, const BHMetalParameters &bhmp, bool printN) {
  BHCluster c (fname);
  unsigned NofAtoms_ = c.getNofAtoms ();
  if (printN)
    cout << "*********\t" << NofAtoms_ << " Atoms" << endl;
  BHdouble *x = new BHdouble[3 * NofAtoms_];
  for (unsigned j = 0; j < NofAtoms_; j++) {
    x[3 * j] = c[j].X ();
    x[3 * j + 1] = c[j].Y ();
    x[3 * j + 2] = c[j].Z ();
  }

  tp start, stop;
  ms cpp_time;

  bhmp.setInteractionLabels (c);

  for (unsigned j = 0; j < NofAtoms_; j++) {
    x[3 * j] = c[j].X ();
    x[3 * j + 1] = c[j].Y ();
    x[3 * j + 2] = c[j].Z ();
  }

  BHEnergyCalculator_SMATB EC (c, bhmp);
  BHMinimizationAlgorithm_LBFGSB LBFGSB;

  start = std::chrono::steady_clock::now ();
  E = LBFGSB.Minimization_tol (x, NofAtoms_, &EC);
  stop = std::chrono::steady_clock::now ();
  cpp_time = stop - start;

  delete[] x;
  return cpp_time;
}

ms MinTimerCpp (
  std::string fname, double &E, const BHMetalParameters &bhmp, bool printN) {
  BHCluster c (fname);
  unsigned NofAtoms_ = c.getNofAtoms ();
  if (printN)
    cout << "*********\t" << NofAtoms_ << " Atoms" << endl;
  BHdouble *x = new BHdouble[3 * NofAtoms_];

  tp start, stop;
  ms cpp_time;

  bhmp.setInteractionLabels (c);

  for (unsigned j = 0; j < NofAtoms_; j++) {
    x[3 * j] = c[j].X ();
    x[3 * j + 1] = c[j].Y ();
    x[3 * j + 2] = c[j].Z ();
  }

  BHEnergyCalculator_SMATB EC (c, bhmp);
  BHMinimizationAlgorithm_LBFGSB LBFGSB;

  start = std::chrono::steady_clock::now ();
  E = LBFGSB.Minimization (x, NofAtoms_, &EC);
  stop = std::chrono::steady_clock::now ();
  cpp_time = stop - start;

  delete[] x;
  return cpp_time;
}

ms MinTimerFIRECpp (
  std::string fname, double &E, const BHMetalParameters &bhmp, bool printN) {
  BHCluster c (fname);
  unsigned NofAtoms_ = c.getNofAtoms ();
  if (printN)
    cout << "*********\t" << NofAtoms_ << " Atoms" << endl;
  BHdouble *x = new BHdouble[3 * NofAtoms_];

  tp start, stop;
  ms cpp_time;

  bhmp.setInteractionLabels (c);

  for (unsigned j = 0; j < NofAtoms_; j++) {
    x[3 * j] = c[j].X ();
    x[3 * j + 1] = c[j].Y ();
    x[3 * j + 2] = c[j].Z ();
  }

  BHEnergyCalculator_SMATB EC (c, bhmp);
  BHMinimizationAlgorithm_FIRE FIRE;

  start = std::chrono::steady_clock::now ();
  E = FIRE.Minimization (x, NofAtoms_, &EC);
  stop = std::chrono::steady_clock::now ();
  cpp_time = stop - start;
  // std::cerr << E <<std::endl;
  delete[] x;
  return cpp_time;
}
