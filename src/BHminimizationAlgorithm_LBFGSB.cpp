/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of class BHMinimizationAlgorithm_LBFGSB



@file BHminimizationAlgorithm .hpp
  @author Daniele Rapetti (iximiel@gmail.com)

  @date 12/11/2019
  @version 1.0

  base class is complete
    */
#include <cmath>
#include <cstring>

#include "BHminimizationAlgorithm_LBFGSB.hpp"
#include "BHworkWithFortran.hpp"

namespace BH {
  using BHWWF::BHLBFGSBInterface;
  constexpr BHWWF::real8 lbfgs_ftol (1.0e-7);
  constexpr BHWWF::real8
    eps_lbfgsb (1e-50); //(std::numeric_limits<double>::min());
  constexpr int itmax = 2000;
  constexpr unsigned lbfgsCorrections = 7;
  BHMinimizationAlgorithm_LBFGSB::BHMinimizationAlgorithm_LBFGSB () = default;

  BHMinimizationAlgorithm_LBFGSB::~BHMinimizationAlgorithm_LBFGSB () = default;

  BHdouble BHMinimizationAlgorithm_LBFGSB::Minimization (
    BHdouble *x, const unsigned &nat, BHEnergyCalculator *calculator) {
    BHLBFGSBInterface lbfgsInterface (3 * nat, lbfgsCorrections);
    BHWWF::real8 *g = new BHWWF::real8[3 * nat];
    BHWWF::real8 energy = 0.0, deltaE, rtol;
    bool work = true;
    do {
      lbfgsInterface.step (energy, x, g);
      switch (lbfgsInterface.giveCurrentTask ()) {
        using tr = BHLBFGSBInterface::taskResult;
      case tr::FG:
        energy = calculator->Gradient (x, g);
        break;
      case tr::NEW_X:
        ///@todo: try another criterion for convergence
        deltaE =
          std::abs (energy - lbfgsInterface.fValuePreviousStep ()); // was en-
        rtol =
          2.0 *
          (deltaE /
           (std::abs (energy) +
            std::abs (lbfgsInterface.fValuePreviousStep ()))); // was abs(en)
        if ((lbfgsInterface.nstep () >= itmax) && (deltaE > eps_lbfgsb)) {
          lbfgsInterface.setTaskToSTOP ();
          ///\todo alert with a warnig of nstep excedeed
        } else if ((rtol < lbfgs_ftol) && (deltaE > eps_lbfgsb)) {
          lbfgsInterface.setTaskToSTOP ();
        } else if ((lbfgsInterface.projectedGradientNorm () <
                    lbfgs_ftol)) { // gradient is small
          lbfgsInterface.setTaskToSTOP ();
        }
        break;
      case tr::ERROR:
      case tr::ABNO:
      case tr::WARN:
      case tr::unknown:
        ///\todo inform the user about the abnormal exit
        lbfgsInterface.setTaskToSTOP ();
        break;
      case tr::CONV:
      case tr::STOP:
        work = false;
        break;
      }
    } while (work);
    delete[] g;
    return energy;
  }

  BHdouble BHMinimizationAlgorithm_LBFGSB::Minimization_tol (
    BHdouble *x, const unsigned &nat, BHEnergyCalculator *calculator) {
    // initializing
    calculator->Neighbours_for_tol (x);

    BHLBFGSBInterface lbfgsInterface (3 * nat, lbfgsCorrections);
    BHWWF::real8 *g = new BHWWF::real8[3 * nat];
    BHWWF::real8 energy = 0.0, deltaE, rtol;
    bool work = true;
    do {
      lbfgsInterface.step (energy, x, g);
      switch (lbfgsInterface.giveCurrentTask ()) {
        using tr = BHLBFGSBInterface::taskResult;
      case tr::FG:
        energy = calculator->Gradient_tol (x, g);
        break;
      case tr::NEW_X:
        ///@todo: try another criterion for convergence
        deltaE =
          std::abs (energy - lbfgsInterface.fValuePreviousStep ()); // was en-
        rtol =
          2.0 *
          (deltaE /
           (std::abs (energy) +
            std::abs (lbfgsInterface.fValuePreviousStep ()))); // was abs(en)
        if ((lbfgsInterface.nstep () >= itmax) && (deltaE > eps_lbfgsb)) {
          lbfgsInterface.setTaskToSTOP ();
          ///\todo alert with a warnig of nstep excedeed
        } else if ((rtol < lbfgs_ftol) && (deltaE > eps_lbfgsb)) {
          lbfgsInterface.setTaskToSTOP ();
        } else if ((lbfgsInterface.projectedGradientNorm () <
                    lbfgs_ftol)) { // gradient is small
          lbfgsInterface.setTaskToSTOP ();
        }
        break;
      case tr::ERROR:
      case tr::ABNO:
      case tr::WARN:
      case tr::unknown:
        ///\todo inform the user about the abnormal exit
        lbfgsInterface.setTaskToSTOP ();
        break;
      case tr::CONV:
      case tr::STOP:
        work = false;
        break;
      }
    } while (work);
    delete[] g;
    return energy;
  }
} // namespace BH
