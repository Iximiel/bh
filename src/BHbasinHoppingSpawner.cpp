/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Here the definitions the basin hopping spawner algorithm

@file BHbasinHopping.cpp
  @author Daniele Rapetti (iximiel@gmail.com)

  @date 12/6/2019
  @version 0.1

  */
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "BHbasinHoppingSpawner.hpp"
#include "BHclusterUtilities.hpp"
#include "BHworkWithFortran.hpp"
//#include "BHdebug.hpp"
#include "BHconstructors.hpp"
#include "BHmetalParameters.hpp"

// remember always to use wID when selecting a walker!!!
// it is more stylish and easy to read

/*
instruction for a benchmark:
/usr/bin/time -o tempo -p ../main
*/
namespace BH {

  BasinHoppingSpawner::BasinHoppingSpawner (const BHSettings &settings)
    : BHAbstractBasinHopping (settings) {
    std::cout << "\t#########################\n"
              << "\t#INITIALIZE BASINHOPPING#\n"
              << "\t####SPAWNER ALGORITHM####\n"
              << "\t#########################\n";
    settings_.separateWalkerOutput_ = true;
    if (settings_.nWalkers_ != 2) {
      throw "To run a spawner run you must declare 2 walkers:\n"
            "the first is the base one\n"
            "the second is the \"fine tuning\" one";
    }
  }

  BasinHoppingSpawner::~BasinHoppingSpawner () = default;
  const std::string BasinHoppingSpawner::BHStyleName () { return "Spawner"; }

  void BasinHoppingSpawner::Initialization () {
    BHAbstractBasinHopping::Initialization ();
    // start first with only one walker
    walkers_.resize (1);
  }

  void BasinHoppingSpawner::MoveAcceptedPostProduction () {
    BHdouble newEne = walkers_[runnigWalker_].lastAcceptedEnergy ();
    if (
      (newEne - GloInfoGeneral_.energy) <
      -MINIMUN_ENERGY_DIFFERENCE) { // 1e-05?
      BH::BHWalkerSettings walkersettings =
        BH::loadBHWalkerSettings (settings_.walkerFilenames_[1].flyingWalker);
      walkers_.push_back (BHConstructors::getConstructor ().createWalker (
        walkersettings, rndEngine_));
      walkers_.back ().copyAllConfigurations (walkers_[runnigWalker_]);
      walkers_.back ().printSettings ();
      {
        std::stringstream fname;
        fname << "w" << std::setw (2) << std::setfill ('0')
              << (walkers_.size () - 1);
        walkers_.back ().identificator = fname.str ();
        enerAll_.emplace (enerAll_.end () - 1, fname.str () + "ener_all.out");
        enerBest_.emplace (
          enerBest_.end () - 1, fname.str () + "ener_best.out");
      }
    }
  }
} // namespace BH
