#include "BHconstructors.hpp"
#include "BHenergyCalculator_SMATB.hpp"
#include "BHmetalParameters.hpp"
#include "BHrandom.hpp"
#include "BHwalker.hpp"
#include "BHwalkerAlgorithms.hpp"
#include "testhelper.hpp"
#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <sstream>

using BHw = BH::BHWalker;
using BH::BHdouble;
// Catch::literals contains _a thta can be used in place of Catch::Approx
using namespace Catch::literals;

bool checkCloned (const BHw &a, const BHw &b) {
  // for you that do not know what is this is down here:
  // I declared a lambda and used it asap by doing [](lmbda args){lambda
  // text}(args) <- the parentesis after the declaration makes the lambda to be
  // called immediately
  return (a.lastAcceptedEnergy () == b.lastAcceptedEnergy ()) &&
         ([] (const BHw &wa, const BHw &wb) -> bool {
           bool t = true;
           for (unsigned i = 0; i < 2; ++i) {
             t &= wa.lastAcceptedOP (i) == wb.lastAcceptedOP (i);
           }
           return t;
         }(a, b)) &&
         (a.lastAcceptedConf () == b.lastAcceptedConf ());
}

TEST_CASE ("BHWalker testing", "[BHWalker]") {
  BH::rndEngine rndEngine_;
  BH::BHMetalParameters bhmp{getBHMPWithAgCu ()};
  std::stringstream ss (
    "#ALGORITHM\n"
    "algorithm	=	stnd\n"
    "seed	=	seed0.in\n"
    "#MOVES\n"
    "shake                prob = 1.0, accTemp = 300, Rmin = 0, Rmax = 1.4\n");
  BH::BHWalkerSettings wSettings =
    BH::loadBHWalkerSettings (ss /*"input_WalkerTEST.in"*/);
  BHw walker = BH::BHConstructors::getConstructor ().createStandardWalker (
    wSettings, bhmp, getAg27Cu7 ());
  SECTION ("Setter and Getters") {
    SECTION ("testing force* for lastAccepted") {
      BHw::forceLastAcceptedEnergy (walker, 0.1);
      std::vector<BH::BHdouble> testAOPs = {0.3, 0.6};
      BHw::forceLastAcceptedOPs (walker, testAOPs[0], testAOPs[1]);

      REQUIRE (walker.lastAcceptedEnergy () == 0.1_a);
      for (unsigned i = 0; i < testAOPs.size (); ++i) {
        REQUIRE (walker.lastAcceptedOP (i) == Catch::Approx (testAOPs[i]));
      }
      testAOPs.push_back (0.9);
      BHw::forceLastAcceptedOPs (walker, testAOPs);
      for (unsigned i = 0; i < testAOPs.size (); ++i) {
        REQUIRE (walker.lastAcceptedOP (i) == Catch::Approx (testAOPs[i]));
      }
    }
    SECTION ("testing force* for lastMove") {
      BHw::forceLastMoveEnergy (walker, 0.2);
      std::vector<BH::BHdouble> testMOPs = {0.4, 0.8};
      BHw::forceLastMoveOPs (walker, testMOPs[0], testMOPs[1]);

      REQUIRE (walker.lastMoveEnergy () == 0.2_a);
      for (unsigned i = 0; i < testMOPs.size (); ++i) {
        REQUIRE (walker.lastMoveOP (i) == Catch::Approx (testMOPs[i]));
      }
      testMOPs.push_back (1.0);
      BHw::forceLastMoveOPs (walker, testMOPs);
      for (unsigned i = 0; i < testMOPs.size (); ++i) {
        REQUIRE (walker.lastMoveOP (i) == Catch::Approx (testMOPs[i]));
      }
    }
  }

  SECTION ("Copy ctor") {
    BHw::forceLastAcceptedEnergy (walker, 0.1);
    std::vector<BH::BHdouble> testAOPs = {0.3, 0.6, 0.9};
    BHw::forceLastAcceptedOPs (walker, testAOPs);
    BHw wCopyCted = walker;
    REQUIRE (walker == wCopyCted);
    REQUIRE (wCopyCted.lastAcceptedEnergy () == 0.1_a);
    for (unsigned i = 0; i < testAOPs.size (); ++i) {
      REQUIRE (wCopyCted.lastAcceptedOP (i) == Catch::Approx (testAOPs[i]));
    }
  }

  SECTION ("BHWalker in action") {
    std::vector<BH::BHOrderParameter> ops = {"555", "sqrt555"};
    BH::BHHisto::BHHistoPars histoPars_[2];
    BH::BHWalkerSupport ::BHWalkerAlgorithmSupport bws{
      {ops, bhmp}, {histoPars_[0], histoPars_[1], false}};
    BH::BHEnergyCalculator_SMATB ec (walker.lastAcceptedConf (), bhmp);

    BH::BHMinimizationAlgorithm_LBFGSB minimizer;
    auto OPs = walker.AnalyzeWorkingConf (bws.Analyzer, bhmp);
    auto calculateEnergy = [&ec] (const BH::BHCluster &x) -> BHdouble {
      BHdouble *coords = new BHdouble[x.getNofAtoms () * 3];
      x.AssignToVector (coords);
      BHdouble ene = ec.Energy (coords);
      delete[] coords;
      return ene;
    };

    SECTION ("Simulation of a step") {
      BHdouble tmpenergy = calculateEnergy (walker.lastMoveConf ());
      BHw::forceLastMoveEnergy (walker, tmpenergy);
      REQUIRE (
        walker.lastMoveClusterData ().Energy () == Catch::Approx (tmpenergy));
      BHw::forceLastMoveOPs (walker, OPs[0], OPs[1]);

      // this updates the last accepted parameters anc copies the working conf
      // in the lastaccepted conf
      REQUIRE_FALSE (
        walker.lastMoveEnergy () ==
        Catch::Approx (walker.lastAcceptedEnergy ()));
      for (unsigned i = 0; i < ops.size (); ++i) {
        REQUIRE_FALSE (
          walker.lastMoveOP (i) == Catch::Approx (walker.lastAcceptedOP (i)));
      }
      walker.moveAccepted ();

      SECTION ("Simulation of an accepted MC step") {
        REQUIRE (
          walker.lastAcceptedClusterData ().Energy () ==
          Catch::Approx (tmpenergy));
        REQUIRE (
          walker.lastMoveEnergy () ==
          Catch::Approx (walker.lastAcceptedEnergy ()));
        for (unsigned i = 0; i < ops.size (); ++i) {
          REQUIRE (
            walker.lastMoveOP (i) == Catch::Approx (walker.lastAcceptedOP (i)));
        }

        BH::BHClusterData PreviouslyAcceptedData =
          walker.lastAcceptedClusterData ();
        auto Temp = walker.doMove (bws.Analyzer, bhmp, rndEngine_);

        REQUIRE (walker.getTotalMovesDone () == 1);
        CHECK (Temp == 300.0_a); // should be the value in the inputFile
        REQUIRE_FALSE (walker.lastAcceptedConf () == walker.lastMoveConf ());
        walker.minimizeWorkingConf (&minimizer, &ec);
        { // these brakets emulates walker.confirmStep with an accepted move
          auto newOPs = walker.AnalyzeWorkingConf (bws.Analyzer, bhmp);
          BHw::forceLastMoveOPs (walker, newOPs[0], newOPs[1]);
          walker.moveAccepted ();
        }
        REQUIRE (
          walker.lastMoveClusterData () == walker.lastAcceptedClusterData ());
        ///@todo make the actual BH walker in a service class
      }

      SECTION ("Interaction between walkers") {
        // interaction betweeen walkers:
        BHw walker2 =
          BH::BHConstructors::getConstructor ().createStandardWalker (
            wSettings, bhmp, getAg27Cu7 ());
        REQUIRE_FALSE (checkCloned (walker, walker2));
        // this make possible to create the FL algorithm
        walker2.copyAcceptedConfiguration (walker);
        REQUIRE (checkCloned (walker, walker2));
        { // this simulate algorithm working within bh++
          std::vector<BHw> vBHco{walker, walker2};
          BHdouble excEne =
            BH::BHWalkerAlgorithms::BHWalkerAlgorithmParallelExcited (
              vBHco[0], vBHco, bhmp, bws, rndEngine_);
          REQUIRE (excEne == Catch::Approx (-0.7));
          BHw::forceLastAcceptedOPs (vBHco[1], 100.0, 100.0);
          excEne = BH::BHWalkerAlgorithms::BHWalkerAlgorithmParallelExcited (
            vBHco[0], vBHco, bhmp, bws, rndEngine_);
          REQUIRE_FALSE (excEne == Catch::Approx (-0.7));
        }
      }
    }
  }
}

SCENARIO ("Automatic BHSettings to BHFL", "[BHSettings]") {
  GIVEN ("A default BHSettings with two walkers") {
    BH::BHSettings settings;
    settings.walkerFilenames_.emplace_back ();
    REQUIRE (settings.walkerFilenames_.size () == 2);
    WHEN ("We set up a BHFL simulation") {
      BH::BHSettingsUtilities::setFlyingLandingMinimization (settings);
      THEN ("All the walkers should have a landing called \"companion.in\" and "
            "no best walkers should be present") {
        REQUIRE (
          settings.walkerFilenames_[0].landingWalker == "landing:companion.in");
        REQUIRE (settings.walkerFilenames_[0].hikingWalker == "");
        REQUIRE (
          settings.walkerFilenames_[1].landingWalker == "landing:companion.in");
        REQUIRE (settings.walkerFilenames_[1].hikingWalker == "");

        REQUIRE (settings.uniqueBestWalker_ == "");
        REQUIRE (settings.separateWalkerOutput_);
      }
    }

    WHEN ("We set up a BHFLH_1 simulation") {
      BH::BHSettingsUtilities::setFLMinimizationwBestWalker (settings);
      THEN ("All the walkers should have a landing called \"companion.in\" and "
            "a unique best walker should be present, shared by all, the best "
            "walker should provoke a restart") {
        REQUIRE (
          settings.walkerFilenames_[0].landingWalker == "landing:companion.in");
        REQUIRE (settings.walkerFilenames_[0].hikingWalker == "");
        REQUIRE (
          settings.walkerFilenames_[1].landingWalker == "landing:companion.in");
        REQUIRE (settings.walkerFilenames_[1].hikingWalker == "");

        REQUIRE (settings.uniqueBestWalker_ == "bestWalker.in");
        REQUIRE (settings.BWRestartsLanding_ == true);
        REQUIRE (settings.separateWalkerOutput_);
      }
    }

    WHEN ("We set up a BHFLH_2 simulation") {
      BH::BHSettingsUtilities::setFLMinimizationwBestWalkerNoRestart (settings);
      THEN ("All the walkers should have a landing called \"companion.in\" and "
            "an unique best walker should be present, shared by all, the best "
            "walker should not provoke a restart") {
        REQUIRE (
          settings.walkerFilenames_[0].landingWalker == "landing:companion.in");
        REQUIRE (settings.walkerFilenames_[0].hikingWalker == "");
        REQUIRE (
          settings.walkerFilenames_[1].landingWalker == "landing:companion.in");
        REQUIRE (settings.walkerFilenames_[1].hikingWalker == "");

        REQUIRE (settings.uniqueBestWalker_ == "bestWalker.in");
        REQUIRE (settings.BWRestartsLanding_ == false);
        REQUIRE (settings.separateWalkerOutput_);
      }
    }

    WHEN ("We set up a BHFLH_3 simulation") {
      BH::BHSettingsUtilities::setFlyingLandingHikingMinimization (settings);
      THEN ("All the walkers should have a landing called \"companion.in\" and "
            "also a restarting hikingR walker should be present, no best "
            "walker should be present") {
        REQUIRE (
          settings.walkerFilenames_[0].landingWalker == "landing:companion.in");
        REQUIRE (
          settings.walkerFilenames_[0].hikingWalker == "hikingR:bestWalker.in");
        REQUIRE (
          settings.walkerFilenames_[1].landingWalker == "landing:companion.in");
        REQUIRE (
          settings.walkerFilenames_[1].hikingWalker == "hikingR:bestWalker.in");

        REQUIRE (settings.uniqueBestWalker_ == "");
        REQUIRE (settings.separateWalkerOutput_);
      }
    }

    WHEN ("We set up a BHFLH_4 simulation") {
      BH::BHSettingsUtilities::setFlyingLandingHikingMinimizationNoRebound (
        settings);
      THEN ("All the walkers should have a landing called \"companion.in\" and "
            "also a non restarting hiking walker should be present, no best "
            "walker should be present") {
        REQUIRE (
          settings.walkerFilenames_[0].landingWalker == "landing:companion.in");
        REQUIRE (
          settings.walkerFilenames_[0].hikingWalker == "hiking:bestWalker.in");
        REQUIRE (
          settings.walkerFilenames_[1].landingWalker == "landing:companion.in");
        REQUIRE (
          settings.walkerFilenames_[1].hikingWalker == "hiking:bestWalker.in");

        REQUIRE (settings.uniqueBestWalker_ == "");
        REQUIRE (settings.separateWalkerOutput_);
      }
    }
  }
}
