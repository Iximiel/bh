#ifndef TESTHELPER_HPP
#define TESTHELPER_HPP
#include "BHclusterAtoms.hpp"
#include "BHvector.hpp"
#include "BHwalker.hpp"
#include "catch2/catch_tostring.hpp"
#include <sstream>

bool operator== (const BH::BHVector &a, const BH::BHVector &b);
bool operator== (const BH::BHAtom &a, const BH::BHAtom &b);
bool operator== (const BH::BHClusterAtoms &a, const BH::BHClusterAtoms &b);
bool operator== (const BH::BHClusterData &a, const BH::BHClusterData &b);
bool operator== (const BH::BHWalker &a, const BH::BHWalker &b);

const BH::BHClusterAtoms getAg27Cu7 ();
const BH::BHClusterAtoms getMimimumSignature666 ();
const BH::BHClusterAtoms getMimimumSignature555 ();
const BH::BHClusterAtoms getMimimumSignature444 ();
const BH::BHClusterAtoms getMimimumSignature422 ();
const BH::BHClusterAtoms getMimimumSignature421 ();

BH::BHMetalParameters getBHMPWithAgCu ();
std::stringstream AgCuBHMP ();
std::stringstream ArArBHMP ();

namespace Catch {
  template <>
  struct StringMaker<BH::BHVector> {
    static std::string convert (BH::BHVector const &value) {
      return "(" + std::to_string (value.X ()) + " " +
             std::to_string (value.Y ()) + " " + std::to_string (value.Z ()) +
             ")";
    }
  };
} // namespace Catch

#endif // TESTHELPER_HPP
