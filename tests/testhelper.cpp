#include "testhelper.hpp"

constexpr BH::BHdouble doublThreshold = 1e-10;

const BH::BHClusterAtoms seed0 (
  {{"Ag", -12.82867009, 5.06443445, -0.25272611},
   {"Ag", -10.23729016, 0.22837973, 0.30396981},
   {"Ag", -5.40584488, 2.45941343, -1.16551323},
   {"Ag", -11.05460527, -1.15205545, -2.03530874},
   {"Ag", -8.16693094, 6.47713585, -0.73206084},
   {"Ag", -13.25702283, 0.25761371, -3.13814966},
   {"Ag", -7.51916530, 1.04749018, 0.02418676},
   {"Ag", -12.94872469, 0.17323653, -0.38939686},
   {"Ag", -12.37097978, 2.71721829, -4.15781312},
   {"Ag", -13.13690323, 5.14881345, -3.00156392},
   {"Ag", -5.71438765, 2.54320482, -3.91419105},
   {"Ag", -10.13110910, 2.70854183, -5.95039161},
   {"Ag", -10.11669597, 4.84040035, 0.43202794},
   {"Ag", -10.76589172, 0.37293502, -4.40961814},
   {"Ag", -11.84258694, 2.57261664, 0.55566368},
   {"Ag", -7.97351083, 4.04208339, -4.61022234},
   {"Ag", -10.64524214, 4.98498715, -4.28153849},
   {"Ag", -6.41404044, 4.82120499, -2.37295051},
   {"Ag", -8.47541422, 6.56152358, -3.48082554},
   {"Ag", -7.44473066, 3.89760106, 0.10322629},
   {"Ag", -8.66946238, -1.35277867, -3.70173854},
   {"Ag", -8.36102761, -1.43684846, -0.95302131},
   {"Ag", -10.78151668, 6.41194512, -1.83365775},
   {"Ag", -6.58275594, 0.14644003, -2.49703636},
   {"Ag", -8.04799398, 1.19152821, -4.68948750},
   {"Ag", -13.64956671, 2.72032098, -1.62579256},
   {"Ag", -9.26168936, 2.47066783, 1.80443068},
   {"Cu", -9.54623159, 2.54847325, -0.73352305},
   {"Cu", -11.41271937, 1.49723009, -1.91405673},
   {"Cu", -11.25394099, 3.89012451, -1.85841524},
   {"Cu", -8.94294660, 4.48564441, -2.09908592},
   {"Cu", -9.19966389, 0.61385734, -2.18919929},
   {"Cu", -9.84661076, 2.63061825, -3.41237190},
   {"Cu", -7.67324008, 2.46081087, -2.30386398}});

const BH::BHClusterAtoms seed421 (
  {{"Cu", 4.33500, 0.83427, 2.35968},
   {"Cu", 2.89000, 0.00000, 4.71935},
   {"Ag", 1.44500, 0.83427, 2.35968},
   {"Ag", 2.89000, -1.66854, 2.35968},
   {"Ag", 4.33500, 2.50281, 4.71935},
   {"Ag", 5.78000, 0.00000, 4.71935}});

const BH::BHClusterAtoms seed422 (
  {{"Cu", 1.44500, 2.50281, 4.71935},
   {"Cu", 2.89000, 0.00000, 4.71935},
   {"Ag", 1.44500, 0.83427, 2.35968},
   {"Ag", 0.00000, 0.00000, 4.71935},
   {"Ag", 1.44500, 0.83427, 7.07903},
   {"Ag", 4.33500, 2.50281, 4.71935}});

const BH::BHClusterAtoms seed555 (
  {{"Cu", -0.364776857, -1.16683214, 0.215925143},
   {"Cu", 0.424477143, 1.35708786, -0.251014857},
   {"Ag", -1.93382586, 0.304076857, -1.39194086},
   {"Ag", -1.79985286, 0.776672857, 1.38829614},
   {"Ag", 2.28071814, -0.753118143, 0.0178561429},
   {"Ag", 0.587981143, -0.641160143, -2.23879286},
   {"Ag", 0.805279143, 0.123272857, 2.25967114}});

const BH::BHClusterAtoms seed444 (
  {{"Ag", 1.66854, 1.66854, 1.66854},
   {"Ag", 5.00563, 1.66854, 1.66854},
   {"Ag", 3.33708, 0.00000, 0.00000},
   {"Ag", 3.33708, 3.33708, 0.00000},
   {"Ag", 3.33708, 0.00000, 3.33708},
   {"Ag", 3.33708, 3.33708, 3.33708}});

const BH::BHClusterAtoms seed666 (
  {{"Ag", 1.66854, 1.66854, 1.66854},
   {"Ag", 0.00000, 3.33708, 3.33708},
   {"Ag", -1.66854, 1.66854, 1.66854},
   {"Ag", 0.00000, 3.33708, 0.00000},
   {"Ag", 0.00000, 0.00000, 3.33708},
   {"Ag", 1.66854, 5.00563, 1.66854},
   {"Ag", 1.66854, 1.66854, 5.00563},
   {"Ag", 3.33708, 3.33708, 3.33708}});

bool operator== (const BH::BHVector &a, const BH::BHVector &b) {
  return std::abs (a.X () - b.X ()) < doublThreshold &&
         std::abs (a.Y () - b.Y ()) < doublThreshold &&
         std::abs (a.Z () - b.Z ()) < doublThreshold;
}

bool operator== (const BH::BHAtom &a, const BH::BHAtom &b) {
  return a.Type () == b.Type () &&
         std::abs (a.X () - b.X ()) < doublThreshold &&
         std::abs (a.Y () - b.Y ()) < doublThreshold &&
         std::abs (a.Z () - b.Z ()) < doublThreshold;
}

bool operator== (const BH::BHClusterAtoms &a, const BH::BHClusterAtoms &b) {
  if (a.getNofAtoms () != b.getNofAtoms ()) {
    return false;
  }
  for (unsigned i = 0; i < a.getNofAtoms (); ++i) {
    if (a[i] == b[i])
      continue;
    return false;
  }
  return true;
}

bool operator== (const BH::BHClusterData &a, const BH::BHClusterData &b) {
  if (a.numberOfOP () != b.numberOfOP ()) {
    return false;
  }
  bool equal = std::abs (a.Energy () - b.Energy ()) < doublThreshold;
  for (unsigned i = 0; i < a.numberOfOP (); ++i) {
    equal &=
      std::abs (a.orderParameter (i) - b.orderParameter (i)) < doublThreshold;
  }
  return equal;
}

bool operator== (const BH::BHWalker &a, const BH::BHWalker &b) {
  // this tests only energy and OPs
  return a.lastAcceptedClusterData () == b.lastAcceptedClusterData () &&
         a.lastMoveClusterData () == b.lastMoveClusterData () &&
         a.lastAcceptedConf () == b.lastAcceptedConf () &&
         a.lastMoveConf () == b.lastMoveConf ();
}

const BH::BHClusterAtoms getAg27Cu7 () { return seed0; }
const BH::BHClusterAtoms getMimimumSignature666 () { return seed666; }
const BH::BHClusterAtoms getMimimumSignature555 () { return seed555; }
const BH::BHClusterAtoms getMimimumSignature444 () { return seed444; }
const BH::BHClusterAtoms getMimimumSignature422 () { return seed422; }
const BH::BHClusterAtoms getMimimumSignature421 () { return seed421; }

BH::BHMetalParameters getBHMPWithAgCu () {
  BH::BHMetalParameters toret{};
  std::stringstream ss = AgCuBHMP ();
  toret.loadFromStream (ss);
  return toret;
}

std::stringstream AgCuBHMP () {
  std::stringstream ss (
    "#NofMetals:\n"
    "2\n"
    "#Metal parameters:\n"
    "#Cohesion energy[eV]	Atomic radius[A]	Mass[amu]\n"
    "Ag     2.949710351788	    1.445	108.\n"
    "Cu     3.4997844019608	    1.28	64.\n"
    "#Interaction Parameters\n"
    "#kind	     met1	     met2	p	q	a	"
    "qsi	cutOff_Start	cutOff_End	"
    "interaction_NNdist[facultative]\n"
    "SMATB	     Ag	     Ag		10.85	3.18	0.1031	1.1895	"
    "4.08707719	5.0056268338740553\n"
    "SMATB	     Ag	     Cu		10.70	2.805	0.0977	1.2275	"
    "4.08707719	4.4340500673763259\n"
    "SMATB	     Cu	     Cu		10.55	2.43	0.0894	1.2799	"
    "3.62038672	4.4340500673763259\n"
    "#comments terminate the reading process\n");
  return ss;
}
std::stringstream ArArBHMP () {
  std::stringstream ss (
    "#NofMetals:\n"
    "1\n"
    "#Metal parameters:\n"
    "#Cohesion energy[eV]	Atomic radius[A]	Mass[amu]\n"
    "Ar     0.0	    0.97	40\n"
    "#Interaction Parameters\n"
    "#kind	     met1	     met2	eps	sigma	"
    "cutOff_Start	cutOff_End\n"
    "LJ	     Ar	     Ar		 119.8	3.405 0.0 0.0\n"
    "#comments terminate the reading process\n");
  return ss;
}
