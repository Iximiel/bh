#include <catch2/catch_approx.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include <catch2/catch_test_macros.hpp>

#include "BHclusterCreatorUtilities.hpp"
#include "BHclusterUtilities.hpp"
#include "BHmetalParameters.hpp"
#include "testhelper.hpp"
#include <algorithm>
#include <iostream>
#include <sstream>

using BH::BHClusterUtilities::BHClusterRandomizerData;
using BH::BHClusterUtilities::CountIslands;

SCENARIO (
  "Loading data for creating a random cluster", "[BHClusterUtilities]") {
  GIVEN ("A no_seedFile") {
    std::stringstream ss ("5000 #nat3d (numero atomi totali)\n"
                          "Ag Cu #nomemet1,nomemet2\n"
                          "4000 1000 #27 di Ag e 7 Cu\n"
                          "scartesian #ref_frame\n"
                          "0,1. \n"
                          "0,0.01\n"
                          "0,0.01\n");
    BHClusterRandomizerData rndmData;
    ss >> rndmData;
    THEN ("BHClusterRandomizerData should load the data correctly") {
      REQUIRE (rndmData.nAtomByTypeAndN[0].Type == "Ag");
      REQUIRE (rndmData.nAtomByTypeAndN[0].NofAtoms == 4000);
      REQUIRE (rndmData.nAtomByTypeAndN[1].Type == "Cu");
      REQUIRE (rndmData.nAtomByTypeAndN[1].NofAtoms == 1000);
    }
  }
  GIVEN ("A BHClusterRandomizerData with 27 Ag and 7 Cu atoms") {
    BHClusterRandomizerData rndmData;
    rndmData.nAtomByTypeAndN.push_back ({"Ag", 27});
    rndmData.nAtomByTypeAndN.push_back ({"Cu", 7});
    rndmData.N = 34;
    rndmData.randomClusterShape = BHClusterRandomizerData::typeOfRandom::sphere;
    rndmData.XLimits = {0.0, 3.0};
    THEN ("randomCluster should create a cluster with the right atoms") {
      BH::rndEngine rng;
      auto cl = BH::BHClusterUtilities::randomCluster (rndmData, rng);
      REQUIRE (std::count_if (cl.begin (), cl.end (), [] (const BH::BHAtom &a) {
                 return a.Type () == "Ag";
               }) == 27);
      REQUIRE (std::count_if (cl.begin (), cl.end (), [] (const BH::BHAtom &a) {
                 return a.Type () == "Cu";
               }) == 7);
      REQUIRE (cl.NofSpecies () == 2);
    }
  }
}

SCENARIO ("Islands analysis", "[BHClusterUtilities]") {
  auto bhmp{getBHMPWithAgCu ()};
  GIVEN ("Two separated icosahedra in the same BHCluster") {
    BH::BHdouble Agdist = bhmp.getNNdist ("AgAg");
    auto ico7 = BH::Icosahedron::icosahedron (7, "Ag", Agdist / 2.0);
    auto ico6 = BH::Icosahedron::icosahedron (6, "Ag", Agdist / 2.0);
    unsigned surfaceAtoms7 = ico7.size () - ico6.size ();
    unsigned surfaceAtoms6 =
      ico6.size () -
      BH::Icosahedron::icosahedron (5, "Ag", Agdist / 2.0).size ();
    BH::BHCluster twoIslands (ico7.size () + ico6.size ());

    for (unsigned i = 0; i < ico7.size (); ++i) {
      twoIslands[i] = ico7[i];
    }
    for (unsigned i = 0; i < ico6.size (); ++i) {
      twoIslands[i + ico7.size ()] = ico6[i] + BH::BHVector (1000.0, 0.0, 0.0);
    }

    bhmp.setInteractionLabels (twoIslands);
    THEN ("CountIslands should return 2 islands") {
      REQUIRE (CountIslands (twoIslands, bhmp) == 2);
      std::vector<BH::BHClusterUtilities::IslandInfo> Islands =
        BH::BHClusterUtilities::IslandsAnalysis (twoIslands, bhmp);
      REQUIRE (Islands.size () == 2);
      REQUIRE (Islands[0].nOfAtoms == ico7.size ());
      REQUIRE (Islands[1].nOfAtoms == ico6.size ());
      std::vector<BH::BHClusterUtilities::IslandInfoSurf> IslandsSurf =
        BH::BHClusterUtilities::IslandsAnalysisWithSurface (twoIslands, bhmp);
      REQUIRE (IslandsSurf[0].nOfAtoms == ico7.size ());
      REQUIRE (IslandsSurf[1].nOfAtoms == ico6.size ());
      REQUIRE (IslandsSurf[0].nOfAtomsOnSurf == surfaceAtoms7);
      REQUIRE (IslandsSurf[1].nOfAtomsOnSurf == surfaceAtoms6);
    }
    WHEN ("The BHCLuster is shuffled") {
      // this can be useful to make the thing more realistic in the atom
      // ordering
      std::random_shuffle (twoIslands.begin (), twoIslands.end ());
      THEN ("CountIsland should return 2 again") {
        REQUIRE (CountIslands (twoIslands, bhmp) == 2);
        // due to the random_shuffle the first island may be the one related to
        // ico2, so i won't test Islands again (also because it is not required)
      }
    }
  }
  GIVEN ("A cluster with and isolated atom \"nearby\"") {
    BH::BHdouble Agdist = bhmp.getNNdist ("AgAg");
    auto ico3 = BH::Icosahedron::icosahedron (3, "Ag", Agdist / 2.0);
    BH::BHCluster cl (ico3.size () + 1);

    for (unsigned i = 0; i < ico3.size (); ++i) {
      cl[i] = ico3[i];
    }
    cl[ico3.size ()] = BH::BHAtom ("Cu", 1000.0, 0.0, 0.0);
    bhmp.setInteractionLabels (cl);
    THEN ("It should have 2 islands") {
      std::vector<BH::BHClusterUtilities::IslandInfo> Islands =
        BH::BHClusterUtilities::IslandsAnalysis (cl, bhmp);
      REQUIRE (Islands.size () == 2);
      REQUIRE (Islands[0].nOfAtoms == ico3.size ());
      REQUIRE (Islands[1].nOfAtoms == 1);
    }
    AND_WHEN ("IslandsCollapser is run on the cluster") {
      BH::rndEngine rng (123);
      BH::BHClusterUtilities::IslandsCollapser (cl, bhmp, rng);
      THEN ("The cluster should have only one island") {
        REQUIRE (CountIslands (cl, bhmp) == 1);
      }
    }
  }
}
