#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <sstream>

#include "BHsettings.hpp"

using namespace Catch::literals;

SCENARIO ("Loading BHsettings from file", "[BHSettings]") {
  std::stringstream ss ("#GENERAL\n"
                        "rng_seed = 369\n"
                        "#POTENTIAL\n"
                        "name_potpar = 'AgCu.in'\n"
                        "#RUN\n"
                        "n_mc = 1000\n"
                        "#GOA\n"
                        "histo0.nbins = 100\n"
                        "histo1.nbins = 2\n"
                        "OP0 = signature 555\n"
                        "OP1 = CuAg\n"
                        "#OUTPUT\n"
                        "OP0_param_min = 0\n"
                        "OP0_param_max = 1\n"
                        "OP0_parameter_partitioning = 50\n"
                        "#WALKERS 1\n"
                        "input_WalkerSTND.in\n");

  BH::BHSettings settings = BH::BHSettingsUtilities::loadBHSettings (ss, false);
  THEN ("The example file should be loaded correctly") {
    REQUIRE (settings.walkerFilenames_.size () == 1u);
    REQUIRE (
      settings.minimizationAlgorithm_ ==
      BH::BHminimizationAlgorithmType::LBFGSB);
    REQUIRE (
      settings.walkerFilenames_[0].flyingWalker == "input_WalkerSTND.in");
    REQUIRE (settings.nMC_ == 1000);
    REQUIRE (settings.RngSeed_ == 369);
    REQUIRE (settings.OPSettings_[0].histoPars_.getNbins () == 100);
    REQUIRE (settings.OPSettings_[1].histoPars_.getNbins () == 2);
    REQUIRE (
      settings.OPSettings_[0].OP_.OPtype_ ==
      BH::BHOrderParameter::BHOrderParameter_type::signature);
    REQUIRE (
      settings.OPSettings_[1].OP_.OPtype_ ==
      BH::BHOrderParameter::BHOrderParameter_type::bonds);
    // zero is not always approximated correctly...
    REQUIRE (
      settings.OPSettings_[0].parametersForOutput_.paramMin ==
      Catch::Approx (0.0).margin (0.001));
    REQUIRE (settings.OPSettings_[0].parametersForOutput_.paramMax == 1.0_a);
    REQUIRE (settings.OPSettings_[0].parametersForOutput_.partitioning == 50);
  }
}

SCENARIO ("Loading BHsettings from file with landing walker", "[BHSettings]") {
  std::stringstream ss ("#GENERAL\n"
                        "rng_seed = 369\n"
                        "#POTENTIAL\n"
                        "name_potpar = 'AgCu.in'\n"
                        "#RUN\n"
                        "n_mc = 1000\n"
                        "#GOA\n"
                        "histo0.nbins = 100\n"
                        "histo1.nbins = 2\n"
                        "OP0 = signature 555\n"
                        "OP1 = CuAg\n"
                        "#OUTPUT\n"
                        "OP0_param_min = 0\n"
                        "OP0_param_max = 1\n"
                        "OP0_parameter_partitioning = 50\n"
                        "#WALKERS 1\n"
                        "input_WalkerSTND.in  landing:input_WalkerSTND.in\n");

  BH::BHSettings settings = BH::BHSettingsUtilities::loadBHSettings (ss, false);
  THEN ("The example file should be loaded correctly") {
    REQUIRE (
      settings.minimizationAlgorithm_ ==
      BH::BHminimizationAlgorithmType::LBFGSB);
    REQUIRE (
      settings.walkerFilenames_[0].flyingWalker == "input_WalkerSTND.in");
    // REQUIRE(settings.walkerFilenames_[0].landingWalker=="input_WalkerSTND.in");
    REQUIRE (settings.nMC_ == 1000);
    REQUIRE (settings.RngSeed_ == 369);
    REQUIRE (settings.OPSettings_[0].histoPars_.getNbins () == 100);
    REQUIRE (settings.OPSettings_[1].histoPars_.getNbins () == 2);
    REQUIRE (
      settings.OPSettings_[0].OP_.OPtype_ ==
      BH::BHOrderParameter::BHOrderParameter_type::signature);
    REQUIRE (
      settings.OPSettings_[1].OP_.OPtype_ ==
      BH::BHOrderParameter::BHOrderParameter_type::bonds);
    // zero is not always approximated correctly...
    REQUIRE (
      settings.OPSettings_[0].parametersForOutput_.paramMin ==
      Catch::Approx (0.0).margin (0.001));
    REQUIRE (settings.OPSettings_[0].parametersForOutput_.paramMax == 1.0_a);
    REQUIRE (settings.OPSettings_[0].parametersForOutput_.partitioning == 50);
  }
}
